package com.example.storymodulesampleapp;

import android.app.Application;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.touchtalent.bobblesdk.core.BobbleCoreSDK;
import com.touchtalent.bobblesdk.core.config.BobbleCoreConfig;

public class BobbleApp extends Application {

    private static BobbleApp sAppInstance;

    private Gson mGson;

    public static synchronized BobbleApp getInstance() {
        return sAppInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sAppInstance = this;

        BobbleCoreConfig bobbleCoreConfig = new BobbleCoreConfig(new CustomCrossInterface());
        GsonBuilder gsonBuilder = new GsonBuilder();
        this.mGson = gsonBuilder.create();

        BobbleCoreSDK.INSTANCE.initialise(this, bobbleCoreConfig);

    }

    public Gson getGson() {
        return this.mGson;
    }
}
