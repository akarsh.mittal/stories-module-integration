package com.example.storymodulesampleapp

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.absolutePadding
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.storymodulesampleapp.ui.theme.StoryModuleSampleAppTheme
import com.touchtalent.bobblesdk.content_core.interfaces.stories.StoryUIController
import com.touchtalent.bobblesdk.stories_ui.sdk.BobbleStoryUiSDK
import com.touchtalent.bobblesdk.stories_ui.ui.activity.BobbleStoriesActivity

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            StoryModuleSampleAppTheme {
                // A surface container using the 'background' color from the theme
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    Greeting("Android", context = this)
                }
            }
        }
    }
}

fun openActivity(context: Context) {
    BobbleStoryUiSDK.getStoryUiController()
        .newBuilder(context)
        .setScreenName("app_my_story_screen")
        .setKeyboardView(false)
        .setSource(StoryUIController.Source.VERTICAL_STORIES)
        .startActivity()
}

@Composable
fun Greeting(name: String, modifier: Modifier = Modifier, context : Context) {
    Column(Modifier.fillMaxWidth().absolutePadding(10.dp, 200.dp, 10.dp, 0.dp), horizontalAlignment = Alignment.CenterHorizontally) {
        Button(onClick = {
            openActivity(context)
        }, modifier = Modifier.height(100.dp).width(200.dp)) {
            Text(text = "OPEN STORIES")
        }
    }
}
