package com.example.storymodulesampleapp

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
internal data class LocationData(
    var countryCode: String? = null,
    var geoLocationCountryCode: String? = null,
    var geoLocationAdmin1: String? = null,
    var geoLocationAdmin2: String? = null,
    var geoipLocationCountryCode: String? = null,
    var geoipLocationAdmin1: String? = null,
    var geoipLocationAdmin2: String? = null,
    var latitude: String? = null,
    var longitude: String? = null,
    var locationAccuracy: String? = null,
    var locationPermissionStatus: String? = null
)
