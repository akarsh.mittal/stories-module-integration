package com.example.storymodulesampleapp

import android.content.Intent
import android.view.View
import com.androidnetworking.error.ANError
import com.google.gson.Gson
import com.touchtalent.bobblesdk.core.BobbleCoreSDK
import com.touchtalent.bobblesdk.core.builders.ApiParamsBuilder
import com.touchtalent.bobblesdk.core.interfaces.CrossAppInterface
import com.touchtalent.bobblesdk.core.interfaces.EncryptionManager
import com.touchtalent.bobblesdk.core.interfaces.WebViewCacheConfigSettingsInterface
import com.touchtalent.bobblesdk.core.pojo.UserCredentials
import okhttp3.OkHttpClient

class CustomCrossInterface : CrossAppInterface {

    companion object {
        const val BASE_URL = "https://api.bobble.ai/v4"
        const val LOG_BASE_URL = "https://log-api.bobbleapp.me/v4"
        const val BOBBLIFICATION_BASE_URL = "https://bobblification.bobbleapp.me/v4"

        const val CLIENT_ID = "7wZFJWA5chjgat68y826IAIKQ6s197RM"
        const val API_KEY = "7pPmNZN7nV0Jcee1yjmczaVyKEXOELXDVHffu5iEqFc"
    }


    override fun baseUrl(): String {
        return BASE_URL
    }

    override fun logUrl(): String {
        return LOG_BASE_URL
    }

    override fun baseUrlWithoutVersion(): String {
        return BASE_URL.replace("/v4", "")
    }

    override fun bobblificationUrl(): String {
        return BOBBLIFICATION_BASE_URL
    }

    override fun getLoginCredentials(): UserCredentials? {
        return null
    }

    override fun handleGeneralANErrorResponse(anError: ANError, reference: String) {
        return
    }

    override fun getGson(): Gson {
        return BobbleApp.getInstance().gson
    }

    override fun getAppInviteLink(): String {
        return ""
    }

    override fun getActiveLanguageLocale(): String {
        return "en-IN"
    }

    override fun getActiveLanguageId(): String {
        return ""
    }

    override fun getActiveLanguageLayoutId(): String {
        return ""
    }

    override fun canLogEvents(): Boolean {
        return false
    }

    override fun logException(tag: String, exception: Throwable) {
        return
    }

    override fun getVersion(): Int {
        return BuildConfig.VERSION_CODE
    }

    override fun performVibration(view: View?) {
        return
    }

    override fun getBasicFont(otfText: String): String {
        return "Basic"
    }

    override fun sendOpenKeyboardIntent(sourceIntent: Intent?) {
        return
    }

    override fun modifyActivityIntentForKeyboard(intent: Intent, deepLink: Int?) {
        return
    }

    override fun getAdvertisingId(): String {
        return ""
    }

    override fun isLimitAdTrackingEnabled(): Boolean {
        return false
    }

    override fun getCurrentPackageName(isKeyboardView: Boolean): String {
        return BuildConfig.APPLICATION_ID
    }

    override fun getSessionId(isKeyboardView: Boolean): String {
        return "id_" + System.currentTimeMillis()
    }

    override fun getOkHttpClient(): OkHttpClient? {
        return BobbleCoreSDK.okHttpClient
    }

    override fun logEvents(
        eventType: String?,
        eventAction: String?,
        screenAt: String?,
        data: String?,
        logMultiple: Boolean
    ) {
       return
    }

    override fun runInBootAwareMode(runnable: Runnable) {
        runnable.run()
    }

    override fun getAppName(): String {
        return "Bobble"
    }

    override fun getApiParamsBuilder(): ApiParamsBuilder {
        return BobbleApiParamBuilder()
    }

    override fun getApiKey(): String {
        return ""
    }

    override fun getEncryptionManager(): EncryptionManager {
        return BobbleEncryptionManager()
    }

    override fun getAppInviteMessageAndLink(): String? {
        return null
    }

    override fun forceSyncEvents() {
        return
    }

    override fun getWebViewCacheConfigSettingsInterface(): WebViewCacheConfigSettingsInterface?{
        return null
    }
}