package com.example.storymodulesampleapp

import android.content.res.Resources
import android.os.Build
import android.webkit.WebSettings
import com.touchtalent.bobblesdk.core.builders.ApiParamsBuilder

class BobbleApiParamBuilder : ApiParamsBuilder() {
    override fun populateDeviceIdParams(hashMap: HashMap<String, String>, key: String?) {
        hashMap[key ?: "deviceId"] =
            AppUtils.getAndroidDeviceId(BobbleApp.getInstance().applicationContext)
    }

    override fun populateClientIdParams(hashMap: HashMap<String, String>, key: String?) {
        hashMap[key ?: "clientId"] = CustomCrossInterface.CLIENT_ID
    }

    override fun populateVersionParams(hashMap: HashMap<String, String>) {
        hashMap["appVersion"] = "999999992"
        hashMap["sdkVersion"] = Build.VERSION.RELEASE
    }

    override fun populateGeoLocationParams(hashMap: HashMap<String, String>) {
        return
    }

    override fun populateUtmCampaignParams(hashMap: HashMap<String, String>) {
        return
    }

    override fun populateDeviceManufacturer(hashMap: HashMap<String, String>) {
        return
    }

    override fun populateAdvertisementId(hashMap: java.util.HashMap<String, String>){
        return
    }

    override fun populateLocationV2Params(hashMap: java.util.HashMap<String, String>) {
        return
    }

    override fun populateGPSCoordinates(hashMap: java.util.HashMap<String, String>) {
        return
    }

    override fun populateLimitAdTracking(hashMap: java.util.HashMap<String, String>) {
        return
    }

    override fun populateCellularData(hashMap: java.util.HashMap<String, String>){
        return
    }

    override fun populateUserAgent(hashMap: java.util.HashMap<String, String>) {
        kotlin.runCatching {
            WebSettings.getDefaultUserAgent(BobbleApp.getInstance())
        }.recover {
            System.getProperty("http.agent")
        }.onSuccess {
            hashMap["deviceUserAgent"] = it
        }
    }

    override fun populateDeviceLanguage(hashMap: java.util.HashMap<String, String>) {
        val locale = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Resources.getSystem().configuration.locales.get(0)
        } else {
            Resources.getSystem().configuration.locale
        }
        hashMap["deviceLanguage"] = locale.language
    }

    override fun populateDeviceModel(hashMap: java.util.HashMap<String, String>) {
        return
    }
}
