
package com.example.storymodulesampleapp;


import android.content.Context;
import android.util.Base64;
import android.util.Pair;

import com.touchtalent.bobblesdk.core.utils.FileUtil;

import java.io.FileOutputStream;
import java.security.KeyFactory;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

public class BobbleFileEncryption {

    public BobbleFileEncryption() {
    }

    public void encrypt() {
        try {
            Pair<Byte, PublicKey> publicKey = getRandomRSAPublicKey();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void encryptFile(String inputFilePath, String outputFilePath) throws Exception {
        Pair<Byte, PublicKey> publicKey = getRandomRSAPublicKey();
        byte[] inputData = FileUtil.readAllBytes(inputFilePath);

        encrypt(inputData, outputFilePath, publicKey);
    }

    public byte[] encryptByteArray(byte[] inputData) throws Exception {
        Pair<Byte, PublicKey> publicKey = getRandomRSAPublicKey();
        return encryptAndSerialize(inputData, publicKey);
    }

    private void encrypt(byte[] inputData, String filePath, Pair<Byte, PublicKey> publicKey) throws Exception {
        if (inputData.length > 0) {
            byte[] encryptedSerializedData = encryptAndSerialize(inputData, publicKey);
            FileOutputStream outputStream = new FileOutputStream(filePath);

            try {
                outputStream.write(encryptedSerializedData);
            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {
                outputStream.close();
            }

        } else {

            throw new RuntimeException("Empty payload data.");
        }
    }

    private SecretKey getRandomAESKey() throws Exception {
        KeyGenerator generator = KeyGenerator.getInstance("AES");
        generator.init(256);
        return generator.generateKey();
    }

    public Pair<Byte, PublicKey> getRandomRSAPublicKey() throws Exception {
        final Context context = BobbleApp.getInstance().getApplicationContext();
        SecureRandom objSecureRandom = new SecureRandom();

        int randomPoolIndex = objSecureRandom.nextInt(32);

        String randomRSAPublicKeyBase64 = context.getResources().getString(R.string.prefix).trim()
                + context.getResources().getString(R.string.key_pool).split(",")[randomPoolIndex]
                + context.getResources().getString(R.string.suffix);

        byte[] randomRSAPublicKey = Base64.decode(randomRSAPublicKeyBase64.getBytes(), 0);
        X509EncodedKeySpec encodedKeySpec = new X509EncodedKeySpec(randomRSAPublicKey);
        return new Pair((byte) randomPoolIndex, KeyFactory.getInstance("RSA").generatePublic(encodedKeySpec));
    }

    public byte[] encryptAndSerialize(byte[] inputData, Pair<Byte, PublicKey> publicKey) throws Exception {
        SecretKey randomAESKey = getRandomAESKey();
        Cipher cipherAES = Cipher.getInstance("AES/ECB/PKCS5Padding");
        cipherAES.init(1, randomAESKey);
        byte[] encryptedData = cipherAES.doFinal(inputData);
        Cipher cipherRSA = Cipher.getInstance("RSA/ECB/PKCS1Padding");
        cipherRSA.init(1, publicKey.second);
        byte[] encryptedAESKey = cipherRSA.doFinal(randomAESKey.getEncoded());
        byte[] serializedData = new byte[sizeof(Byte.TYPE) + sizeof(Byte.TYPE) + sizeof(Integer.TYPE) + encryptedAESKey.length + encryptedData.length];
        int dataPtr = 0;
        System.arraycopy(new byte[]{3}, 0, serializedData, 0, sizeof(Byte.TYPE));
        dataPtr = dataPtr + sizeof(Byte.TYPE);
        System.arraycopy(new byte[]{publicKey.first}, 0, serializedData, dataPtr, sizeof(Byte.TYPE));
        dataPtr += sizeof(Byte.TYPE);
        System.arraycopy(intToByteArray(encryptedAESKey.length), 0, serializedData, dataPtr, sizeof(Integer.TYPE));
        dataPtr += sizeof(Integer.TYPE);
        System.arraycopy(encryptedAESKey, 0, serializedData, dataPtr, encryptedAESKey.length);
        dataPtr += encryptedAESKey.length;
        System.arraycopy(encryptedData, 0, serializedData, dataPtr, encryptedData.length);
        return serializedData;
    }

    private int sizeof(Class dataType) throws NoSuchMethodException {
        if (dataType == null) {
            throw new NullPointerException();
        } else if (dataType != Integer.TYPE && dataType != Integer.class) {
            if (dataType != Short.TYPE && dataType != Short.class) {
                if (dataType != Byte.TYPE && dataType != Byte.class) {
                    if (dataType != Character.TYPE && dataType != Character.class) {
                        if (dataType != Long.TYPE && dataType != Long.class) {
                            if (dataType != Float.TYPE && dataType != Float.class) {
                                if (dataType != Double.TYPE && dataType != Double.class) {
                                    throw new NoSuchMethodException();
                                } else {
                                    return 8;
                                }
                            } else {
                                return 4;
                            }
                        } else {
                            return 8;
                        }
                    } else {
                        return 2;
                    }
                } else {
                    return 1;
                }
            } else {
                return 2;
            }
        } else {
            return 4;
        }
    }

    private byte[] intToByteArray(int value) {
        return new byte[]{(byte) (value >>> 24), (byte) (value >>> 16), (byte) (value >>> 8), (byte) value};
    }
}

