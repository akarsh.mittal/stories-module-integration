package com.example.storymodulesampleapp

import com.touchtalent.bobblesdk.core.interfaces.EncryptionManager
import com.touchtalent.bobblesdk.core.utils.FileUtil
import java.io.File
import java.nio.charset.StandardCharsets

class BobbleEncryptionManager : EncryptionManager {

    override fun encrypt(filePath: File): Result<ByteArray> = kotlin.runCatching {
        val encryption = BobbleFileEncryption()
        val inputData = FileUtil.readAllBytes(filePath.absolutePath)
        encryption.encryptByteArray(inputData)
    }

    override fun encrypt(data: String): Result<ByteArray> = kotlin.runCatching {
        val encryption = BobbleFileEncryption()
        val inputData = data.toByteArray(StandardCharsets.UTF_8)
        encryption.encryptByteArray(inputData)
    }

    override fun encrypt(byteArray: ByteArray): Result<ByteArray> = kotlin.runCatching {
        val encryption = BobbleFileEncryption()

        val publicKey = encryption.randomRSAPublicKey
        encryption.encryptAndSerialize(byteArray, publicKey)
    }
}