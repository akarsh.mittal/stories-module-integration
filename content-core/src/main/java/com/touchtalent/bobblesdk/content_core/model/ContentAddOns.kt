package com.touchtalent.bobblesdk.content_core.model

import com.touchtalent.bobblesdk.content_core.model.accessory.Accessory
import com.touchtalent.bobblesdk.content_core.model.expression.Expression
import com.touchtalent.bobblesdk.content_core.model.wig.Wig

data class ContentAddOns(
    val wigs: List<Wig>? = null,
    val accessories: List<Accessory>? = null,
    val expression: List<Expression>? = null,
    val locale: String? = null,
    val packId: Int? = null,
    val otf: String? = null,
    val creContentCategory: Int? = null,
    val recVersion: String? = null
)