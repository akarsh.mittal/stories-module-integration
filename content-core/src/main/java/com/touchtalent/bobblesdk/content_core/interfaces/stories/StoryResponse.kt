package com.touchtalent.bobblesdk.content_core.interfaces.stories

data class StoryResponse(
    val storyList: List<BobbleStory>,
    val nextToken: String?
)
