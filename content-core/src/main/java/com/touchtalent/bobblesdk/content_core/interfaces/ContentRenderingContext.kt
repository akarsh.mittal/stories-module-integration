package com.touchtalent.bobblesdk.content_core.interfaces

import androidx.annotation.CallSuper
import androidx.annotation.RestrictTo
import androidx.recyclerview.widget.RecyclerView
import com.touchtalent.bobblesdk.content_core.model.ContentMetadata
import com.touchtalent.bobblesdk.content_core.sdk.BobbleContentOutput
import com.touchtalent.bobblesdk.content_core.sdk.BobbleContentView
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.cancel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

/**
 * Interface to provide logic of rendering a [BobbleContent] on to a [BobbleContentView] and
 * managing a [BobbleContentView].
 * Implementations of this class acts as a lifecycle for rendering content. Lifecycle starts with
 * [start] and ends with [dispose]. This lifecycle can be used creates threads, executors, run async
 * tasks, log event in batch, etc for multiple content being rendered within the lifecycle
 *
 * Implementors should take care of:
 * - processing multiple content within the lifecycle
 * - downloading and processing raw resources
 * - managing threads
 * - managing OpenGL context, if required
 * - logic to play/pause animation, if applicable
 * - clearing resources when a certain [BobbleContentView] is not in focus or is detached
 */
abstract class ContentRenderingContext {

    open var screenName: String? = null
    open var packageName: String? = null
    open var isKeyboardView: Boolean = false
    open var poolSize: Int? = null

    val contextScope = MainScope()

    /**
     * Starts the lifecycle
     */
    abstract suspend fun start()

    /**
     * Render given [content] and [contentMetadata] on [contentView].
     * Implementor should make sure
     * - to handle attach and detach state of this view and clear resources used by the content accordingly
     * - to handle recycling of [BobbleContentView]. The instance of [BobbleContentView] might be an
     * recycled view. Ensure any modifications made to the [contentView] (e.g - any listeners attached,
     * any view added, etc) must be cleared in [onViewRecycled]
     */
    @RestrictTo(RestrictTo.Scope.LIBRARY)
    abstract fun render(
        content: BobbleContent,
        contentMetadata: ContentMetadata?,
        contentView: BobbleContentView
    )

    @RestrictTo(RestrictTo.Scope.LIBRARY)
    open fun renderPlayable(
        content: BobbleContent,
        contentMetadata: ContentMetadata?,
        contentView: BobbleContentView,
    ): Flow<Int> = flow {
        render(content, contentMetadata, contentView)
        for (i in 1..100) {
            delay(500)
            emit(i)
        }
    }

    open suspend fun preCacheContent(content: BobbleContent) = Unit

    /**
     * In a scenario where multiple content is being rendered in a single view, multiple context
     * would be used. This function would be used to determine whether this context is capable of
     * rendering the given [content].
     */
    @RestrictTo(RestrictTo.Scope.LIBRARY)
    abstract fun canRender(content: BobbleContent): Boolean

    /**
     * Export this content to a shareable file. The type of file to export should be decided by the
     * [ContentMetadata.supportedMimeTypes] if [metadata] is non null, otherwise fallback to a
     * default format supported by the content, e.g - PNG for images, GIF for animations, MP4 for videos
     *
     * @return [Result] carrying [BobbleContentOutput] containing the exported file with additional
     * metadata related to exporting.
     */
    @RestrictTo(RestrictTo.Scope.LIBRARY)
    abstract suspend fun export(
        content: BobbleContent,
        metadata: ContentMetadata? = null,
    ): Result<BobbleContentOutput>

    /**
     * Start playing the animation present on [contentView], if applicable.
     */
    open fun play(contentView: BobbleContentView) {}

    /**
     * Pause the animation present on [contentView], if applicable.
     */
    open fun pause(contentView: BobbleContentView) {}

    /**
     * Called when a view inside RecyclerView is recycled, acts as an delegation for [RecyclerView.Adapter.onViewRecycled].
     */
    abstract fun onViewRecycled(contentView: BobbleContentView)

    /**
     * Clear the lifecycle, clearing all resources
     */
    @CallSuper
    open fun dispose() {
        contextScope.cancel()
    }

}
