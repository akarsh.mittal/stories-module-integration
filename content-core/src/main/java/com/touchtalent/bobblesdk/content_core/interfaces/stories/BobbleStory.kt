package com.touchtalent.bobblesdk.content_core.interfaces.stories

import com.touchtalent.bobblesdk.content_core.interfaces.BobbleContent

abstract class BobbleStory(id: Int, type: Type) : BobbleContent(id, type) {
    override val contentType = "story"
    open val noOfShares: Int? = null
    abstract val normalisedTitle: String
    open val forceEnableStoryNotification: Boolean = false

    open suspend fun getHeadIds(): List<String> =  emptyList()
}