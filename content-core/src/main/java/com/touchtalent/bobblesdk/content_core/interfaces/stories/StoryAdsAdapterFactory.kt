package com.touchtalent.bobblesdk.content_core.interfaces.stories

import com.touchtalent.bobblesdk.content_core.model.Story


interface StoryAdsAdapterFactory {
    fun newAdapter(): StoryAdapter
    suspend fun createAdStory(): Story
}