package com.touchtalent.bobblesdk.content_core.util

import android.graphics.Typeface
import android.util.Log
import com.touchtalent.bobblesdk.content_core.sdk.ContentCoreSDK
import com.touchtalent.bobblesdk.core.interfaces.fonts.FontManager
import com.touchtalent.bobblesdk.core.utils.BLog
import com.touchtalent.bobblesdk.core.utils.FileUtil
import com.touchtalent.bobblesdk.core.utils.ResourceDownloader
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.File

/**
 * [FontManager] implementation to provide a shared resource management for fonts across all content
 * modules
 */
class ContentFontManager : FontManager {

    private fun getFontDirPath(): String {
        //create directory for type
        return FileUtil.createDirAndGetPath(ContentCoreSDK.rootDir, "resources", "font")
    }

    private fun isFontPresentInAssets(url: String): String {
        val fileNameList = url.split("/")
        if (fileNameList.size < 0) {
            Log.e("FontDownloader", "File name is wrong")
            return ""
        }
        val fileName = fileNameList[fileNameList.size - 1]
        if ("obelix.ttf" == fileName) return "fonts/obelix.ttf"
        return ""
    }

    private fun isFontPresent(url: String): Boolean {
        val fileNameList = url.split("/")
        if (fileNameList.size < 0) {
            Log.e("FontDownloader", "File name is wrong")
            return false
        }
        val fileName = fileNameList[fileNameList.size - 1]
        val path = FileUtil.join(getFontDirPath(), fileName)
        return File(path).exists()
    }

    private fun getLocalFullFontPath(url: String): File? {
        val fileNameList = url.split("/")
        if (fileNameList.size < 0) {
            Log.e("FontDownloader", "File name is wrong")
            return null
        }
        val fileName = fileNameList[fileNameList.size - 1]
        val path = FileUtil.join(getFontDirPath(), fileName)
        return File(path)
    }

    override fun exists(url: String): Boolean {
        val font = isFontPresentInAssets(url)
        return if (font.isEmpty()) {
            isFontPresent(url)
        } else true
    }

    override suspend fun getFontTypeface(
        url: String,
        timeoutInMillis: Long,
        forceCache: Boolean
    ): Typeface? =
        withContext(Dispatchers.IO) {
            val assetsFont = isFontPresentInAssets(url)
            if (assetsFont.isNotEmpty()) {
                try {
                    return@withContext Typeface.createFromAsset(
                        ContentCoreSDK.applicationContext.assets,
                        assetsFont
                    )
                } catch (e: Exception) {
                    BLog.printStackTrace(e)
                }
            }
            val isLocalAvailable = isFontPresent(url)
            if (isLocalAvailable) {
                try {
                    return@withContext Typeface.createFromFile(getLocalFullFontPath(url)?.absolutePath)
                } catch (e: Exception) {
                    BLog.printStackTrace(e)
                }
            }
            val font = downloadFont(url, timeoutInMillis, forceCache) ?: return@withContext null
            return@withContext try {
                Typeface.createFromFile(font)
            } catch (e: Exception) {
                BLog.printStackTrace(e)
                null
            }
        }

    private suspend fun downloadFont(
        url: String,
        timeoutInMillis: Long,
        forceCache: Boolean
    ): String? {
        return ResourceDownloader.downloadResourcesSuspend(
            url = url,
            forceCache = forceCache,
            destDir = getFontDirPath(),
            timeoutInMillis = timeoutInMillis,
            cacheKey = ContentResourceDownloader.FONT_CACHE_KEY
        ).first
    }

    override fun getDefaultTypeface(): Typeface {
        return Typeface.createFromAsset(
            ContentCoreSDK.applicationContext.assets,
            "fonts/obelix.ttf"
        )
    }

}