package com.touchtalent.bobblesdk.content_core.model

import com.touchtalent.bobblesdk.content_core.interfaces.stories.BobbleStory
import kotlinx.coroutines.CoroutineScope
import java.util.Locale

abstract class Story {
    abstract val id: Int
    abstract val type: String
    abstract val format: String
    open suspend fun getHeadIds(): List<String> = emptyList()

    class ContentStory(
        val bobbleStory: BobbleStory,
        val contentMetadata: ContentMetadata?
    ) : Story() {
        override val id: Int
            get() = bobbleStory.id

        override val type: String
            get() = "user_story"

        override val format: String
            get() = bobbleStory.type.toString().toLowerCase(Locale.ROOT)

        override suspend fun getHeadIds(): List<String> {
            return bobbleStory.getHeadIds()
        }
    }

    class SummaryStory(
        val stories: List<BobbleStory>,
    ) : Story() {
        override val id: Int
            get() = 0

        override val type: String
            get() = "grid_story"

        override val format: String
            get() = ""
    }

    abstract class AddOnStory : Story() {
        override val id: Int
            get() = 0

        override val type: String
            get() = "external_ad"

        override val format: String
            get() = ""

        abstract suspend fun preCache(scope: CoroutineScope)

        abstract fun destroyStory()

    }

    class VerticalStory(
        val bobbleStory: BobbleStory,
        val contentMetadata: ContentMetadata?
    ) : Story() {
        override val id: Int
            get() = bobbleStory.id

        override val type: String
            get() = "user_story"

        override val format: String
            get() = bobbleStory.type.toString().toLowerCase(Locale.ROOT)

        override suspend fun getHeadIds(): List<String> {
            return bobbleStory.getHeadIds()
        }
    }
}