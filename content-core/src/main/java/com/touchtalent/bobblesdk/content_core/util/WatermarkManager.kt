package com.touchtalent.bobblesdk.content_core.util

import com.touchtalent.bobblesdk.content_core.sdk.ContentCoreSDK
import com.touchtalent.bobblesdk.core.utils.FileUtil
import com.touchtalent.bobblesdk.core.utils.ResourceDownloader
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

/**
 * Utility class to provide watermarks from URL as a shared resource to avoid redundancy
 */
object WatermarkManager {

    /**
     * Fetch a watermark from a given URL and cache it for later use.
     * @param forceCache Force result from cache only
     * @param url URL of the watermark
     * @return Pair of the local path to the watermark, if found, to the cache status, true if from
     * internet, false, if from cache
     */
    suspend fun getWatermark(url: String, forceCache: Boolean): Pair<String?, Boolean> {
        return ResourceDownloader.downloadResourcesSuspend(
            url = url,
            destDir = getWatermarkDir(),
            forceCache = forceCache,
            cacheKey = ContentResourceDownloader.CACHE_KEY
        )
    }

    /**
     * Preload the watermark for later use
     */
    suspend fun preloadWatermark(url: String) {
        ResourceDownloader.downloadResourcesSuspend(
            url = url,
            destDir = getWatermarkDir(),
            forceCache = true,
            cacheKey = ContentResourceDownloader.CACHE_KEY
        )
    }

    private suspend fun getWatermarkDir(): String = withContext(Dispatchers.IO) {
        FileUtil.createDirAndGetPath(ContentCoreSDK.cacheDir, "resources", "watermark")
    }

}

