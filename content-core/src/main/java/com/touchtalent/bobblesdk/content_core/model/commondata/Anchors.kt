package com.touchtalent.bobblesdk.content_core.model.commondata

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Anchors(
    val x: String?,
    val xCoordinatesTemplate: List<Int>?,
    val y: String?,
    val yCoordinatesTemplate: List<Int>?
)