package com.touchtalent.bobblesdk.content_core.model.commondata

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Accessories(
    val angle: Double?,
    val boundLeft: Int?,
    val boundTop: Int?,
    val expansion: Boolean?,
    val scaleX: Double?,
    val scaleY: Double?
)