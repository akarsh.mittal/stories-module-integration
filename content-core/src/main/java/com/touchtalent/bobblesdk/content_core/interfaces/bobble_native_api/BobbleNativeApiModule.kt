package com.touchtalent.bobblesdk.content_core.interfaces.bobble_native_api


/**
 * Base interface for *bobble-native-api* module
 */
interface BobbleNativeApiModule {
    /**
     * Get [NativeHeadProcessor] instance
     * @see NativeHeadProcessor
     */
    fun getHeadProcessor(): NativeHeadProcessor

    /**
     * Get [NativeWebpConvertor] instance
     * @see NativeWebpConvertor
     */
    fun getWebpConvertor(): NativeWebpConvertor
}