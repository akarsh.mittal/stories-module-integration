package com.touchtalent.bobblesdk.content_core.model.expression

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Deviation(
    val x: Int?,
    val y: Int?
)