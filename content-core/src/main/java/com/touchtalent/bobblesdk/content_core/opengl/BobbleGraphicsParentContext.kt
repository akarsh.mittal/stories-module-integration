package com.touchtalent.bobblesdk.content_core.opengl

import android.opengl.EGL14
import android.opengl.EGLConfig
import android.opengl.EGLContext
import android.opengl.EGLDisplay

/**
 * Wrapper class for creating and managing OpenGL context which can be used individually as well a
 * parent context to [BobbleGraphicsChildContext]
 */
class BobbleGraphicsParentContext : IBobbleGraphicsContext() {

    private var mEGLDisplay = EGL14.EGL_NO_DISPLAY
    private var mEGLContext = EGL14.EGL_NO_CONTEXT
    private val mEGLContextShared = EGL14.EGL_NO_CONTEXT
    private var mEGLConfig: EGLConfig? = null

    override val display: EGLDisplay
        get() = mEGLDisplay

    override val context: EGLContext
        get() = mEGLContext

    override val config: EGLConfig
        get() = mEGLConfig!!

    /**
     * Initialise the display (Default Android window display), config and context
     */
    init {
        mEGLDisplay = EGL14.eglGetDisplay(EGL14.EGL_DEFAULT_DISPLAY)

        if (mEGLDisplay === EGL14.EGL_NO_DISPLAY) {
            throw Exception("no EGL display")
        }

        val version = IntArray(2)
        if (!EGL14.eglInitialize(mEGLDisplay, version, 0, version, 1)) {
            mEGLDisplay = null
            throw Exception("unable to initialize EGL display")
        }

        mEGLConfig = storeConfig()
        if (mEGLConfig == null) {
            throw Exception("no EGL config found")
        }

        val attrib2List = intArrayOf(
            EGL_CONTEXT_CLIENT_VERSION,
            EGL_VERSION_GLES2,
            EGL14.EGL_NONE
        )

        mEGLContext =
            EGL14.eglCreateContext(mEGLDisplay, mEGLConfig, mEGLContextShared, attrib2List, 0)
        if (mEGLContext == null)
            throw Exception("Failed to create context")
        checkError("initContext")
    }

    private fun storeConfig(): EGLConfig? {
        val attribList = intArrayOf(
            EGL14.EGL_RED_SIZE, 8,
            EGL14.EGL_GREEN_SIZE, 8,
            EGL14.EGL_BLUE_SIZE, 8,
            EGL14.EGL_ALPHA_SIZE, 8,
            EGL14.EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT,
            EGL_RECORDABLE_ANDROID, 1,
            EGL14.EGL_NONE
        )
        val configs = arrayOfNulls<EGLConfig>(1)
        val numConfigs = IntArray(1)
        return if (!EGL14.eglChooseConfig(
                mEGLDisplay,
                attribList,
                0,
                configs,
                0,
                configs.size,
                numConfigs,
                0
            )
        ) {
            null
        } else configs[0]
    }

    override fun activate(surface: BobbleGraphicsSurface?) {
        synchronized(this) {
            if (!EGL14.eglMakeCurrent(
                    mEGLDisplay,
                    surface?.eglSurface ?: EGL14.EGL_NO_SURFACE,
                    surface?.eglSurface ?: EGL14.EGL_NO_SURFACE,
                    mEGLContext
                )
            ) {
                throw Exception("activate failed")
            }
            checkError("activate")
        }
    }

    override fun deactivate() {
        synchronized(this) {
            if (!EGL14.eglMakeCurrent(
                    mEGLDisplay,
                    EGL14.EGL_NO_SURFACE,
                    EGL14.EGL_NO_SURFACE,
                    EGL14.EGL_NO_CONTEXT
                )
            ) {
                throw Exception("deactivate failed")
            }
            checkError("deactivate")
        }
    }

    /**
     * Destroy the display, context and release OpenGL from the thread
     */
    override fun dispose(releaseThread: Boolean) {
        if (mEGLDisplay !== EGL14.EGL_NO_DISPLAY) {
            EGL14.eglMakeCurrent(
                mEGLDisplay,
                EGL14.EGL_NO_SURFACE,
                EGL14.EGL_NO_SURFACE,
                EGL14.EGL_NO_CONTEXT
            )
            EGL14.eglDestroyContext(mEGLDisplay, mEGLContext)
            EGL14.eglTerminate(mEGLDisplay)
            if (releaseThread)
                EGL14.eglReleaseThread()
        }
        mEGLDisplay = EGL14.EGL_NO_DISPLAY
        mEGLContext = EGL14.EGL_NO_CONTEXT
        mEGLConfig = null
    }

}