package com.touchtalent.bobblesdk.content_core.cache

/**
 * Convenient function to create cache using [CacheGenerator]
 */
@Deprecated(message = "This function has been moved to bobble-core instead")
fun cache(block: CacheGenerator.() -> Unit): String {
    val cacheGenerator = CacheGenerator()
    block.invoke(cacheGenerator)
    return cacheGenerator.getCacheKey()
}