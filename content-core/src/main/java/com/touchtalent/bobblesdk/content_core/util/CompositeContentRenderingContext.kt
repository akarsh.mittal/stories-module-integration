package com.touchtalent.bobblesdk.content_core.util

import com.touchtalent.bobblesdk.content_core.interfaces.BobbleContent
import com.touchtalent.bobblesdk.content_core.interfaces.ContentRenderingContext
import com.touchtalent.bobblesdk.content_core.model.ContentMetadata
import com.touchtalent.bobblesdk.content_core.sdk.BobbleContentOutput
import com.touchtalent.bobblesdk.content_core.sdk.BobbleContentView
import com.touchtalent.bobblesdk.core.utils.createError
import kotlinx.coroutines.Job
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch

/**
 * A composite content rendering context which encapsulates multiple [ContentRenderingContext]
 * @param renderingContexts List of [ContentRenderingContext] which needs to be encapsulated
 */
class CompositeContentRenderingContext(
    private val renderingContexts: List<ContentRenderingContext>
) : ContentRenderingContext() {

    override var screenName: String? = null
        set(value) {
            field = value
            renderingContexts.forEach { it.screenName = value }
        }

    override var packageName: String? = null
        set(value) {
            field = value
            renderingContexts.forEach { it.packageName = value }
        }

    override var isKeyboardView: Boolean = false
        set(value) {
            field = value
            renderingContexts.forEach { it.isKeyboardView = value }
        }

    override suspend fun start() = coroutineScope {
        val jobs = mutableListOf<Job>()
        renderingContexts.forEach {
            jobs.add(launch { it.start() })
        }
        jobs.forEach { it.join() }
    }

    override fun render(
        content: BobbleContent,
        contentMetadata: ContentMetadata?,
        contentView: BobbleContentView
    ) {
        renderingContexts.forEach {
            if (it.canRender(content)) {
                it.render(content, contentMetadata, contentView)
                return
            }
        }
        createError<Unit>(Exception("No rendering context found for type ${content.javaClass}"))
    }

    override fun renderPlayable(
        content: BobbleContent,
        contentMetadata: ContentMetadata?,
        contentView: BobbleContentView,
    ): Flow<Int> {
        renderingContexts.forEach {
            if (it.canRender(content)) {
                return it.renderPlayable(content, contentMetadata, contentView)
            }
        }
        throw Exception("No rendering context found for type ${content.javaClass}")
    }

    override suspend fun preCacheContent(content: BobbleContent) {
        renderingContexts.forEach {
            if (it.canRender(content)) {
                it.preCacheContent(content)
                return
            }
        }
        throw Exception("No rendering context found for type ${content.javaClass}")
    }

    override fun canRender(content: BobbleContent): Boolean {
        renderingContexts.forEach {
            if (it.canRender(content)) {
                return true
            }
        }
        return false
    }

    override suspend fun export(
        content: BobbleContent,
        metadata: ContentMetadata?
    ): Result<BobbleContentOutput> {
        renderingContexts.forEach {
            if (it.canRender(content)) {
                return it.export(content, metadata)
            }
        }
        return createError(Exception("No rendering context found for type ${content.javaClass}"))
    }

    override fun play(contentView: BobbleContentView) {
        contentView.currentContent?.let { bobbleContent ->
            renderingContexts.forEach {
                if (it.canRender(bobbleContent)) {
                    it.play(contentView)
                    return
                }
            }
            createError<Unit>(Exception("No rendering context found for type ${contentView.javaClass}"))
        }
    }

    override fun pause(contentView: BobbleContentView) {
        contentView.currentContent?.let { bobbleContent ->
            renderingContexts.forEach {
                if (it.canRender(bobbleContent)) {
                    it.pause(contentView)
                    return
                }
            }
            createError<Unit>(Exception("No rendering context found for type ${contentView.javaClass}"))
        }
    }

    override fun onViewRecycled(contentView: BobbleContentView) {
        contentView.currentContent?.let { currentContent ->
            renderingContexts.forEach {
                if (it.canRender(currentContent)) {
                    it.onViewRecycled(contentView)
                    return@forEach
                }
            }
        }
    }

    override fun dispose() {
        super.dispose()
        renderingContexts.forEach {
            it.dispose()
        }
    }

}
