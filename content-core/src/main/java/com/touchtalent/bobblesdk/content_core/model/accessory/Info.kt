package com.touchtalent.bobblesdk.content_core.model.accessory

import com.squareup.moshi.JsonClass
import com.touchtalent.bobblesdk.content_core.model.commondata.Accessories
import com.touchtalent.bobblesdk.content_core.model.commondata.Anchors

@JsonClass(generateAdapter = true)
data class Info(
    val accessories: Accessories?,
    val anchors: Anchors?,
    val overrideAlpha: Boolean?
)