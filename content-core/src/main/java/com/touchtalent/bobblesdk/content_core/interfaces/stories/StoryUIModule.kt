package com.touchtalent.bobblesdk.content_core.interfaces.stories

interface StoryUIModule {
    fun getStoryUiController(): StoryUIController
    fun getStoryDataController(): StoriesDataController
}