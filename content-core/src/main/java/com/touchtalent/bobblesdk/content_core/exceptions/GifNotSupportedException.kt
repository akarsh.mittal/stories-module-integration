package com.touchtalent.bobblesdk.content_core.exceptions

class GifNotSupportedException : Exception()