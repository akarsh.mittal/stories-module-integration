package com.touchtalent.bobblesdk.content_core.opengl

import android.os.Handler
import android.os.HandlerThread
import android.os.SystemClock
import com.touchtalent.bobblesdk.content_core.BuildConfig
import com.touchtalent.bobblesdk.core.utils.BLog
import kotlinx.coroutines.*
import java.util.*
import java.util.concurrent.CountDownLatch
import java.util.concurrent.Executors

/**
 * An executor for OpenGL operations.
 * Made up of 2 sets of threads:
 * - Set of [noOfThreads] Handler threads, each thread associated with a OpenGL
 * - A single threaded executor for that can work with [sharedContext], supports suspend functions
 *
 * This executor must be started with [start] and closed with [quitSafelyAndWait]. Please note that
 * both are blocking methods and not main thread safe.
 *
 * @param name of the executor, used to name child threads
 * @param noOfThreads Max number of handler threads
 * @param sharedContext Shared context which can be used with single thread executor. This context
 * is shared with context of all handler threads
 */
class GraphicsHandlerExecutor(
    val name: String,
    private val noOfThreads: Int,
    val sharedContext: IBobbleGraphicsContext
) {

    private val threads = mutableListOf<CountdownHandlerThread>()
    private val countDown = CountDownLatch(noOfThreads)
    private val closeCountDownLatch = CountDownLatch(noOfThreads)
    private var lastAllocatedThreadIndex = 0
    private val customScope = CoroutineScope(
        Executors.newSingleThreadExecutor { runnable ->
            Thread(runnable, "$name-init")
        }.asCoroutineDispatcher()
    )
    private val jobMap = Collections.synchronizedMap(mutableMapOf<Any, MutableList<Job>>())

    /**
     * Starts the executor, not main thread safe
     */
    fun start() {
        repeat(noOfThreads) { count ->
            val handlerThread = CountdownHandlerThread(
                "$name-$count",
                countDown,
                closeCountDownLatch,
                sharedContext
            )
            threads.add(handlerThread)
            handlerThread.start()
        }
        countDown.await()
    }

    /**
     * Execute a block of code on the single threaded executor with the [sharedContext].
     * Suspend functions are also accepted.
     * Each call is annotated with a [token] object, which can be used to cancel the calls.
     * Call [clear] to cancel/dispose calls
     *
     * @param token Token object, which can be used later to cancel the execution
     * @param block Block of code to be executed
     */
    fun executeShared(
        token: Any,
        block: suspend CoroutineScope.(graphicsContext: IBobbleGraphicsContext) -> Unit
    ): Job {
        val job = customScope.launch {
            block.invoke(this, sharedContext)
        }
        token.let {
            jobMap[token]?.add(job) ?: run {
                jobMap[token] = (mutableListOf(job))
            }
        }
        return job
    }

    suspend fun <T> executeSharedForResult(
        token: Any,
        block: suspend CoroutineScope.(graphicsContext: IBobbleGraphicsContext) -> T
    ): T {
        val job = customScope.async {
            block.invoke(this, sharedContext)
        }
        token.let {
            jobMap[token]?.add(job) ?: run {
                jobMap[token] = (mutableListOf(job))
            }
        }
        return job.await()
    }

    /**
     * Execute a block of code on a thread from the pool of handler threads.
     * Each call is annotated with a [token] object, which can be used to cancel the calls.
     * Call [clear] to cancel/dispose calls
     *
     * The [command] can be executed on a random thread or on a dedicated thread via [graphicsContext]
     * In rendering scenario, it is necessary that all calls made to OpenGL for a specific content
     * be made from a single context, as it might be holding necessary textures to process them. To
     * achieve this, caller must keep a reference of [graphicsContext] and pass it to ensure that
     * the thread containing [graphicsContext] is used to invoke [command]
     *
     * If [graphicsContext] is null (first time use), a random thread is assigned on counter basis.
     * @param token Token object to annotate the call, which can be used to cancel it later
     * @param graphicsContext Preferred graphics context with which [command] must be run
     * @param command Block of code to execute
     */
    fun execute(
        token: Any,
        graphicsContext: IBobbleGraphicsContext? = null,
        command: (graphicsContext: IBobbleGraphicsContext) -> Unit
    ) {
        kotlin.runCatching {
            val threadToUse = findThread(graphicsContext)
            val graphicsContextToUse = threadToUse.graphicsContext
                ?: throw Exception("graphicsContext hasn't bene initialised")
            val modifiedRunnable = ContextRunnable(command, graphicsContextToUse)
            val success = threadToUse.postAtTime(modifiedRunnable, token)
            if (!success && BuildConfig.DEBUG) {
                throw Exception("Failed to post message, looks like looper has been closed. This might cause memory leak")
            }
        }.onFailure {
            BLog.printStackTrace(it)
            if (BuildConfig.DEBUG)
                throw RuntimeException(it)
        }
    }

    /**
     * Wrapper runnable to wrap [graphicsContext] and execute the runnable with the given [graphicsContext]
     */
    private inner class ContextRunnable(
        private val command: (graphicsContext: IBobbleGraphicsContext) -> Unit,
        private val graphicsContext: IBobbleGraphicsContext
    ) : Runnable {
        override fun run() {
            command.invoke(graphicsContext)
        }
    }

    private fun findThread(token: IBobbleGraphicsContext? = null): CountdownHandlerThread {
        return token?.let {
            threads.find { it.graphicsContext == token }
        } ?: run {
            val index = lastAllocatedThreadIndex++ % (threads.size)
            lastAllocatedThreadIndex %= (threads.size)
            threads[index]
        }
    }

    /**
     * Clear any task associated with the given [token]
     */
    fun clear(token: Any) {
        threads.forEach { thread ->
            thread.removeCallbacksAndMessages(token)
        }
        jobMap.remove(token)?.forEach {
            it.cancel()
        }
    }

    /**
     * Close the executor, making sure that any pending tasks is completed.
     * All graphics context related to handler threads will be released
     * This function blocks until the threads are closed, hence not main thread safe
     */
    fun quitSafelyAndWait() {
        threads.forEach { it.quitSafely() }
        customScope.cancel()
        closeCountDownLatch.await()
    }

    /**
     * Handler thread with
     * - graphics context bound to it.
     * - CountDown latch support to blocking notify of start and quit
     *
     * @param countDown CountDown latch to notify that the handler has started
     * @param closeCountDownLatch CountDown latch to notify that the handler has quit
     * @param sharedContext OpenGL context which will shared with the context of this thread
     * @param name Name of the thread
     */
    internal class CountdownHandlerThread(
        name: String,
        private val countDown: CountDownLatch,
        private val closeCountDownLatch: CountDownLatch,
        private val sharedContext: IBobbleGraphicsContext
    ) : HandlerThread(name) {

        private lateinit var handler: Handler
        var graphicsContext: IBobbleGraphicsContext? = null

        /**
         * Cleanup resources on quit and notify countdown latch
         */
        override fun run() {
            super.run()
            // Executed when this HandlerThread shuts down
            graphicsContext?.dispose()
            closeCountDownLatch.countDown()
        }

        override fun onLooperPrepared() {
            super.onLooperPrepared()
            handler = Handler(looper)
            graphicsContext = kotlin.runCatching {
                BobbleGraphicsUtil.createChildGraphicsContext(sharedContext)
            }.getOrNull()
            countDown.countDown()
        }

        fun removeCallbacksAndMessages(token: Any) {
            handler.removeCallbacksAndMessages(token)
        }

        fun postAtTime(runnable: Runnable, token: Any): Boolean {
            return handler.postAtTime(runnable, token, SystemClock.uptimeMillis())
        }
    }
}
