package com.touchtalent.bobblesdk.content_core.interfaces

import com.touchtalent.bobblesdk.content_core.model.ContentAddOns

abstract class BobbleContentAdapter {
    abstract fun parseData(type: String, rawData: String, addOns: ContentAddOns): BobbleContent
    abstract fun getSupportedContentTypes(): Set<String>
    abstract fun getSupportedMimeTypes(): List<String>
    open suspend fun getRecentContent(vararg types: String): List<BobbleContent> = emptyList()
    open suspend fun getRecentCount(vararg types: String): Int = 0
}