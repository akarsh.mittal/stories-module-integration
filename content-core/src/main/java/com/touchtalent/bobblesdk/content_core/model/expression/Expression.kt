package com.touchtalent.bobblesdk.content_core.model.expression

import com.squareup.moshi.JsonClass
import com.touchtalent.bobblesdk.content_core.cache.CacheGenerator
import com.touchtalent.bobblesdk.content_core.cache.Cacheable

@JsonClass(generateAdapter = true)
class Expression(
    val category: String?,
    val id: Int,
    val info: Info?,
    val name: String?,
    val variantsInfo: List<VariantsInfo>?
) : Cacheable {
    override fun generateCacheKey(source: CacheGenerator) {
        source.process(id)
    }
}