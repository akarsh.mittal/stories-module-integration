package com.touchtalent.bobblesdk.content_core.util

import com.touchtalent.bobblesdk.content_core.cache.cache
import com.touchtalent.bobblesdk.core.utils.FileLruCache
import okhttp3.*
import okio.BufferedSource

/**
 * OkHttp Interceptor to provide custom LRU based cache with selective query parameters.
 * Normally HTTP cache gets invalidated if any query param changes.
 * At times, cache might be the best fallback even if a query parameter has been changed.
 * This interceptor comes handy in such situation
 *
 * @param parametersToCache Query parameters that need to be retained for cache key
 * @param maxSize Max size of the cache
 * @param cachePath Cache path on local directory
 */
class ClientCacheInterceptor(
    val parametersToCache: List<String>,
    val maxSize: Long,
    val cachePath: String,
    val ignoreOkhttpCache: Boolean = true
) : Interceptor {

    private val fileLruCache by lazy { FileLruCache(maxSize, cachePath) }

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = if (ignoreOkhttpCache)
            chain.request().newBuilder().header("Cache-Control", "no-cache").build()
        else chain.request()
        val cacheKey = getCacheKey(request)
        try {
            val response = chain.proceed(request)
            if (response.isSuccessful) {
                cacheResponse(cacheKey, response)
            } else {
                if (response.code() % 100 == 5)
                    return getCachedResponse(cacheKey, request)
                        ?: response
            }
            return response
        } catch (e: Exception) {
            return getCachedResponse(cacheKey, request) ?: throw e
        }
    }

    private fun getCachedResponse(cacheKey: String, request: Request): Response? {
        val byteArray = fileLruCache.get(cacheKey) ?: return null
        val responseBody = ResponseBody.create(MediaType.get("application/json"), byteArray)
        return Response.Builder().protocol(Protocol.HTTP_2).request(request).code(200)
            .body(responseBody).message("200 OK Client cached").build()
    }

    private fun cacheResponse(cacheKey: String, result: Response) {
        val body = result.body() ?: return
        val source: BufferedSource = body.source()
        source.request(Long.MAX_VALUE)
        val bytes: String = source.buffer.snapshot().utf8() ?: return
        fileLruCache.put(cacheKey, bytes)
    }

    private fun getCacheKey(request: Request): String {
        return cache {
            process(request.url().encodedPath())
            for (param in parametersToCache) {
                process(request.url().queryParameter(param))
            }
        }
    }
}