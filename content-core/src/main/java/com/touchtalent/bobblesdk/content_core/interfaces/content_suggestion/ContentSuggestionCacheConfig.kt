package com.touchtalent.bobblesdk.content_core.interfaces.content_suggestion

import com.touchtalent.bobblesdk.core.model.BobbleHead

class ContentSuggestionCacheConfig(
    var searchString    : String,
    var selectedPopTextStyleId: Int,
    var bobbleHead: BobbleHead?,
    var searchStringSource: String,
    var packageName : String,
    var locale: String)