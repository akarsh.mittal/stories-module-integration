package com.touchtalent.bobblesdk.content_core.head

import android.graphics.Point
import com.touchtalent.bobblesdk.content_core.model.expression.Expression
import com.touchtalent.bobblesdk.content_core.model.expression.Info
import com.touchtalent.bobblesdk.content_core.model.expression.VariantsInfo
import com.touchtalent.bobblesdk.core.model.BobbleHead
import com.touchtalent.bobblesdk.core.utils.GeneralUtils.stringify


/**
 * Utility class to process expression data received from server and create JSON data for native.
 */
object ExpressionProcessor {

    /**
     * This function estimates current expression on the given [head] and decides which variant
     * should be used.
     * @param expression Expression data received from server
     * @param head Head on which expression needs to be applied
     *
     * @return JSON string which needs to be passed on to native (cpp) code for further processing
     */
    fun getExpressionString(
        expression: Expression,
        head: BobbleHead,
    ): String? {
        val facePointArray = head.getFeaturePointArray() ?: return null
        val currentExpression: FaceType = getCurrentExpression(facePointArray)
        val expressionString = expression.variantsInfo?.let {
            getVariantInfo(it, currentExpression.value)
        } ?: expression.info
        return expressionString.stringify()
    }

    private fun getVariantInfo(list: List<VariantsInfo>, index: Int): Info? {
        if (index >= 0 && index < list.size) {
            return list[index].info
        }
        return null
    }

    private fun getCurrentExpression(facialPoints: List<Point>): FaceType {
        if (facialPoints.size < 72) {
            return FaceType.FACE_TYPE_NORMAL
        }
        return if (facialPoints[63].y - facialPoints[3].y > 0 && facialPoints[65].y - facialPoints[4].y > 0) {      //Smiling or Laughing
            if (facialPoints[63].y - facialPoints[60].y >= 5) {
                FaceType.FACE_TYPE_HAPPY_MO
            } else {
                FaceType.FACE_TYPE_HAPPY_MC
            }
        } else if (facialPoints[63].y - facialPoints[3].y < 0 && facialPoints[65].y - facialPoints[4].y < 0) {        //Sad or Normal
            if (facialPoints[54].y - facialPoints[49].y < 25) {
                FaceType.FACE_TYPE_SAD_MC
            } else {
                FaceType.FACE_TYPE_NORMAL
            }
        } else {
            FaceType.FACE_TYPE_NORMAL
        }
    }

    enum class FaceType(val value: Int) {
        FACE_TYPE_NORMAL(0),
        FACE_TYPE_HAPPY_MC(1),
        FACE_TYPE_HAPPY_MO(1),
        FACE_TYPE_SAD_MC(2)
    }

}