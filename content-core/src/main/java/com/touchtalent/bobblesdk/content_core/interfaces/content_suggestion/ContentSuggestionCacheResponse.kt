package com.touchtalent.bobblesdk.content_core.interfaces.content_suggestion

import com.touchtalent.bobblesdk.content_core.interfaces.BobbleContent

data class ContentSuggestionCacheResponse(
    val list : List<BobbleContent>,
    val nextToken : String?,
    val cacheString: String?
)
