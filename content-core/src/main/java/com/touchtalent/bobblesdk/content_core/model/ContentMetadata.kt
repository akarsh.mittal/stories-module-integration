package com.touchtalent.bobblesdk.content_core.model

import com.touchtalent.bobblesdk.core.model.BobbleHead

/**
 * Metadata used for specifying customisation to be done on a content
 * @property primaryHead Primary bobble head to be applied on the content
 * @property otf OTF text to be written on the content
 * @property locale Locale of the content, could be used to translate content text
 * @property supportedMimeTypes List of mime types supported by the app where this content will be used.
 * Generally used for exporting purposes, but can be used by rendering too to optimise exporting
 * @property includeWatermark true, if watermark needs to be added, false otherwise
 * @property shareAs Identifier which specifies the purpose of exporting the content, used for events.
 * For e.g - Shared via app as an "image" or shared via keyboard as an "sticker"
 */
open class ContentMetadata(
    var primaryHead: BobbleHead? = null,
    var secondaryHead : BobbleHead? = null,
    var otf: String? = null,
    var locale: String? = null,
    var supportedMimeTypes: List<String> = emptyList(),
    var includeWatermark: Boolean = true,
    var addToRecent: Boolean = true,
    var preferredRenderingAspectRatio: Boolean = false,
    var mute: Boolean = false,
    var showLoader: Boolean = false,
    var fitXY: Boolean= false
) {

    var shareAs: String? = null

    constructor(copy: ContentMetadata) : this(
        copy.primaryHead,
        copy.secondaryHead,
        copy.otf,
        copy.locale,
        copy.supportedMimeTypes,
        copy.includeWatermark,
        copy.addToRecent,
        copy.preferredRenderingAspectRatio,
        copy.mute,
        copy.showLoader,
        copy.fitXY
    ) {
        shareAs = copy.shareAs
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ContentMetadata

        if (primaryHead?.headId != other.primaryHead?.headId) return false
        if (secondaryHead?.headId != other.secondaryHead?.headId) return false
        if (otf != other.otf) return false
        if (locale != other.locale) return false
        if (!supportedMimeTypes.containsAll(other.supportedMimeTypes)) return false
        if (includeWatermark != other.includeWatermark) return false
        if (addToRecent != other.addToRecent) return false
        if (shareAs != other.shareAs) return false
        if (preferredRenderingAspectRatio != other.preferredRenderingAspectRatio) return false

        return true
    }

    override fun hashCode(): Int {
        var result = primaryHead?.hashCode() ?: 0
        result = 31 * result + (secondaryHead?.hashCode() ?: 0)
        result = 31 * result + (otf?.hashCode() ?: 0)
        result = 31 * result + (locale?.hashCode() ?: 0)
        result = 31 * result + supportedMimeTypes.hashCode()
        result = 31 * result + includeWatermark.hashCode()
        result = 31 * result + addToRecent.hashCode()
        result = 31 * result + (shareAs?.hashCode() ?: 0)
        result = 31 * result + preferredRenderingAspectRatio.hashCode()
        return result
    }
}
