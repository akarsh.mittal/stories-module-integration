package com.touchtalent.bobblesdk.content_core.head

import com.touchtalent.bobblesdk.content_core.model.accessory.Accessory
import com.touchtalent.bobblesdk.content_core.model.accessory.Info
import com.touchtalent.bobblesdk.content_core.model.wig.Wig
import com.touchtalent.bobblesdk.content_core.model.wig.WigInfo
import com.touchtalent.bobblesdk.content_core.util.ContentResourceDownloader
import com.touchtalent.bobblesdk.core.BobbleCoreSDK
import com.touchtalent.bobblesdk.core.utils.FileUtil
import com.touchtalent.bobblesdk.core.utils.GeneralUtils.toJsonObject
import kotlinx.coroutines.CoroutineStart
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.coroutineScope
import org.json.JSONArray
import org.json.JSONObject

/**
 * Utility class to parse server response of accessory and wigs to download asset's URL at local path
 * and modify the JSON with local path
 */
object AccessoryAndWigsProcessor {

    /**
     * @param accessories Accessories to be applied
     * @param wig Wig to be applied
     * @param forceCache true, if internet cannot be used, false otherwise
     *
     * @return JSON that can be passed to native (cpp) code to apply accessories and wigs
     */
    suspend fun getInfo(
        accessories: List<Accessory>?,
        wig: Wig?,
        forceCache: Boolean = false
    ): String? = coroutineScope {
        val outputArray = JSONArray()

        val accessoriesJob = async(start = CoroutineStart.LAZY) {
            accessories?.let { ContentResourceDownloader.downloadAccessoryResources(it) }
        }
        val wigsJob = async(start = CoroutineStart.LAZY) {
            wig?.let { ContentResourceDownloader.downloadWigResources(it) }
        }
        // Run download jobs only if forceCache is false
        if (!forceCache) {
            awaitAll(accessoriesJob, wigsJob)
        }
        accessories?.forEach { accessory ->
            val url = accessory.images?.front
            val fileName = FileUtil.getFileNameFromUrl(url) ?: return@forEach
            val assetPath =
                ContentResourceDownloader.getAccessoryAssetPath(fileName) ?: return@forEach
            val infoObject =
                JSONObject(BobbleCoreSDK.moshi.adapter(Info::class.java).toJson(accessory.info))
            infoObject.put("assetPath", assetPath)
            outputArray.put(infoObject)
        }

        var alphaWigObject: WigInfo? = null
        var frontWigObject: WigInfo? = null

        wig?.info?.forEach { info ->
            val filename = info.filename ?: return@forEach
            val filepath = ContentResourceDownloader.getWigAssetPath(filename) ?: return@forEach
            info.type?.let { type ->
                info.assetPath = filepath
                if (type.equals("wig_alpha_image", true)) {
                    // set override Alpha true for Alpha image
                    info.overrideAlpha = true
                    alphaWigObject = info
                }
                if (type.equals("wig_front_image", true)) {
                    frontWigObject = info
                }
            }
        }

        if (alphaWigObject != null && frontWigObject != null) {
            outputArray.put(alphaWigObject.toJsonObject())
            outputArray.put(frontWigObject.toJsonObject())
        }

        return@coroutineScope if (outputArray.length() == 0)
            null
        else outputArray.toString()
    }


}