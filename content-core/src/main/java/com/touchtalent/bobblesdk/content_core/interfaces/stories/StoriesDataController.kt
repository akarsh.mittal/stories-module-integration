package com.touchtalent.bobblesdk.content_core.interfaces.stories

import com.touchtalent.bobblesdk.content_core.model.Story

interface StoriesDataController {

    suspend fun getListOfStoriesWithoutAds(): Result<List<Story>>
    suspend fun getListOfVerticalStories(
        nextToken: String?,
        limit: Int,
        searchString: String? = null
    ): Result<Pair<List<Story>, String?>>
}