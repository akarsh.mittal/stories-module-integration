package com.touchtalent.bobblesdk.content_core.model.wig

import com.squareup.moshi.JsonClass
import com.touchtalent.bobblesdk.content_core.model.commondata.Accessories
import com.touchtalent.bobblesdk.content_core.model.commondata.Anchors

@JsonClass(generateAdapter = true)
data class WigInfo(
    val accessories: Accessories?,
    val anchors: Anchors?,
    val filename: String?,
    val type: String?,
    val url: String?,
    var overrideAlpha: Boolean?,
    var assetPath: String?,
)