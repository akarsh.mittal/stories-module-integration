package com.touchtalent.bobblesdk.content_core.interfaces.watermark_injection

import android.content.Context
import android.graphics.Bitmap
import com.touchtalent.bobblesdk.content_core.model.commondata.Watermark

interface WatermarkInjectionModule {

    fun isWatermarkInjectionSdkAvailable(): Boolean

    public suspend fun applyWatermarkOnAnimatedSticker(
        applicationContext: Context,
        watermarkType: String?,
        filePath: String,
        contentWidth: Int,
        contentHeight: Int,
        watermark: Watermark?,
    ): Pair<String, String?>

}