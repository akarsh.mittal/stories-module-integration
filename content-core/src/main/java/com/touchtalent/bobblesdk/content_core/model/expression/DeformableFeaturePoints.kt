package com.touchtalent.bobblesdk.content_core.model.expression

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class DeformableFeaturePoints(
    val leftEye: List<Int>?,
    val mouth: List<Int>?,
    val rightEye: List<Int>?
)