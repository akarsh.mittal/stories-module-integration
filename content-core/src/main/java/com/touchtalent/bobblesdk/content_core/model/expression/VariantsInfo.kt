package com.touchtalent.bobblesdk.content_core.model.expression

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class VariantsInfo(
    val info: Info?,
    val type: String?
)