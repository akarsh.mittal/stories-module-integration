package com.touchtalent.bobblesdk.content_core.singletons

const val MODULE_CODE_NAME = "content-core"

const val SEARCH_TAB_CATEGORY_ID = -3
const val OTF_TAB_CATEGORY_ID = -2
const val RECENT_TAB_CATEGORY_ID = -1