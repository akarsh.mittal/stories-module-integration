package com.touchtalent.bobblesdk.content_core.sdk

import android.content.Context
import androidx.annotation.Keep
import com.touchtalent.bobblesdk.content_core.interfaces.BobbleContentAdapter
import com.touchtalent.bobblesdk.content_core.interfaces.BobbleContentModule
import com.touchtalent.bobblesdk.content_core.interfaces.ContentRenderingContext
import com.touchtalent.bobblesdk.content_core.singletons.MODULE_CODE_NAME
import com.touchtalent.bobblesdk.content_core.util.CleanCacheWorker
import com.touchtalent.bobblesdk.content_core.util.CompositeBobbleContentAdapter
import com.touchtalent.bobblesdk.content_core.util.CompositeContentRenderingContext
import com.touchtalent.bobblesdk.core.BobbleCoreSDK
import com.touchtalent.bobblesdk.core.config.BobbleCoreConfig
import kotlinx.coroutines.Job

/**
 * Base class for *content-core* module.
 */
@Keep
object ContentCoreSDK : BobbleContentModule() {

    lateinit var applicationContext: Context

    private var cleanCacheJob: Job? = null

    /**
     * Stores config that holds variables valid for all content modules
     */
    var contentCoreConfig: ContentCoreConfig? = null

    /**
     * Keeps a record of all content modules available at runtime.
     * Make sure to initialise other content modules before initialising this modules.
     */
    private val contentModule = mutableListOf<BobbleContentModule>()

    /**
     * Creates a [CompositeContentRenderingContext] which is a combination of all [ContentRenderingContext]
     * that is available at runtime.
     */
    override fun newContentRenderingInstance(): ContentRenderingContext {
        return CompositeContentRenderingContext(
            contentModule.filter { it != this }
                .map { it.newContentRenderingInstance() }
        )
    }

    /**
     * Gets the content module by its interface class. This is required for loosely-coupled modules where
     * the module might not be available during compile-time and only its interface might be available
     * for compilation. The parent app must use this function to get a reference to the module's base
     * class and communicate via interface functions instead of directing accessing classes from the
     * dependent module.
     */
    @JvmStatic
    @Suppress("unchecked_cast")
    fun <T> getContentModule(className: Class<T>): T? {
        for (module in contentModule) {
            if (className.isAssignableFrom(module.javaClass)) {
                return module as? T
            }
        }
        return null
    }

    override fun initialise(applicationContext: Context, config: BobbleCoreConfig) {
        this.applicationContext = applicationContext
        BobbleCoreSDK.getAllModules().forEach { bobbleModule ->
            if (bobbleModule is BobbleContentModule)
                contentModule.add(bobbleModule)
        }
    }

    /**
     * Called to clean cache folders in all content modules
     */
    override fun cleanCache() {
        CleanCacheWorker.enqueue()
    }

    override fun getBobbleContentAdapter(): BobbleContentAdapter {
        return CompositeBobbleContentAdapter(contentModule.filter { it != this }
            .map { it.getBobbleContentAdapter() })
    }

    internal fun forEachModule(action: (module: BobbleContentModule) -> Unit) {
        contentModule.forEach { action.invoke(it) }
    }

    override fun getCodeName() = MODULE_CODE_NAME

}