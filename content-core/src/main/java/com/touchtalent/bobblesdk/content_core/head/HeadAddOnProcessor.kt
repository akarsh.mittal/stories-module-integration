package com.touchtalent.bobblesdk.content_core.head

import android.graphics.Bitmap
import android.graphics.Point
import com.touchtalent.bobblesdk.content_core.interfaces.bobble_native_api.BobbleNativeApiModule
import com.touchtalent.bobblesdk.content_core.interfaces.bobble_native_api.IBobbleGraphicsTexture
import com.touchtalent.bobblesdk.content_core.model.HeadAddOns
import com.touchtalent.bobblesdk.content_core.opengl.IBobbleGraphicsContext
import com.touchtalent.bobblesdk.core.BobbleCoreSDK
import com.touchtalent.bobblesdk.core.model.BobbleHead
import com.touchtalent.bobblesdk.core.model.HeadAttributes

/**
 * Utility class to apply wigs, accessories and expression on a given head. It takes care of
 * downloading required resources
 * This function is functional only if *bobble-native-api* is included in the gradle since it
 * depends on native code to apply all add ons.
 *
 * [prepare] needs to be called before [getHeadBitmap] or [getHeadTexture] is used.
 *
 * @param bobbleHead Head on which add ons need to be applied
 */
class HeadAddOnProcessor(private val bobbleHead: BobbleHead, private val headAddOns: HeadAddOns) {

    var headAttributes: HeadAttributes? = null

    /**
     * Prepares resources required for modifying the head.
     */
    suspend fun prepare(forceCache: Boolean) {
        headAttributes = getHeadAttributes(forceCache)
    }

    /**
     * Process the [bobbleHead] to add wigs, accessories and expression and return new [Bitmap] and
     * updated facial feature points map of the new Bitmap
     *
     * @return Pair of updated bitmap to updated facial feature points map
     */
    fun getHeadBitmap(): Pair<Bitmap, Map<Long, Point>>? {
        val headAttributes = headAttributes ?: return null
        val bobbleNativeApiSDK = BobbleCoreSDK.getModule(BobbleNativeApiModule::class.java)
        val processor = bobbleNativeApiSDK?.getHeadProcessor() ?: return null

        return if ((!headAttributes.accessoryAndWigsArrayString.isNullOrEmpty()) ||
            !headAttributes.expressionString.isNullOrEmpty()
        ) {
            processor.getHeadBitmap(bobbleHead, headAttributes)
        } else null
    }

    /**
     * Process the [bobbleHead] to add wigs, accessories and expression and return new [IBobbleGraphicsTexture]
     * and updated facial feature points map of the new head
     *
     * @return Pair of updated texture to updated facial feature points map
     */
    fun getHeadTexture(graphicsContext: IBobbleGraphicsContext): Pair<IBobbleGraphicsTexture, Map<Long, Point>>? {
        if (headAddOns.isEmpty())
            return null
        val headAttributes = headAttributes ?: return null
        val bobbleNativeApiSDK = BobbleCoreSDK.getModule(BobbleNativeApiModule::class.java)
        val processor = bobbleNativeApiSDK?.getHeadProcessor() ?: return null
        return if ((!headAttributes.accessoryAndWigsArrayString.isNullOrEmpty()) ||
            !headAttributes.expressionString.isNullOrEmpty()
        ) {
            processor.getHeadTexture(bobbleHead, headAttributes, graphicsContext)
        } else null
    }

    private suspend fun getHeadAttributes(forceCache: Boolean): HeadAttributes {
        val headAttributes = HeadAttributes()
        headAttributes.accessoryAndWigsArrayString =
            AccessoryAndWigsProcessor.getInfo(headAddOns.accessories, headAddOns.wig, forceCache)
        headAttributes.expressionString = headAddOns.expression?.let {
            ExpressionProcessor.getExpressionString(it, bobbleHead)
        }
        return headAttributes
    }
}