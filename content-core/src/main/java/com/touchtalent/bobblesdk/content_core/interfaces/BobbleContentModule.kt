package com.touchtalent.bobblesdk.content_core.interfaces

import com.touchtalent.bobblesdk.core.interfaces.BobbleModule

/**
 * Base class for all content modules. Each content module must be responsible to provide a [ContentRenderingContext]
 * which denotes a lifecycle within with multiple content.
 */
abstract class BobbleContentModule : BobbleModule() {
    /**
     * Provides a new instance of [ContentRenderingContext].
     * P.S - Each lifecycle requires a new instance
     */
    abstract fun newContentRenderingInstance(): ContentRenderingContext

    /**
     * Invoked when cache needs to be cleaned. Usually called when the context of content rendering
     * is over so that storage consumption size is not abused
     */
    open fun cleanCache() {}

    open fun getContentCacheProvider(
        text: String?,
        languageCode: String
    ): ContentCacheProvider? = null

    abstract fun getBobbleContentAdapter(): BobbleContentAdapter
}
