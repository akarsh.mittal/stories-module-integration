package com.touchtalent.bobblesdk.content_core.interfaces.animation_sticker

interface AnimatedStickerCompatibilityManager {

    suspend fun addAnimatedStickerPacks(ids: IntArray)

    suspend fun addRecentSticker(
        id: Int,
        locale: String?,
        otfText: String?,
        previewUrl: String?,
        webpUrl: String? = null,
        packId: Int?
    )

}