package com.touchtalent.bobblesdk.content_core.util

import com.touchtalent.bobblesdk.content_core.interfaces.BobbleContent
import com.touchtalent.bobblesdk.content_core.interfaces.BobbleContentAdapter
import com.touchtalent.bobblesdk.content_core.model.ContentAddOns

class EmptyContentAdapter : BobbleContentAdapter() {
    override fun parseData(type: String, rawData: String, addOns: ContentAddOns): BobbleContent {
        throw UnsupportedOperationException()
    }

    override fun getSupportedContentTypes(): Set<String> {
        return emptySet()
    }

    override fun getSupportedMimeTypes(): List<String> {
        return emptyList()
    }
}