package com.touchtalent.bobblesdk.content_core.interfaces.movie_gif

interface MovieGifCompatibilityManager {

    suspend fun addRecentMovieGif(
        id: String,
        gifUrl: String?,
        height: Int,
        width: Int,
        packId: Int,
        timeStamp: Long,
    )
}