package com.touchtalent.bobblesdk.content_core.opengl

import android.opengl.EGL14
import android.opengl.EGLConfig
import android.opengl.EGLContext
import android.opengl.EGLDisplay

/**
 * Wrapper class for creating and managing OpenGL context which shares resources with a parent
 * [sharedContext]
 */
class BobbleGraphicsChildContext(private val sharedContext: IBobbleGraphicsContext) :
    IBobbleGraphicsContext() {

    private var mEGLContext = EGL14.EGL_NO_CONTEXT
    private val mEGLContextShared = sharedContext.context

    override val config: EGLConfig
        get() = sharedContext.config

    override val context: EGLContext
        get() = mEGLContext

    override val display: EGLDisplay
        get() = sharedContext.display

    /**
     * Initialise OpenGL context
     */
    init {
        val attrib2List = intArrayOf(
            EGL_CONTEXT_CLIENT_VERSION,
            EGL_VERSION_GLES2,
            EGL14.EGL_NONE
        )
        mEGLContext =
            EGL14.eglCreateContext(
                sharedContext.display,
                sharedContext.config,
                mEGLContextShared,
                attrib2List,
                0
            )
        if (mEGLContext == null)
            throw Exception("Failed to create context")
        checkError("initContext")
    }

    override fun activate(surface: BobbleGraphicsSurface?) {
        synchronized(sharedContext) {
            if (!EGL14.eglMakeCurrent(
                    sharedContext.display,
                    surface?.eglSurface ?: EGL14.EGL_NO_SURFACE,
                    surface?.eglSurface ?: EGL14.EGL_NO_SURFACE,
                    mEGLContext
                )
            ) {
                throw Exception("activate failed")
            }
            checkError("activate")
        }
    }

    override fun deactivate() {
        synchronized(sharedContext) {
            if (!EGL14.eglMakeCurrent(
                    sharedContext.display,
                    EGL14.EGL_NO_SURFACE,
                    EGL14.EGL_NO_SURFACE,
                    EGL14.EGL_NO_CONTEXT
                )
            ) {
                throw Exception("deactivate failed")
            }
            checkError("deactivate")
        }
    }

    /**
     * Destroy the content and release OpenGL from this thread
     */
    override fun dispose(releaseThread: Boolean) {
        if (sharedContext.display !== EGL14.EGL_NO_DISPLAY) {
            EGL14.eglDestroyContext(sharedContext.display, mEGLContext)
            if (releaseThread)
                EGL14.eglReleaseThread()
        }
        mEGLContext = EGL14.EGL_NO_CONTEXT
    }
}