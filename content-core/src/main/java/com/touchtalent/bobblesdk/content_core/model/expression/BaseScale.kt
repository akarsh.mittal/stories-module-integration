package com.touchtalent.bobblesdk.content_core.model.expression

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class BaseScale(
    val x: Int?,
    val y: Int?
)