package com.touchtalent.bobblesdk.content_core.model.expression

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Info(
    val baseScale: BaseScale?,
    val expression: String?,
    val featurePointsDeviations: FeaturePointsDeviations?,
    val unisex: Boolean?
)