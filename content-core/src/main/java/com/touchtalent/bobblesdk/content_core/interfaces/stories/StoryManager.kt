package com.touchtalent.bobblesdk.content_core.interfaces.stories

import android.graphics.Bitmap
import com.touchtalent.bobblesdk.content_core.model.Story

interface StoryManager {
    suspend fun getStories(): Result<List<BobbleStory>>
    suspend fun getStoryForNotification(): Result<BobbleStory>
    suspend fun getStoryForCard(): Result<BobbleStory>
    suspend fun downloadStoryPreview(story: BobbleStory): Bitmap
    suspend fun getVerticalStories(
        nextToken: String?,
        limit: Int,
        searchString: String? = null
    ): Result<StoryResponse>
    suspend fun getStoryById( storyId : Int) : Result<Story>
}
