package com.touchtalent.bobblesdk.content_core.sdk

import android.net.Uri
import com.touchtalent.bobblesdk.content_core.interfaces.BobbleContent
import com.touchtalent.bobblesdk.content_core.model.ContentMetadata

open class BobbleContentOutput(
    open val sourceContent: BobbleContent,
    open val finalContentMetadata: ContentMetadata?,
    open val localPath: String?,
    open val shareUri: Uri?,
    open val mimeType: String?
) {
    override fun toString(): String {
        return "BobbleContentOutput(sourceContent=$sourceContent, localPath=$localPath, shareUri=$shareUri), mimeType=$mimeType"
    }
}