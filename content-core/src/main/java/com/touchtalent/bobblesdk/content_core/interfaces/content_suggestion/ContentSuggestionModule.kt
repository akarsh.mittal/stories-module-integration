package com.touchtalent.bobblesdk.content_core.interfaces.content_suggestion

interface ContentSuggestionModule {
    suspend fun  getFirstPageContent(
        contentSuggestionCacheConfig : ContentSuggestionCacheConfig
    ) : Result<ContentSuggestionCacheResponse>
}