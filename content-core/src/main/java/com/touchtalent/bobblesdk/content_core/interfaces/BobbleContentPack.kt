package com.touchtalent.bobblesdk.content_core.interfaces

/**
 * Abstract class for a content pack.
 *
 * @property id Server id of the content pack
 * @property name User visible name of the pack, should be localised
 * @property icon Icon of the pack
 * @property description User visible description of the pack, should be localised
 */
abstract class BobbleContentPack {
    abstract val id: Int
    abstract val name: String?
    abstract val icon: String?
    abstract val banner: String?
    abstract val description: String?
    open val isVisited: Boolean = false
    open val isAllowedUserDelete: Boolean = false
    open val isRequireUserHead: Boolean = false
    open val isHeadSupported: Boolean = true
    open val isAutoDownloaded: Boolean = false
    open val shareUrl: String? = null
}