package com.touchtalent.bobblesdk.content_core.interfaces.bobble_native_api

import android.graphics.Bitmap
import android.graphics.Point
import com.touchtalent.bobblesdk.content_core.opengl.IBobbleGraphicsContext
import com.touchtalent.bobblesdk.content_core.opengl.UsesGLContext
import com.touchtalent.bobblesdk.core.model.BobbleHead
import com.touchtalent.bobblesdk.core.model.HeadAttributes

/**
 * Interface for interaction with *bobble-native-api* module for expression and accessory support
 * in heads
 */
interface NativeHeadProcessor {

    /**
     * Apply expression and accessory to given [head]
     * @param head Head to apply expression and accessory
     * @param headAttributes Meta information for expression and accessories
     * @return Pair of bitmap after applying expressions & accessories and modified face feature
     * point map
     * */
    @UsesGLContext
    fun getHeadBitmap(
        head: BobbleHead,
        headAttributes: HeadAttributes
    ): Pair<Bitmap, Map<Long, Point>>?

    /**
     * Apply expression and accessory to given [head]
     * @param head Head to apply expression and accessory
     * @param headAttributes Meta information for expression and accessories
     * @return Pair of OpenGL texture of head after applying expressions & accessories and modified
     * face feature point map
     * */
    @UsesGLContext
    fun getHeadTexture(
        head: BobbleHead,
        headAttributes: HeadAttributes,
        graphicsContext: IBobbleGraphicsContext
    ): Pair<IBobbleGraphicsTexture, Map<Long, Point>>?
}