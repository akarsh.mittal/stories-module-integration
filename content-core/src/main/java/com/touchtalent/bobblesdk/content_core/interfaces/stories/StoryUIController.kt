package com.touchtalent.bobblesdk.content_core.interfaces.stories

import android.content.Context
import android.content.Intent
import android.os.Bundle

abstract class StoryUIController {

    abstract class Builder(protected val context: Context){
        protected var bundle: Bundle? = null
        protected var screenName: String? = null
        protected var isKeyboardView = true
        protected var source: Source = Source.OTHERS
        protected var storyId = -1
        protected var searchString: String? = null
        protected var notificationText: String? = null

        /**
         * Set whether triggered from keyboard
         */
        fun setKeyboardView(isKeyboardView: Boolean) = apply {
            this.isKeyboardView = isKeyboardView
        }

        /**
         * Set the source from where activity launch
         */
        fun setSource(source: Source) = apply {
            this.source = source
        }

        /**
         * Set screen name
         */
        fun setScreenName(screenName: String) = apply {
            this.screenName = screenName
        }

        /**
         * Set landing story id
         */
        fun setStoryId(id: Int) = apply {
            this.storyId = id
        }

        fun setNotificationText(text: String) = apply {
            this.notificationText = text
        }

        fun setSearchString(search: String) = apply {
            this.searchString = search
        }

        /**
         * Start story activity
         */
        abstract fun startActivity()

        /**
         * Get explicit intent to start story activity
         */
        abstract fun getIntent(): Intent

    }

    /**
     * @return a new [Builder] instance
     *
     */
    abstract fun newBuilder(context: Context): Builder

    /**
     * Source to open stories activity
     */
    enum class Source {
        ICON,
        OTHERS,
        VERTICAL_STORIES,
        SEARCH,
        SAVED_STORIES,
        SEARCH_RESULTS,
        NOTIFICATION
    }


}