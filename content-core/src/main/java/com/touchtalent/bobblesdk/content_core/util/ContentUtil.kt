package com.touchtalent.bobblesdk.content_core.util

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Build
import com.touchtalent.bobblesdk.content_core.interfaces.bobble_native_api.BobbleNativeApiModule
import com.touchtalent.bobblesdk.content_core.sdk.ContentCoreSDK
import com.touchtalent.bobblesdk.core.BobbleCoreSDK
import com.touchtalent.bobblesdk.core.utils.BLog
import com.touchtalent.bobblesdk.core.utils.FileUtil
import com.touchtalent.bobblesdk.core.utils.GIF
import com.touchtalent.bobblesdk.core.utils.WEBP
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.File
import java.io.FileOutputStream

fun filterContentTypes(supportedMimeTypes: List<String>, contentTypes: List<String>): List<String> {
    val supportedContentTypes = mutableListOf<String>()
    val adapter = ContentCoreSDK.getBobbleContentAdapter()
    for (supportedMimeType in supportedMimeTypes) {
        if (adapter.getSupportedContentTypes().contains(supportedMimeType))
            supportedContentTypes.addAll(adapter.getSupportedContentTypes())
    }
    return (supportedContentTypes intersect contentTypes.toSet()).toList()
}

suspend fun convertToWebp(image: String) = withContext(Dispatchers.Default) {
    val imageFile = File(image)
    val outputFolder = FileUtil.getParent(image)
    val outputFile = FileUtil.join(
        outputFolder, "${imageFile.nameWithoutExtension}.webp"
    )
    if (FileUtil.exists(outputFile)) {
        return@withContext
    }
    kotlin.runCatching {
        if (imageFile.extension == GIF)
            convertGifToWebP(image, outputFile)
        else if (imageFile.extension != WEBP && outputFolder != null)
            StaticContentFileUtil.getShareableUri(
                outputFolder,
                image,
                listOf("image/webp.wasticker")
            )
    }.onSuccess {
        return@withContext
    }.onFailure {
        BLog.printStackTrace(it)
    }
    return@withContext
}

suspend fun convertImageToWebP(source: String, outputPath: String) = withContext(Dispatchers.IO) {
    val bmp: Bitmap = BitmapFactory.decodeFile(source)
    FileOutputStream(File(outputPath)).use { out ->
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            bmp.compress(
                Bitmap.CompressFormat.WEBP_LOSSLESS, // or WEBP_LOSSY
                100, out
            )
        } else {
            bmp.compress(Bitmap.CompressFormat.WEBP, 100, out)
        }
    }
}

private suspend fun convertGifToWebP(source: String, outputPath: String) =
    withContext(Dispatchers.Default) {
        val nativeModule = BobbleCoreSDK.getModule(BobbleNativeApiModule::class.java)
            ?: throw Exception("No native module present")
        val success = nativeModule.getWebpConvertor().getWebpFromGif(source, outputPath, 5f, 2)
        if (!success) throw Exception("Failed to convert")
    }