package com.touchtalent.bobblesdk.content_core.interfaces.animation_sticker

import com.touchtalent.bobblesdk.content_core.interfaces.BobbleContent
import com.touchtalent.bobblesdk.content_core.interfaces.BobbleContentPack
import com.touchtalent.bobblesdk.content_core.model.PaginationParams
import kotlinx.coroutines.flow.Flow

/**
 * Interface to facilitate all CRUD related operations on the module such as:
 * fetch list of animated stickers, fetch list of animated sticker packs, etc
 */
interface AnimatedStickerManager {
    /**
     * Add a animated sticker pack by id, the pack is downloaded from the server and added as a user
     * downloaded pack. Can be used to download packs from deeplink URLs
     *
     * @param ids List of ids to add
     */
    fun addAnimatedStickerPackByIds(vararg ids: Int)

    /**
     * Get count of un-explored packs, could be used to show red dot in the UI
     * @return number of animated sticker pack user has not visited
     */
    suspend fun getCountOfUnExploredPacks(): Int

    /**
     * Return a list of animated sticker packs.
     * @param locale Locale which must be used to customise the parameters
     * @return [Result] object containing [List] of [BobbleContentPack]
     */
    suspend fun getAnimatedStickerPacks(locale: String?): Result<List<BobbleContentPack>>

    /**
     * Return a [Flow] of recent animated stickers.
     */
    fun getRecentStickers(): Flow<List<BobbleContent>>

    /**
     * Delete a recent animated sticker from the database
     */
    suspend fun deleteRecentSticker(content: BobbleContent)

    /**
     * Get list of animated stickers corresponding to a sticker pack.
     * @param id Id of the pack, whose content needs to be fetched
     * @param locale Locale which should be used to customise the content
     * @param isHeadEnabled Locale which should be used to customise the content
     * @param locale Locale which should be used to customise the content
     */
    suspend fun getAnimatedStickers(
        id: Int,
        locale: String,
        isHeadEnabled: Boolean,
        isOtfEnabled: Boolean,
        pagination: PaginationParams?
    ): Result<List<BobbleContent>>

    /**
     * Check if the animated content pack is updated since last user visit
     * @param contentPackId id of the animated content pack to check
     * @param lastUpdatedTime Timestamp when the animated content pack was last updated from server.
     * @return true if the animated content is updated, false otherwise
     */
    suspend fun isUpdated(contentPackId: Int, lastUpdatedTime: Long): Boolean

    /**
     * Get list of trending search keywords that are being used in animated sticker pack store.
     * @param locale Locale in which the trending search keywords need to be localised
     * @return [kotlin.Result] Containing list of keywords
     */
    suspend fun getTrendingSearches(locale: String?): Result<List<String>>

    /**
     * Save given [bobbleContentPack] in the database.
     * @param bobbleContentPack Content pack to be saved.
     */
    suspend fun saveContentPack(bobbleContentPack: BobbleContentPack)

    /**
     * Get list of animated content packs in the store.
     * @param searchString Search text for which data needs to be fetched. If null, default trending
     * packs are fetched
     * @param locale Locale in which the result need to be localised
     * @param pagination Pagination params
     * @return [kotlin.Result] Containing list of [BobbleContentPack]
     */
    suspend fun getStoreContentPacks(
        searchString: String?,
        locale: String?,
        pagination: PaginationParams,
    ): Result<List<BobbleContentPack>>

    /**
     * Fetch details for a given animated pack id.
     * @param id Id of animated sticker pack whose data needs to be fetched
     * @return [kotlin.Result] containing [BobbleContentPack] corresponding to given [id]
     */
    suspend fun getStickerPackDetails(id: Int): Result<BobbleContentPack>

    /**
     * Mark the given [bobbleContentPack] as visited by the user
     * @param bobbleContentPack pack to be marked visited
     */
    suspend fun markVisited(bobbleContentPack: BobbleContentPack)

    /**
     * Remove the content pack corresponding to the given [contentPackId]
     * @param contentPackId Id if pack to be deleted
     */
    suspend fun deleteContentPack(contentPackId: Int)

    suspend fun getAnimatedStickerById(
        stickerId: Int,
        includeHeadDetails: Boolean,
        includeOtfDetails: Boolean,
        locale: String?
    ): Result<BobbleContent>
}