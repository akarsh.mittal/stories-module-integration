package com.touchtalent.bobblesdk.content_core.util

import android.view.View
import android.view.ViewGroup
import com.touchtalent.bobblesdk.content_core.sdk.BobbleContentView
import java.util.*

/**
 * A pool to recycle views and inflate them into a [ViewGroup] when required.
 * This needs to be used with [BobbleContentView], wherein different types of views can be inflated
 * inside it for different content formats.
 * @param poolSize Maximum number of off-screen views that can be cached
 */
abstract class ViewRecyclerPool<T : View>(var poolSize: Int) {

    private val viewPool = LinkedList<T>()

    /**
     * Inflates a instance of [T] provided by [onCreateView] or from recycled pool into [parent] and
     * returns the instance
     */
    fun inflateView(parent: ViewGroup): T {
        recycle(parent)
        val view = if (viewPool.isNotEmpty())
            viewPool.pop()
        else onCreateView(parent)
        parent.addView(view, 0)
        return view
    }

    /**
     * Recycles any instance of [T] present as a child view inside [parent] and puts it in the pool
     */
    fun recycle(parent: ViewGroup) {
        var i = 0
        while (i < parent.childCount) {
            val view = parent.getChildAt(i)
            parent.removeView(view)
            i--
            runCatching {
                if (canRecycle(view as T)) {
                    viewPool.add(view)
                    onRecycled(view)
                }
            }.onFailure {
                it.printStackTrace()
            }
            i++
        }
        evictIfAboveThreshold()
    }

    /**
     * Clears the pool of views
     */
    fun clear() {
        while (viewPool.isNotEmpty())
            onDestroyed(viewPool.remove())
    }

    private fun evictIfAboveThreshold() {
        while (viewPool.size > poolSize)
            onDestroyed(viewPool.remove())
    }

    /**
     * Called when a recycled view - [view] - is discarded from the pool
     */
    open fun onDestroyed(view: T) = Unit

    /**
     * Called when a [view] is recycled and placed into the pool
     */
    open fun onRecycled(view: T) = Unit

    /**
     * Check whether the given [view] is recyclable or not
     */
    abstract fun canRecycle(view: T): Boolean

    /**
     * Creates a new view in case no view is present in the pool
     * @param parent The parent view inside which the new view will be added
     */
    abstract fun onCreateView(parent: ViewGroup): T
}