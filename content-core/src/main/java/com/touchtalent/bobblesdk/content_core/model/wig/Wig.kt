package com.touchtalent.bobblesdk.content_core.model.wig

import com.squareup.moshi.JsonClass
import com.touchtalent.bobblesdk.content_core.cache.CacheGenerator
import com.touchtalent.bobblesdk.content_core.cache.Cacheable

@JsonClass(generateAdapter = true)
data class Wig(
    val gender: String?,
    val id: Int?,
    val info: List<WigInfo>?,
    val name: String?,
    val type: String?
) : Cacheable {
    override fun generateCacheKey(source: CacheGenerator) {
        id?.let { source.process(it) }
    }
}