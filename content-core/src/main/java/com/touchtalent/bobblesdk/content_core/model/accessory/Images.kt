package com.touchtalent.bobblesdk.content_core.model.accessory

import com.google.gson.annotations.SerializedName
import com.squareup.moshi.JsonClass
import com.touchtalent.bobblesdk.content_core.cache.CacheGenerator
import com.touchtalent.bobblesdk.content_core.cache.Cacheable

@JsonClass(generateAdapter = true)
class Images(
    val front: String?,
    @SerializedName("front_source")
    val frontSource: String?
) : Cacheable {
    override fun generateCacheKey(source: CacheGenerator) {
        source.process(front)
        source.process(frontSource)
    }
}