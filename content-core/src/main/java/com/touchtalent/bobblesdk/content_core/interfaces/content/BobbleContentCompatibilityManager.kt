package com.touchtalent.bobblesdk.content_core.interfaces.content

import android.net.Uri

/**
 * Utility class providing functions to perform migrations from existing content format to new
 * *content-module*
 */
interface BobbleContentCompatibilityManager {

    /**
     * Adds a recent sticker to the module.
     * @param stickerId Id of the sticker
     * @param stickerUri Uri pointing to the final rendered sticker
     * @param headId Id of the head used
     * @param headSource Source of the head used - "mascot" or "user"
     * @param headRelation Relation of the head used
     * @param headGender Gender of the head used
     * @param headType Bobble type of the head used
     * @param isTranslated Whether the sticker was translated
     * @param translationLocale Translation locale, if the sticker was translated
     * @param otfText OTF text used on the sticker
     */
    fun addRecentSticker(
        stickerId: Int,
        stickerUri: Uri,
        headId: String?,
        headSource: String?,
        headRelation: String?,
        headGender: String?,
        headType: Int?,
        isTranslated: Boolean,
        translationLocale: String?,
        otfText: String?
    )

    /**
     * Add a sticker pack.
     * @param id Id of the pack
     * @param iconUri Uri pointing to the icon of the pack
     * @param name Name of the pack
     * @param description Description of the pack
     * @param bannerUrl URL of the pack banner
     * @param isAutoDownloaded is the pack auto-downloaded
     * @param isVisited has the user already visited the pack
     * @param timestamp timestamp when the pack was added
     * @param lastVisited timestamp when the pack was last visited by the user
     */
    fun addStickerPack(
        id: Int,
        iconUri: String?,
        name: String?,
        description: String?,
        bannerUrl: String?,
        isAutoDownloaded: Boolean,
        isVisited: Boolean,
        timestamp: Long,
        lastVisited: Long
    )

}