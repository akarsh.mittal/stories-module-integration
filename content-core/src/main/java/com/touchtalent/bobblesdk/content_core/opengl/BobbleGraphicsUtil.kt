package com.touchtalent.bobblesdk.content_core.opengl

import android.graphics.SurfaceTexture

/**
 * Utility functions to create/manage OpenGL context and surfaces
 */
object BobbleGraphicsUtil {

    /**
     * Creates an OpenGL context without any shared context
     */
    fun createParentGraphicsContext(): IBobbleGraphicsContext {
        return BobbleGraphicsParentContext()
    }

    /**
     * Creates an OpenGL context with a shared context [sharedContext]
     */
    fun createChildGraphicsContext(sharedContext: IBobbleGraphicsContext): IBobbleGraphicsContext {
        return BobbleGraphicsChildContext(sharedContext)
    }

    /**
     * Creates an OpenGL surface from a [SurfaceTexture] using [context]
     */
    fun createGraphicsSurface(
        context: IBobbleGraphicsContext,
        surfaceTexture: SurfaceTexture
    ): BobbleGraphicsSurface {
        return BobbleGraphicsSurface(context, surfaceTexture)
    }

    /**
     * Creates an OpenGL surface on a new pBuffer with dimensions [width]*[height]
     */
    fun createGraphicsSurface(
        context: IBobbleGraphicsContext,
        width: Int,
        height: Int
    ): BobbleGraphicsSurface {
        return BobbleGraphicsSurface(context, width, height)
    }

    /**
     * Sets this context as current, invokes the [function] and deactivates it from current thread.
     * This function is thread-safe.
     * @param surface Optional surface which needs to be set current
     */
    fun <T> IBobbleGraphicsContext.execute(
        surface: BobbleGraphicsSurface? = null,
        function: () -> T
    ): T {
        synchronized(this) {
            activate(surface)
            val returnVal = function.invoke()
            deactivate()
            return returnVal
        }
    }
}