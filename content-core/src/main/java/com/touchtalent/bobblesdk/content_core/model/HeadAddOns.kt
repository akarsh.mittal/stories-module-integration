package com.touchtalent.bobblesdk.content_core.model

import com.touchtalent.bobblesdk.content_core.cache.CacheGenerator
import com.touchtalent.bobblesdk.content_core.cache.Cacheable
import com.touchtalent.bobblesdk.content_core.model.accessory.Accessory
import com.touchtalent.bobblesdk.content_core.model.expression.Expression
import com.touchtalent.bobblesdk.content_core.model.wig.Wig

/**
 * Params holding add ons that can be applied on a head
 *
 * @param accessories List of accessories that needs to be applied
 * @param wig Wig that needs to be applied
 * @param expression Expression that needs to be applied
 */
class HeadAddOns(
    val accessories: List<Accessory>? = null,
    val wig: Wig? = null,
    val expression: Expression? = null
) : Cacheable {
    fun isEmpty(): Boolean {
        return accessories.isNullOrEmpty() && wig == null && expression == null
    }

    companion object {
        val EMPTY = HeadAddOns()
    }

    override fun generateCacheKey(source: CacheGenerator) {
        source.processList(accessories)
        source.process(expression)
        source.process(wig)
    }
}