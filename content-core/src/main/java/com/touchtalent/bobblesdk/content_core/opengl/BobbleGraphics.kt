package com.touchtalent.bobblesdk.content_core.opengl

import android.opengl.EGL14

/**
 * Base class for all OpenGL wrapper classes for basic OpenGL utility functions and constants
 */
abstract class BobbleGraphics {

    companion object {
        const val EGL_VERSION_GLES2 = 2
        const val EGL_OPENGL_ES2_BIT = 4
        const val EGL_CONTEXT_CLIENT_VERSION = 0x3098
        const val EGL_RECORDABLE_ANDROID = 0x3142
    }

    /**
     * Check if last OpenGL command was successful or not.
     * @param msg Tag to be tagged with exception
     * @throws Exception if last command was unsuccessful
     */
    fun checkError(msg: String) {
        var error: Int
        if (EGL14.eglGetError().also { error = it } != EGL14.EGL_SUCCESS) {
            throw Exception(msg + ": EGL error: 0x" + Integer.toHexString(error))
        }
    }

    /**
     * Releases any resources that this class might be holding
     */
    abstract fun dispose(releaseThread: Boolean = true)
}