package com.touchtalent.bobblesdk.content_core.util

import android.view.ViewGroup
import com.touchtalent.bobblesdk.content_core.views.GlideImageView

class RecyclableImageView(poolSize: Int) : ViewRecyclerPool<GlideImageView>(poolSize) {


    override fun onCreateView(parent: ViewGroup): GlideImageView {
        return GlideImageView(parent.context)
    }

    override fun onRecycled(view: GlideImageView) {
        view.onViewRecycled()
    }

    override fun canRecycle(view: GlideImageView) = true

    override fun onDestroyed(view: GlideImageView) {
        view.onViewRecycled()
    }
}