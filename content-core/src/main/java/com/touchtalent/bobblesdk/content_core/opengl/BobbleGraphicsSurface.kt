package com.touchtalent.bobblesdk.content_core.opengl

import android.graphics.SurfaceTexture
import android.opengl.EGL14
import android.opengl.EGLSurface

/**
 * Wrapper class to creating and managing OpenGL surfaces
 * @property onDispose Callback to be invoked when [dispose] is called
 *
 */
class BobbleGraphicsSurface : BobbleGraphics {

    val graphicsContext: IBobbleGraphicsContext
    private var surfaceTexture: SurfaceTexture? = null

    private var mEGLSurface = EGL14.EGL_NO_SURFACE

    var onDispose: (() -> Unit)? = null

    val eglSurface: EGLSurface
        get() {
            return mEGLSurface
        }

    /**
     * Creates a OpenGL surface by allocating a new off-screen pBuffer of dimension [width]*[height]
     * @param graphicsContext Graphics context to use for creating this surface
     * @property width Width of pBuffer surface
     * @property height Height of pBuffer surface
     */
    constructor(graphicsContext: IBobbleGraphicsContext, width: Int, height: Int) : super() {
        this.graphicsContext = graphicsContext
        check(!(mEGLSurface !== EGL14.EGL_NO_SURFACE)) { "surface already created" }

        val surfaceAttr = intArrayOf(
            EGL14.EGL_WIDTH, width,
            EGL14.EGL_HEIGHT, height,
            EGL14.EGL_NONE
        )

        mEGLSurface = EGL14.eglCreatePbufferSurface(
            graphicsContext.display,
            graphicsContext.config,
            surfaceAttr,
            0
        )
        checkError("initSurface")
        if (mEGLSurface == null) {
            throw RuntimeException("surface was null")
        }
    }

    /**
     * Creates a OpenGL surface from a [SurfaceTexture]
     * @param graphicsContext Graphics context to use for creating this surface
     */
    constructor(graphicsContext: IBobbleGraphicsContext, surfaceTexture: SurfaceTexture) : super() {
        this.graphicsContext = graphicsContext
        val surfaceAttr = intArrayOf(EGL14.EGL_NONE)
        mEGLSurface =
            EGL14.eglCreateWindowSurface(
                graphicsContext.display,
                graphicsContext.config,
                surfaceTexture,
                surfaceAttr,
                0
            )
        checkError("initSurface")
        if (mEGLSurface == null) {
            throw Exception("surface was null")
        }
    }

    override fun dispose(releaseThread: Boolean) {
        if (mEGLSurface !== EGL14.EGL_NO_SURFACE) {
            EGL14.eglDestroySurface(graphicsContext.display, mEGLSurface)
            mEGLSurface = EGL14.EGL_NO_SURFACE
        }

        surfaceTexture?.release()

        if (graphicsContext.display !== EGL14.EGL_NO_DISPLAY) {
            EGL14.eglMakeCurrent(
                graphicsContext.display,
                EGL14.EGL_NO_SURFACE,
                EGL14.EGL_NO_SURFACE,
                EGL14.EGL_NO_CONTEXT
            )
        }
        onDispose?.invoke()
        checkError("dispose")
    }

    /**
     * Swap buffers to commit the contents of this surface onto the screen
     */
    fun swapBuffers(): Boolean {
        return EGL14.eglSwapBuffers(graphicsContext.display, mEGLSurface)
    }
}