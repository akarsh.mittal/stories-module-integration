package com.touchtalent.bobblesdk.content_core.model

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Fonts(val id: Int, val url: String)