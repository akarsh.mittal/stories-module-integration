package com.touchtalent.bobblesdk.content_core.sdk

import androidx.annotation.DrawableRes
import com.touchtalent.bobblesdk.content_core.config.BobbleStoryConfig
import com.touchtalent.bobblesdk.content_core.interfaces.BobbleContent

/**
 * Config class for content-core
 */
class ContentCoreConfig {
    @DrawableRes
    var seededWatermark: Int = 0

    var onContentShare: ((BobbleContent) -> Unit)? = null

    /**
     * for hiding otf_text in mint_kb due to security reasons.
     */
    var hideOtfInEvents: Boolean = false

    var useAspectRatio: Boolean = false

    var storyConfig: BobbleStoryConfig? = null
}