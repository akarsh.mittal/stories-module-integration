package com.touchtalent.bobblesdk.content_core.model.accessory

import com.squareup.moshi.JsonClass
import com.touchtalent.bobblesdk.content_core.cache.CacheGenerator
import com.touchtalent.bobblesdk.content_core.cache.Cacheable

@JsonClass(generateAdapter = true)
data class Accessory(
    val disableFaceComponents: Any?,
    val gender: String?,
    val id: Int?,
    val images: Images?,
    val info: Info?,
    val name: String?,
    val previewImageURL: String?,
    val type: String?
) : Cacheable {
    override fun generateCacheKey(source: CacheGenerator) {
        source.process(id)
    }
}