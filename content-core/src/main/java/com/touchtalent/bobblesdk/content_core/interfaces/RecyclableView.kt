package com.touchtalent.bobblesdk.content_core.interfaces

import androidx.recyclerview.widget.RecyclerView

/**
 * A view that can be recycled, to be used with [RecyclerView.Adapter.onViewRecycled].
 */
interface RecyclableView {
    fun onViewRecycled()
}
