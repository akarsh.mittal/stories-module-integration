package com.touchtalent.bobblesdk.content_core.util

import com.touchtalent.bobblesdk.content_core.interfaces.BobbleContent
import com.touchtalent.bobblesdk.content_core.interfaces.BobbleContentAdapter
import com.touchtalent.bobblesdk.content_core.model.ContentAddOns
import com.touchtalent.bobblesdk.core.utils.concatenateUnique
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.coroutineScope

class CompositeBobbleContentAdapter(private val adapters: List<BobbleContentAdapter>) :
    BobbleContentAdapter() {

    override fun parseData(type: String, rawData: String, addOns: ContentAddOns): BobbleContent {
        for (adapter in adapters) {
            if (adapter.getSupportedContentTypes().contains(type))
                return adapter.parseData(type, rawData, addOns)
        }
        throw Exception("No supported adapter found for type: $type")
    }

    override fun getSupportedContentTypes(): Set<String> {
        val set = mutableSetOf<String>()
        adapters.forEach { set.addAll(it.getSupportedContentTypes()) }
        return set
    }

    override fun getSupportedMimeTypes(): List<String> {
        return concatenateUnique(adapters.map { it.getSupportedMimeTypes() })
    }

    override suspend fun getRecentContent(
        vararg types: String
    ): List<BobbleContent> = coroutineScope {
        val list = mutableListOf<Deferred<List<BobbleContent>>>()
        for (adapter in adapters) {
            for (type in types) {
                if (adapter.getSupportedContentTypes().contains(type))
                    list.add(
                        async {
                            adapter.getRecentContent(type)
                        }
                    )
            }
        }
        val mutableList = mutableListOf<BobbleContent>()
        list.awaitAll().forEach { mutableList.addAll(it) }
        return@coroutineScope mutableList.sortedByDescending { it.lastShareTimeStamp }
    }

    override suspend fun getRecentCount(vararg types: String): Int = coroutineScope {
        val list = mutableListOf<Deferred<Int>>()
        for (adapter in adapters) {
            for (type in types) {
                if (adapter.getSupportedContentTypes().contains(type))
                    list.add(
                        async {
                            adapter.getRecentCount(type)
                        }
                    )
            }
        }
        return@coroutineScope list.awaitAll().sum()
    }
}
