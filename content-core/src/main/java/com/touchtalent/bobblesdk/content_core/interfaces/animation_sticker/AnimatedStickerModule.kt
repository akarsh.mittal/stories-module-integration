package com.touchtalent.bobblesdk.content_core.interfaces.animation_sticker

import com.touchtalent.bobblesdk.content_core.interfaces.animation_processor.AnimationProcessorModule

/**
 * Base module class for *animated-stickers*. This module is responsible for rendering animated
 * stickers - both static (no user customisation) and dynamic (including custom head, otf, etc).
 *
 * For dynamic animated rendering, additional dependency - *animation-processor* -
 * [AnimationProcessorModule] must be added
 * explicitly by the parent app
 */
interface AnimatedStickerModule {
    /**
     * Get instance of [AnimatedStickerManager] to facilitate all CRUD related operations on the
     * module
     * @see [AnimatedStickerManager]
     */
    fun getAnimatedStickerManager(): AnimatedStickerManager

    fun getAnimatedStickerCompatibilityManager(): AnimatedStickerCompatibilityManager
}