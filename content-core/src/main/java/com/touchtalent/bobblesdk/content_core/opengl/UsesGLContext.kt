package com.touchtalent.bobblesdk.content_core.opengl

/**
 * This annotation marks functions that might setup a OpenGL context on the calling thread.
 * Callers must ensure that any previously set OpenGL context will be cleared by this function
 */
@Target(AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.SOURCE)
@MustBeDocumented
annotation class UsesGLContext(val msg: String = "empty")
