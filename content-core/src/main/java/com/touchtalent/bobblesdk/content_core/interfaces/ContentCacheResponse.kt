package com.touchtalent.bobblesdk.content_core.interfaces

data class ContentCacheResponse(val contents: List<BobbleContent>, val nextToken: String?)