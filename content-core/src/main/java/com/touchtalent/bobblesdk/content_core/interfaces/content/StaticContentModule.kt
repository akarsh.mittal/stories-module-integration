package com.touchtalent.bobblesdk.content_core.interfaces.content

import android.content.Context
import com.touchtalent.bobblesdk.content_core.interfaces.BobbleContent
import io.reactivex.Single

/**
 * Base interface for *bobble-content* module
 */
interface StaticContentModule {
    /**
     * Returns a content which can be used for sharing a head.
     * @param context Context used for locale
     * @param gender Gender for which the content is created.
     * @return [Single] to fetch content
     */
    fun getHeadShareContent(context: Context, gender: String): Single<BobbleContent>

    /**
     * Returns content pack manager.
     * @see BobbleContentManager
     */
    fun getContentManager(): BobbleContentManager

    /**
     * Returns content pack compatibility manager.
     * @see BobbleContentCompatibilityManager
     */
    fun getContentCompatibilityManager(): BobbleContentCompatibilityManager
}