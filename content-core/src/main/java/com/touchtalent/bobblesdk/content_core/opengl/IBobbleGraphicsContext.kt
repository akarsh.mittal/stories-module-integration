package com.touchtalent.bobblesdk.content_core.opengl

import android.opengl.EGLConfig
import android.opengl.EGLContext
import android.opengl.EGLDisplay
import androidx.annotation.RestrictTo
import com.touchtalent.bobblesdk.content_core.opengl.BobbleGraphicsUtil.execute

/**
 * Wrapper class for creating and managing OpenGL context which can be used individually as well a
 * parent context to [BobbleGraphicsChildContext]
 */
abstract class IBobbleGraphicsContext : BobbleGraphics() {
    abstract val display: EGLDisplay
    abstract val config: EGLConfig
    abstract val context: EGLContext

    /**
     * Sets this context active along with [surface] on the calling thread.
     * [BobbleGraphicsUtil.execute] should be used to ensure that this context is released after being used
     */
    @RestrictTo(RestrictTo.Scope.LIBRARY)
    abstract fun activate(surface: BobbleGraphicsSurface?)

    /**
     * Unsets this context and any previously set surface on the calling thread
     * [BobbleGraphicsUtil.execute] should be used to ensure that this context is released after being used
     */
    @RestrictTo(RestrictTo.Scope.LIBRARY)
    abstract fun deactivate()
}