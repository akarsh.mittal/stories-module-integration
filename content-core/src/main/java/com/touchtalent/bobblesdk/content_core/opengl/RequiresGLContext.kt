package com.touchtalent.bobblesdk.content_core.opengl

/**
 * This annotation marks functions that require an OpenGL context ([IBobbleGraphicsContext]) to be
 * setup on the calling thread before being invoked
 */
@Target(AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.SOURCE)
@MustBeDocumented
annotation class RequiresGLContext
