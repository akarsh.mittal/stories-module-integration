package com.touchtalent.bobblesdk.content_core.util

/**
 * Checks whether given [supportedMimeTypes] supports animation or not.
 * Empty list is considered universal and true will be returned
 */
fun isAnimationSupported(supportedMimeTypes: List<String>?): Boolean {
    supportedMimeTypes?.let {
        return !(it.isNotEmpty() &&
                !it.contains("image/*") &&
                !it.contains("image/gif") &&
                !it.contains("image/webp.wasticker"))
    }
    return true
}