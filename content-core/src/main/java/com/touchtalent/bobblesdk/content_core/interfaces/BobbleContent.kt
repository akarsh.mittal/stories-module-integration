package com.touchtalent.bobblesdk.content_core.interfaces

import com.touchtalent.bobblesdk.content_core.model.accessory.Accessory
import com.touchtalent.bobblesdk.content_core.model.expression.Expression
import com.touchtalent.bobblesdk.content_core.model.wig.Wig
import com.touchtalent.bobblesdk.core.enums.Gender
import com.touchtalent.bobblesdk.core.model.Tracker

/**
 * Base class to represent all types of content - stickers, animated stickers, BigMoji, etc.
 *
 * P.S - [accessories] [wigs] & [expressions] are just data received from server in the
 * response of list of content. It doesn't necessarily mean that these need to be applied in this content.
 * Each content should define its own set of accessory id, wig id and expression id and search
 * within this list to find matches. It is the responsibility of the subclass to populate
 * these values from the server response, if applicable
 *
 * @property id Server id of the content
 * @property type Type of content - animated, static or video
 * @property gender Gender preference of the content - [Gender.UNISEX] for gender neutral content
 * @property impressionTrackers Impression trackers - check [Tracker]
 * @property shareTrackers Share trackers - check [Tracker]
 * @property isOtfSupported Indicates whether this content has support for customising the text on it
 * @property shareURL Invite URL for the parent app which would deeplink to this content. This
 * would be app specific
 * @property rawResourcesUrl URL pointing towards raw resources required to render this content.
 * @property accessories List of accessories that the content might require.
 * @property wigs List of wigs that the content might require.
 * @property expressions List of expressions that the content might require.
 */
abstract class BobbleContent(
    open val id: Int,
    open val type: Type,
    open val gender: Gender = Gender.UNISEX,
    open val recVersion: String? = null,
    open val contentCategoryId: Int? = null
) {

    abstract val isOtfSupported: Boolean
    abstract val isHeadSupported: Boolean
    open val impressionTrackers: List<Tracker>? = null
    open val shareTrackers: List<Tracker>? = null
    //deprecated
    open val shareURL: String? = null
    open val shareTexts: List<String>? = null
    open val enableShareTextInKeyboard: Boolean = false

    open val rawResourcesUrl: String? = null
    open val accessories: List<Accessory>? = null
    open val wigs: List<Wig>? = null
    open val expressions: List<Expression>? = null
    open val aspectRatio: String = "1:1"
    abstract val contentType: String
    open val lastShareTimeStamp: Long? = null

    /**
     * Enum denoting type of this content
     */
    enum class Type {
        /**
         * Animated content represent stream of images without any audio
         */
        ANIMATED,

        /**
         * Static content represent a single image
         */
        STATIC,

        /**
         * Video content represent a stream of images with audio
         */
        VIDEO
    }

    /**
     * Get a preview URL for this content. The preview URL should be a lightweight asset which can
     * be rendered instantly
     */
    abstract fun getPreviewUrl(): String?

    open val shareText: String? = null

    override fun toString(): String {
        return "${javaClass.simpleName}-$id"
    }

}