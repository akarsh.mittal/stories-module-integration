package com.touchtalent.bobblesdk.content_core.interfaces.content

import com.touchtalent.bobblesdk.content_core.interfaces.BobbleContent
import com.touchtalent.bobblesdk.content_core.interfaces.BobbleContentPack
import com.touchtalent.bobblesdk.content_core.model.PaginationParams
import kotlinx.coroutines.flow.Flow

/**
 * Interface to manage content packs within the module
 */
interface BobbleContentManager {
    /**
     * Add a sticker pack by id, the pack is downloaded from the server and added as a user
     * downloaded pack. Can be used to download packs from deeplink URLs
     *
     * @param ids List of ids to add
     */
    fun addStickerPackByIds(vararg ids: Int)

    /**
     * Get count of un-explored packs, could be used to show red dot in the UI
     * @return number of sticker pack user has not visited
     */
    suspend fun getCountOfUnExploredStickerPacks(): Int

    /**
     * Return a list of sticker packs.
     * @param locale Locale which must be used to customise the parameters
     * @return [Result] object containing [List] of [BobbleContentPack]
     */
    suspend fun getBobbleStickerPacks(locale: String?): Result<List<BobbleContentPack>>

    /**
     * Return a [Flow] of recent stickers.
     */
    fun getRecentStickers(): Flow<List<BobbleContent>>

    /**
     * Delete a recent sticker from the database
     */
    suspend fun deleteRecentSticker(content: BobbleContent)

    /**
     * Get list of stickers corresponding to a sticker pack.
     * @param id Id of the pack, whose content needs to be fetched
     * @param locale Locale which should be used to customise the content
     * @param isHeadEnabled Locale which should be used to customise the content
     * @param locale Locale which should be used to customise the content
     */
    suspend fun getBobbleStickers(
        id: Int,
        locale: String,
        isHeadEnabled: Boolean,
        isOtfEnabled: Boolean,
        pagination: PaginationParams?
    ): Result<List<BobbleContent>>

    /**
     * Check if the content pack is updated since last user visit
     * @param contentPackId id of the content pack to check
     * @param lastUpdatedTime Timestamp when the content pack was last updated from server.
     * @return true if the content is updated, false otherwise
     */
    suspend fun isUpdated(contentPackId: Int, lastUpdatedTime: Long): Boolean
    suspend fun getTrendingSearches(locale: String?): Result<List<String>>
    suspend fun saveContentPack(bobbleContentPack: BobbleContentPack)
    suspend fun getStoreContentPacks(
        pageNumber: Int,
        limit: Int,
        searchString: String?,
        locale: String?
    ): Result<List<BobbleContentPack>>

    suspend fun getStickerPackDetails(id: Int): Result<BobbleContentPack>
    suspend fun markVisited(bobbleContentPack: BobbleContentPack)
    suspend fun deleteContentPack(contentPackId: Int)
}