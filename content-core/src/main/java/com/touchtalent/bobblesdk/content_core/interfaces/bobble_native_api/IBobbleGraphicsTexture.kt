package com.touchtalent.bobblesdk.content_core.interfaces.bobble_native_api

/**
 *
 */
interface IBobbleGraphicsTexture {
    @Throws(Exception::class)
    fun getTextureId(): Int

    @Throws(Exception::class)
    fun getWidth(): Int

    @Throws(Exception::class)
    fun getHeight(): Int

    @Throws(Exception::class)
    fun dispose()
}