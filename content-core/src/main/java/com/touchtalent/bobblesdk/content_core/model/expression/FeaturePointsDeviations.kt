package com.touchtalent.bobblesdk.content_core.model.expression

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class FeaturePointsDeviations(
    val deformableFeaturePoints: DeformableFeaturePoints?,
    val sets: List<Set>?
)