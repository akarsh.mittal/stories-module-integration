package com.touchtalent.bobblesdk.content_core.util

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import com.touchtalent.bobblesdk.core.utils.BLog
import com.touchtalent.bobblesdk.core.utils.FileUtil
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

/**
 * Utility class to generate a shareable format of a image file based on supported mime type
 */
object StaticContentFileUtil {

    /**
     * In case of JPEG and other unknown formats, background should not be transparent
     */
    fun shouldUseTransparentBackground(supportedMimeTypes: List<String>): Boolean {
        if (supportedMimeTypes.isEmpty())
            return false
        return supportedMimeTypes.contains("image/webp.wasticker") ||
                supportedMimeTypes.contains("image/webp") ||
                supportedMimeTypes.contains("image/png")
    }

    /**
     * Get a shareable URI of the of path matching a path that is supported by [supportedMimeTypes]
     *
     * @param destPath Destination path where the final image needs to be copied
     * @param path Path of the source file
     * @param supportedMimeTypes List of supported mime types
     *
     * @return Pair of path of the shareable sticker to the mime type chosen
     */
    fun getShareableUri(
        destPath: String,
        path: String,
        supportedMimeTypes: List<String>
    ): Pair<String?, String> {
        if (supportedMimeTypes.isEmpty()) return getShareablePath(destPath, path) to "image/png"
        return when {
            supportedMimeTypes.contains("image/webp.wasticker") -> {
                getWhatsappShareablePath(destPath, path) to "image/webp.wasticker"
            }
            supportedMimeTypes.contains("image/png") -> {
                getShareablePath(destPath, path, Bitmap.CompressFormat.PNG) to "image/png"
            }
            supportedMimeTypes.contains("image/webp") -> {
                getShareablePath(destPath, path, Bitmap.CompressFormat.WEBP) to "image/webp"
            }
            supportedMimeTypes.contains("image/jpeg") -> {
                getShareablePath(destPath, path, Bitmap.CompressFormat.JPEG) to "image/jpeg"
            }
            supportedMimeTypes.contains("image/jpg") -> {
                getShareablePath(destPath, path, Bitmap.CompressFormat.JPEG) to "image/jpg"
            }
            else -> getShareablePath(destPath, path) to "image/png"
        }
    }

    /**
     * Convert image at [path] to make it shareable on WhatsApp as per WhatsApp's guideline
     */
    private fun getWhatsappShareablePath(destPath: String, path: String): String? {
        try {
            var bitmap = BitmapFactory.decodeFile(path) ?: return null
            val file = File(path)
            val shareablePath = FileUtil.join(destPath, file.name)
            val extension = File(shareablePath).extension
            val shareableFile = File(
                if (extension.isNotBlank())
                    shareablePath.replace(extension, "webp")
                else "$shareablePath.webp"
            )
            shareableFile.createNewFile()
            bitmap = Bitmap.createScaledBitmap(bitmap, 512, 512, true)
            val fos = FileOutputStream(shareableFile.absolutePath)
            bitmap.compress(Bitmap.CompressFormat.WEBP, 70 /* ignored for PNG */, fos)
            fos.flush()
            fos.close()
            bitmap.recycle()
            return shareableFile.absolutePath
        } catch (e: IOException) {
            BLog.printStackTrace(e)
        }
        return null
    }

    private fun getShareablePath(
        destPath: String,
        path: String,
        compressFormat: Bitmap.CompressFormat? = null
    ): String? {
        val file = File(path)
        var newFilePath = FileUtil.join(destPath, file.name)
        if (compressFormat == null ||
            (compressFormat == Bitmap.CompressFormat.PNG && path.endsWith(".png")) ||
            (compressFormat == Bitmap.CompressFormat.WEBP && path.endsWith(".webp")) ||
            (compressFormat == Bitmap.CompressFormat.JPEG &&
                    (path.endsWith(".jpg") || path.endsWith(".jpeg")))
        ) {
            FileUtil.copy(file.absolutePath, newFilePath)
            if (FileUtil.exists(newFilePath))
                return newFilePath
            return null
        }
        val extension = File(path).extension
        newFilePath = if (extension.isBlank())
            when (compressFormat) {
                Bitmap.CompressFormat.WEBP -> "$newFilePath.webp"
                Bitmap.CompressFormat.JPEG -> "$newFilePath.jpeg"
                else -> "$newFilePath.png"
            }
        else when (compressFormat) {
            Bitmap.CompressFormat.WEBP -> newFilePath.replace(extension, "webp")
            Bitmap.CompressFormat.JPEG -> newFilePath.replace(extension, "jpeg")
            else -> newFilePath.replace(extension, "png")
        }
        try {
            val bitmap = BitmapFactory.decodeFile(path) ?: return null
            File(newFilePath).createNewFile()
            val fos = FileOutputStream(newFilePath)
            bitmap.compress(compressFormat, 100, fos)
            fos.flush()
            fos.close()
            bitmap.recycle()
            return newFilePath
        } catch (e: IOException) {
            BLog.printStackTrace(e)
        }
        return null
    }


}