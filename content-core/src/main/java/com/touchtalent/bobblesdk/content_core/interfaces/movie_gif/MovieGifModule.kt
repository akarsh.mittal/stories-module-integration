package com.touchtalent.bobblesdk.content_core.interfaces.movie_gif

/**
 * Base interface for *moviegif* module
 */

interface MovieGifModule {
    /**
     * Returns movieGif compatibility manager.
     * @see MovieGifCompatibilityManager
     */
    fun getMovieGifCompatibilityManager(): MovieGifCompatibilityManager
}