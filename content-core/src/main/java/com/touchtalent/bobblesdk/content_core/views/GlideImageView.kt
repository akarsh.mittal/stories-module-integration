package com.touchtalent.bobblesdk.content_core.views

import android.content.Context
import android.util.AttributeSet
import com.bumptech.glide.Glide
import com.touchtalent.bobblesdk.content_core.interfaces.RecyclableView
import com.touchtalent.bobblesdk.core.utils.GeneralUtils
import com.touchtalent.bobblesdk.core.views.ImpressionImageView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch


class GlideImageView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null
) : ImpressionImageView(context, attrs), RecyclableView {

    private var job: Job? = null

    fun launch(scope: CoroutineScope, block: suspend CoroutineScope.() -> Unit) {
        job?.cancel()
        job = scope.launch { block() }
    }

    override fun onViewRecycled() {
        job?.cancel()
        reset()
        if (GeneralUtils.isValidContextForGlide(context)) {
            Glide.with(this).clear(this)
        }
    }
}