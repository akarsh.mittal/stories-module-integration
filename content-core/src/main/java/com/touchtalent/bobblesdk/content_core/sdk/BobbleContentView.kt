package com.touchtalent.bobblesdk.content_core.sdk

import android.content.Context
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.TextureView
import android.widget.FrameLayout
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.touchtalent.bobblesdk.content_core.interfaces.BobbleContent
import com.touchtalent.bobblesdk.content_core.interfaces.ContentRenderingContext
import com.touchtalent.bobblesdk.content_core.interfaces.RecyclableView
import com.touchtalent.bobblesdk.content_core.model.ContentMetadata
import com.touchtalent.bobblesdk.content_core.util.ViewRecyclerPool
import kotlinx.coroutines.Job
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.cancelChildren
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch

/**
 * Helper view where all types of [BobbleContent] can be rendered.
 * This view is a [FrameLayout] and expects each content to populate its own choice of view on which
 * it would render content. For e.g - Static stickers might inflate a [ImageView], Animated stickers
 * might inflate a [TextureView].
 *
 * For [BobbleContentView] to work smoothly with multiple content formats inside a [RecyclerView], consider populating it
 * with child views with the help of [ViewRecyclerPool].
 */
class BobbleContentView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null
) : FrameLayout(context, attrs), RecyclableView {

    private var detachScope = MainScope()
    val recycleAbleScope = MainScope()

    private var content: BobbleContent? = null
    private var metadata: ContentMetadata? = null
    private var clickJob: Job? = null

    var placeholder: Drawable? = null

    val currentContent: BobbleContent?
        get() = content

    /**
     * [ContentRenderingContext] to be set for animated content to be run
     */
    private var _contentRenderingContext: ContentRenderingContext? = null

    var contentRenderingContext: ContentRenderingContext
        get() = _contentRenderingContext
            ?: throw  Exception("ContentRenderingInstance cannot be null")
        set(value) {
            _contentRenderingContext = value
        }

    /**
     * Render the [content] on this view customised with [metadata]
     */
    fun setContent(content: BobbleContent, metadata: ContentMetadata? = null) {
        // Should be called before setting new content, as it is required by
        // CompositeContentRenderingContext to decide Context to recycle based on current context
        onViewRecycled()
        this.content = content
        this.metadata = metadata
        if (contentRenderingContext.canRender(content))
            contentRenderingContext.render(content, metadata, this)
    }

    /**
     * Render the [content] on this view customised with [metadata]
     */
    fun setPlayableContent(
        content: BobbleContent,
        metadata: ContentMetadata? = null,
    ): Flow<Int> {
        // Should be called before setting new content, as it is required by
        // CompositeContentRenderingContext to decide Context to recycle based on current context
        onViewRecycled()
        this.content = content
        this.metadata = metadata
        if (contentRenderingContext.canRender(content))
            return contentRenderingContext.renderPlayable(content, metadata, this)
        else throw Exception("Unsupported content type")
    }

    /**
     * *Play* Media control for the animation playing in this content view, if any
     */
    fun play() {
        _contentRenderingContext?.play(this)
    }

    /**
     * *Pause* Media control for the animation playing in this content view, if any
     */
    fun pause() {
        _contentRenderingContext?.pause(this)
    }

    /**
     * Function to listen for clicks on content after they have been rendered. A instance of [BobbleContentOutput]
     * is returned upon successful exporting.
     * [ContentRenderingContext.export] will be called to export the content
     * @param listener Listener to listen for clicks
     */
    fun setContentClickListener(listener: (Result<BobbleContentOutput>) -> Unit) {
        setOnClickListener {
            val contentToExport =
                content ?: run {
                    listener.invoke(
                        Result.failure(Exception("No content has been set yet"))
                    )
                    return@setOnClickListener
                }
            clickJob?.cancel()
            clickJob = detachScope.launch {
                listener.invoke(contentRenderingContext.export(contentToExport, metadata))
            }
        }
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        detachScope.coroutineContext.cancelChildren()
    }

    override fun onViewRecycled() {
        recycleAbleScope.coroutineContext.cancelChildren()
        _contentRenderingContext?.onViewRecycled(this)
    }
}
