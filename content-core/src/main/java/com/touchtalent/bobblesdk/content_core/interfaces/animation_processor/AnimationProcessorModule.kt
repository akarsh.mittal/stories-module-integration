package com.touchtalent.bobblesdk.content_core.interfaces.animation_processor

import com.touchtalent.bobblesdk.content_core.interfaces.BobbleContent
import com.touchtalent.bobblesdk.content_core.interfaces.ContentRenderingContext
import com.touchtalent.bobblesdk.content_core.sdk.BobbleContentView

/**
 * Base module class for animated-processor. This module is responsible for processing and rendering
 * dynamic (including custom head, otf, etc) animations.
 *
 * This module is responsible only for the business logic handling and does not involve any data layer.
 * It acts as an extension to the module *animated-stickers*. It depends on *bobble-native-api* for
 * animated rendering.
 */
interface AnimationProcessorModule {
    /**
     * Returns a new [ContentRenderingContext] which implements the logic for downloading, preparing and
     * rendering a supported [BobbleContent] on a [BobbleContentView]. The [ContentRenderingContext]
     * is also responsible for
     */
    fun newContentRenderingInstance(): ContentRenderingContext

    fun isDeviceCompatible(): Boolean
}