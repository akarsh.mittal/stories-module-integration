package com.touchtalent.bobblesdk.content_core.util

import android.content.Context
import androidx.work.CoroutineWorker
import androidx.work.ExistingWorkPolicy
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkerParameters
import com.touchtalent.bobblesdk.content_core.sdk.ContentCoreSDK
import com.touchtalent.bobblesdk.core.BobbleCoreSDK
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import java.util.concurrent.TimeUnit

/**
 * Worker to clean cache resources that are no longer required to free up user storage space
 */
internal class CleanCacheWorker(
    appContext: Context,
    params: WorkerParameters
) : CoroutineWorker(appContext, params) {

    companion object {
        private const val MIN_TIME_TO_RETAIN_FILES_IN_MS = 30 * 60 * 1000L // 30 minutes
        fun enqueue() {
            val request = OneTimeWorkRequest
                .Builder(CleanCacheWorker::class.java)
                .setInitialDelay(MIN_TIME_TO_RETAIN_FILES_IN_MS, TimeUnit.MILLISECONDS)
                .build()
            val workManager = BobbleCoreSDK.getWorkManager() ?: return
            workManager.enqueueUniqueWork("clean-cache", ExistingWorkPolicy.KEEP, request)
        }
    }

    override suspend fun doWork(): Result = coroutineScope {
        launch {
            ContentResourceDownloader.trimResourceCache()
        }
        launch {
            ContentCoreSDK.forEachModule {
                kotlin.runCatching {
                    it.cleanCache()
                }
            }
        }
        return@coroutineScope Result.success()
    }
}