package com.touchtalent.bobblesdk.content_core.interfaces

abstract class ContentCacheProvider(open val languageCode: String, open val text: String?) {
    abstract suspend fun getContent(token: String?, limit: Int): ContentCacheResponse
    abstract fun isCacheToken(token: String): Boolean
}