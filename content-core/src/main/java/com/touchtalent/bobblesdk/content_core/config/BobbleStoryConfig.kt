package com.touchtalent.bobblesdk.content_core.config

import android.app.Activity
import com.touchtalent.bobblesdk.content_core.interfaces.stories.StoryAdsAdapterFactory

class BobbleStoryConfig {
    var adsAdapterFactory: StoryAdsAdapterFactory? = null
    var onViewMoreClick: ((activity: Activity) -> Unit)? = null
    var onHeadsCLick: ((activity: Activity) -> Unit)? = null
}