package com.touchtalent.bobblesdk.content_core.util

import com.touchtalent.bobblesdk.content_core.interfaces.BobbleContent
import com.touchtalent.bobblesdk.content_core.model.accessory.Accessory
import com.touchtalent.bobblesdk.content_core.model.wig.Wig
import com.touchtalent.bobblesdk.content_core.model.wig.WigInfo
import com.touchtalent.bobblesdk.content_core.sdk.ContentCoreSDK
import com.touchtalent.bobblesdk.core.interfaces.BobbleModule
import com.touchtalent.bobblesdk.core.utils.FileUtil
import com.touchtalent.bobblesdk.core.utils.ResourceDownloader
import kotlinx.coroutines.*
import java.util.*

/**
 * Utility class to download raw resources for any content.
 */
object ContentResourceDownloader {

    const val CACHE_KEY = "default_content_resource"
    // Since Fonts are required for multiple content formats and in some cases, like pop-text, it
    // is required very frequently and cannot afford to be evicted in case where user has consumed a
    // lot of content, a separate cache is being made for this.
    const val FONT_CACHE_KEY = "font_content_resource"

    /**
     * Downloads and cache the [BobbleContent.rawResourcesUrl] of [content]. Resources having ZIP
     * file are unzipped and stored. [module] is used to resolve the path where the cache is stored.
     *
     * Cache will be stored at [BobbleModule.cacheDir]/resources
     *
     * @param forceCache false if internet cannot be used, true otherwise
     *
     * @throws Exception if failed to download the file and no cache exists
     * @return Pair containing path of the raw resource on local path, to true/false indicating
     * cache status of the result, true if from internet, false otherwise.
     */
    suspend fun downloadRawResources(
        content: BobbleContent,
        module: BobbleModule,
        forceCache: Boolean = false,
        cacheKey: String = CACHE_KEY
    ): Pair<String, Boolean> = withContext(Dispatchers.IO) {
        val resourceUrl = content.rawResourcesUrl
            ?: throw Exception("rawResources for $content empty")
        val destDir = FileUtil.createDirAndGetPath(module.cacheDir, "resources", "raw")
        val pair = ResourceDownloader.downloadResourcesSuspend(
            url = resourceUrl,
            destDir = destDir,
            cacheKey = cacheKey,
            forceCache = forceCache
        )
        val path = pair.first
            ?: throw Exception("Failed to download raw resources zip for ${content.rawResourcesUrl}")
        return@withContext path to pair.second
    }

    suspend fun trimResourceCache() {
        ResourceDownloader.restrictCacheDir(CACHE_KEY, evaluateCacheSize())
        ResourceDownloader.restrictCacheDir(FONT_CACHE_KEY, evaluateFontCacheSize())
    }

    private fun evaluateCacheSize(): Long {
        // TODO implement better logic to decide cache size based on total storage size, available
        //  storage size, etc
        return 150 * 1024 * 1024
    }

    private fun evaluateFontCacheSize(): Long {
        // TODO implement better logic to decide cache size based on total storage size, available
        //  storage size, etc
        return 20 * 1024 * 1024 //20MB
    }

    private suspend fun getWigAssetDir() = withContext(Dispatchers.IO) {
        FileUtil.createDirAndGetPath(ContentCoreSDK.cacheDir, "resources", "wigs")
    }

    internal suspend fun getWigAssetPath(assetName: String): String? = withContext(Dispatchers.IO) {
        val path = FileUtil.join(getWigAssetDir(), assetName)
        return@withContext if (FileUtil.exists(path))
            path
        else null
    }

    private suspend fun getAccessoryAssetDir() = withContext(Dispatchers.IO) {
        FileUtil.createDirAndGetPath(ContentCoreSDK.cacheDir, "resources", "accessories")
    }

    internal suspend fun getAccessoryAssetPath(assetName: String): String? =
        withContext(Dispatchers.IO) {
            val path = FileUtil.join(getAccessoryAssetDir(), assetName)
            return@withContext if (FileUtil.exists(path))
                path
            else null
        }

    internal suspend fun downloadWigResources(wig: Wig) = coroutineScope {
        val id = wig.id ?: return@coroutineScope
        val wigDirectoryPath = getWigAssetDir()
        val infoList = wig.info ?: return@coroutineScope
        val jobs = Collections.synchronizedList(mutableListOf<Job>())
        for (info: WigInfo in infoList) {
            info.url?.let {
                if (it.trim().isNotEmpty()) {
                    jobs.add(launch(Dispatchers.IO) {
                        ResourceDownloader.downloadResourcesSuspend(
                            url = it,
                            destDir = wigDirectoryPath,
                            cacheKey = CACHE_KEY
                        )
                    })
                } else throw Exception("URL empty for WIG with $id ")
            }
        }
        jobs.forEach { it.join() }
    }

    internal suspend fun downloadAccessoryResources(accessories: List<Accessory>) = coroutineScope {
        val jobs = Collections.synchronizedList(mutableListOf<Job>())
        for (accessory: Accessory in accessories) {
            val accessoryPath = getAccessoryAssetDir()
            accessory.images?.front?.let { url ->
                if (url.trim().isNotEmpty())
                    jobs.add(
                        withContext(Dispatchers.IO) {
                            launch {
                                ResourceDownloader.downloadResourcesSuspend(
                                    url = url,
                                    destDir = accessoryPath,
                                    cacheKey = CACHE_KEY
                                )
                            }
                        }
                    )
                else throw Exception("Empty URL for accessory ID $accessory")
            }
        }
        jobs.forEach { it.join() }
    }
}