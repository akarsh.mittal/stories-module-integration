package com.touchtalent.bobblesdk.content_core.model

data class PaginationParams(val offset: Int, val limit: Int)