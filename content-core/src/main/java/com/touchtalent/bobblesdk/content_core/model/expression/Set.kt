package com.touchtalent.bobblesdk.content_core.model.expression

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class Set(
    val _comment: String?,
    val deviationData: DeviationData?,
    val deviationType: String?
)