package com.touchtalent.bobblesdk.content_core.opengl

/**
 * This annotation marks functions that require an OpenGL surface ([BobbleGraphicsSurface]) to be
 * setup on the calling thread before being invoked
 */
@Target(AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.SOURCE)
@MustBeDocumented
annotation class RequiresGLSurface
