package com.touchtalent.bobblesdk.content_core.interfaces.stories

import android.view.ViewGroup
import androidx.viewbinding.ViewBinding
import com.touchtalent.bobblesdk.content_core.interfaces.ContentRenderingContext
import com.touchtalent.bobblesdk.content_core.model.Story
import kotlinx.coroutines.flow.Flow

abstract class StoryAdapter {
    abstract fun getIdentifier(): String
    abstract fun onCreateView(parent: ViewGroup): ViewBinding
    abstract fun onDestroyView(viewBinding: ViewBinding, story: Story?)
    abstract suspend fun play(viewBinding: ViewBinding, story: Story): Flow<Int>?
    abstract fun resume(viewBinding: ViewBinding, story: Story? = null)
    abstract fun pause(viewBinding: ViewBinding, story: Story? = null)

    /*
     For setting just the image and not playing the video
     */
    open fun setStaticContent(viewBinding: ViewBinding, story: Story) {}

    var renderingContext: ContentRenderingContext? = null
    var onClose: (() -> Unit)? = null
    var onShare: ((story: Story) -> Unit)? = null
    var onDownload: ((story: Story) -> Unit)? = null
    var retryLoading: (() -> Unit)? = null
}
