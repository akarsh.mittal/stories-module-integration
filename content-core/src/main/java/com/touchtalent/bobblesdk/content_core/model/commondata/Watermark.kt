package com.touchtalent.bobblesdk.content_core.model.commondata

data class Watermark(
    val width: Float,
    val x: Float,
    val y: Float,
    val url: String?,
    val fallbackResource: Int?
) {
    fun valid(): Boolean {
        return width > 0 && x > 0 && y > 0
    }
}
