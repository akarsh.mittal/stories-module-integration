package com.touchtalent.bobblesdk.content_core.interfaces.bobble_native_api


/**
 * Interface for interaction with *bobble-native-api* module for gif to webp converting
 */
interface NativeWebpConvertor {
    /**
     * crate webp from gif
     * @param path Gif file path
     * @param quality (0-100) Webb quality wanted for export
     * @param compressionLevel (1-6) of webp for export. 1 indicates fastest export,
     * highest file size. 6 indicates slowest export, lowest file size
     * @return webp file path for success and for failure null
     */
    @Deprecated("Use getWebpFromGif with output path instead")
    fun getWebpFromGif(path: String, quality: Float, compressionLevel: Int): String?

    fun getWebpFromGif(
        path: String,
        outputPath: String,
        quality: Float,
        compressionLevel: Int
    ): Boolean
}