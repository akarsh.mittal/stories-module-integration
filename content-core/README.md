# Module content-core

<b>Content Core</b> module acts an base module for all content related modules

## Responsibilities of this modules:

1. Expose a single abstract
   class [BobbleContent](com.touchtalent.bobblesdk.content_core.interfaces.BobbleContent)
   for all types of content which would be used by the end consumer
2. Expose a single view for the end consumer
   [BobbleContentView](com.touchtalent.bobblesdk.content_core.sdk.BobbleContentView)
   on which all [BobbleContent](com.touchtalent.bobblesdk.content_core.interfaces.BobbleContent)
   can be rendered
3. Expose an abstract
   class [ContentRenderingContext](com.touchtalent.bobblesdk.content_core.interfaces.ContentRenderingContext)
   which emulates a lifecycle for rendering multiple content and includes logic for rendering
   a [BobbleContentView](com.touchtalent.bobblesdk.content_core.sdk.BobbleContentView)
   on [BobbleContentView](com.touchtalent.bobblesdk.content_core.sdk.BobbleContentView)
4. Exposing utility functions for all content module, for e.g:
   1. shared resource access for wigs, accessories & expressions
   1. create head with wigs, accessories, expressions
   2. shared resource for fonts

## Content rendering context

Rendering context is an abstraction of a lifecycle that is responsible for maintaining the necessary
resources required to render a particular type
of [BobbleContent](com.touchtalent.bobblesdk.content_core.interfaces.BobbleContent). A context also
carries the logic required to render a particular type
of [BobbleContent](com.touchtalent.bobblesdk.content_core.interfaces.BobbleContent)
on a [BobbleContentView](com.touchtalent.bobblesdk.content_core.sdk.BobbleContentView).

Every module of a content must be provide a instance
of [ContentRenderingContext](com.touchtalent.bobblesdk.content_core.interfaces.ContentRenderingContext)
which should contain logic to render and export contents supported by the modules.

Multiple RenderingContext's can be combined together to create a
single [CompositeContentRenderingContext](com.touchtalent.bobblesdk.content_core.util.CompositeContentRenderingContext)
which encapsulates functionalities of underlying contexts

### Creating a implementation of ContentRenderingContext

```kotlin
internal class NewContentRenderingContext : ContentRenderingContext {

   private val viewPool = CustomContentViewPool()

   override suspend fun start() {
      // Initialisation code
   }

   override fun play(contentView: BobbleContentView) {
      // Play the content, if animated content is used.
   }

   override fun pause(contentView: BobbleContentView) {
      // Pause the content, if animated content is used.
   }

   override fun onViewRecycled(contentView: BobbleContentView) {
      // Recycle the previously rendered views on contentView.
      viewPool.recycle(contentView)
   }

   override fun dispose() {
      // Recycle all resources.
      viewPool.clear()
   }

   override fun render(
      content: BobbleContent,
      contentMetadata: ContentMetadata?,
      contentView: BobbleContentView
   ) {
      val customContentView = viewPool.inflateView(contentView)
      customContentView.setContent(content, contentMetadata)
   }

   override fun handles(content: BobbleContent): Boolean {
      return content is CustomBobbleContent
   }

   override suspend fun export(
      content: BobbleContent,
      metadata: ContentMetadata?
   ): Result<BobbleContentOutput> {
      // Export the content
   }
}
```

## Guidelines for creating a new content module:

1. Create a interface for the module
   in [interfaces package](com.touchtalent.bobblesdk.content_core.interfaces). This interface should
   be the only contact point for outside world.
2. The module must
   implement [BobbleContentModule](com.touchtalent.bobblesdk.content_core.interfaces.BobbleContentModule)
   instead of [BobbleModule](com.touchtalent.bobblesdk.core.interfaces.BobbleModule) and provide
   logic to create a
   new [ContentRenderingContext](com.touchtalent.bobblesdk.content_core.interfaces.ContentRenderingContext)
2. Implement a subclass
   of [BobbleContent](com.touchtalent.bobblesdk.content_core.interfaces.BobbleContent)
2. Implement a subclass
   of [BobbleContentPack](com.touchtalent.bobblesdk.content_core.interfaces.BobbleContentPack), if
   the module supports content pack
3. Implement a subclass
   of [ContentRenderingContext](com.touchtalent.bobblesdk.content_core.interfaces.ContentRenderingContext)
   to provide rendering logic of the [BobbleContent] handled by that module
3. Provide data sources to fetch content in the format `List<BobbleContent>`. Interface for all data
   source should be added in `interfaces` package. All subclass
   of [BobbleContent](com.touchtalent.bobblesdk.content_core.interfaces.BobbleContent)
   should have visibility modifier as `internal`, since only [BobbleContent] should be exposed to
   the outer world
4. Provide data sources to fetch content packs in the format `List<BobbleContentPack>`, if
   applicable. Interface for all data source should be added in `interfaces` package. All subclass
   of [BobbleContentPack](com.touchtalent.bobblesdk.content_core.interfaces.BobbleContentPack)
   should have visibility modifier as `internal`, since only [BobbleContentPack] should be exposed
   to the outer world

## Usage of content by consumer modules

   ```kotlin

// Creates a CompositeContentRenderingContext aggregating ContentRenderingContext from all 
// BobbleContentModule present in the runtime
val renderingContext = ContentCoreSDK.newContentRenderingInstance()

fun onCreate() {
   renderingContext.start()
}

fun onBind() {
   val content: BobbleContent = contentList[i]
   val contentView: BobbleContentView = binding.contentView

   contentView.contentRenderingContext = renderingContext
   // Create customisation for the content
   val metadata = ContentMetaData(bobbleHead = getBobbleHead(), otf = "hello", locale = "en")
   contentView.setContent(content, metadata)
}

fun onDestroy() {
   // Dispose the animation engine when not required to release resources and avoid memory leaks
   renderingContext.dispose()
}
   ```

## Shared responsibilities of content modules provided by this module

1. [ContentFontManager](com.touchtalent.bobblesdk.content_core.util.ContentFontManager) - Download
   and manage fonts.
2. [ContentResourceDownloader](com.touchtalent.bobblesdk.content_core.util.ContentResourceDownloader)
   - Download and manage raw resources for content, wigs, accessories.
3. [StaticContentFileUtil](com.touchtalent.bobblesdk.content_core.util.StaticContentFileUtil) -
   Process static content for sharing
4. [ViewRecyclerPool](com.touchtalent.bobblesdk.content_core.util.ViewRecyclerPool) - Abstract class
   to recycle views
5. [WatermarkManager](com.touchtalent.bobblesdk.content_core.util.WatermarkManager) - Download and
   manage watermark
6. [BobbleGraphicsUtil](com.touchtalent.bobblesdk.content_core.opengl.BobbleGraphicsUtil) - Utility
   classes to create and manage OpenGL contexts and surfaces

## Dependencies:

1. [bobble-core](com.touchtalent.bobblesdk.core.BobbleCoreSDK)
1. bobble-native-api (loose coupled)

# Package com.touchtalent.bobblesdk.content_core.cache

Utility class and interface to extract a cache key from any object

# Package com.touchtalent.bobblesdk.content_core.head

Utility classes to pre-process any BobbleHead before adding to a content, such as adding accessory,
wig, expressions.

# Package com.touchtalent.bobblesdk.content_core.interfaces

Interfaces for all content modules which is exposed to the outer world

# Package com.touchtalent.bobblesdk.content_core.model

Model classes for shared resources like ContentMetadata, Fonts, HeadAddOns, etc

# Package com.touchtalent.bobblesdk.content_core.opengl

Utility functions to create/manage OpenGL context and surfaces

# Package com.touchtalent.bobblesdk.content_core.sdk

Classes to be exposed

# Package com.touchtalent.bobblesdk.content_core.util

Utility classes such as download resources, content font manager, ViewRecyclerPool, etc

# Package com.touchtalent.bobblesdk.content_core.interfaces.animation_processor

Interfaces for animation-processor module

# Package com.touchtalent.bobblesdk.content_core.interfaces.animated_sticker

Interfaces for animated-sticker module

# Package com.touchtalent.bobblesdk.content_core.interfaces.bobble_native_api

Interfaces for bobble_native_api module

# Package com.touchtalent.bobblesdk.content_core.interfaces.bobble_native_api

Interfaces for bobble_native_api module

# Package com.touchtalent.bobblesdk.content_core.interfaces.content

Interfaces for bobble_native_api module

# Package com.touchtalent.bobblesdk.content_core.model.accessory

Contains API model classes for accessories

# Package com.touchtalent.bobblesdk.content_core.model.expression

Contains API model classes for expressions

# Package com.touchtalent.bobblesdk.content_core.model.wig

Contains API model classes for wigs
