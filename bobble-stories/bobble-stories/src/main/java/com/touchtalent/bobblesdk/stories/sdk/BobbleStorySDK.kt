package com.touchtalent.bobblesdk.stories.sdk

import android.content.Context
import androidx.annotation.Keep
import com.squareup.moshi.Moshi
import com.touchtalent.bobblesdk.content_core.interfaces.BobbleContentAdapter
import com.touchtalent.bobblesdk.content_core.interfaces.BobbleContentModule
import com.touchtalent.bobblesdk.content_core.interfaces.ContentRenderingContext
import com.touchtalent.bobblesdk.content_core.interfaces.stories.StoryContentModule
import com.touchtalent.bobblesdk.content_core.interfaces.stories.StoryManager
import com.touchtalent.bobblesdk.content_core.util.CompositeContentRenderingContext
import com.touchtalent.bobblesdk.content_core.util.EmptyContentAdapter
import com.touchtalent.bobblesdk.core.BobbleCoreSDK
import com.touchtalent.bobblesdk.core.config.BobbleCoreConfig
import com.touchtalent.bobblesdk.core.utils.FileUtil
import com.touchtalent.bobblesdk.stories.cache.DefaultStoriesCacheManager
import com.touchtalent.bobblesdk.stories.cache.ExoPlayerCacheManager
import com.touchtalent.bobblesdk.stories.cache.StoriesCacheManager
import com.touchtalent.bobblesdk.stories.cache.StoriesCacheManagerImpl
import com.touchtalent.bobblesdk.stories.data.datastore.BobbleStoriesDataStore
import com.touchtalent.bobblesdk.stories.domain.StoryManagerImpl
import com.touchtalent.bobblesdk.stories.domain.animated_non_personalised.AnimatedVideoRenderingContext
import com.touchtalent.bobblesdk.stories.domain.image_story.ImageStoryRenderingContext
import com.touchtalent.bobblesdk.stories.domain.musical_image_story.MusicalImageStoryRenderingContext
import com.touchtalent.bobblesdk.stories.singletons.BobbleStoriesConfigHandler
import com.touchtalent.bobblesdk.stories.singletons.StoriesPeriodicUpdater
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.json.JSONObject

@Keep
object BobbleStorySDK : BobbleContentModule(), StoryContentModule {

    lateinit var appContext: Context
    lateinit var moshi: Moshi

    override fun newContentRenderingInstance(): ContentRenderingContext {
        return CompositeContentRenderingContext(
            listOf(
                AnimatedVideoRenderingContext(),
                MusicalImageStoryRenderingContext(),
                ImageStoryRenderingContext()
            )
        )
    }

    override fun initialise(applicationContext: Context, config: BobbleCoreConfig) {
        appContext = applicationContext
        moshi = BobbleCoreSDK.moshi
        registerPeriodicUpdater(StoriesPeriodicUpdater)
    }

    override fun getCodeName() = "bobble-stories"

    fun getFileProviderAuthority() =
        "${appContext.packageName}.bobble.stories.fileprovider"

    fun getStoryManager(): StoryManager {
        return StoryManagerImpl
    }

    fun getCacheManager(): StoriesCacheManager = StoriesCacheManagerImpl(
        listOf(
            DefaultStoriesCacheManager, ExoPlayerCacheManager
        )
    )

    suspend fun getAdDisplayInterval(): Int{
        return BobbleStoriesDataStore.getAdDisplayInterval()
    }

    override fun handleUserConfig(response: JSONObject) {
        BobbleCoreSDK.applicationScope.launch {
            BobbleStoriesConfigHandler.handle(response)
        }
    }

    override fun cleanCache() {
        BobbleCoreSDK.applicationScope.launch {
            withContext(Dispatchers.IO) {
                val sharingFolder = FileUtil.join(cacheDir, "sharing")
                FileUtil.delete(sharingFolder) { pathname ->
                    System.currentTimeMillis() - pathname.lastModified() >= 10 * 60 * 1000L // 10 minutes
                }
                if (System.currentTimeMillis() - BobbleStoriesDataStore.getLastViewedTime() > 7 * 24 * 3600 * 1000L) // 7 days
                    getCacheManager().deleteCache()
                else
                    getCacheManager().trimCacheSize()
            }
        }
    }

    override fun getBobbleContentAdapter(): BobbleContentAdapter {
        return EmptyContentAdapter()
    }

}
