package com.touchtalent.bobblesdk.stories.data.model.api

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class FixedWidthResource(
    val height: Int,
    val width: Int,
    val gif: FixedWidthUrl? = null,
    val png: FixedWidthUrl? = null,
    val jpg: FixedWidthUrl? = null,
    val mp4: FixedWidthUrl? = null
) {
    fun getImage(): String? {
        return png?.url ?: jpg?.url
    }
}