package com.touchtalent.bobblesdk.stories.data.model.api

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class WatermarkDetails(
    val url: String,
    val position: WatermarkPosition
)