package com.touchtalent.bobblesdk.stories.data.model.api

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ApiStory(
    val id: Int,
    val contentType: String,
    @Json(name = "actors")
    val headDetails: List<ApiHeadDetails> = emptyList(),
    val textBubbles: List<ApiTextBubble> = emptyList(),
    val watermarkDetails: WatermarkDetails? = null,
    val rawResourcesURL: String? = null,
    val displayInterval: Int?,
    val enableWatermark: Boolean = true,
    val isHeadSupported: Boolean = false,
    val fixedWidthFull: FixedWidthResource? = null,
    val fixedWidthMedium: FixedWidthResource? = null,
    val fixedWidthSmall: FixedWidthResource? = null,
    val numShares: Int? = null,
    val orientation: String? = null,
    val shareText: String? = null,
    val title: String,
    val videoDetails: VideoDetails? = null,
    val forceEnableStoryOfTheDayNotification: Boolean = false,
    val ctaDetails: CtaDetails?
)