package com.touchtalent.bobblesdk.stories.cache

import com.touchtalent.bobblesdk.core.utils.ResourceDownloader

object DefaultStoriesCacheManager: StoriesCacheManager {
    const val CACHE_KEY = "story_static_resource"

    override val activeStateMaxSize: Long
        get() = Long.MAX_VALUE
    override val idealStateMaxSize: Long
        get() = 2 * 1024 * 1024 // 2MB ideal state cache size

    override suspend fun trimCacheSize() {
        ResourceDownloader.restrictCacheDir(CACHE_KEY, idealStateMaxSize)
    }

    override suspend fun increaseCacheSize() { }

    override suspend fun deleteCache() {
        ResourceDownloader.restrictCacheDir(CACHE_KEY, 1)
    }
}