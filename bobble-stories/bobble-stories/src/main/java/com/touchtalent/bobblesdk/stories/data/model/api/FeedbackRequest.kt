package com.touchtalent.bobblesdk.stories.data.model.api

data class FeedbackRequest(
    val message: String,
    val type: String
)