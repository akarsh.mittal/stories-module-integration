package com.touchtalent.bobblesdk.stories.domain.image_story

import android.graphics.Bitmap

data class ImageStoryOutput(val bitmap: Bitmap){

}