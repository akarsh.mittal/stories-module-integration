package com.touchtalent.bobblesdk.stories.data.model.api

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class VideoDetails(
    val duration: Int?,
    val audioSnippetURL: String
)