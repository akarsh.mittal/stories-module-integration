package com.touchtalent.bobblesdk.stories.cache

interface StoriesCacheManager {

    val activeStateMaxSize: Long
    val idealStateMaxSize: Long

    /**
     * reduce the cache size to idealStateMaxSize
     */
    suspend fun trimCacheSize()

    /**
     * increase the cache size to activeStateMaxSize
     */
    suspend fun increaseCacheSize()

    /**
     * delete the cache files
     */
    suspend fun deleteCache() // delete the cache files
}