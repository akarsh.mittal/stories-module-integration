package com.touchtalent.bobblesdk.stories.domain.network

import com.touchtalent.bobblesdk.core.BobbleCoreSDK
import okhttp3.Interceptor
import okhttp3.Response

class CommonQueryInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        val apiParams = BobbleCoreSDK.crossAppInterface.getApiParamsBuilder()
            .withDeviceId()
            .withClientId()
            .withVersion()
            .withLocale()
            .withTimeZone()
            .withUtmCampaign()
            .withLocationV2()
            .build()

        val url = request.url().newBuilder().also {
            apiParams.keys.forEach { key ->
                it.addQueryParameter(key, apiParams[key])
            }
        }.build()

        request = request.newBuilder().url(url).build()
        return chain.proceed(request)
    }
}