package com.touchtalent.bobblesdk.stories.domain.image_story

import android.graphics.Bitmap
import androidx.core.content.FileProvider
import androidx.core.view.forEach
import com.touchtalent.bobblesdk.content_core.interfaces.BobbleContent
import com.touchtalent.bobblesdk.content_core.interfaces.ContentRenderingContext
import com.touchtalent.bobblesdk.content_core.model.ContentMetadata
import com.touchtalent.bobblesdk.content_core.sdk.BobbleContentOutput
import com.touchtalent.bobblesdk.content_core.sdk.BobbleContentView
import com.touchtalent.bobblesdk.core.utils.FileUtil
import com.touchtalent.bobblesdk.core.utils.renderBitmap
import com.touchtalent.bobblesdk.core.utils.renderBitmapFit
import com.touchtalent.bobblesdk.core.utils.renderDrawable
import com.touchtalent.bobblesdk.core.views.ImpressionImageView
import com.touchtalent.bobblesdk.stories.data.pojo.ImageStory
import com.touchtalent.bobblesdk.stories.domain.StoriesCircularProgressDrawable
import com.touchtalent.bobblesdk.stories.sdk.BobbleStorySDK
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.File
import java.io.FileOutputStream

class ImageStoryRenderingContext : ContentRenderingContext() {

    private val imagePool = ImageViewPool(4)
    private val bitmapPool = BitmapPool(9)
    private var pause = false

    override suspend fun start() {
    }

    override fun render(
        content: BobbleContent,
        contentMetadata: ContentMetadata?,
        contentView: BobbleContentView
    ) {
        val imageStory = content as? ImageStory
            ?: throw Exception("Unsupported content format")
        contentView.recycleAbleScope.launch {
            kotlin.runCatching {
                val imageStoryEngine = ImageStoryEngine(imageStory)
                val imageView = imagePool.inflateView(contentView)

                // show progress while loading
                if (contentMetadata?.showLoader == true)
                    imageView.renderDrawable(StoriesCircularProgressDrawable)
                pause = true

                if (!bitmapPool.containsKey(imageStory.id)) {
                    val storyResult = imageStoryEngine.createStory(contentMetadata)
                    bitmapPool[imageStory.id] = storyResult.bitmap
                }
                bitmapPool[imageStory.id]?.let {
                    if (contentMetadata?.fitXY ?: false) {
                        imageView.renderBitmapFit(it)
                    } else {
                        imageView.renderBitmap(it)

                    }
                }
                pause = false
            }
        }
    }

    override fun renderPlayable(
        content: BobbleContent,
        contentMetadata: ContentMetadata?,
        contentView: BobbleContentView,
    ): Flow<Int> {
        val imageStory = content as? ImageStory
            ?: throw Exception("Unsupported content format")
        render(imageStory, contentMetadata, contentView)

        return progressFlow(imageStory.displayInterval * 1000L)
    }

    private fun progressFlow(displayInterval: Long) = flow {
        var currentMillis = 0L
        val factor = 100F / displayInterval
        while (currentMillis != displayInterval) {
            emit((currentMillis * factor).toInt())
            if (!pause)
                currentMillis += 50
            delay(50)
        }
        emit(100)
    }.distinctUntilChanged().cancellable().flowOn(Dispatchers.Main)


    override fun canRender(content: BobbleContent): Boolean {
        return content is ImageStory
    }

    override suspend fun export(
        content: BobbleContent,
        metadata: ContentMetadata?
    ): Result<BobbleContentOutput> = withContext(Dispatchers.IO) {
        kotlin.runCatching {
            val imageStory = content as? ImageStory ?: throw Exception("Unsupported content format")
            val imageStoryEngine = ImageStoryEngine(imageStory)
            val storyResult = imageStoryEngine.createStory(metadata)
            val destPath = FileUtil.createDirAndGetPath(BobbleStorySDK.cacheDir, "sharing")
            val mimeType: String
            val extension: String
            val compressionFormat: Bitmap.CompressFormat
            val mimeTypes = metadata?.supportedMimeTypes
            if (mimeTypes == null || mimeTypes.contains("image/png")) {
                mimeType = "image/png"
                extension = "png"
                compressionFormat = Bitmap.CompressFormat.PNG
            } else if (mimeTypes.contains("image/webp")) {
                mimeType = "image/webp"
                extension = "webp"
                compressionFormat = Bitmap.CompressFormat.WEBP
            } else throw Exception("Unsupported mime types")
            val destFile = FileUtil.join(destPath, "${imageStory.normalisedTitle}.$extension")
            FileOutputStream(destFile).use {
                storyResult.bitmap.compress(compressionFormat, 100, it)
            }
            val shareableUri = FileProvider.getUriForFile(
                BobbleStorySDK.appContext,
                BobbleStorySDK.getFileProviderAuthority(),
                File(destFile)
            )
            BobbleContentOutput(
                sourceContent = content,
                finalContentMetadata = metadata,
                localPath = destFile,
                shareUri = shareableUri,
                mimeType = mimeType
            )
        }
    }

    override fun play(contentView: BobbleContentView) {
        pause = false
    }

    override suspend fun preCacheContent(content: BobbleContent) {
        val imageStoryEngine = ImageStoryEngine(content as ImageStory)
        kotlin.runCatching {
            imageStoryEngine.getRawResourceFolder()
        }
    }

    override fun pause(contentView: BobbleContentView) {
        pause = true
    }

    override fun onViewRecycled(contentView: BobbleContentView) {
        contentView.forEach {
            if (it is ImpressionImageView)
                imagePool.onRecycled(it)
        }
    }

    override fun dispose() {
        super.dispose()
        bitmapPool.forEach { it.value.recycle() }
        imagePool.clear()
    }
}