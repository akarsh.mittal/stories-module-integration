package com.touchtalent.bobblesdk.stories.domain.musical_image_story

import android.graphics.Bitmap
import android.media.MediaPlayer
import androidx.core.content.FileProvider
import androidx.core.view.forEach
import com.touchtalent.bobblesdk.content_core.interfaces.BobbleContent
import com.touchtalent.bobblesdk.content_core.interfaces.ContentRenderingContext
import com.touchtalent.bobblesdk.content_core.model.ContentMetadata
import com.touchtalent.bobblesdk.content_core.sdk.BobbleContentOutput
import com.touchtalent.bobblesdk.content_core.sdk.BobbleContentView
import com.touchtalent.bobblesdk.core.utils.FileUtil
import com.touchtalent.bobblesdk.core.utils.renderBitmap
import com.touchtalent.bobblesdk.core.utils.renderBitmapFit
import com.touchtalent.bobblesdk.core.utils.renderDrawable
import com.touchtalent.bobblesdk.core.views.ImpressionImageView
import com.touchtalent.bobblesdk.stories.data.pojo.MusicalImageStory
import com.touchtalent.bobblesdk.stories.domain.StoriesCircularProgressDrawable
import com.touchtalent.bobblesdk.stories.domain.image_story.BitmapPool
import com.touchtalent.bobblesdk.stories.domain.image_story.ImageViewPool
import com.touchtalent.bobblesdk.stories.sdk.BobbleStorySDK
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.cancellable
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.File

class MusicalImageStoryRenderingContext : ContentRenderingContext() {
    private val imagePool = ImageViewPool(1)
    private val bitmapPool = BitmapPool(9)
    private val mediaPlayer = MediaPlayer()

    override suspend fun start() {

    }

    override fun render(
        content: BobbleContent,
        contentMetadata: ContentMetadata?,
        contentView: BobbleContentView
    ) {
        val musicStory =
            content as? MusicalImageStory ?: throw Exception("Unsupported content format")
        contentView.recycleAbleScope.launch {
            kotlin.runCatching {
                val imageStoryEngine = MusicalStoryEngine(musicStory)
                val imageView = imagePool.inflateView(contentView)
                if (!bitmapPool.containsKey(musicStory.id)) {
                    val storyResult = imageStoryEngine.createStory(contentMetadata)
                    bitmapPool[musicStory.id] = storyResult.bitmap
                }
                bitmapPool[musicStory.id]?.let {
                    if(contentMetadata?.fitXY?:false){
                        imageView.renderBitmapFit(it)
                    }else{
                        imageView.renderBitmap(it)
                    }
                }
            }
        }
    }

    override fun renderPlayable(
        content: BobbleContent,
        contentMetadata: ContentMetadata?,
        contentView: BobbleContentView,
    ): Flow<Int> {
        val musicStory =
            content as? MusicalImageStory ?: throw Exception("Unsupported content format")
        return flow {
            kotlin.runCatching {
                val imageStoryEngine = MusicalStoryEngine(musicStory)
                val imageView = imagePool.inflateView(contentView)

                // show progress while loading
                if (contentMetadata?.showLoader == true)
                    imageView.renderDrawable(StoriesCircularProgressDrawable)

                val storyResult = imageStoryEngine.createStory(contentMetadata)
                val audioPath = imageStoryEngine.getAudioPath()
                val duration = musicStory.videoDuration ?: 0

                val progressFlow =
                    setResources(
                        imageView,
                        audioPath,
                        storyResult.bitmap,
                        duration,
                        mute = contentMetadata?.mute ?: false
                    )
                progressFlow.collect { emit(it) }
            }
        }
    }

    fun setResources(
        imageView: ImpressionImageView,
        audioUrl: String?,
        bitmap: Bitmap,
        duration: Int,
        playWhenReady: Boolean = true,
        mute: Boolean = false
    ): Flow<Int> {
        with(mediaPlayer) {
            reset()
            setDataSource(audioUrl)
            prepareAsync()
            isLooping = true
            if (mute)
                setVolume(0F, 0F)
            else
                setVolume(1F, 1F)
            setOnPreparedListener {
                imageView.renderBitmap(bitmap)
                if (playWhenReady)
                    it.start()
                else
                    it.pause()
            }
        }
        return progressFlow(duration * 1000L)
    }

    private fun progressFlow(displayInterval: Long) = flow {
        var currentMillis = 0L
        val factor = 100F / displayInterval
        while (currentMillis != displayInterval) {
            emit((currentMillis * factor).toInt())
            if (mediaPlayer.isPlaying)
                currentMillis += 50
            delay(50)
        }
        emit(100)
    }.distinctUntilChanged().cancellable()


    override fun canRender(content: BobbleContent): Boolean {
        return content is MusicalImageStory
    }

    override suspend fun preCacheContent(content: BobbleContent) {
        val engine = MusicalStoryEngine(content as MusicalImageStory)
        kotlin.runCatching {
            engine.getRawResourceFolder()
        }
    }

    override suspend fun export(
        content: BobbleContent,
        metadata: ContentMetadata?
    ): Result<BobbleContentOutput> = withContext(Dispatchers.IO) {
        kotlin.runCatching {
            val musicExporter = MusicalImageExporter(content as MusicalImageStory)
            val destFolder = withContext(Dispatchers.IO) {
                FileUtil.createDirAndGetPath(BobbleStorySDK.cacheDir, "sharing")
            }
            val outputFile = musicExporter.export(metadata, destFolder)
            val sharingFile = FileProvider.getUriForFile(
                BobbleStorySDK.appContext,
                BobbleStorySDK.getFileProviderAuthority(),
                File(outputFile)
            )
            BobbleContentOutput(
                sourceContent = content,
                finalContentMetadata = metadata,
                shareUri = sharingFile,
                mimeType = "video/mp4",
                localPath = outputFile
            )
        }
    }

    override fun play(contentView: BobbleContentView) {
        if (!mediaPlayer.isPlaying)
            mediaPlayer.start()
    }

    override fun pause(contentView: BobbleContentView) {
        if (mediaPlayer.isPlaying)
            mediaPlayer.pause()
    }

    override fun onViewRecycled(contentView: BobbleContentView) {
        contentView.forEach {
            if (it is ImpressionImageView) {
                imagePool.onRecycled(it)
                mediaPlayer.stop()
            }
        }
    }

    override fun dispose() {
        super.dispose()
        mediaPlayer.release()
        bitmapPool.forEach { it.value.recycle() }
        imagePool.clear()
    }
}