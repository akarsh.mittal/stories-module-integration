package com.touchtalent.bobblesdk.stories.data.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface SearchSuggestionsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertKeyword(searchSuggestion: SearchSuggestions)

    @Query("SELECT * FROM SearchSuggestions WHERE keyword LIKE :charSeq GROUP BY keyword")
    suspend fun getKeywords(charSeq: String): List<SearchSuggestions>

    @Query("SELECT keyword, uid FROM SearchSuggestions GROUP BY keyword ORDER by uid desc")
    suspend fun getTopKeywords(): List<SearchSuggestions>
}