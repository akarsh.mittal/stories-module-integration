package com.touchtalent.bobblesdk.stories.data.datastore

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.*
import androidx.datastore.preferences.preferencesDataStore
import com.touchtalent.bobblesdk.stories.sdk.BobbleStorySDK
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map

object BobbleStoriesDataStore {
    private val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = "story_config")

    private val STORY_NATIVE_AD_INDEX = intPreferencesKey("story_native_ad_index")
    private val STORY_NATIVE_AD_DISPLAY_INTERVAL =
        intPreferencesKey("story_native_ad_display_interval")
    private val STORY_VIEWED_ID_SET = stringSetPreferencesKey("story_viewed_id_set")
    private val STORY_LAST_VIEWED_TIME = longPreferencesKey("story_last_viewed_time")
    private val STORY_API_INTERVAL = longPreferencesKey("story_api_interval")
    private val IS_SWIPE_ANIMATION_SHOWN = intPreferencesKey("is_swipe_animation_shown")


    suspend fun getAdDisplayIndex(): Int {
        return BobbleStorySDK.appContext.dataStore.data
            .map { it[STORY_NATIVE_AD_INDEX] ?: 4 }
            .catch { emit(4) }
            .first()
    }


    suspend fun setAdDisplayIndex(index: Int) {
        BobbleStorySDK.appContext.dataStore.edit {
            it[STORY_NATIVE_AD_INDEX] = index
        }
    }

    suspend fun getAdDisplayInterval(): Int {
        return BobbleStorySDK.appContext.dataStore.data
            .map { it[STORY_NATIVE_AD_DISPLAY_INTERVAL] ?: 10 }
            .catch { emit(10) }
            .first()
    }


    suspend fun setAdDisplayInterval(timeInSec: Int) {
        BobbleStorySDK.appContext.dataStore.edit {
            it[STORY_NATIVE_AD_DISPLAY_INTERVAL] = timeInSec
        }
    }

    private suspend fun getViewedStoryIds(): Set<String>{
        return BobbleStorySDK.appContext.dataStore.data
            .map { it[STORY_VIEWED_ID_SET] ?: emptySet() }
            .catch { emptySet<String>() }
            .first()
    }

    suspend fun addStoryViewed(id: Int) {
        val set = getViewedStoryIds().toMutableSet()
        if (!set.contains(id.toString())) {
            set.add(id.toString())
            BobbleStorySDK.appContext.dataStore.edit {
                it[STORY_VIEWED_ID_SET] = set
            }
        }
    }

    suspend fun isStoryViewed(id: Int): Boolean{
        return getViewedStoryIds().contains(id.toString())
    }

    suspend fun updateStoryViewedSet(serverIds: List<Int>) {
        val set = getViewedStoryIds().filter { serverIds.contains(it.toInt()) }.toSet()
        BobbleStorySDK.appContext.dataStore.edit {
            it[STORY_VIEWED_ID_SET] = set
        }
    }

    suspend fun getLastViewedTime(): Long {
        return BobbleStorySDK.appContext.dataStore.data
            .map { it[STORY_LAST_VIEWED_TIME] ?: 0 }
            .catch { emit(0) }
            .first()
    }

    suspend fun setLastViewedTime(timeInMs: Long) {
        BobbleStorySDK.appContext.dataStore.edit {
            it[STORY_LAST_VIEWED_TIME] = timeInMs
        }
    }

    suspend fun getStoryApiInterval(): Long {
        return BobbleStorySDK.appContext.dataStore.data
            .map { it[STORY_API_INTERVAL] ?: 21600 }
            .catch { emit(21600) }
            .first()
    }

    suspend fun setStoryApiInterval(timeInSec: Long) {
        BobbleStorySDK.appContext.dataStore.edit {
            it[STORY_API_INTERVAL] = timeInSec
        }
    }

    suspend fun setSwipeAnimationCount(count: Int) {
        BobbleStorySDK.appContext.dataStore.edit {
            it[IS_SWIPE_ANIMATION_SHOWN] = count
        }
    }

    suspend fun getSwipeAnimationCount(): Int {
        return BobbleStorySDK.appContext.dataStore.data
            .map { it[IS_SWIPE_ANIMATION_SHOWN] ?: -1 }
            .catch { emit(3) }
            .first()
    }

}