package com.touchtalent.bobblesdk.stories.domain.musical_image_story

import com.touchtalent.bobblesdk.core.utils.FileUtil
import com.touchtalent.bobblesdk.core.utils.ResourceDownloader
import com.touchtalent.bobblesdk.stories.cache.DefaultStoriesCacheManager
import com.touchtalent.bobblesdk.stories.data.pojo.MusicalImageStory
import com.touchtalent.bobblesdk.stories.domain.image_story.ImageStoryEngine
import com.touchtalent.bobblesdk.stories.sdk.BobbleStorySDK

class MusicalStoryEngine(private val imageStory: MusicalImageStory) : ImageStoryEngine(imageStory) {

    suspend fun getAudioPath(): String? {
        val audioUrl = imageStory.audioUrl ?: return null
        val destDir = FileUtil.createDirAndGetPath(BobbleStorySDK.cacheDir, "resources", "audio")

        val result = ResourceDownloader.downloadResourcesSuspend(
            url = audioUrl,
            destDir = destDir,
            cacheKey = DefaultStoriesCacheManager.CACHE_KEY
        )
        return result.first
    }

}