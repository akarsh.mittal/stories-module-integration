package com.touchtalent.bobblesdk.stories.data.model.api

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class WatermarkPosition(
    val height: Float,
    val width: Float,
    val x: Float,
    val y: Float
)