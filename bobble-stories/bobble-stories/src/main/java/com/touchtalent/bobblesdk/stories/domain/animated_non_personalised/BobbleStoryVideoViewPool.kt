package com.touchtalent.bobblesdk.stories.domain.animated_non_personalised

import android.view.LayoutInflater
import android.view.ViewGroup
import com.touchtalent.bobblesdk.content_core.util.ViewRecyclerPool
import com.touchtalent.bobblesdk.stories.databinding.BobbleStoryViewBinding

class BobbleStoryVideoViewPool(poolSize: Int) : ViewRecyclerPool<BobbleStoryVideoView>(poolSize) {

    override fun canRecycle(view: BobbleStoryVideoView): Boolean {
        return true
    }

    override fun onRecycled(view: BobbleStoryVideoView) {
        view.onViewRecycled()
    }

    override fun onCreateView(parent: ViewGroup): BobbleStoryVideoView {
        return BobbleStoryViewBinding.inflate(LayoutInflater.from(parent.context), null, false).root
    }
}
