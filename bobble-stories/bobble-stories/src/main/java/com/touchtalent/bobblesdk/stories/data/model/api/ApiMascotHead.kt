package com.touchtalent.bobblesdk.stories.data.model.api

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ApiMascotHead(
    val id: Long,
    val categoryId: Int,
    val name: String?,
    val chinX: Float,
    val chinY: Float,
    val fixedWidthFull: FixedWidthResource?,
    val fixedWidthMedium: FixedWidthResource?,
    val fixedWidthSmall: FixedWidthResource?,
    val gender: String = "unisex"
) {
    fun getHeadUrl(): String? {
        return fixedWidthSmall?.getImage() ?: fixedWidthMedium?.getImage()
        ?: fixedWidthFull?.getImage()
    }

    fun getHeight(): Int? {
        return fixedWidthSmall?.height ?: fixedWidthMedium?.height ?: fixedWidthFull?.height
    }

    fun getWidth(): Int? {
        return fixedWidthSmall?.width ?: fixedWidthMedium?.width ?: fixedWidthFull?.width
    }
}