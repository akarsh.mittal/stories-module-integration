package com.touchtalent.bobblesdk.stories.domain

import android.graphics.Color
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.touchtalent.bobblesdk.stories.sdk.BobbleStorySDK

object StoriesCircularProgressDrawable : CircularProgressDrawable(BobbleStorySDK.appContext) {
    init {
        strokeWidth = 7f
        centerRadius = 32f
        setColorSchemeColors(Color.CYAN)
        start()
    }
}