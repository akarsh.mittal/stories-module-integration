package com.touchtalent.bobblesdk.stories.cache

class StoriesCacheManagerImpl(private val cacheManagers: List<StoriesCacheManager>) : StoriesCacheManager {

    override val activeStateMaxSize: Long
        get() = 0
    override val idealStateMaxSize: Long
        get() = 0

    override suspend fun trimCacheSize() {
        cacheManagers.forEach {
            it.trimCacheSize()
        }
    }

    override suspend fun increaseCacheSize() {
        cacheManagers.forEach {
            it.increaseCacheSize()
        }
    }

    override suspend fun deleteCache() {
        cacheManagers.forEach {
            it.increaseCacheSize()
        }
    }
}