package com.touchtalent.bobblesdk.stories.data.model.api

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class StoryTrendingSearches(
    val trends: List<Trends>?,
    val suggestions: List<Trends>?
)

@JsonClass(generateAdapter = true)
data class Trends(
    val type: Int?,
    val keyword: String?
)