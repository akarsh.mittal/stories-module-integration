package com.touchtalent.bobblesdk.stories.cache

import android.net.Uri
import com.google.android.exoplayer2.database.DatabaseProvider
import com.google.android.exoplayer2.database.StandaloneDatabaseProvider
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.upstream.DataSpec
import com.google.android.exoplayer2.upstream.DefaultHttpDataSource
import com.google.android.exoplayer2.upstream.cache.*
import com.touchtalent.bobblesdk.stories.domain.exoplayer.LRUCacheEvictor
import com.touchtalent.bobblesdk.stories.sdk.BobbleStorySDK
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.File

object ExoPlayerCacheManager : StoriesCacheManager {

    override val activeStateMaxSize: Long
        get() = 50 * 1024 * 1024 // 50 MB max cache
    override val idealStateMaxSize: Long
        get() = 10 * 1024 * 1024 // 10 MB ideal state cache

    private val leastRecentlyUsedCacheEvictor: LRUCacheEvictor =
        LRUCacheEvictor(idealStateMaxSize)

    private var standaloneDatabaseProvider: DatabaseProvider =
        StandaloneDatabaseProvider(BobbleStorySDK.appContext)

    val cache: Cache = SimpleCache(
        File(BobbleStorySDK.cacheDir, "exoplayer"),
        leastRecentlyUsedCacheEvictor,
        standaloneDatabaseProvider
    )

    val httpDataSourceFactory = DefaultHttpDataSource.Factory()
        .setUserAgent(System.getProperty("http.agent"))

    private val cacheDataSourceFactory = CacheDataSource.Factory()
        .setCache(cache)
        .setUpstreamDataSourceFactory(httpDataSourceFactory)
        .setFlags(CacheDataSource.FLAG_IGNORE_CACHE_ON_ERROR)

    val mediaSourceFactory = ProgressiveMediaSource.Factory(
        cacheDataSourceFactory
    )

    suspend fun preCacheVideo(videoUrl: String): Unit = withContext(Dispatchers.IO) {
        runCatching {
            val videoUri = Uri.parse(videoUrl)
            val dataSpec = DataSpec(videoUri, 0, 100 * 1024L)
            CacheWriter(
                cacheDataSourceFactory.createDataSourceForDownloading(),
                dataSpec,
                null,
                null
            ).cache()
        }.onFailure {
            it.printStackTrace()
        }
    }

    override suspend fun trimCacheSize() {
        leastRecentlyUsedCacheEvictor.changeCacheSize(idealStateMaxSize)
        leastRecentlyUsedCacheEvictor.evictCache(cache,0)
    }

    override suspend fun increaseCacheSize() {
        leastRecentlyUsedCacheEvictor.changeCacheSize(activeStateMaxSize)
    }

    override suspend fun deleteCache() {
        leastRecentlyUsedCacheEvictor.changeCacheSize(0)
        leastRecentlyUsedCacheEvictor.evictCache(cache,0)
    }

}
