package com.touchtalent.bobblesdk.stories.data.pojo

import com.touchtalent.bobblesdk.content_core.model.accessory.Accessory
import com.touchtalent.bobblesdk.content_core.model.expression.Expression
import com.touchtalent.bobblesdk.content_core.model.wig.Wig
import com.touchtalent.bobblesdk.core.utils.ThemeUtil
import com.touchtalent.bobblesdk.stories.data.model.api.ApiMascotHead
import com.touchtalent.bobblesdk.stories.data.model.api.ApiStory
import com.touchtalent.bobblesdk.stories.data.model.api.FixedWidthResource
import com.touchtalent.bobblesdk.stories.sdk.BobbleStorySDK

class MusicalImageStory(
    apiStory: ApiStory,
    accessories: List<Accessory>?,
    wigs: List<Wig>?,
    expressions: List<Expression>?,
    mascotHead: List<ApiMascotHead>?
) : ImageStory(apiStory, accessories, wigs, expressions, mascotHead, Type.VIDEO) {

    override val shareText: String?
        get() = apiStory.shareText

    override val noOfShares: Int?
        get() = apiStory.numShares

    override val normalisedTitle: String
        get() = apiStory.title.replace("\\W+", "")

    override val forceEnableStoryNotification: Boolean
        get() = apiStory.forceEnableStoryOfTheDayNotification

    override val aspectRatio: String
        get() =  "${fixedWidth?.width ?: 9}:${fixedWidth?.height ?: 16}"

    val videoDuration = apiStory.videoDetails?.duration
    val audioUrl: String? = apiStory.videoDetails?.audioSnippetURL

    /**
     * Resolves the preview URL of appropriate size - [FixedWidthResource], based on device DPI
     */
    private val fixedWidth: FixedWidthResource?
        get() = with(apiStory) {
            var fixedWidth = when (ThemeUtil.getScreenResolution(BobbleStorySDK.appContext)) {
                "xxxhdpi", "xxhdpi" -> fixedWidthFull
                "xhdpi" -> fixedWidthMedium
                else -> fixedWidthSmall
            }
            if (fixedWidth == null) {
                fixedWidth = fixedWidthSmall
                if (fixedWidth == null) {
                    fixedWidth = fixedWidthMedium
                    if (fixedWidth == null) fixedWidth = fixedWidthFull
                }
            }
            return fixedWidth
        }
}