package com.touchtalent.bobblesdk.stories.singletons

import com.touchtalent.bobblesdk.core.interfaces.PeriodicUpdater
import com.touchtalent.bobblesdk.stories.data.datastore.BobbleStoriesDataStore
import com.touchtalent.bobblesdk.stories.domain.StoryManagerImpl

object StoriesPeriodicUpdater : PeriodicUpdater() {
    override fun getId() = "bobble_stories"

    override suspend fun getTimeIntervalInMillis(): Long =
        BobbleStoriesDataStore.getStoryApiInterval() * 1000

    override suspend fun onUpdate() {
        // pre cache stories api response
        val response = StoryManagerImpl.getStories()
        if (response.isSuccess)
            registerSuccess()
        else
            registerFailure()
    }
}