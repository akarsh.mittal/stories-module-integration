package com.touchtalent.bobblesdk.stories.data.model.api

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CustomTextPosition(
    val originalHeight: Int,
    val originalWidth: Int,
    val angle: Int = 0,
    val width: Int,
    val x: Int,
    val y: Int
)