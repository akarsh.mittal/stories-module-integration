package com.touchtalent.bobblesdk.stories.singletons

import com.touchtalent.bobblesdk.core.utils.handleAsInt
import com.touchtalent.bobblesdk.core.utils.handleAsJSONObject
import com.touchtalent.bobblesdk.core.utils.handleAsLong
import com.touchtalent.bobblesdk.stories.data.datastore.BobbleStoriesDataStore
import org.json.JSONObject

object BobbleStoriesConfigHandler {
    suspend fun handle(response: JSONObject) = with(response) {
        handleAsJSONObject("inContentNativeRecommendationSettings") { jsonObject ->
            jsonObject.handleAsInt("featuredStoriesInterstitialRecommendationIndex") {
                BobbleStoriesDataStore.setAdDisplayIndex(it)
            }
            jsonObject.handleAsInt("featuredStoriesNativeAdDisplayInterval") {
                BobbleStoriesDataStore.setAdDisplayInterval(it)
            }
        }
        handleAsLong("featuredStoriesAPIInterval"){
            BobbleStoriesDataStore.setStoryApiInterval(it)
        }
    }
}