package com.touchtalent.bobblesdk.stories.data.model.api

import com.touchtalent.bobblesdk.content_core.interfaces.stories.BobbleStory

abstract class BobbleStoryImpl(id: Int, type: Type) : BobbleStory(id, type) {
    open val ctaDetails: CtaDetails? = null
}