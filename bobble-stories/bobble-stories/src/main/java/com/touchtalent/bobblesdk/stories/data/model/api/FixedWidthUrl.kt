package com.touchtalent.bobblesdk.stories.data.model.api

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class FixedWidthUrl(val url: String)