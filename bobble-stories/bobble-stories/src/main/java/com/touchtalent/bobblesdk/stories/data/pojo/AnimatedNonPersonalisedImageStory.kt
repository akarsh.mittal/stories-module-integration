package com.touchtalent.bobblesdk.stories.data.pojo

import com.touchtalent.bobblesdk.core.utils.ThemeUtil
import com.touchtalent.bobblesdk.stories.data.model.api.ApiStory
import com.touchtalent.bobblesdk.stories.data.model.api.BobbleStoryImpl
import com.touchtalent.bobblesdk.stories.data.model.api.CtaDetails
import com.touchtalent.bobblesdk.stories.data.model.api.FixedWidthResource
import com.touchtalent.bobblesdk.stories.sdk.BobbleStorySDK

open class AnimatedNonPersonalisedImageStory(
    private val apiStory: ApiStory,
) : BobbleStoryImpl(apiStory.id, Type.VIDEO) {

    override val isOtfSupported: Boolean
        get() = false
    override val isHeadSupported: Boolean
        get() = false

    override val shareText: String?
        get() = apiStory.shareText

    override val noOfShares: Int?
        get() = apiStory.numShares

    override val normalisedTitle: String
        get() = apiStory.title.replace("\\W+", "")

    override val aspectRatio: String
        get() = "${fixedWidth?.width ?: 9}:${fixedWidth?.height ?: 16}"

    override val forceEnableStoryNotification: Boolean
        get() = apiStory.forceEnableStoryOfTheDayNotification

    /**
     * Resolves the preview URL of appropriate size - [FixedWidthResource], based on device DPI
     */
    private val fixedWidth: FixedWidthResource?
        get() = with(apiStory) {
            var fixedWidth = when (ThemeUtil.getScreenResolution(BobbleStorySDK.appContext)) {
                "xxxhdpi", "xxhdpi" -> fixedWidthFull
                "xhdpi" -> fixedWidthMedium
                else -> fixedWidthSmall
            }
            if (fixedWidth == null) {
                fixedWidth = fixedWidthSmall
                if (fixedWidth == null) {
                    fixedWidth = fixedWidthMedium
                    if (fixedWidth == null) fixedWidth = fixedWidthFull
                }
            }
            return fixedWidth
        }

    override fun getPreviewUrl(): String? =
        fixedWidth?.png?.url ?: fixedWidth?.jpg?.url ?: fixedWidth?.gif?.url

    val videoAssetUrl: String?
        get() = fixedWidth?.mp4?.url

    override val ctaDetails: CtaDetails?
        get() = apiStory.ctaDetails

}