@file:Suppress("BlockingMethodInNonBlockingContext")

package com.touchtalent.bobblesdk.stories.domain.musical_image_story

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.media.*
import com.touchtalent.bobblesdk.content_core.model.ContentMetadata
import com.touchtalent.bobblesdk.core.utils.FileUtil
import com.touchtalent.bobblesdk.stories.data.pojo.MusicalImageStory
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.nio.ByteBuffer

class MusicalImageExporter(private val musicalImageStory: MusicalImageStory) {

    private var finalOutputFilePath: String? = null
    private val mime = "video/avc"
    private var totalTimeInMs = musicalImageStory.videoDuration?.times(1000) ?: -1
    private val frameRate = 1
    private val maxAudioChunkSize = 1024 * 1024

    private val imageStoryEngine = MusicalStoryEngine(musicalImageStory)
    private var audioFilePath: String? = null

    private class DrainInfo {
        val bufferInfo = MediaCodec.BufferInfo()
        var presentationTimeUs = 0L
        val timeoutUs = 500000L
        var videoTrackIndex = -1
    }

    @Suppress("BlockingMethodInNonBlockingContext")
    @Throws(Exception::class)
    suspend fun export(contentMetadata: ContentMetadata?, destFolder: String): String =
        withContext(Dispatchers.Default) {
            withContext(Dispatchers.IO) {
                FileUtil.create(destFolder)
            }
            finalOutputFilePath =
                FileUtil.join(destFolder, "${musicalImageStory.normalisedTitle}.mp4")
            audioFilePath = imageStoryEngine.getAudioPath()

            val storyResult = imageStoryEngine.createStory(contentMetadata)
            val encoder = MediaCodec.createEncoderByType(mime)
            val capabilities = encoder.codecInfo.getCapabilitiesForType(mime)
            val maxHeight = capabilities.videoCapabilities.supportedHeights.upper
            val maxWidth = capabilities.videoCapabilities.supportedWidths.upper
            val bitmap =
                if (storyResult.bitmap.height > maxHeight || storyResult.bitmap.width > maxWidth)
                    resize(storyResult.bitmap, maxHeight, maxWidth)
                else
                    storyResult.bitmap

            val mediaFormat = getMediaFormat(bitmap.width, bitmap.height)
            encoder.configure(mediaFormat, null, null, MediaCodec.CONFIGURE_FLAG_ENCODE)
            val surface = encoder.createInputSurface()
            encoder.start()
            val outputMuxer = createOutputMuxer()
            val drainInfo = DrainInfo()
            val videoSurface = VideoRecordingSurface(surface)
            try {
                var renderedFrameCount = 0
                do {
                    // Render the bitmap/texture with OpenGL here
                    videoSurface.render(bitmap)
                    // Feed encoder with next frame produced by OpenGL
                    videoSurface.swapBuffer()
                    // Get encoded data and feed it to muxer
                    drainEncoder(encoder, drainInfo, outputMuxer, false)
                    renderedFrameCount++
                } while (renderedFrameCount < getTotalFrameCount() - 1)
                drainEncoder(encoder, drainInfo, outputMuxer, true)
                outputMuxer.stop()
                outputMuxer.release()
                encoder.stop()
                encoder.release()
                surface.release()
            } finally {
                videoSurface.destroy()
                bitmap.recycle()
            }
            requireNotNull(finalOutputFilePath)
        }

    private fun createOutputMuxer(): MediaMuxer {
        return MediaMuxer(
            requireNotNull(finalOutputFilePath),
            MediaMuxer.OutputFormat.MUXER_OUTPUT_MPEG_4
        )
    }

    private fun resize(image: Bitmap, maxWidth: Int, maxHeight: Int): Bitmap {
        var image = image
        return if (maxHeight > 0 && maxWidth > 0) {
            val width = image.width
            val height = image.height
            val ratioBitmap = width.toFloat() / height.toFloat()
            val ratioMax = maxWidth.toFloat() / maxHeight.toFloat()
            var finalWidth = maxWidth
            var finalHeight = maxHeight
            if (ratioMax > ratioBitmap) {
                finalWidth = (maxHeight.toFloat() * ratioBitmap).toInt()
            } else {
                finalHeight = (maxWidth.toFloat() / ratioBitmap).toInt()
            }
            image = Bitmap.createScaledBitmap(image, finalWidth / 2 * 2, finalHeight / 2 * 2, true)
            image
        } else {
            image
        }
    }

    private fun drainEncoder(
        encoder: MediaCodec,
        drainInfo: DrainInfo,
        muxer: MediaMuxer,
        endOfStream: Boolean
    ) {
        if (endOfStream)
            encoder.signalEndOfInputStream()
        val bufferInfo = drainInfo.bufferInfo
        while (true) {
            val outBufferId: Int = encoder.dequeueOutputBuffer(bufferInfo, drainInfo.timeoutUs)

            if (outBufferId >= 0) {
                val encodedBuffer = encoder.getOutputBuffer(outBufferId)!!

                // MediaMuxer is ignoring KEY_FRAMERATE, so I set it manually here
                // to achieve the desired frame rate

                bufferInfo.presentationTimeUs = drainInfo.presentationTimeUs
                require(drainInfo.videoTrackIndex != -1) { "Invalid trackInfo" }
                muxer.writeSampleData(drainInfo.videoTrackIndex, encodedBuffer, bufferInfo)

                drainInfo.presentationTimeUs += (1000000 / frameRate)

                encoder.releaseOutputBuffer(outBufferId, false)

                // Are we finished here?
                if ((bufferInfo.flags and MediaCodec.BUFFER_FLAG_END_OF_STREAM) != 0)
                    break
                // We found our frame
                if (bufferInfo.size != 0 && (bufferInfo.flags and MediaCodec.BUFFER_FLAG_CODEC_CONFIG) == 0)
                    break
            } else if (outBufferId == MediaCodec.INFO_TRY_AGAIN_LATER) {
                if (!endOfStream)
                    break

                // End of stream, but still no output available. Try again.
            } else if (outBufferId == MediaCodec.INFO_OUTPUT_FORMAT_CHANGED) {
                drainInfo.videoTrackIndex = initMuxerAndWriteAudio(encoder.outputFormat, muxer)
            }
        }
    }

    @SuppressLint("WrongConstant")
    private fun initMuxerAndWriteAudio(videoFormat: MediaFormat, muxer: MediaMuxer): Int {
        val audioExtractor = MediaExtractor()
        audioExtractor.setDataSource(
            requireNotNull(audioFilePath)
        )
        // Assuming only one track per file. Adjust code if this is not the case.
        audioExtractor.selectTrack(0)
        val audioFormat = audioExtractor.getTrackFormat(0)

        val audioTrackIndex = muxer.addTrack(audioFormat)
        val videoTrackIndex = muxer.addTrack(videoFormat)
        muxer.start()

        val buffer = ByteBuffer.allocate(maxAudioChunkSize)
        val bufferInfo = MediaCodec.BufferInfo()
        var isFirstTime = true
        var lastPresentationTimeUs = 0L

        while (lastPresentationTimeUs.usToMs() < totalTimeInMs) {
            if (!isFirstTime)
                audioExtractor.seekTo(0, MediaExtractor.SEEK_TO_CLOSEST_SYNC)
            isFirstTime = false
            while (true) {
                val chunkSize = audioExtractor.readSampleData(buffer, 0)
                if (chunkSize >= 0) {
                    bufferInfo.presentationTimeUs =
                        lastPresentationTimeUs + audioExtractor.sampleTime
                    bufferInfo.flags = audioExtractor.sampleFlags
                    bufferInfo.size = chunkSize

                    muxer.writeSampleData(audioTrackIndex, buffer, bufferInfo)
                    audioExtractor.advance()
                } else {
                    lastPresentationTimeUs = bufferInfo.presentationTimeUs
                    break
                }
            }
            if (totalTimeInMs < 0) {
                totalTimeInMs = lastPresentationTimeUs.usToMs()
                break
            }
        }
        return videoTrackIndex
    }

    private fun Long.usToMs(): Int {
        return (this / 1000).toInt()
    }

    private fun getTotalFrameCount(): Int {
        return if (totalTimeInMs == -1)
            Int.MAX_VALUE
        else frameRate * totalTimeInMs / 1000
    }

    fun getMediaFormat(width: Int, height: Int): MediaFormat {
        return MediaFormat.createVideoFormat(mime, width, height)
            .apply {
                setInteger(
                    MediaFormat.KEY_COLOR_FORMAT,
                    MediaCodecInfo.CodecCapabilities.COLOR_FormatSurface
                )
                setInteger(MediaFormat.KEY_BIT_RATE, 16000)
                setInteger(MediaFormat.KEY_FRAME_RATE, 30)
                setInteger(MediaFormat.KEY_I_FRAME_INTERVAL, 15)
            }
    }

}