package com.touchtalent.bobblesdk.stories.domain.animated_non_personalised

import androidx.core.content.FileProvider
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.ui.StyledPlayerView.SHOW_BUFFERING_ALWAYS
import com.google.android.exoplayer2.ui.StyledPlayerView.SHOW_BUFFERING_NEVER
import com.touchtalent.bobblesdk.content_core.interfaces.BobbleContent
import com.touchtalent.bobblesdk.content_core.interfaces.ContentRenderingContext
import com.touchtalent.bobblesdk.content_core.model.ContentMetadata
import com.touchtalent.bobblesdk.content_core.sdk.BobbleContentOutput
import com.touchtalent.bobblesdk.content_core.sdk.BobbleContentView
import com.touchtalent.bobblesdk.core.utils.FileUtil
import com.touchtalent.bobblesdk.core.utils.preloadImage
import com.touchtalent.bobblesdk.core.utils.renderUrl
import com.touchtalent.bobblesdk.core.utils.renderUrlFit
import com.touchtalent.bobblesdk.stories.cache.ExoPlayerCacheManager
import com.touchtalent.bobblesdk.stories.data.pojo.AnimatedNonPersonalisedImageStory
import com.touchtalent.bobblesdk.stories.domain.exoplayer.ExoPlayerExportManager
import com.touchtalent.bobblesdk.stories.domain.image_story.ImageViewPool
import com.touchtalent.bobblesdk.stories.sdk.BobbleStorySDK
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.io.File

class AnimatedVideoRenderingContext : ContentRenderingContext() {

    private val player by lazy{ExoPlayer.Builder(BobbleStorySDK.appContext).build()}
    private val exportManager by lazy { ExoPlayerExportManager() }
    private val videoViewPool = BobbleStoryVideoViewPool(4)
    private val imagePool = ImageViewPool(4)

    override suspend fun start() {

    }

    override fun render(
        content: BobbleContent,
        contentMetadata: ContentMetadata?,
        contentView: BobbleContentView
    ) {
        contentView.recycleAbleScope.launch {
            content.getPreviewUrl()?.let {
                val imageView = imagePool.inflateView(contentView)
                if(contentMetadata?.fitXY?:false){
                    imageView.renderUrlFit(it)
                }else {
                    imageView.renderUrl(it)
                }
            }
        }
    }

    override fun renderPlayable(
        content: BobbleContent,
        contentMetadata: ContentMetadata?,
        contentView: BobbleContentView
    ): Flow<Int> {
        val storyView = videoViewPool.inflateView(contentView)
        content.videoContentMp4Url?.let {
            val mediaSource = ExoPlayerCacheManager.mediaSourceFactory
                .createMediaSource(MediaItem.fromUri(it))
            player.apply {
                setMediaSource(mediaSource)
                volume = if (contentMetadata?.mute == true) 0F else 1F
                repeatMode = Player.REPEAT_MODE_ONE
                this.playWhenReady = true
                prepare()
            }
            storyView.player = player

            if(contentMetadata?.showLoader==true)
                storyView.setShowBuffering(SHOW_BUFFERING_ALWAYS)
            else
                storyView.setShowBuffering(SHOW_BUFFERING_NEVER)
        }
        return progressFlow()
    }

    private fun progressFlow() = flow {
        var lastReportedPosition: Int = -1
        while (true) {
            val position = ((player.currentPosition / (player.duration.toFloat())) * 100).toInt()
            emit(position)
            if (position < lastReportedPosition) {
                emit(100)
                break
            }
            lastReportedPosition = position
            delay(50)
        }
    }.distinctUntilChanged().cancellable().flowOn(Dispatchers.Main)

    override fun canRender(content: BobbleContent): Boolean {
        return content is AnimatedNonPersonalisedImageStory
    }

    override suspend fun export(
        content: BobbleContent,
        metadata: ContentMetadata?
    ): Result<BobbleContentOutput> = withContext(Dispatchers.IO) {
        if (content !is AnimatedNonPersonalisedImageStory) return@withContext Result.failure(
            Exception("Unsupported content format")
        )
        if (metadata != null &&
            (!metadata.supportedMimeTypes.contains("video/mp4") ||
                    !metadata.supportedMimeTypes.contains("video/*"))
        ) return@withContext Result.failure(Exception("Unsupported mime types"))
        val file = withContext(Dispatchers.IO) {
            val folder = FileUtil.createDirAndGetPath(BobbleStorySDK.cacheDir, "sharing")
            File(folder, "${content.normalisedTitle}.mp4")
        }
        val url = content.videoContentMp4Url
            ?: return@withContext Result.failure(Exception("No URL found"))
        var isSuccess = true
        exportManager.exportToFile(url, file.absolutePath)
            .catch { isSuccess = false }
            .collect {}
        val shareUri = withContext(Dispatchers.IO) {
            FileProvider.getUriForFile(
                BobbleStorySDK.appContext,
                BobbleStorySDK.getFileProviderAuthority(),
                file
            )
        }
        val output =
            BobbleContentOutput(
                content,
                null,
                localPath = file.absolutePath,
                shareUri = shareUri,
                "video/mp4"
            )
        if (isSuccess)
            return@withContext Result.success(output)
        return@withContext Result.failure(java.lang.Exception("Something went wrong"))
    }

    override suspend fun preCacheContent(content: BobbleContent) {
        content.videoContentMp4Url?.let { ExoPlayerCacheManager.preCacheVideo(it) }
        content.getPreviewUrl()?.let { BobbleStorySDK.appContext.preloadImage(it) }
    }

    override fun play(contentView: BobbleContentView) {
        player.play()
    }

    override fun pause(contentView: BobbleContentView) {
        player.pause()
    }

    override fun onViewRecycled(contentView: BobbleContentView) {
        player.stop()
        videoViewPool.recycle(contentView)
        imagePool.recycle(contentView)
    }

    override fun dispose() {
        super.dispose()
        player.release()
        videoViewPool.clear()
        imagePool.clear()
    }

    private val BobbleContent.videoContentMp4Url: String?
        get() {
            return ((this as? AnimatedNonPersonalisedImageStory)
                ?: throw Exception("Unsupported content format")
                    ).videoAssetUrl
        }
}