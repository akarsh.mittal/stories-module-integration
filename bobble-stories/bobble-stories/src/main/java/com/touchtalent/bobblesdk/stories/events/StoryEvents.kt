package com.touchtalent.bobblesdk.stories.events

import com.touchtalent.bobblesdk.content_core.model.Story
import com.touchtalent.bobblesdk.core.BobbleCoreSDK
import com.touchtalent.bobblesdk.core.enums.AppElements
import com.touchtalent.bobblesdk.core.interfaces.logger.EventBuilder
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.json.JSONArray

/**
 * Singleton class to log events related story
 */

object StoryEvents {
    private const val USER_STORY = "user_story"
    private const val STORY = "story"

    suspend fun logStoryViewed(
        fromIcon: Boolean,
        fromKeyboard: Boolean,
        isManual: Boolean,
        switchType: String?,
        screenName: String?,
        story: Story,
        prevStory: Story?,
        nextStory: Story?,
        bgStoryId: Int,
        audio: Int?,
        verticalCounter: Int?,
        stod: Int?,
        myStory: Int?
    ) = withContext(Dispatchers.IO) {
        EventBuilder()
            .withScreenName(screenName ?: "unknown")
            .withEventName("user_stats_story_displayed")
            .withEventAction("feature")
            .withPackageName(fromKeyboard)
            .addLabelParam("card_type", USER_STORY)
            .addLabelParam("from_icon", fromIcon)
            .addLabelParam("story_id", story.id)
            .addLabelParam("head_id", story.getHeadIds().toString())
            .addLabelParam("background_story_id", bgStoryId)
            .addLabelParam("story_format", story.format)
            .addLabelParam("internal_content_type", STORY)
            .addLabelParam("story_type", story.type)
            .addLabelParam("previous_story_type", prevStory?.type)
            .addLabelParam("previous_story_id", prevStory?.id)
            .addLabelParam("next_story_type", nextStory?.type)
            .addLabelParam("next_story_id", nextStory?.id)
            .addLabelParam("switch_type", switchType)
            .addLabelParam("is_manual", isManual)
            .addLabelParam("audio", audio)
            .addLabelParam("vertical_scroll_counter", verticalCounter ?: 0)
            .addLabelParam("stod_placement", stod ?: 0)
            .addLabelParam("my_story", myStory ?: 0)
            .withSessionId(false)
            .log()
    }

    suspend fun logStoryClosed(
        fromIcon: Boolean,
        fromKeyboard: Boolean,
        isManual: Boolean,
        screenName: String?,
        story: Story,
        bgStoryId: Int,
        storyIndex: Int
    ) = withContext(Dispatchers.IO) {
        EventBuilder()
            .withScreenName(screenName ?: "unknown")
            .withEventName("user_stats_story_closed")
            .withEventAction("feature")
            .withPackageName(fromKeyboard)
            .addLabelParam("card_type", USER_STORY)
            .addLabelParam("from_icon", fromIcon)
            .addLabelParam("story_id", story.id)
            .addLabelParam("head_id", story.getHeadIds().toString())
            .addLabelParam("background_story_id", bgStoryId)
            .addLabelParam("story_format", story.format)
            .addLabelParam("internal_content_type", STORY)
            .addLabelParam("story_type", story.type)
            .addLabelParam("is_manual", isManual)
            .addLabelParam("story_close_no", storyIndex)
            .log()
    }

    suspend fun logShareIconClicked(
        fromIcon: Boolean,
        fromKeyboard: Boolean,
        screenName: String?,
        story: Story,
        bgStoryId: Int,
        flow: String?
    ) = withContext(Dispatchers.IO) {
        EventBuilder()
            .withScreenName(screenName ?: "unknown")
            .withEventName("user_stats_story_share_now_landed")
            .withEventAction("feature")
            .withPackageName(fromKeyboard)
            .addLabelParam("card_type", USER_STORY)
            .addLabelParam("from_icon", fromIcon)
            .addLabelParam("story_id", story.id)
            .addLabelParam("head_id", story.getHeadIds().toString())
            .addLabelParam("background_story_id", bgStoryId)
            .addLabelParam("story_format", story.format)
            .addLabelParam("internal_content_type", STORY)
            .addLabelParam("story_type", story.type)
            .addLabelParam("flow", flow)
            .log()
    }

    suspend fun logStoryShared(
        fromIcon: Boolean,
        fromKeyboard: Boolean,
        screenName: String?,
        story: Story,
        sharedPackage: String,
        bgStoryId: Int,
        flow: String?,
        audio: Int,
        stod_flow: Int
    ) = withContext(Dispatchers.IO) {
        EventBuilder()
            .withScreenName(screenName ?: "unknown")
            .withEventName("user_stats_story_shared")
            .withEventAction("feature")
            .withPackageName(fromKeyboard)
            .addLabelParam("card_type", USER_STORY)
            .addLabelParam("from_icon", fromIcon)
            .addLabelParam("story_id", story.id)
            .addLabelParam("head_id", story.getHeadIds().toString())
            .addLabelParam("background_story_id", bgStoryId)
            .addLabelParam("story_format", story.format)
            .addLabelParam("internal_content_type", STORY)
            .addLabelParam("story_type", story.type)
            .addLabelParam("flow", flow)
            .addLabelParam("shared_package", sharedPackage)
            .addLabelParam("audio", audio)
            .addLabelParam("stod_placement", stod_flow)
            .log()
    }

    suspend fun logSavedStoryShared(
        screenName: String?,
        sharedPackage: String,
        audio: Int,
    ) = withContext(Dispatchers.IO) {
        EventBuilder()
            .withScreenName(screenName ?: "unknown")
            .withEventName("user_stats_story_shared")
            .withEventAction("feature")
            .addLabelParam("shared_package", sharedPackage)
            .addLabelParam("audio", audio)
            .log()
    }

    suspend fun logStoryDownloaded(
        fromIcon: Boolean,
        fromKeyboard: Boolean,
        screenName: String?,
        story: Story,
        bgStoryId: Int,
        sotd_placement: Int
    ) = withContext(Dispatchers.IO) {
        EventBuilder()
            .withScreenName(screenName ?: "unknown")
            .withEventName("user_stats_story_shared")
            .withEventAction("feature")
            .withPackageName(fromKeyboard)
            .addLabelParam("card_type", USER_STORY)
            .addLabelParam("from_icon", fromIcon)
            .addLabelParam("story_id", story.id)
            .addLabelParam("head_id", story.getHeadIds().toString())
            .addLabelParam("background_story_id", bgStoryId)
            .addLabelParam("story_format", story.format)
            .addLabelParam("internal_content_type", STORY)
            .addLabelParam("story_type", story.type)
            .addLabelParam("shared_package", "saved_to_gallery")
            .addLabelParam("stod_placement", sotd_placement)
            .log()
    }

    suspend fun logSearchResultsSuccess(
        screenName: String?,
        searchText: String?,
        isTrending: Boolean?,
    ) = withContext(Dispatchers.IO) {
        EventBuilder()
            .withScreenName(screenName ?: "unknown")
            .withEventName("search_suggestion_response")
            .withEventAction("feature")
            .withSessionId(false)
            .addLabelParam("search_text", searchText ?: "")
            .addLabelParam("is_trending", isTrending)
            .addLabelParam("is_recent", isTrending?.not())
            .addLabelParam("content_type", "story")
            .log()
    }

    suspend fun logSearchResultsFailure(
        screenName: String?,
        searchText: String?,
    ) = withContext(Dispatchers.IO) {
        EventBuilder()
            .withScreenName(screenName ?: "unknown")
            .withEventName("search_suggestion_failed")
            .withEventAction("feature")
            .withSessionId(false)
            .addLabelParam("search_text", searchText ?: "")
            .addLabelParam("content_type", "story")
            .log()
    }

    suspend fun logStoryViewedEvent(
        screenName: String?,
        storiesArray: JSONArray? = null
    ) = withContext(Dispatchers.IO) {
        val builder = EventBuilder()
            .withScreenName(screenName ?: "unknown")
            .withEventName("story_viewed")
            .withEventAction("feature")
            .withSessionId(false)
        storiesArray?.let {
            builder.addLabelParam("stories", it)
        }
        builder.log()
    }

    suspend fun logSOTDNotificationClicked(
        storyId: Int?,
        text: String?,
    ) = withContext(Dispatchers.IO) {
        EventBuilder()
            .withScreenName("kb_notification")
            .withEventAction("notification")
            .withEventName("notification_clicked")
            .withSessionId(false)
            .withPackageName(false)
            .addLabelParam(
                "language_code", BobbleCoreSDK.getAppController().getAppElement(
                    AppElements.KB_LANGUAGE_CODE
                )
            )
            .addLabelParam("notification_type", "sotd")
            .addLabelParam("notification_text", text)
            .addLabelParam("story_id", storyId)
            .log()
    }
}