package com.touchtalent.bobblesdk.stories.domain

import android.graphics.Bitmap
import android.util.Log
import com.touchtalent.bobblesdk.content_core.interfaces.stories.BobbleStory
import com.touchtalent.bobblesdk.content_core.interfaces.stories.StoryManager
import com.touchtalent.bobblesdk.content_core.interfaces.stories.StoryResponse
import com.touchtalent.bobblesdk.content_core.model.ContentMetadata
import com.touchtalent.bobblesdk.content_core.model.Story
import com.touchtalent.bobblesdk.content_core.model.accessory.Accessory
import com.touchtalent.bobblesdk.content_core.model.expression.Expression
import com.touchtalent.bobblesdk.content_core.model.wig.Wig
import com.touchtalent.bobblesdk.core.utils.downloadBitmapSync
import com.touchtalent.bobblesdk.stories.data.datastore.BobbleStoriesDataStore
import com.touchtalent.bobblesdk.stories.data.model.api.ApiMascotHead
import com.touchtalent.bobblesdk.stories.data.model.api.ApiStory
import com.touchtalent.bobblesdk.stories.data.model.api.ApiStoryList
import com.touchtalent.bobblesdk.stories.data.model.api.StoryTrendingSearches
import com.touchtalent.bobblesdk.stories.data.pojo.AnimatedNonPersonalisedImageStory
import com.touchtalent.bobblesdk.stories.data.pojo.ImageStory
import com.touchtalent.bobblesdk.stories.data.pojo.MusicalImageStory
import com.touchtalent.bobblesdk.stories.domain.image_story.ImageStoryRenderingContext
import com.touchtalent.bobblesdk.stories.domain.network.StoryApiClient
import com.touchtalent.bobblesdk.stories.sdk.BobbleStorySDK

object StoryManagerImpl : StoryManager {

    override suspend fun getStories(): Result<List<BobbleStory>> {
        val storiesResult = getStoriesFromServer()
        storiesResult.getOrNull()?.map { it.id }
            ?.let { BobbleStoriesDataStore.updateStoryViewedSet(it) }
        return storiesResult
    }

    override suspend fun getVerticalStories(
        nextToken: String?,
        limit: Int,
        searchString: String?
    ): Result<StoryResponse> {
        val storiesResult = getVerticalStoriesFromServer(
            nextToken,
            limit,
            searchString
        )
        return storiesResult
    }

    override suspend fun getStoryForNotification(): Result<BobbleStory> {
        var result = getStoriesFromCache()
        if (result.isFailure) result = getStories()
        return result.map { stories ->
            kotlin.runCatching {
                stories.first { it.forceEnableStoryNotification }
            }.getOrElse {
                kotlin.runCatching {
                    stories.first { !BobbleStoriesDataStore.isStoryViewed(it.id) }
                }.getOrDefault(stories.first())
            }
        }
    }


    override suspend fun getStoryForCard(): Result<BobbleStory> {
        var result = getStoriesFromCache()
        if (result.isFailure) result = getStories()
        return result.map { stories ->
            kotlin.runCatching {
                stories.first { !BobbleStoriesDataStore.isStoryViewed(it.id) }
            }.getOrDefault(stories.first())
        }
    }

    override suspend fun downloadStoryPreview(story: BobbleStory): Bitmap {
        val previewUrl = if (story is ImageStory) {
            ImageStoryRenderingContext().export(story)
                .map { it.localPath }
                .getOrNull()
        } else {
            story.getPreviewUrl()
        }
        return BobbleStorySDK.appContext.downloadBitmapSync(previewUrl)
            ?: throw java.lang.Exception("Failed to load Bitmap")
    }

    private suspend fun getStoriesFromServer(): Result<List<BobbleStory>> {
        return kotlin.runCatching {
            StoryApiClient.apiService.getFeaturedStories()
                .getOrNull() ?: throw Exception("Body is null")
        }.mapCatching { storyList ->
            convertApiStoryToBobbleStory(storyList)
        }
    }

    private suspend fun getVerticalStoriesFromServer(
        nextToken: String?,
        limit: Int,
        searchString: String?
    ): Result<StoryResponse> {
        return StoryApiClient.apiService.getVerticalStories(
            nextToken = nextToken,
            limit = limit,
            searchString
        )
            .mapCatching { storyList ->
                StoryResponse(
                    convertApiStoryToBobbleStory(storyList),
                    storyList.nextToken
                )
            }
    }

    private suspend fun getStoriesFromCache(): Result<List<BobbleStory>> {
        return kotlin.runCatching {
            StoryApiClient.apiPreCacheService.getFeaturedStories()
                .getOrNull() ?: throw Exception("Body is null")
        }.mapCatching { storyList ->
            convertApiStoryToBobbleStory(storyList)
        }
    }

    private fun convertApiStoryToBobbleStory(storyList: ApiStoryList): List<BobbleStory> {
        val listOfStories = mutableListOf<BobbleStory>()
        for (apiStory in storyList.stories) {
            val story = convertStoryToBobbleStory(
                apiStory = apiStory,
                characters = storyList.characters,
                accessories = storyList.accessories,
                expressions = storyList.expressions,
                wigs = storyList.wigs
            )
            story?.let { listOfStories.add(it) }
        }
        if (listOfStories.isEmpty())
            throw Exception("list of stories is empty")
        return listOfStories
    }

    private fun convertStoryToBobbleStory(
        apiStory: ApiStory,
        characters: List<ApiMascotHead>?,
        accessories: List<Accessory>?,
        expressions: List<Expression>?,
        wigs: List<Wig>?
    ): BobbleStory? {
       return when (apiStory.contentType) {
            "static" -> {
                if (apiStory.videoDetails?.audioSnippetURL != null)
                    MusicalImageStory(
                        apiStory = apiStory,
                        accessories = accessories,
                        expressions = expressions,
                        wigs = wigs,
                        mascotHead = characters
                    ) else {
                    ImageStory(
                        apiStory = apiStory,
                        accessories = accessories,
                        expressions = expressions,
                        wigs = wigs,
                        mascotHead = characters
                    )
                }
            }
            "animated" -> ImageStory(
                apiStory = apiStory,
                accessories = accessories,
                expressions = expressions,
                wigs = wigs,
                mascotHead = characters
            )
            "video" -> AnimatedNonPersonalisedImageStory(apiStory)
            else -> null
        }
    }
    suspend fun getTrendingSearches(): Result<StoryTrendingSearches> {
        return StoryApiClient.apiService.getTrendingSearches()
    }

    suspend fun getSearches(keyword: String): Result<StoryTrendingSearches> {
        return StoryApiClient.apiService.getSearches(keyword)
    }

    override suspend fun getStoryById(storyId: Int): Result<Story> {
        return StoryApiClient.apiService.getStoryById(storyId).mapCatching {
            val verticalStory = convertStoryToBobbleStory(
                apiStory = it.story,
                characters = it.characters,
                accessories = it.accessories,
                expressions = it.expressions,
                wigs = it.wigs
            )
            if(verticalStory != null) {
                Story.VerticalStory(
                    verticalStory,
                    ContentMetadata(showLoader = true)
                )
            }else
                throw Exception("Story is null")
        }.onFailure {
            Log.d("DANGER", it.toString())
        }
    }
}
