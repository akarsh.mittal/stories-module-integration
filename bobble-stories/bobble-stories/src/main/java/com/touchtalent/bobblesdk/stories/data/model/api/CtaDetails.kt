package com.touchtalent.bobblesdk.stories.data.model.api

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CtaDetails(
    val type: String,
    val text: String,
    val items:List<CtaDetailsItems>?
)

@JsonClass(generateAdapter = true)
data class CtaDetailsItems(
    val type: String?,
    val text: String?,
    val storyFilterId: String?,
    val normalStateDetails: CtaState?,
    val focusedStateDetails: CtaState?,
    val url: String?,

    )

@JsonClass(generateAdapter = true)
data class CtaState(
    val textColor: String?,
    val backgroundColor: String?
)
