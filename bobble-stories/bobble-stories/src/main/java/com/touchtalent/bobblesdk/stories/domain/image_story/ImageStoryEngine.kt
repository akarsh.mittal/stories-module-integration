package com.touchtalent.bobblesdk.stories.domain.image_story

import android.graphics.*
import android.text.StaticLayout
import android.text.TextPaint
import androidx.core.graphics.withRotation
import androidx.core.graphics.withTranslation
import com.touchtalent.bobblesdk.content_core.model.ContentMetadata
import com.touchtalent.bobblesdk.content_core.model.HeadAddOns
import com.touchtalent.bobblesdk.content_core.util.ContentResourceDownloader
import com.touchtalent.bobblesdk.content_core.util.WatermarkManager
import com.touchtalent.bobblesdk.core.BobbleCoreSDK
import com.touchtalent.bobblesdk.core.enums.Gender
import com.touchtalent.bobblesdk.core.interfaces.head.HeadModule
import com.touchtalent.bobblesdk.core.model.BobbleHead
import com.touchtalent.bobblesdk.core.utils.ViewUtil
import com.touchtalent.bobblesdk.core.utils.newStaticLayout
import com.touchtalent.bobblesdk.stories.cache.DefaultStoriesCacheManager
import com.touchtalent.bobblesdk.stories.data.model.api.ApiHeadDetails
import com.touchtalent.bobblesdk.stories.data.model.api.ApiTextBubble
import com.touchtalent.bobblesdk.stories.data.model.api.CustomHeadPosition
import com.touchtalent.bobblesdk.stories.data.pojo.ImageStory
import com.touchtalent.bobblesdk.stories.sdk.BobbleStorySDK
import kotlinx.coroutines.*
import java.io.File
import kotlin.math.min

open class ImageStoryEngine(private val imageStory: ImageStory) {

    suspend fun createStory(metadata: ContentMetadata?): ImageStoryOutput {
        val (path, isFromInternet) = downloadResources()
        val heads = resolveHeads(metadata)
        val backgroundBitmap = loadBackground(path)
        val canvas = Canvas(backgroundBitmap)
        drawHeads(canvas, heads)
        drawTextBubbles(canvas)
        drawWatermark(canvas)
        return ImageStoryOutput(bitmap = backgroundBitmap)
    }

    suspend fun getRawResourceFolder(): String {
        return ContentResourceDownloader.downloadRawResources(
            content = imageStory,
            module = BobbleStorySDK,
            cacheKey = DefaultStoriesCacheManager.CACHE_KEY
        ).first
    }

    private suspend fun drawWatermark(canvas: Canvas) {
        if (!imageStory.enableWatermark)
            return
        val watermarkUrl = imageStory.watermarkDetails?.url ?: return
        val watermarkPosition = imageStory.watermarkDetails.position
        val (watermarkPath, isFromInternet) = WatermarkManager.getWatermark(watermarkUrl, false)
        val watermarkBitmap = BitmapFactory.decodeFile(watermarkPath)
        val scaleX = watermarkBitmap.width / (watermarkPosition.width * canvas.width)
        val scaleY = watermarkBitmap.height / (watermarkPosition.height * canvas.height)
        val matrix = Matrix().apply {
            postScale(scaleX, scaleY)
            postTranslate(canvas.width * watermarkPosition.x, canvas.height * watermarkPosition.y)
        }
        canvas.drawBitmap(watermarkBitmap, matrix, null)
        watermarkBitmap.recycle()
    }

    private fun drawTextBubbles(canvas: Canvas) {
        imageStory.textDetails.forEach { renderTextBubble(canvas, it) }
    }

    private fun renderTextBubble(canvas: Canvas, textBubble: ApiTextBubble) {
        val text = textBubble.text ?: return
        val textPaint = TextPaint()
            .apply {
                textSize = 25F
                color = Color.BLACK
                isAntiAlias = true
            }

        val position = textBubble.customTextDetails.position

        val scale: Float = position.originalWidth / canvas.width.toFloat()
        val textPadding: Int = ViewUtil.dpToPx(10F, BobbleStorySDK.appContext)
        val maxTextWidth: Float = position.width * scale - 2 * textPadding
        val acquiredWidth: Float = min(textPaint.measureText(text), maxTextWidth)
        val staticLayout: StaticLayout = newStaticLayout(text, textPaint, acquiredWidth.toInt())

        val bubbleHeight: Int = staticLayout.height + 2 * textPadding
        val maxWidth: Float = staticLayout.width.toFloat()
        val bubbleWidth: Float = maxWidth + (2 * textPadding)

        val bubblePaint = Paint()
            .apply {
                color = Color.WHITE
                style = Paint.Style.FILL_AND_STROKE
                isAntiAlias = true
            }

        val bubbleRadius: Float = ViewUtil.dpToPx(8F, BobbleStorySDK.appContext).toFloat()

        val left: Float = position.x * scale
        val top: Float = position.y * scale
        val right: Float = left + bubbleWidth
        val bottom: Float = top + bubbleHeight
        val rectF = RectF(left, top, right, bottom)
        canvas.withRotation(position.angle.toFloat(), rectF.centerX(), rectF.centerY()) {
            drawRoundRect(rectF, bubbleRadius, bubbleRadius, bubblePaint)
            withTranslation(left + textPadding, top + textPadding) {
                staticLayout.draw(this)
            }
        }
    }

    private suspend fun drawHeads(canvas: Canvas, heads: List<Pair<ApiHeadDetails, HeadWrapper>>) =
        coroutineScope {
            val jobs = mutableListOf<Job>()
            heads.forEach { (apiDetails, headWrapper) ->
                jobs.add(
                    launch {
                        val headAddOns = getHeadAddOns(apiDetails)
                        headWrapper.prepare(headAddOns, false)
                    }
                )
            }
            jobs.joinAll()
            heads.forEach { renderHead(canvas, it.second, it.first) }
        }

    private suspend fun renderHead(
        canvas: Canvas,
        headWrapper: HeadWrapper,
        apiDetails: ApiHeadDetails
    ) {
        val (headBitmap, chinPoint) = headWrapper.getHeadBitmapWithChin()
        val customHeadPosition = scale(
            apiDetails.customHeadDetails.position,
            apiDetails.customHeadDetails.originalWidth to apiDetails.customHeadDetails.originalHeight,
            canvas.width to canvas.height
        )
        // Scale the face to match the required height to height of face
        // (from top to chin, excluding any hanging hair, if present)
        val scale = customHeadPosition.height / chinPoint.y.toFloat()

        val left = customHeadPosition.x - chinPoint.x * scale
        val top = customHeadPosition.y - chinPoint.y * scale

        val matrix = Matrix().apply {
            postRotate(
                90f - customHeadPosition.angle,
                headBitmap.width / 2f,
                headBitmap.height / 2f
            )
            postScale(scale, scale)
            postTranslate(left, top)
        }
        canvas.drawBitmap(headBitmap, matrix, Paint(Paint.FILTER_BITMAP_FLAG))
        headBitmap.recycle()
    }

    private fun scale(
        customHeadPosition: CustomHeadPosition,
        originalDimensions: Pair<Int, Int>,
        scaledDimensions: Pair<Int, Int>
    ): CustomHeadPosition {
        val widthScale = 1f * scaledDimensions.first / originalDimensions.first
        val heightScale = 1f * scaledDimensions.second / originalDimensions.second
        if (widthScale == 1f && heightScale == 1f)
            return customHeadPosition
        return CustomHeadPosition(
            angle = customHeadPosition.angle,
            height = (customHeadPosition.height * heightScale).toInt(),
            x = (customHeadPosition.x * widthScale).toInt(),
            y = (customHeadPosition.y * heightScale).toInt(),
        )
    }

    private fun getHeadAddOns(apiDetails: ApiHeadDetails): HeadAddOns {
        val accessories = apiDetails.customHeadDetails.accessoryIds?.map { accessoryId ->
            imageStory.accessories?.find { it.id == accessoryId }
                ?: throw Exception("Accessory $accessoryId not found")
        }
        val wigs = apiDetails.customHeadDetails.wigId?.let { wigId ->
            imageStory.wigs?.find { it.id == wigId } ?: throw Exception("Wig $wigId not found")
        }
        val expressions = apiDetails.customHeadDetails.expressionId?.let { expressionId ->
            imageStory.expressions?.find { it.id == expressionId }
                ?: throw Exception("Expression $expressionId not found")
        }
        return HeadAddOns(
            accessories = accessories,
            expression = expressions,
            wig = wigs
        )
    }

    private fun loadBackground(rawResourcesPath: String): Bitmap {
        val backgroundFileRegex = Regex("background\\..+")
        val backgroundFilePath = File(rawResourcesPath)
            .walk()
            .filter { backgroundFileRegex.matches(it.name) }
            .first()
        val bitmapOptions = BitmapFactory.Options()
            .apply { inMutable = true }
        return BitmapFactory.decodeFile(backgroundFilePath.absolutePath, bitmapOptions)
    }

    suspend fun resolveHeads(
        metadata: ContentMetadata?
    ): List<Pair<ApiHeadDetails, HeadWrapper>> = coroutineScope {
        val currentHeadMap = mutableMapOf<Gender, MutableMap<Int?, BobbleHead?>>()
        val listOfPair = imageStory.headDetails.map { apiHeadDetails ->
            if (apiHeadDetails.isPrimary) {
                val gender: Gender = Gender.from(apiHeadDetails.gender)
                val headType: Int? = apiHeadDetails.customHeadDetails.headType
                val head = currentHeadMap[gender]
                    ?.get(apiHeadDetails.customHeadDetails.headType)
                    ?: loadCurrentUserHeadAsync(gender, metadata?.primaryHead, headType)
                        .also {
                            currentHeadMap.getOrPut(gender) { mutableMapOf() }[headType] = it
                        }
                if (head != null && !head.isMascotHead())
                    return@map apiHeadDetails to HeadWrapper.UserHead(head)
            }
            val headWrapper = imageStory.mascotHead?.find { it.id == apiHeadDetails.mascotId }
                ?: throw Exception("Failed to find the mascot head with id ${apiHeadDetails.mascotId}")
            return@map apiHeadDetails to HeadWrapper.ApiMascot(headWrapper)
        }
        return@coroutineScope listOfPair
    }

    private suspend fun loadCurrentUserHeadAsync(
        gender: Gender,
        primaryHead: BobbleHead?,
        bobbleType: Int?,
    ): BobbleHead? {
        if (primaryHead != null && (primaryHead.gender == gender.value || gender == Gender.UNISEX))
            return primaryHead
        val headModule = BobbleCoreSDK.getModule(HeadModule::class.java) ?: return null
        return kotlin.runCatching {
            val head = withContext(Dispatchers.IO) {
                headModule.getHeadManager()
                    .getCurrentHead(gender.value, bobbleType)
                    .blockingGet()
            }
            if (head.isMascotHead())
                return@runCatching null
            else return@runCatching head
        }.getOrNull()
    }

    private suspend fun downloadResources(): Pair<String, Boolean> {
        return ContentResourceDownloader.downloadRawResources(
            content = imageStory,
            module = BobbleStorySDK,
            cacheKey = DefaultStoriesCacheManager.CACHE_KEY
        )
    }
}