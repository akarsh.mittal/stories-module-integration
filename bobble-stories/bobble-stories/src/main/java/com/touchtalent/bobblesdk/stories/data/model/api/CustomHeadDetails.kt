package com.touchtalent.bobblesdk.stories.data.model.api

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CustomHeadDetails(
    val wigId: Int?,
    val accessoryIds: List<Int>?,
    val expressionId: Int?,
    val headType: Int?,
    val originalHeight: Int,
    val originalWidth: Int,
    val position: CustomHeadPosition
)