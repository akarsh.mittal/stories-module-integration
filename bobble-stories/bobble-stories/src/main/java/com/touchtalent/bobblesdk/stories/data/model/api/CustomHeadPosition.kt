package com.touchtalent.bobblesdk.stories.data.model.api

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CustomHeadPosition(
    val angle: Int = 0,
    val height: Int,
    val x: Int,
    val y: Int
)