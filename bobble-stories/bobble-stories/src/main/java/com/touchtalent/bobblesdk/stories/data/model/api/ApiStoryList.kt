package com.touchtalent.bobblesdk.stories.data.model.api

import com.squareup.moshi.JsonClass
import com.touchtalent.bobblesdk.content_core.model.accessory.Accessory
import com.touchtalent.bobblesdk.content_core.model.expression.Expression
import com.touchtalent.bobblesdk.content_core.model.wig.Wig

@JsonClass(generateAdapter = true)
data class ApiStoryList(
    val stories: List<ApiStory>,
    val characters: List<ApiMascotHead>?,
    val accessories: List<Accessory>?,
    val expressions: List<Expression>?,
    val wigs: List<Wig>?,
    val nextToken: String?
)

