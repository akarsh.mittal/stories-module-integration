package com.touchtalent.bobblesdk.stories.domain.image_story

import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.touchtalent.bobblesdk.content_core.util.ViewRecyclerPool
import com.touchtalent.bobblesdk.core.utils.GeneralUtils
import com.touchtalent.bobblesdk.core.views.ImpressionImageView

/**
 * View pool for normal [ImpressionImageView]. Used for headless animated sticker rendering and
 * recent stickers
 */
class ImageViewPool(poolSize: Int) : ViewRecyclerPool<ImpressionImageView>(poolSize) {
    override fun canRecycle(view: ImpressionImageView): Boolean {
        return true
    }

    override fun onCreateView(parent: ViewGroup): ImpressionImageView {
        return ImpressionImageView(parent.context)
    }

    override fun onRecycled(view: ImpressionImageView) {
        view.reset()
        if (GeneralUtils.isValidContextForGlide(view.context))
            Glide.with(view).clear(view)
    }
}