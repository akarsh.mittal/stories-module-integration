package com.touchtalent.bobblesdk.stories.domain.image_story

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Point
import com.touchtalent.bobblesdk.content_core.head.HeadAddOnProcessor
import com.touchtalent.bobblesdk.content_core.model.HeadAddOns
import com.touchtalent.bobblesdk.core.enums.Gender
import com.touchtalent.bobblesdk.core.model.BobbleHead
import com.touchtalent.bobblesdk.core.utils.FileUtil
import com.touchtalent.bobblesdk.core.utils.ResourceDownloader
import com.touchtalent.bobblesdk.stories.data.model.api.ApiMascotHead
import com.touchtalent.bobblesdk.stories.sdk.BobbleStorySDK
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

sealed class HeadWrapper {
    abstract suspend fun prepare(headAddOns: HeadAddOns, forceCache: Boolean)
    abstract suspend fun getHeadBitmapWithChin(): Pair<Bitmap, Point>
    abstract fun getGender(): Gender
    abstract fun getHeadId(): String

    class UserHead(val bobbleHead: BobbleHead) : HeadWrapper() {
        private var headAddOnProcessor: HeadAddOnProcessor? = null

        private val faceFeaturePoint by lazy {
            bobbleHead.getFeatureFacePointMap() ?: throw Exception("Face feature point corrupt")
        }

        override suspend fun getHeadBitmapWithChin(): Pair<Bitmap, Point> {
            val (bitmap, facePointMap) = headAddOnProcessor?.getHeadBitmap()
                ?: BitmapFactory.decodeFile(bobbleHead.headPath) to faceFeaturePoint
            return bitmap to (facePointMap[11] ?: throw Exception("Missing chin points"))
        }

        override fun getGender(): Gender {
            return Gender.from(bobbleHead.gender)
        }

        override fun getHeadId(): String {
            return bobbleHead.headId
        }

        override suspend fun prepare(headAddOns: HeadAddOns, forceCache: Boolean) {
            headAddOnProcessor = HeadAddOnProcessor(bobbleHead, headAddOns)
                .apply { prepare(forceCache) }
        }
    }

    class ApiMascot(private val apiMascotHead: ApiMascotHead) : HeadWrapper() {

        override suspend fun prepare(headAddOns: HeadAddOns, forceCache: Boolean) {
            val headUrl = apiMascotHead.getHeadUrl() ?: throw java.lang.Exception("Head missing")
            val headDest = withContext(Dispatchers.IO) {
                FileUtil.createDirAndGetPath(BobbleStorySDK.rootDir, "mascotHeads")
            }
            ResourceDownloader.downloadResourcesSuspend(headUrl, headDest).first
                ?: throw java.lang.Exception("Couldn't download head")
        }

        override suspend fun getHeadBitmapWithChin(): Pair<Bitmap, Point> {
            val headUrl = apiMascotHead.getHeadUrl() ?: throw java.lang.Exception("Head missing")
            val headDest = withContext(Dispatchers.IO) {
                FileUtil.createDirAndGetPath(BobbleStorySDK.rootDir, "mascotHeads")
            }
            val path = ResourceDownloader.downloadResourcesSuspend(headUrl, headDest).first
                ?: throw java.lang.Exception("Couldn't download head")
            val bitmap = BitmapFactory.decodeFile(path)
            val chinPoint = getChin()
            return bitmap to chinPoint
        }

        private fun getChin(): Point {
            val height = apiMascotHead.getHeight() ?: throw Exception("height not found")
            val width = apiMascotHead.getWidth() ?: throw Exception("width not found")
            return Point(
                (apiMascotHead.chinX * width).toInt(),
                (apiMascotHead.chinY * height).toInt()
            )
        }

        override fun getGender(): Gender {
            return Gender.from(apiMascotHead.gender)
        }

        override fun getHeadId(): String {
            return apiMascotHead.id.toString()
        }
    }
}