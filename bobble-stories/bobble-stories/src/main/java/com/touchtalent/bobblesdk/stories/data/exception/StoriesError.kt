package com.touchtalent.bobblesdk.stories.data.exception

import androidx.annotation.Keep

@Keep
class StoryOfTheDayError(exception: Throwable) : Exception(exception)
class VerticalStoriesError(exception: Throwable) : Exception(exception)