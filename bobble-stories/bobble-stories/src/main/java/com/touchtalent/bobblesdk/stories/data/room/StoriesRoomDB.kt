package com.touchtalent.bobblesdk.stories.data.room

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.touchtalent.bobblesdk.stories.sdk.BobbleStorySDK

@Database(
    entities = [
        SearchSuggestions::class
    ],
    version = 1
)
abstract class StoriesRoomDB : RoomDatabase() {
    abstract fun searchSuggestionsDao(): SearchSuggestionsDao

    companion object {

        private var instance: StoriesRoomDB? = null

        @JvmStatic
        fun getInstance(): StoriesRoomDB {
            if (instance == null) {
                instance = Room.databaseBuilder(
                    BobbleStorySDK.appContext,
                    StoriesRoomDB::class.java,
                    "bobble-stories"
                ).build()
            }
            return instance as StoriesRoomDB
        }
    }
}