package com.touchtalent.bobblesdk.stories.domain.image_story

import android.graphics.Bitmap

/**
 * A HashMap with a fixed number of elements (poolSize) working like a FIFO queue.
 * So until the element number is <= poolSize new elements are simply put in the map.
 * For element number > poolSize the first inserted element is removed and the newest is put in the map.
 **/

class BitmapPool(private val poolSize: Int) : LinkedHashMap<Int, Bitmap>(poolSize) {
    override fun removeEldestEntry(eldest: MutableMap.MutableEntry<Int, Bitmap>?): Boolean {
        return size > poolSize
    }
}