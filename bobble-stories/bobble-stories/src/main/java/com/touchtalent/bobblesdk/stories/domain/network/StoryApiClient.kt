package com.touchtalent.bobblesdk.stories.domain.network

import com.chuckerteam.chucker.api.ChuckerInterceptor
import com.touchtalent.bobblesdk.core.BobbleCoreSDK
import com.touchtalent.bobblesdk.core.api.BobbleCallAdapterFactory
import com.touchtalent.bobblesdk.core.api.CachingControlInterceptor
import com.touchtalent.bobblesdk.core.utils.FileUtil
import com.touchtalent.bobblesdk.stories.sdk.BobbleStorySDK
import okhttp3.Cache
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.io.File

object StoryApiClient {

    private const val STORY_API_CACHE_MAX_SIZE = 2 * 1024 * 1024L // 2MB cache for API responses
    private val STORY_API_CACHE_PATH = FileUtil.join(BobbleStorySDK.cacheDir, "network")
    private val STORY_OKHTTP = BobbleCoreSDK.okHttpClient.newBuilder()
        .addInterceptor(CommonQueryInterceptor())
        .cache(Cache(File(STORY_API_CACHE_PATH), STORY_API_CACHE_MAX_SIZE))
        .build()

    val apiService: StoriesApiService by lazy {
        Retrofit.Builder()
            .baseUrl(BobbleCoreSDK.crossAppInterface.baseUrl() + "/")
            .client(
                STORY_OKHTTP.newBuilder()
                    .addInterceptor(CachingControlInterceptor())
                    .addInterceptor(ChuckerInterceptor(BobbleStorySDK.appContext))
                    .build()
            )
            .addCallAdapterFactory(BobbleCallAdapterFactory.create())
            .addConverterFactory(MoshiConverterFactory.create(BobbleCoreSDK.moshi))
            .build()
            .create(StoriesApiService::class.java)
    }

    val apiPreCacheService: StoriesApiService by lazy {
        Retrofit.Builder()
            .baseUrl(BobbleCoreSDK.crossAppInterface.baseUrl() + "/")
            .client(
                STORY_OKHTTP.newBuilder()
                    .addInterceptor(CachingControlInterceptor(true))
                    .build()
            )
            .addCallAdapterFactory(BobbleCallAdapterFactory.create())
            .addConverterFactory(MoshiConverterFactory.create(BobbleCoreSDK.moshi))
            .build()
            .create(StoriesApiService::class.java)
    }
}