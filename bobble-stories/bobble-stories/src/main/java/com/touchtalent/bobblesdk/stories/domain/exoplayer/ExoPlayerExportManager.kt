package com.touchtalent.bobblesdk.stories.domain.exoplayer

import android.net.Uri
import com.google.android.exoplayer2.C
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DataSpec
import com.google.android.exoplayer2.upstream.FileDataSource
import com.google.android.exoplayer2.upstream.TransferListener
import com.google.android.exoplayer2.upstream.cache.CacheDataSink
import com.google.android.exoplayer2.upstream.cache.CacheDataSource
import com.touchtalent.bobblesdk.stories.cache.ExoPlayerCacheManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.cancel
import kotlinx.coroutines.channels.trySendBlocking
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.flowOn
import java.io.FileOutputStream
import java.io.IOException

class ExoPlayerExportManager {
    @Suppress("BlockingMethodInNonBlockingContext")
    fun exportToFile(url: String, exportPath: String) = callbackFlow<Int> {
        val dataSource = CacheDataSource(
            ExoPlayerCacheManager.cache,
            // If the cache doesn't have the whole content, the missing data will be read from upstream
            ExoPlayerCacheManager.httpDataSourceFactory.createDataSource(),
            FileDataSource(),
            // Set this to null if you don't want the downloaded data from upstream to be written to cache
            CacheDataSink(
                ExoPlayerCacheManager.cache,
                CacheDataSink.DEFAULT_BUFFER_SIZE.toLong()
            ),
            /* flags= */ 0,
            /* eventListener= */ null
        )
        var totalBytesToRead = C.LENGTH_UNSET.toLong()
        var totalBytesTransferred = 0

        // Listen to the progress of the reads from cache and the network.
        dataSource.addTransferListener(object : TransferListener {
            override fun onTransferInitializing(
                source: DataSource,
                dataSpec: DataSpec,
                isNetwork: Boolean
            ) {

            }

            override fun onTransferStart(
                source: DataSource,
                dataSpec: DataSpec,
                isNetwork: Boolean
            ) {

            }

            override fun onBytesTransferred(
                source: DataSource,
                dataSpec: DataSpec,
                isNetwork: Boolean,
                bytesTransferred: Int
            ) {
                totalBytesTransferred += bytesTransferred
                trySendBlocking((totalBytesTransferred / totalBytesToRead.toFloat() * 100).toInt())
            }

            override fun onTransferEnd(
                source: DataSource,
                dataSpec: DataSpec,
                isNetwork: Boolean
            ) {
            }

        })

        var outFile: FileOutputStream? = null
        var bytesRead = 0
        try {
            outFile = FileOutputStream(exportPath)
            totalBytesToRead = dataSource.open(DataSpec(Uri.parse(url)))
            // Just read from the data source and write to the file.
            val data = ByteArray(CacheDataSink.DEFAULT_BUFFER_SIZE)

            while (bytesRead != C.RESULT_END_OF_INPUT) {
                bytesRead = dataSource.read(data, 0, data.size)
                if (bytesRead != C.RESULT_END_OF_INPUT) {
                    outFile.write(data, 0, bytesRead)
                }
            }
        } catch (e: IOException) {
            cancel("exception", e)
        } finally {
            dataSource.close()
            outFile?.flush()
            outFile?.close()
        }
        channel.close()
    }.flowOn(Dispatchers.IO)
}