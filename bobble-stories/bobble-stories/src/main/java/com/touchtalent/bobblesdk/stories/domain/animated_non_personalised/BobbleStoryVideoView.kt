package com.touchtalent.bobblesdk.stories.domain.animated_non_personalised

import android.content.Context
import android.util.AttributeSet
import com.google.android.exoplayer2.ui.StyledPlayerView
import com.touchtalent.bobblesdk.content_core.interfaces.RecyclableView
import com.touchtalent.bobblesdk.stories.R

class BobbleStoryVideoView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null
) : StyledPlayerView(context, attrs, R.style.BobbleStoryVideoView), RecyclableView {

    init {
        useController = false
        setKeepContentOnPlayerReset(true)
    }

    override fun onViewRecycled() {
        player = null
    }

}

