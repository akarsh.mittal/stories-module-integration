package com.touchtalent.bobblesdk.stories.domain.network

import com.touchtalent.bobblesdk.stories.data.model.api.ApiSingleStory
import com.touchtalent.bobblesdk.stories.data.model.api.ApiStoryList
import com.touchtalent.bobblesdk.stories.data.model.api.FeedbackRequest
import com.touchtalent.bobblesdk.stories.data.model.api.StoryTrendingSearches
import retrofit2.http.*

interface StoriesApiService {
    @GET("stories/featured")
    suspend fun getFeaturedStories(): Result<ApiStoryList>


    @GET("stories/{storyId}")
    suspend fun getStoryById(@Path("storyId") storyId: Int): Result<ApiSingleStory>

    @GET("stories")
    suspend fun getVerticalStories(
        @Query("nextToken") nextToken: String?,
        @Query("limit") limit: Int,
        @Query("searchString") searchString: String?
    ): Result<ApiStoryList>

    @GET("stories/trendingSearches")
    suspend fun getTrendingSearches(@Query("limit") limit: Int = 5): Result<StoryTrendingSearches>

    @GET("stories/searchSuggestions")
    suspend fun getSearches(
        @Query("searchString") searchString: String,
        @Query("limit") limit: Int = 5
    ): Result<StoryTrendingSearches>

    @POST("users/feedback")
    suspend fun submitFeedback(
        @Body body: FeedbackRequest
    ): Result<Unit>
}