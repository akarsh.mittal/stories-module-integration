package com.touchtalent.bobblesdk.stories.data.pojo

import com.touchtalent.bobblesdk.content_core.model.accessory.Accessory
import com.touchtalent.bobblesdk.content_core.model.expression.Expression
import com.touchtalent.bobblesdk.content_core.model.wig.Wig
import com.touchtalent.bobblesdk.core.utils.ThemeUtil
import com.touchtalent.bobblesdk.stories.data.model.api.*
import com.touchtalent.bobblesdk.stories.domain.image_story.ImageStoryEngine
import com.touchtalent.bobblesdk.stories.sdk.BobbleStorySDK

open class ImageStory(
    val apiStory: ApiStory,
    override val accessories: List<Accessory>?,
    override val wigs: List<Wig>?,
    override val expressions: List<Expression>?,
    val mascotHead: List<ApiMascotHead>?,
    override val type: Type = Type.STATIC,
) : BobbleStoryImpl(apiStory.id, type) {

    override val noOfShares: Int?
        get() = apiStory.numShares

    override val normalisedTitle: String
        get() = apiStory.title.replace("\\W+", "")

    override val isOtfSupported: Boolean
        get() = false

    override val shareText: String?
        get() = apiStory.shareText

    override val aspectRatio: String
        get() = "${fixedWidth?.width ?: 9}:${fixedWidth?.height ?: 16}"

    override val forceEnableStoryNotification: Boolean
        get() = apiStory.forceEnableStoryOfTheDayNotification


    /**
     * Resolves the preview URL of appropriate size - [FixedWidthResource], based on device DPI
     */
    private val fixedWidth: FixedWidthResource?
        get() = with(apiStory) {
            var fixedWidth = when (ThemeUtil.getScreenResolution(BobbleStorySDK.appContext)) {
                "xxxhdpi", "xxhdpi" -> fixedWidthFull
                "xhdpi" -> fixedWidthMedium
                else -> fixedWidthSmall
            }
            if (fixedWidth == null) {
                fixedWidth = fixedWidthSmall
                if (fixedWidth == null) {
                    fixedWidth = fixedWidthMedium
                    if (fixedWidth == null) fixedWidth = fixedWidthFull
                }
            }
            return fixedWidth
        }

    override fun getPreviewUrl(): String? =
        fixedWidth?.png?.url ?: fixedWidth?.jpg?.url ?: fixedWidth?.gif?.url

    override val rawResourcesUrl: String? = apiStory.rawResourcesURL
    val headDetails: List<ApiHeadDetails> = apiStory.headDetails
    val textDetails: List<ApiTextBubble> = apiStory.textBubbles
    val displayInterval: Int = apiStory.displayInterval ?: 10
    val enableWatermark: Boolean = apiStory.enableWatermark
    override val isHeadSupported: Boolean = apiStory.isHeadSupported
    val watermarkDetails: WatermarkDetails? = apiStory.watermarkDetails

    override suspend fun getHeadIds() =
        ImageStoryEngine(this@ImageStory).resolveHeads(null).map { it.second.getHeadId() }

    override val ctaDetails: CtaDetails?
        get() = apiStory.ctaDetails

}