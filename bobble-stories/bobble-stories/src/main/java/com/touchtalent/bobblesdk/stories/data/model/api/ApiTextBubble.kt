package com.touchtalent.bobblesdk.stories.data.model.api

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ApiTextBubble(
    val customTextDetails: CustomTextDetails,
    val id: Int?,
    val text: String?
)