package com.touchtalent.bobblesdk.stories.data.model.api

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ApiHeadDetails(
    @Json(name = "characterId")
    val mascotId: Long,
    val customHeadDetails: CustomHeadDetails,
    val gender: String = "unisex",
    val id: Int,
    val isPrimary: Boolean = false
)