package com.touchtalent.bobblesdk.stories_ui.domain.enums

import android.net.Uri

data class SavedStory(
    val type: String,
    val uri: Uri
)
