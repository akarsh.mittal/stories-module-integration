package com.touchtalent.bobblesdk.stories_ui.domain.enums

    const val TYPE_SHARE_STORY="shareStory"
    const val TYPE_SHARE_STORY_WHATSAPP="shareStoryWhatsapp"
    const val TYPE_DOWNLOAD_STORY="downloadStory"
    const val TYPE_EDIT_STORY="editStory"
    const val TYPE_OPEN_WEB_BROWSER="openWebBrowser"
    const val TYPE_OPEN_WEB_VIEW="openWebView"
    const val TYPE_OPEN_DEEPLINK="openDeeplink"
    const val TYPE_OPEN_STORY_FILTER="openStoryFilter"
    const val TYPE_MORE_OPTIONS = "moreOptions"
    const val TYPE_BLANK = "blank"


