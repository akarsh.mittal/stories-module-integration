package com.touchtalent.bobblesdk.stories_ui.ui.stories

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.viewbinding.ViewBinding
import com.touchtalent.bobblesdk.content_core.interfaces.stories.StoryAdapter
import com.touchtalent.bobblesdk.content_core.model.Story
import com.touchtalent.bobblesdk.stories_ui.R
import com.touchtalent.bobblesdk.stories_ui.databinding.StoryContentBinding
import com.touchtalent.bobblesdk.stories_ui.ui.activity.formatNumberWithSuffix
import kotlinx.coroutines.flow.Flow


class ContentStoryAdapter() : StoryAdapter() {

    override fun getIdentifier(): String = "content"

    override fun onCreateView(parent: ViewGroup): ViewBinding {
        val binding = StoryContentBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        binding.contentStoryView.crossTapListener = onClose
        return binding
    }

    override fun onDestroyView(viewBinding: ViewBinding, story: Story?) {
        if (viewBinding !is StoryContentBinding) error("Only StoryContentBinding allowed")
        viewBinding.contentStoryView.onViewDestroy()
    }

    override suspend fun play(viewBinding: ViewBinding, story: Story): Flow<Int>? {
        if (story !is Story.ContentStory) error("Only ContentStory allowed")
        if (viewBinding !is StoryContentBinding) error("Only StoryContentBinding allowed")

        with(viewBinding) {
            contentStoryView.shareListener = {
                onShare?.invoke(story)
            }
            contentStoryView.downloadListener = {
                onDownload?.invoke(story)
            }
        }

        val shareCount = story.bobbleStory.noOfShares?.formatNumberWithSuffix() ?: "0"
        viewBinding.contentStoryView.setShareCount("$shareCount Shares")

        return renderingContext?.let {
            viewBinding.contentStoryView.setPlayableContent(
                story.bobbleStory,
                it,
                story.contentMetadata
            )
        }
    }

    override fun resume(viewBinding: ViewBinding, story: Story?) {
        if (viewBinding !is StoryContentBinding) error("Only StoryContentBinding allowed")
        viewBinding.contentStoryView.resume()
    }

    override fun pause(viewBinding: ViewBinding, story: Story?) {
        if (viewBinding !is StoryContentBinding) error("Only StoryContentBinding allowed")
        viewBinding.contentStoryView.pause()
    }

    fun setStoryCount(viewBinding: ViewBinding,shareCount: String) {
        if (viewBinding !is StoryContentBinding) error("Only StoryContentBinding allowed")
        viewBinding.contentStoryView.setShareCount(
            String.format(
                viewBinding.root.resources.getString(
                    R.string.story_no_of_shares
                ), shareCount
            )
        )
    }

}