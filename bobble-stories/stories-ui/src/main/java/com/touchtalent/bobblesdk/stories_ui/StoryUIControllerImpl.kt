package com.touchtalent.bobblesdk.stories_ui

import android.content.Context
import android.content.Intent
import com.touchtalent.bobblesdk.content_core.interfaces.stories.StoryUIController
import com.touchtalent.bobblesdk.core.BobbleCoreSDK
import com.touchtalent.bobblesdk.core.utils.ContextUtils
import com.touchtalent.bobblesdk.stories_ui.sdk.BobbleStoryUiSDK
import com.touchtalent.bobblesdk.stories_ui.ui.activity.BobbleStoriesActivity
import com.touchtalent.bobblesdk.stories_ui.ui.activity.StoriesBaseActivity
import com.touchtalent.bobblesdk.stories_ui.ui.model.StoriesViewModel

object StoryUIControllerImpl : StoryUIController() {

    override fun newBuilder(context: Context) = object : Builder(context) {

        override fun startActivity() {
            if (isKeyboardView) {
                BobbleStoryUiSDK.applicationContext.startActivity(getIntent())
            } else {
                context.startActivity(getIntent())
            }
        }

        override fun getIntent(): Intent {
            val intent = StoriesViewModel.getLaunchIntent(
                source,
                isKeyboardView,
                screenName,
                storyId,
                searchString,
                notificationText
            )
            if (isKeyboardView) {
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
                BobbleCoreSDK.crossAppInterface.modifyActivityIntentForKeyboard(intent, null)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            }
            intent.setClass(context, BobbleStoriesActivity::class.java)
            StoriesBaseActivity.locale = ContextUtils.getLocale(context).toString()
            return intent
        }

    }
}