package com.touchtalent.bobblesdk.stories_ui.ui.views

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.widget.LinearLayout
import com.touchtalent.bobblesdk.stories_ui.R

class StoriesProgressView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null
) : LinearLayout(context, attrs) {

    init {
        orientation = HORIZONTAL
    }

    private var currentPos = 0

    fun setStoriesCount(count: Int) {
        weightSum = count.toFloat()
        if (childCount > count) {
            while (childCount != count) {
                removeView(getChildAt(childCount - 1))
            }
        } else if (childCount < count) {
            while (childCount != count) {
                addView(newProgressBar())
            }
        }
    }

    private fun newProgressBar(): CustomProgressBarView {
        return CustomProgressBarView(context, null).apply {
            layoutParams = LayoutParams(MATCH_PARENT, MATCH_PARENT).apply {
                weight = 1f
                val padding = context.resources.getDimension(R.dimen.padding_progress_bar).toInt()
                setPadding(padding, 0, padding, 0)
            }
        }
    }

    fun setProgress(position: Int, progress: Int) {
        currentPos = position
        for (i in 0 until childCount) {
            val child = getChildAt(i) as CustomProgressBarView
            when {
                i < position -> child.progress = 100
                i == position -> child.progress = progress
                else -> child.progress = 0
            }
        }
    }
}