package com.touchtalent.bobblesdk.stories_ui.domain.data

import com.touchtalent.bobblesdk.content_core.model.ContentMetadata
import com.touchtalent.bobblesdk.content_core.model.Story
import com.touchtalent.bobblesdk.content_core.sdk.ContentCoreSDK
import com.touchtalent.bobblesdk.stories.data.datastore.BobbleStoriesDataStore
import com.touchtalent.bobblesdk.stories.data.model.api.FeedbackRequest
import com.touchtalent.bobblesdk.stories.data.model.api.StoryTrendingSearches
import com.touchtalent.bobblesdk.stories.data.room.SearchSuggestions
import com.touchtalent.bobblesdk.stories.data.room.StoriesRoomDB
import com.touchtalent.bobblesdk.stories.domain.StoryManagerImpl
import com.touchtalent.bobblesdk.stories.domain.network.StoryApiClient
import com.touchtalent.bobblesdk.stories.sdk.BobbleStorySDK
import com.touchtalent.bobblesdk.stories_ui.domain.interfaces.StoriesRepository

class StoriesRepositoryImpl : StoriesRepository {
    override suspend fun getListOfStories(): Result<List<Story>> {
        return BobbleStorySDK.getStoryManager().getStories().map { list ->
            mutableListOf<Story>().apply {
                addAll(list.map { content ->
                    Story.ContentStory(
                        content,
                        ContentMetadata(showLoader = true)
                    )
                })

                val adFactoryAdapter =
                    //If index is a negative value. Disable ads for stories
                    if (BobbleStoriesDataStore.getAdDisplayIndex() < 0)
                        null
                    else
                        ContentCoreSDK.contentCoreConfig?.storyConfig?.adsAdapterFactory

                //add story ads in appropriate position
                adFactoryAdapter?.let {
                    if (this.size <= BobbleStoriesDataStore.getAdDisplayIndex())
                        add(adFactoryAdapter.createAdStory())
                    else {
                        var adCounter = 0
                        for (i in 1..this.size) {
                            if (i % BobbleStoriesDataStore.getAdDisplayIndex() == 0) {
                                add(i + adCounter, adFactoryAdapter.createAdStory())
                                adCounter++
                            }
                        }
                    }
                }
                //add story summary
                if (list.size > 1) {
                    add(Story.SummaryStory(list.takeLast(9)))
                }
            }.toList()
        }
    }

    override suspend fun getListOfStoriesWithoutAds(): Result<List<Story>> {
        return BobbleStorySDK.getStoryManager().getStories().map { list ->
            mutableListOf<Story>().apply {
                addAll(list.map { content ->
                    Story.ContentStory(
                        content,
                        ContentMetadata(showLoader = true)
                    )
                })
            }.toList()
        }
    }

    override suspend fun getListOfVerticalStories(
        nextToken: String?,
        limit: Int,
        searchString: String?
    ): Result<Pair<List<Story>, String?>> {
        return BobbleStorySDK.getStoryManager().getVerticalStories(nextToken, limit, searchString)
            .map { response ->
                Pair(mutableListOf<Story>().apply {
                    addAll(response.storyList.map { content ->
                        Story.VerticalStory(
                            content,
                            ContentMetadata(showLoader = true)
                        )
                    })
                }.toList(), response.nextToken)
            }
    }

    override suspend fun getTrendingSearches(): Result<StoryTrendingSearches> {
        return StoryManagerImpl.getTrendingSearches().map {
            it as StoryTrendingSearches
        }
    }

    override suspend fun getSearches(keyword: String): Result<StoryTrendingSearches> {
        return StoryManagerImpl.getSearches(keyword).map {
            it as StoryTrendingSearches
        }
    }

    override suspend fun insertSearchKeyword(keyword: String) {
        StoriesRoomDB.getInstance().searchSuggestionsDao()
            .insertKeyword(SearchSuggestions(keyword = keyword))
    }

    override suspend fun getSavedSearches(keyword: String): List<SearchSuggestions> {
        return StoriesRoomDB.getInstance().searchSuggestionsDao().getKeywords(keyword + "%")
    }

    override suspend fun getTopSavedSearches(): List<SearchSuggestions> {
        return StoriesRoomDB.getInstance().searchSuggestionsDao().getTopKeywords()
    }

    override suspend fun submitFeedback(message: String, type: String): Result<Unit> {
        return StoryApiClient.apiService.submitFeedback(FeedbackRequest(message, type))
    }

    override suspend fun getStoryById(storyId: Int): Result<Story> {
        return BobbleStorySDK.getStoryManager().getStoryById(storyId)
    }
}