package com.touchtalent.bobblesdk.stories_ui.ui.views

import android.content.Context
import android.graphics.drawable.BitmapDrawable
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.PopupWindow
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.touchtalent.bobblesdk.content_core.interfaces.ContentRenderingContext
import com.touchtalent.bobblesdk.content_core.model.Story
import com.touchtalent.bobblesdk.core.utils.ViewUtil
import com.touchtalent.bobblesdk.stories.data.pojo.AnimatedNonPersonalisedImageStory
import com.touchtalent.bobblesdk.stories.data.pojo.MusicalImageStory
import com.touchtalent.bobblesdk.stories_ui.R
import com.touchtalent.bobblesdk.stories_ui.databinding.VerticalStoryViewBinding
import com.touchtalent.bobblesdk.stories_ui.ui.activity.StoriesUtil
import kotlinx.coroutines.launch

class VerticalStoryPlayerView : Fragment() {


    var story: Story? = null
    var renderingContext: ContentRenderingContext? = null
    var storyPlayerListener: StoryPlayerView.StoryPlayerListener? = null

    private lateinit var binding: VerticalStoryViewBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = VerticalStoryViewBinding.inflate(inflater, container, false)

        binding.storyPlayerView.onMoreOptionsClick = { v: View, s: Story ->
            showPopup(v)
        }
        binding.storyPlayerView.playerListener = storyPlayerListener
        viewLifecycleOwner.lifecycleScope.launch {
            renderingContext?.let { renderingContext1 ->
                story?.let { story1 ->
                    binding.storyPlayerView.loadVerticalStory(story1, renderingContext1)
                }
            }
        }
        return binding.root
    }

    override fun onPause() {
        super.onPause()
        binding.storyPlayerView.pauseStory()
    }

    override fun onResume() {
        super.onResume()
        startPlaying()
    }

    fun startPlaying() {
        if (story is Story.VerticalStory && ((story as Story.VerticalStory).bobbleStory is MusicalImageStory
                    || (story as Story.VerticalStory).bobbleStory is AnimatedNonPersonalisedImageStory)
        ) {
            viewLifecycleOwner.lifecycleScope.launch {
                renderingContext?.let { renderingContext1 ->
                    story?.let { story1 ->
                        binding.storyPlayerView.playVerticalStory(story1, renderingContext1)
                    }
                }
            }
        }
    }

    private fun showPopup(v: View) {
        val popup: PopupWindow?
        popup =
            showTemplateSideMenu(context)
        val screenPos = IntArray(2)
        v.getLocationOnScreen(screenPos)
        popup.showAtLocation(
            v,
            Gravity.NO_GRAVITY,
            screenPos[0],
            screenPos[1] - (v.getHeight() * 2) - 20
        )
    }

    private fun showTemplateSideMenu(
        context: Context?,
    ): PopupWindow {
        val layoutInflater = context?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as?
                LayoutInflater
        val layout: View? = layoutInflater?.inflate(
            R.layout.stories_bottom_bar_popup,
            null
        )
        val popup = PopupWindow(
            layout, LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )
        popup.width = ViewUtil.dpToPx(110f, context)
        val reportAbuse = layout?.findViewById<View>(R.id.btn_report_abuse) as? TextView

        reportAbuse?.setOnClickListener {
            popup.dismiss()
            StoriesUtil().showReportDialog(context) {
                storyPlayerListener?.submitFeedback(it)
            }
        }
        popup.setBackgroundDrawable(BitmapDrawable())
        popup.isOutsideTouchable = true
        popup.isFocusable = true
        return popup
    }
}