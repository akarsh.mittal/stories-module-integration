package com.touchtalent.bobblesdk.stories_ui.ui.views

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import com.touchtalent.bobblesdk.stories_ui.R
import com.touchtalent.bobblesdk.stories_ui.databinding.ErrorStoriesBinding

class StoryErrorView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null
) : ConstraintLayout(context, attrs) {

    private val binding = ErrorStoriesBinding.inflate(LayoutInflater.from(context), this)

    init {
        with(binding.bobbleErrorView) {
            setErrorResource(R.drawable.story_error_image)
            setTitleText(R.string.story_no_internet_connection)
            setTitleTextColor(Color.WHITE)
            setSubtitleText(R.string.story_please_check_you_internet_settings)
            setSubtitleTextColor(Color.WHITE)
            setPrimaryColor(R.color.story_primary_color)
        }
    }

    fun setRetryClickListener(listener: OnClickListener?) {
        binding.bobbleErrorView.setRetryClickListener(listener)
    }

    fun setOnCloseListener(listener: OnClickListener?) {
        binding.close.setOnClickListener(listener)
    }
}