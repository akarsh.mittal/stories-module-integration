package com.touchtalent.bobblesdk.stories_ui.domain.interfaces

import com.touchtalent.bobblesdk.content_core.model.Story
import com.touchtalent.bobblesdk.stories.data.model.api.ApiSingleStory
import com.touchtalent.bobblesdk.stories.data.model.api.StoryTrendingSearches
import com.touchtalent.bobblesdk.stories.data.room.SearchSuggestions

interface StoriesRepository {
    suspend fun getListOfStories(): Result<List<Story>>
    suspend fun getListOfStoriesWithoutAds(): Result<List<Story>>
    suspend fun getListOfVerticalStories(
        nextToken: String?,
        limit: Int,
        searchString: String? = null
    ): Result<Pair<List<Story>, String?>>

    suspend fun getTrendingSearches(): Result<StoryTrendingSearches>
    suspend fun getSearches(keyword: String): Result<StoryTrendingSearches>
    suspend fun insertSearchKeyword(keyword: String)
    suspend fun getSavedSearches(keyword: String): List<SearchSuggestions>
    suspend fun getTopSavedSearches(): List<SearchSuggestions>
    suspend fun submitFeedback(message: String, type: String): Result<Unit>
    suspend fun getStoryById(storyId: Int) : Result<Story>
}