package com.touchtalent.bobblesdk.stories_ui.domain.enums

sealed class Resource<T> {
    class Loading<T> : Resource<T>()
    open class Success<T>(val data: T) : Resource<T>()
    class Error<T>(val error: Throwable) : Resource<T>()
    class Sharing<T>(val data: T) : Resource<T>()
}
