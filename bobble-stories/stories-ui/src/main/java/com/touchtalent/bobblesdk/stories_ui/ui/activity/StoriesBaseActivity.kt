package com.touchtalent.bobblesdk.stories_ui.ui.activity

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import com.touchtalent.bobblesdk.core.utils.ContextUtils
import java.util.*

open class StoriesBaseActivity : AppCompatActivity() {
    companion object {
        var locale: String? = null
    }

    override fun attachBaseContext(newBase: Context) {
        locale?.let {
            try {
                super.attachBaseContext(
                    ContextUtils.Modifier(newBase)
                        .updateLocale(constructLocaleFromString(it))
                        .modify()
                )
            } catch (ignore: Exception) {
                super.attachBaseContext(newBase)
            }
        } ?: super.attachBaseContext(newBase)
    }

    open fun constructLocaleFromString(localeStr: String): Locale {
        val localeParams =
            localeStr.split("_").toTypedArray()
        return when (localeParams.size) {
            1 -> Locale(localeParams[0])
            2 -> Locale(localeParams[0], localeParams[1])
            3 -> Locale(localeParams[0], localeParams[1], localeParams[2])
            else -> Locale(localeStr)
        }
    }
}