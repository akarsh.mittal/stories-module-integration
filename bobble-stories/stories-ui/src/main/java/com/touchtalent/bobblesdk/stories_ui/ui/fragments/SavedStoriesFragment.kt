package com.touchtalent.bobblesdk.stories_ui.ui.fragments

import android.Manifest
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.recyclerview.widget.GridLayoutManager
import com.touchtalent.bobblesdk.core.utils.GeneralUtils
import com.touchtalent.bobblesdk.stories_ui.R
import com.touchtalent.bobblesdk.stories_ui.databinding.FragementSavedStoriesBinding
import com.touchtalent.bobblesdk.stories_ui.domain.enums.SavedStory
import com.touchtalent.bobblesdk.stories_ui.ui.activity.findNavControllerSafely
import com.touchtalent.bobblesdk.stories_ui.ui.model.StoriesViewModel
import com.touchtalent.bobblesdk.stories_ui.ui.stories.SavedStoriesAdapter
import kotlinx.coroutines.launch

class SavedStoriesFragment : Fragment() {

    private lateinit var binding: FragementSavedStoriesBinding
    private val model: StoriesViewModel by activityViewModels()
    private var adapter: SavedStoriesAdapter? = null

    companion object {
        val REQUEST_CODE_PERMISSION_STORAGE = 1
        var videoPlayIndex = 0
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragementSavedStoriesBinding.inflate(inflater, container, false)
        requestPermission()
        adapter = SavedStoriesAdapter({ onItemClicked(it) },
            { onVideoComplete() })
        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                launch { listenToStoryList() }
            }
        }
        binding.backIcon.setOnClickListener {
            activity?.finish()
        }
        binding.noStoriesButton.setOnClickListener {
            requestPermission()
        }
        return binding.root
    }

    private fun requestPermission() {
        if (isAdded) {
            if (isExternalImageAndVideoStoragePermissionGranted()
            ) {
                // You can use the API that requires the permission.
                model.loadDownloadedStories()
            } else if (showStoragePermissionRationale()) {
                showNoStories(false)
                GeneralUtils.showToast(
                    context,
                    "Please allow storage permission from settings"
                )
            } else {
                // You can directly ask for the permission.
                askForStoryStoragePermission()
            }
        }
    }

    private fun showStoragePermissionRationale(): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            return shouldShowRequestPermissionRationale(Manifest.permission.READ_MEDIA_VIDEO)
                    && shouldShowRequestPermissionRationale(Manifest.permission.READ_MEDIA_IMAGES)
        } else return shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE)
    }


    private fun askForStoryStoragePermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            activity?.requestPermissions(
                arrayOf(Manifest.permission.READ_MEDIA_IMAGES, Manifest.permission.READ_MEDIA_VIDEO),
                REQUEST_CODE_PERMISSION_STORAGE
            )
        }else{
            activity?.requestPermissions(
                arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE),
                REQUEST_CODE_PERMISSION_STORAGE
            )
        }

    }

    private fun isExternalImageAndVideoStoragePermissionGranted(): Boolean {
        var isPermissionGranted = false

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            isPermissionGranted = (ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.READ_MEDIA_IMAGES
            ) ==
                    PackageManager.PERMISSION_GRANTED) && (ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.READ_MEDIA_VIDEO
            ) ==
                    PackageManager.PERMISSION_GRANTED)

        } else {
            isPermissionGranted = ContextCompat.checkSelfPermission(
                requireContext(), Manifest.permission.READ_EXTERNAL_STORAGE
            ) ==
                    PackageManager.PERMISSION_GRANTED
        }
        return isPermissionGranted
    }


    private suspend fun listenToStoryList() {
        model.savedStoriesList.collect {
            if (it != null) {
                if (it.size > 0) {
                    adapter?.submitList(it)
                    binding.searchRecycler.layoutManager = GridLayoutManager(context, 2)
                    binding.progress.visibility = View.GONE
                    binding.searchRecycler.adapter = adapter
                    binding.noStoriesButton.visibility = View.GONE
                    binding.noStoriesText.visibility = View.GONE
                    binding.notStoriesImage.visibility = View.GONE
                    model.logStoryViewedEvent(null)
                } else {
                    showNoStories(true)
                }
            } else {
                showNoStories(false)
            }
        }
    }

    private fun showNoStories(noStories: Boolean) {
        binding.progress.visibility = View.GONE
        binding.noStoriesButton.visibility = View.VISIBLE
        binding.noStoriesText.visibility = View.VISIBLE
        binding.notStoriesImage.visibility = View.VISIBLE

        if (noStories) {
            binding.noStoriesText.text = activity?.getString(R.string.no_stories)
            binding.noStoriesButton.visibility = View.GONE
        } else {
            binding.noStoriesText.text = activity?.getString(R.string.read_permission)
            binding.noStoriesButton.visibility = View.VISIBLE
        }
    }

    private fun onItemClicked(story: SavedStory) {
        val bundle = Bundle()
        bundle.putString("uri", story.uri.toString())
        bundle.putString("saved_story_type", story.type)
        findNavControllerSafely(R.id.savedStoriesFragment)?.navigate(
            R.id.action_savedStoriesFragment_to_shareStoryFragment,
            bundle
        )
    }

    private fun onVideoComplete() {
        videoPlayIndex++
        if (videoPlayIndex - 1 > -1) {
            adapter?.notifyItemChanged(videoPlayIndex - 1, 0)
        }
        adapter?.notifyItemChanged(videoPlayIndex, 0)
    }
}