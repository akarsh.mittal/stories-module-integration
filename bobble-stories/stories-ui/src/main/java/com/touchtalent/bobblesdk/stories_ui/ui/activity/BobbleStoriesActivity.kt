package com.touchtalent.bobblesdk.stories_ui.ui.activity

import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import androidx.activity.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.NavHostFragment
import com.touchtalent.bobblesdk.content_core.interfaces.ContentRenderingContext
import com.touchtalent.bobblesdk.content_core.interfaces.stories.StoryUIController
import com.touchtalent.bobblesdk.core.BobbleCoreSDK
import com.touchtalent.bobblesdk.stories.sdk.BobbleStorySDK
import com.touchtalent.bobblesdk.stories_ui.R
import com.touchtalent.bobblesdk.stories_ui.ui.fragments.SavedStoriesFragment.Companion.REQUEST_CODE_PERMISSION_STORAGE
import com.touchtalent.bobblesdk.stories_ui.ui.fragments.VerticalStoriesFragment
import com.touchtalent.bobblesdk.stories_ui.ui.model.StoriesViewModel
import kotlinx.coroutines.launch

class BobbleStoriesActivity : StoriesBaseActivity() {

    private val viewModel: StoriesViewModel by viewModels()
    var renderingContext: ContentRenderingContext = BobbleStorySDK.newContentRenderingInstance()
        private set

    var source: Int = StoryUIController.Source.ICON.ordinal

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //android O fix bug orientation
        if (Build.VERSION.SDK_INT != Build.VERSION_CODES.O) {
            requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        }
        window.navigationBarColor = Color.BLACK
        window.statusBarColor = Color.BLACK
        source = intent.getIntExtra(
            StoriesViewModel.SOURCE,
            StoryUIController.Source.ICON.ordinal
        )
        if (source == StoryUIController.Source.ICON.ordinal
        ) {
            overridePendingTransition(
                R.anim.story_anim_pull_up_from_bottom,
                R.anim.story_anim_push_out_to_bottom
            )
        }
        setContentView(R.layout.activity_bobble_stories)
        setNavGraph()
        viewModel.setData(intent)
        lifecycleScope.launch {
            renderingContext.start()
            BobbleStorySDK.getCacheManager().increaseCacheSize()
        }
    }

    private fun setNavGraph() {
        val navHostFragment = supportFragmentManager.findFragmentById(R.id.story_nav_host_fragment)
                as NavHostFragment
        val graphInflater = navHostFragment.navController.navInflater
        val navGraph = graphInflater.inflate(R.navigation.story_nav_graph)
        val navController = navHostFragment.navController

        val destination = if (source == StoryUIController.Source.VERTICAL_STORIES.ordinal
        ) R.id.verticalStoriesFragment
        else if (source == StoryUIController.Source.SEARCH.ordinal)
            R.id.searchStoryFragment
        else if (source == StoryUIController.Source.SAVED_STORIES.ordinal)
            R.id.savedStoriesFragment
        else if (source == StoryUIController.Source.SEARCH_RESULTS.ordinal)
            R.id.searchResultFragment
        else
            R.id.playSotryFragment

        navGraph.startDestination = destination
        navController.graph = navGraph
    }

    override fun onDestroy() {
        super.onDestroy()
        renderingContext.dispose()
        BobbleCoreSDK.applicationScope.launch { BobbleStorySDK.getCacheManager().trimCacheSize() }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == REQUEST_CODE_PERMISSION_STORAGE) {
            if (grantResults.size > 0
                && alreadyAskedStoragePermissionGranted(grantResults)
            ) {
                viewModel.loadDownloadedStories()
            } else {
                viewModel.storagePermissionDenied()
            }
        }
    }

    private fun alreadyAskedStoragePermissionGranted(grantResults: IntArray): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            if (grantResults.size == 2) {
                return grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED
            } else {
                return false
            }
        } else {
            return grantResults[0] == PackageManager.PERMISSION_GRANTED
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == VerticalStoriesFragment.REQUEST_CODE_CREATE_HEAD && resultCode == RESULT_OK) {
            viewModel.setHeadCreation()
        }
    }
}