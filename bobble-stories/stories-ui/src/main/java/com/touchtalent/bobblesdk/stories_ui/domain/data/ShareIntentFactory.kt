package com.touchtalent.bobblesdk.stories_ui.domain.data

import android.content.ComponentName
import android.content.Intent
import android.net.Uri
import android.os.Build
import com.touchtalent.bobblesdk.core.BobbleCoreSDK
import com.touchtalent.bobblesdk.core.enums.AppElements
import com.touchtalent.bobblesdk.stories_ui.utils.*

object ShareIntentFactory {

    fun getIntent(packageName: String?, shareUri: Uri?, shareText: String?, type: String?): Intent {
        return when (packageName) {
            INSTAGRAM -> intentForInstagram(
                shareUri,
                type
            )
            FACEBOOK -> intentForFaceBook(
                shareUri,
                type
            )
            FACEBOOK_MESSENGER -> intentForMessenger(
                shareUri,
                type
            )
            else -> defaultShareIntent(
                packageName,
                shareUri,
                shareText,
                type
            )
        }
    }


    /**
     * @see <a href="https://developers.facebook.com/docs/instagram/sharing-to-stories/">Sharing stories to instagram</a>
     */
    private val intentForInstagram = { backgroundAssetUri: Uri?, type: String? ->
        Intent("com.instagram.share.ADD_TO_STORY")
            .setDataAndType(backgroundAssetUri, type)
            .setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
    }

    /**
     * @see <a href="https://developers.facebook.com/docs/sharing/sharing-to-stories/android-developers/">Sharing stories to facebook</a>
     */
    private val intentForFaceBook = { backgroundAssetUri: Uri?, type: String? ->
        Intent("com.facebook.stories.ADD_TO_STORY")
            .setDataAndType(backgroundAssetUri, type)
            .setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            .putExtra(
                "com.facebook.platform.extra.APPLICATION_ID",
                BobbleCoreSDK.crossAppInterface.getAppElement(AppElements.FACEBOOK_ID)
            )
    }

    /**
     * Can't find any api for messenger stories, So hardcoded share intent for messenger stories
     */
    private val intentForMessenger = { uri: Uri?, type: String? ->
        val name = ComponentName(
            "com.facebook.orca",
            "com.facebook.messenger.intents.MediaEditShareIntentHandler"
        )
        Intent(Intent.ACTION_SEND)
            .setType(type)
            .putExtra(Intent.EXTRA_STREAM, uri)
            .setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            .setComponent(name)
            .apply {
                //Fix for Android 13
                if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.S_V2) {
                    addCategory(Intent.CATEGORY_LAUNCHER)
                }
            }
    }

    /**
     * @return if package name is empty create a chooser or else intent with action send for the package name
     */
    private val defaultShareIntent =
        { packageName: String?, shareUri: Uri?, shareText: String?, type: String? ->
            Intent(Intent.ACTION_SEND)
                .putExtra(Intent.EXTRA_STREAM, shareUri)
                .putExtra(Intent.EXTRA_TEXT, shareText)
                .setType(type)
                .setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                .run {
                    if (packageName.isNullOrBlank() || (packageName.isNotEmpty() && packageName == MORE))
                        Intent.createChooser(this, "Share via")
                    else
                        this.setPackage(packageName)
                }
        }
}