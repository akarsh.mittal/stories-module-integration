package com.touchtalent.bobblesdk.stories_ui.utils

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager

//package name constants
const val WHATSAPP = "com.whatsapp"
const val INSTAGRAM = "com.instagram.android"
const val FACEBOOK = "com.facebook.katana"
const val FACEBOOK_MESSENGER = "com.facebook.orca"

//custom package name constants
const val MORE = "more"

fun showKeyboard(activity: Activity, show: Boolean) {
    val view: View? = activity.getCurrentFocus()
    try {
        val imgr = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        if (show) imgr.toggleSoftInput(
            InputMethodManager.SHOW_FORCED,
            InputMethodManager.HIDE_IMPLICIT_ONLY
        ) else if (view != null) imgr.hideSoftInputFromWindow(view.windowToken, 0)
    } catch (e: Exception) {
        e.printStackTrace()
    }
}