package com.touchtalent.bobblesdk.stories_ui.ui.fragments

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import com.touchtalent.bobblesdk.stories.data.model.api.Trends
import com.touchtalent.bobblesdk.stories_ui.R
import com.touchtalent.bobblesdk.stories_ui.databinding.FragmentSearchStoryBinding
import com.touchtalent.bobblesdk.stories_ui.domain.enums.Resource
import com.touchtalent.bobblesdk.stories_ui.ui.activity.findNavControllerSafely
import com.touchtalent.bobblesdk.stories_ui.ui.model.StoriesViewModel
import com.touchtalent.bobblesdk.stories_ui.ui.stories.SearchSuggestionAdapter
import com.touchtalent.bobblesdk.stories_ui.utils.showKeyboard
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch


class SearchStoryFragment : Fragment() {

    private lateinit var binding: FragmentSearchStoryBinding
    private val model: StoriesViewModel by activityViewModels()
    private var adapter: SearchSuggestionAdapter? = null
    private var searchJob: Job? = null
    private var trendsList = emptyList<Trends>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSearchStoryBinding.inflate(inflater, container, false)
        setupEditTextListener()
        viewLifecycleOwner.lifecycleScope.launch {
            listenToTrendingSearches()
        }
        binding.searchBack.setOnClickListener {
            activity?.finish()
        }
        return binding.root
    }

    private fun setupEditTextListener() {
        binding.searchEdit.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun afterTextChanged(p0: Editable?) {
                searchJob?.cancel()
                searchJob = lifecycleScope.launch {
                    delay(500)
                    if (!p0.isNullOrEmpty()) {
                        model.loadSearches(p0.toString())
                    } else {
                        model.loadTrendingSearches()
                    }
                }
            }

        })

        binding.searchEdit.setOnEditorActionListener { v, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                val searchString = v.text.toString().trim()
                if (searchString.isNotEmpty()) {
                    onSearchButtonClick(searchString)
                }
            }
            false
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val keyword = model.getSearchedKeyword()
        if (keyword.isNotEmpty()) {
            binding.searchEdit.setHint("")
        }
        binding.searchEdit.post {
            binding.searchEdit.setText(keyword)
            binding.searchEdit.requestFocus()
            binding.searchEdit.setSelection(binding.searchEdit.length())
        }
        activity?.let { showKeyboard(it, true) }

    }

    private suspend fun listenToTrendingSearches() {
        model.trendingSearches.collect { result ->
            when (result) {
                is Resource.Success -> {
                    if ((result.data?.size ?: 0) > 0) {
                        adapter = SearchSuggestionAdapter {
                            onSuggestionClick(it)
                        }
                        trendsList = result.data.orEmpty()
                        adapter?.submitList(trendsList)
                        binding.searchRecycler.adapter = adapter
                        binding.errorImage.visibility = View.GONE
                        binding.errorText.visibility = View.GONE
                        binding.searchRecycler.visibility = View.VISIBLE
                    } else {
                        showErrorView(false)
                    }
                }
                else -> {
                    showErrorView(true)
                }
            }
        }
    }

    private fun showErrorView(canShowLogSearchFailed: Boolean) {
        if (model.getSearchedKeyword().isNotBlank()) {
            binding.errorImage.visibility = View.VISIBLE
            binding.errorText.visibility = View.VISIBLE
            binding.searchRecycler.visibility = View.GONE
            if (canShowLogSearchFailed) {
                model.logSearchFailed()
            }
        }
    }

    private fun onSuggestionClick(trends: Trends?) {
        model.loadSearchResults(trends)
        activity?.let { showKeyboard(it, false) }
        findNavControllerSafely(R.id.searchStoryFragment)?.navigate(R.id.action_searchStoryFragment_to_searchResultFragment)
    }

    private fun onSearchButtonClick(searchString: String) {
        model.loadSearchResults(Trends(SearchSuggestionAdapter.TYPE_SAVED_KEYWORD, searchString))
        activity?.let { showKeyboard(it, false) }
        findNavControllerSafely(R.id.searchStoryFragment)?.navigate(R.id.action_searchStoryFragment_to_searchResultFragment)
    }

    override fun onPause() {
        super.onPause()
        activity?.let { showKeyboard(it, false) }
    }
}