package com.touchtalent.bobblesdk.stories_ui.ui.activity

import android.content.Context
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Environment
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.touchtalent.bobblesdk.core.BobbleCoreSDK
import com.touchtalent.bobblesdk.core.BobbleCoreSDK.applicationContext
import com.touchtalent.bobblesdk.core.utils.GeneralUtils
import com.touchtalent.bobblesdk.stories_ui.R
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.text.DecimalFormat
import java.util.*
import kotlin.math.ln
import kotlin.math.pow

class StoriesUtil {

    fun showReportDialog(
        ctx: Context,
        onPositiveClick: (String) -> Unit
    ): AlertDialog.Builder {
        val li = LayoutInflater.from(ctx)
        val promptsView: View = li.inflate(R.layout.report_dialog, null)
        val alertDialog = AlertDialog.Builder(ctx)
        alertDialog.setTitle(R.string.report)
        alertDialog.setMessage(R.string.report_details)
        alertDialog.setView(promptsView)
        val feedback = promptsView
            .findViewById<View>(R.id.edittext) as EditText
        alertDialog.setPositiveButton(
            ctx.getString(R.string.send_l).uppercase(Locale.getDefault())
        ) { dialog, which ->
            if (!feedback.text.toString().isEmpty()
                && feedback.text.toString().length > 0 && !feedback.text.toString()
                    .matches("[_ ]+".toRegex())
            ) {
                dialog.cancel()
                onPositiveClick.invoke(feedback.text.toString())

            } else if (feedback.text.toString().length == 0 || feedback.text.toString()
                    .matches("[_ ]+".toRegex())
            ) {
                GeneralUtils.showToast(ctx, ctx.getString(R.string.empty_feedback))
            }
        }
        alertDialog.setNegativeButton(
            ctx.getString(R.string.cancel_l).uppercase(Locale.getDefault())
        ) { dialog, which -> dialog.cancel() }
        alertDialog.show()
        return alertDialog
    }


    suspend fun getDownloadedImageBobbleStories(): List<Uri> = withContext(Dispatchers.IO) {
        val uriList: MutableList<Uri> = ArrayList()
        val projection = arrayOf(
            MediaStore.Images.ImageColumns._ID,
            MediaStore.Images.ImageColumns.DATA
        )
        val selectionArgs = arrayOf(
            "%" + Environment.DIRECTORY_PICTURES + "/" + BobbleCoreSDK.crossAppInterface.getAppName() + " Stories%"
        )
        val sortOrder: String? = null
        val cursor = applicationContext.contentResolver.query(
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
            projection,
            MediaStore.Images.ImageColumns.DATA + " LIKE ?",
            selectionArgs,
            sortOrder
        )
        try {
            while (cursor?.moveToNext() == true) {
                uriList.add(
                    Uri.parse(
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI.toString() + "/" + cursor.getString(
                            0
                        )
                    )
                )
            }
        } finally {
            cursor?.close()
        }
        uriList
    }

    suspend fun getDownloadedVideoBobbleStories(): List<Uri> = withContext(Dispatchers.IO) {
        val uriList: MutableList<Uri> = ArrayList()
        val projection = arrayOf(
            MediaStore.Video.VideoColumns._ID,
            MediaStore.Video.VideoColumns.DATA
        )
        val selectionArgs = arrayOf(
            "%" + Environment.DIRECTORY_MOVIES + "/" + BobbleCoreSDK.crossAppInterface.getAppName() + " Stories%"
        )
        val sortOrder: String? = null
        val cursor = applicationContext.contentResolver.query(
            MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
            projection,
            MediaStore.Video.VideoColumns.DATA + " LIKE ?",
            selectionArgs,
            sortOrder
        )
        try {
            while (cursor?.moveToNext() == true) {
                uriList.add(
                    Uri.parse(
                        MediaStore.Video.Media.EXTERNAL_CONTENT_URI.toString() + "/" + cursor.getString(
                            0
                        )
                    )
                )
            }
        } finally {
            cursor?.close()
        }
        uriList
    }

    suspend fun getDownloadedImageWithId(id: Int): Boolean = withContext(Dispatchers.IO) {
        var isVideoAvailable = false
        val projection = arrayOf(
            MediaStore.Images.ImageColumns._ID,
            MediaStore.Images.ImageColumns.DISPLAY_NAME
        )
        val selectionArgs = arrayOf(
            "%" + Environment.DIRECTORY_PICTURES + "/" + BobbleCoreSDK.crossAppInterface.getAppName() + " Stories%_$id%"
        )
        val sortOrder: String? = null
        val cursor = applicationContext.contentResolver.query(
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
            projection,
            MediaStore.Images.ImageColumns.DATA + " LIKE ?",
            selectionArgs,
            sortOrder
        )
        try {
            while (cursor?.moveToNext() == true) {
                isVideoAvailable = true
            }
        } finally {
            cursor?.close()
        }
        isVideoAvailable
    }

    suspend fun getDownloadedVideoWithId(id: Int): Boolean = withContext(Dispatchers.IO) {
        var isVideoAvailable = false
        val projection = arrayOf(
            MediaStore.Video.VideoColumns._ID,
            MediaStore.Video.VideoColumns.DISPLAY_NAME
        )
        val selectionArgs = arrayOf(
            "%" + Environment.DIRECTORY_MOVIES + "/" + BobbleCoreSDK.crossAppInterface.getAppName() + " Stories%_$id%"
        )
        val sortOrder: String? = null
        val cursor = applicationContext.contentResolver.query(
            MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
            projection,
            MediaStore.Video.VideoColumns.DATA + " LIKE ?",
            selectionArgs,
            sortOrder
        )
        try {
            while (cursor?.moveToNext() == true) {
                isVideoAvailable = true
                break
            }
        } finally {
            cursor?.close()
        }
        isVideoAvailable
    }
}

/**@return formatted numbers with appropriate suffixes
 * example: 1000 -> 1K
 *          1000000 -> 1M
 */
fun Int.formatNumberWithSuffix(): String {
    if (this < 1000) return "" + this
    val exp = (ln(this.toDouble()) / ln(1000.0)).toInt()
    val format = DecimalFormat("0.#")
    val value: String = format.format(this / 1000.0.pow(exp.toDouble()))
    return String.format("%s%c", value, "kMBTPE"[exp - 1])
}

fun Fragment.findNavControllerSafely(id: Int): NavController? {
    return if (findNavController().currentDestination?.id == id) {
        findNavController()
    } else {
        null
    }
}

fun Context.hideSoftKeyboard(view: View) {
    val imm = this.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(view.windowToken, 0)
}

fun isAppInstalled(context: Context, uri: String): Boolean {
    val pm = context.packageManager
    val appInstalled: Boolean = try {
        pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES)
        true
    } catch (e: PackageManager.NameNotFoundException) {
        e.printStackTrace()
        false
    }
    return appInstalled
}

/**
 * Checks if WhatsApp/WhatsAppBusiness app is installed, via PackageManager
 *
 * @return true : if the targeted device has WhatsApp app installed
 * false : if the targeted device does not have WhatsApp app installed
 */
fun isWhatsAppPresent(context: Context): Boolean {
    return try {
        isAppInstalled(context, "com.whatsapp") || isAppInstalled(context, "com.whatsapp.w4b")
    } catch (e: Exception) {
        e.printStackTrace()
        false
    }
}