package com.touchtalent.bobblesdk.stories_ui.ui.stories

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.android.exoplayer2.ExoPlayer
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.ui.StyledPlayerView
import com.touchtalent.bobblesdk.stories_ui.databinding.ItemSavedStoriesBinding
import com.touchtalent.bobblesdk.stories_ui.databinding.ItemSavedStoryVideoBinding
import com.touchtalent.bobblesdk.stories_ui.domain.enums.SavedStory
import com.touchtalent.bobblesdk.stories_ui.ui.fragments.SavedStoriesFragment


class SavedStoriesAdapter(
    val onClick: (SavedStory) -> Unit,
    val onVideoComplete: () -> Unit
) :

    ListAdapter<SavedStory, RecyclerView.ViewHolder>(SAVED_STORY_DIFFUTIL) {

    override fun getItemViewType(position: Int): Int {
        return if (getItem(position).type.equals(TYPE_IMAGE))
            VIEW_TYPE_IMAGE
        else
            VIEW_TYPE_VIDEO

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == VIEW_TYPE_IMAGE)
            SavedStoriesImageViewholder(
                ItemSavedStoriesBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
        else
            SavedStoriesVideoViewholder(
                ItemSavedStoryVideoBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is SavedStoriesImageViewholder -> {
                holder.bind(getItem(position))
            }
            is SavedStoriesVideoViewholder -> {
                holder.bind(getItem(position))
            }
        }

    }

    companion object {
        val TYPE_IMAGE = "image"
        val TYPE_VIDEO = "video"

        val VIEW_TYPE_IMAGE = 0
        val VIEW_TYPE_VIDEO = 1

        private val SAVED_STORY_DIFFUTIL =
            object : DiffUtil.ItemCallback<SavedStory>() {
                override fun areItemsTheSame(
                    oldItem: SavedStory,
                    newItem: SavedStory
                ): Boolean {
                    return oldItem.uri == newItem.uri
                }

                override fun areContentsTheSame(
                    oldItem: SavedStory,
                    newItem: SavedStory
                ): Boolean {
                    return oldItem == newItem
                }

            }
    }

    override fun onViewRecycled(holder: RecyclerView.ViewHolder) {
        if (holder is SavedStoriesVideoViewholder) {
            holder.releasePlayer()
        }
    }

    inner class SavedStoriesImageViewholder(val binding: ItemSavedStoriesBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(story: SavedStory) {
            Glide.with(itemView.context).load(story.uri).into(binding.contentView)
            binding.root.setOnClickListener {
                onClick.invoke(story)
            }
        }

    }

    inner class SavedStoriesVideoViewholder(val binding: ItemSavedStoryVideoBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private var player = ExoPlayer.Builder(binding.root.context).build()
        fun bind(story: SavedStory) {
            binding.contentImage.visibility = View.VISIBLE
            Glide.with(itemView.context).load(story.uri).into(binding.contentImage)
            if (layoutPosition != SavedStoriesFragment.videoPlayIndex) {
            } else {

                player = getExoPlayer(binding.contentView, story.uri)
                player.prepare()
                player.play()
                player.addListener(object : Player.Listener {
                    override fun onPlaybackStateChanged(playbackState: Int) {
                        if (playbackState == ExoPlayer.STATE_READY) {
                            binding.contentImage.visibility = View.GONE
                        }
                        if (playbackState == ExoPlayer.STATE_ENDED) {
                            player.stop()
                            player.release()
                            onVideoComplete.invoke()
                        }
                    }
                })
            }

            binding.root.setOnClickListener {
                player.stop()
                player.release()
                onClick.invoke(story)
            }

        }

        fun releasePlayer() {
            player?.stop()
            player?.release()
        }

        fun getExoPlayer(exoPlayerView: StyledPlayerView, url: Uri): ExoPlayer {
            return ExoPlayer.Builder(exoPlayerView.context)
                .build()
                .also { exoPlayer ->
                    exoPlayerView.player = exoPlayer
                    val mediaItem = MediaItem.fromUri(url)
                    exoPlayer.playWhenReady = true
                    exoPlayer.volume = 0f
                    exoPlayer.setMediaItem(mediaItem)
                    exoPlayer.repeatMode = Player.REPEAT_MODE_OFF
                    exoPlayerView.player?.play()
                    exoPlayer.play()
                }
        }
    }


}