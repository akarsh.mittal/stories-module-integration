package com.touchtalent.bobblesdk.stories_ui.ui.views

import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import com.touchtalent.bobblesdk.stories.data.model.api.CtaDetailsItems
import com.touchtalent.bobblesdk.stories_ui.R
import com.touchtalent.bobblesdk.stories_ui.databinding.VerticalStoryBottomBarBinding
import com.touchtalent.bobblesdk.stories_ui.domain.enums.*

class VerticalStoryBottomBarView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null
) : ConstraintLayout(context, attrs) {

    private val binding = VerticalStoryBottomBarBinding.inflate(LayoutInflater.from(context), this)
    var playerListener: ((String, String?) -> Unit)? = null
        set(value) {
            binding.download.setOnClickListener {
                playerListener?.invoke(TYPE_DOWNLOAD_STORY, null)
            }
            binding.headIcon.setOnClickListener {
                playerListener?.invoke(TYPE_EDIT_STORY, null)
            }
            field = value
        }

    var moreOptionsClick: ((View) -> Unit)? = null
        set(value) {
            binding.moreOptions.setOnClickListener {
                moreOptionsClick?.invoke(it)
            }
            field = value
        }

    fun setCtaButtons(ctaDetails: List<CtaDetailsItems>?, shares: String) {
        ctaDetails?.let {
            if (ctaDetails.size >= 1) {
                binding.button2.visibility = GONE
                if (ctaDetails.get(0).type.equals(TYPE_SHARE_STORY_WHATSAPP)) {
                    binding.button1Title.text = shares
                } else {
                    binding.button1Title.text = ctaDetails.get(0).text ?: ""
                }
                binding.button1Title.setTextColor(
                    Color.parseColor(
                        ctaDetails.get(0).normalStateDetails?.textColor ?: "#000000"
                    )
                )
                binding.button1.backgroundTintList =
                    ColorStateList.valueOf(
                        Color.parseColor(
                            ctaDetails.get(0).normalStateDetails?.backgroundColor ?: "#000000"
                        )
                    )
                binding.button1Icon.setImageResource(getImageId(ctaDetails.get(0)))
                binding.button1.setOnClickListener {
                    playerListener?.invoke(
                        ctaDetails.get(0).type ?: TYPE_BLANK,
                        ctaDetails.get(0).url
                    )
                }
            }
            if (ctaDetails.size >= 2) {
                binding.button2.visibility = VISIBLE
                if (ctaDetails.get(1).type.equals(TYPE_SHARE_STORY_WHATSAPP)) {
                    binding.button2Title.text = shares
                } else {
                    binding.button2Title.text = ctaDetails.get(1).text ?: ""
                }
                binding.button2Title.setTextColor(
                    Color.parseColor(
                        ctaDetails.get(1).normalStateDetails?.textColor ?: "#000000"
                    )
                )
                binding.button2.backgroundTintList =
                    ColorStateList.valueOf(
                        Color.parseColor(
                            ctaDetails.get(1).normalStateDetails?.backgroundColor ?: "#000000"
                        )
                    )
                binding.button2Icon.setImageResource(getImageId(ctaDetails.get(1)))
                binding.button2.setOnClickListener {
                    playerListener?.invoke(
                        ctaDetails.get(1).type ?: TYPE_BLANK,
                        ctaDetails.get(1).url
                    )
                }
            }
        }

    }

    private fun getImageId(cta: CtaDetailsItems): Int {
        when (cta.type) {
            TYPE_SHARE_STORY_WHATSAPP -> {
                return R.drawable.whatsapp_icon
            }
            TYPE_EDIT_STORY -> {
                return R.drawable.ic_head_black
            }
            TYPE_DOWNLOAD_STORY -> {
                return R.drawable.ic_download_black
            }
            TYPE_SHARE_STORY -> {
                return R.drawable.ic_share_black
            }
            else -> {
                return R.drawable.ic_share_black
            }
        }
    }

    fun setDefaultButtons(shares: String) {
        binding.button1Title.text = shares
        binding.button1.setOnClickListener {
            playerListener?.invoke(TYPE_SHARE_STORY_WHATSAPP, null)
        }
        binding.button2.setOnClickListener {
            playerListener?.invoke(TYPE_SHARE_STORY, null)
        }
    }

}