package com.touchtalent.bobblesdk.stories_ui.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.recyclerview.widget.GridLayoutManager
import com.touchtalent.bobblesdk.content_core.interfaces.ContentRenderingContext
import com.touchtalent.bobblesdk.stories_ui.R
import com.touchtalent.bobblesdk.stories_ui.databinding.FragmentSearchResultBinding
import com.touchtalent.bobblesdk.stories_ui.domain.enums.Resource
import com.touchtalent.bobblesdk.stories_ui.domain.enums.VerticalStoryResource
import com.touchtalent.bobblesdk.stories_ui.ui.activity.BobbleStoriesActivity
import com.touchtalent.bobblesdk.stories_ui.ui.activity.findNavControllerSafely
import com.touchtalent.bobblesdk.stories_ui.ui.model.StoriesViewModel
import com.touchtalent.bobblesdk.stories_ui.ui.stories.SearchResultAdapter
import com.touchtalent.bobblesdk.stories_ui.utils.showKeyboard
import kotlinx.coroutines.launch

class SearchResultFragment : Fragment() {

    private lateinit var binding: FragmentSearchResultBinding
    private val model: StoriesViewModel by activityViewModels()
    private var adapter: SearchResultAdapter? = null
    private lateinit var renderingContext: ContentRenderingContext

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSearchResultBinding.inflate(inflater, container, false)
        renderingContext = (activity as BobbleStoriesActivity).renderingContext
        adapter = SearchResultAdapter(
            { onResultClick(it) }, renderingContext
        )

        binding.searchText.text = model.getSearchedKeyword()
        binding.searchText.setOnClickListener {
            navigate()
        }
        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                launch { listenToStoryList() }
            }
        }

        binding.searchBack.setOnClickListener {
            navigate()
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.let { showKeyboard(it, false) }
    }

    private suspend fun listenToStoryList() {
        model.verticalStoriesList.collect { result ->
            when (result) {
                is Resource.Success -> {
                    if (result is VerticalStoryResource.FirstLoad) {
                        binding.progress.visibility = View.GONE
                        adapter?.submitList(result.data)
                        binding.searchRecycler.visibility = View.VISIBLE
                        binding.searchRecycler.layoutManager = GridLayoutManager(context, 2)
                        binding.searchRecycler.adapter = adapter
                        model.logStoryViewedEvent(result.data)
                    }
                }
                is Resource.Loading -> binding.progress.visibility = View.VISIBLE
                is Resource.Error -> {
                    binding.progress.visibility = View.GONE
                    binding.errorImage.visibility = View.VISIBLE
                    binding.errorText.visibility = View.VISIBLE
                    binding.searchRecycler.visibility = View.GONE

                }
                else -> {}
            }
        }
    }

    private fun onResultClick(position: Int) {
        val bundle = Bundle()
        bundle.putInt("position", position)
        findNavControllerSafely(R.id.searchResultFragment)?.navigate(
            R.id.action_searchResultFragment_to_verticalStoriesFragment,
            bundle
        )
    }

    private fun navigate(){
        val navHostFragment =
            activity?.supportFragmentManager?.findFragmentById(R.id.story_nav_host_fragment)
        if (navHostFragment?.childFragmentManager?.backStackEntryCount == 0) {
            findNavControllerSafely(R.id.searchResultFragment)?.navigate(R.id.action_searchResultFragment_to_searchStoryFragment)
        } else {
            findNavControllerSafely(R.id.searchResultFragment)?.navigateUp()
        }
    }
}
