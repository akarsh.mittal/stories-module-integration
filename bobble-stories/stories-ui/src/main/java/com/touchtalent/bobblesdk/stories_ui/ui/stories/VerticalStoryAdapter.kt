package com.touchtalent.bobblesdk.stories_ui.ui.stories

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.touchtalent.bobblesdk.content_core.interfaces.ContentRenderingContext
import com.touchtalent.bobblesdk.content_core.model.Story
import com.touchtalent.bobblesdk.stories_ui.ui.views.StoryPlayerView
import com.touchtalent.bobblesdk.stories_ui.ui.views.VerticalStoryPlayerView

class VerticalStoryAdapter(
    fa: Fragment,
    val renderingContext: ContentRenderingContext
) : FragmentStateAdapter(fa) {

    private var storyList = ArrayList<Story>()
    private var storyListener: StoryPlayerView.StoryPlayerListener? = null
    public var startPosition = 0

    override fun getItemCount(): Int {
        return storyList.size
    }

    override fun createFragment(position: Int): Fragment {
        if (startPosition != 0 && position == 0) {
            startPosition = 0
            return Fragment()
        } else {
            val fr = VerticalStoryPlayerView()
            fr.story = storyList[position]
            fr.storyPlayerListener = storyListener
            fr.renderingContext = renderingContext
            return fr
        }
    }

    fun addAllStories(list: List<Story>) {
        val lastindex = storyList.size
        storyList.addAll(list)
        notifyItemRangeChanged(lastindex, list.size)
    }

    fun setStoryListener(listener: StoryPlayerView.StoryPlayerListener) {
        storyListener = listener
    }

}