package com.touchtalent.bobblesdk.stories_ui.ui.fragments

import android.graphics.Color
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.bumptech.glide.Glide
import com.touchtalent.bobblesdk.content_core.interfaces.ContentRenderingContext
import com.touchtalent.bobblesdk.content_core.interfaces.stories.StoryUIController
import com.touchtalent.bobblesdk.content_core.model.Story
import com.touchtalent.bobblesdk.core.utils.GeneralUtils
import com.touchtalent.bobblesdk.stories_ui.R
import com.touchtalent.bobblesdk.stories_ui.databinding.FragmentShareStoryBinding
import com.touchtalent.bobblesdk.stories_ui.domain.enums.Resource
import com.touchtalent.bobblesdk.stories_ui.ui.activity.BobbleStoriesActivity
import com.touchtalent.bobblesdk.stories_ui.ui.activity.findNavControllerSafely
import com.touchtalent.bobblesdk.stories_ui.ui.model.StoriesViewModel
import com.touchtalent.bobblesdk.stories_ui.ui.stories.SavedStoriesAdapter
import com.touchtalent.bobblesdk.stories_ui.utils.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class
ShareStoryFragment : Fragment() {
    private val sharedViewModel: StoriesViewModel by activityViewModels()

    private lateinit var binding: FragmentShareStoryBinding
    private lateinit var renderingContext: ContentRenderingContext

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentShareStoryBinding.inflate(inflater, container, false)
        renderingContext = (activity as BobbleStoriesActivity).renderingContext
        binding.bobbleContent.contentRenderingContext = renderingContext
        if (sharedViewModel.getSource() == StoryUIController.Source.SAVED_STORIES.ordinal) {
            binding.bobbleContent.visibility = View.GONE
            if (arguments?.getString("saved_story_type")?.equals(SavedStoriesAdapter.TYPE_IMAGE)
                    ?: true
            ) {

                binding.bobbleContentImage.visibility = View.VISIBLE
                binding.bobbleContentVideo.visibility = View.GONE
                context?.let {
                    Glide.with(it).load(Uri.parse(arguments?.getString("uri")))
                        .into(binding.bobbleContentImage)
                }
            } else {
                binding.bobbleContentVideo.visibility = View.VISIBLE
                binding.bobbleContentImage.visibility = View.GONE
                binding.bobbleContentVideo.setVideoURI(Uri.parse(arguments?.getString("uri")))
                binding.bobbleContentVideo.start()
                binding.bobbleContentVideo.setOnErrorListener(
                    object : MediaPlayer.OnErrorListener {
                        override fun onError(p0: MediaPlayer?, p1: Int, p2: Int): Boolean {
                            return true
                        }

                    }
                )
                binding.bobbleContentVideo.setOnPreparedListener {
                    it.setVolume(0f, 0f)
                }
            }
            binding.close.setOnClickListener {
                findNavControllerSafely(R.id.shareStoryFragment)?.navigateUp()
            }
            setUpSavedStory(Uri.parse(arguments?.getString("uri", "")))
            lifecycleScope.launch {
                repeatOnLifecycle(Lifecycle.State.CREATED) {
                    launch { listenToShowToast() }
                    launch { listenToShareIntent() }
                }
            }
        } else {
            binding.bobbleContent.visibility = View.VISIBLE
            binding.bobbleContentVideo.visibility = View.GONE
            binding.bobbleContentImage.visibility = View.GONE

            lifecycleScope.launch {
                repeatOnLifecycle(Lifecycle.State.CREATED) {
                    launch { listenToShowToast() }
                    launch { listenToStoryStatus() }
                    launch { listenToShareIntent() }
                    launch { listenToVerticalStoryStatus() }
                }
            }
            binding.close.setOnClickListener { sharedViewModel.resumeStory() }
            handleBackPress()
        }
        if (sharedViewModel.getSource() == StoryUIController.Source.VERTICAL_STORIES.ordinal
            || sharedViewModel.getSource() == StoryUIController.Source.SAVED_STORIES.ordinal
        ) {
            changeTheme()
        }

        return binding.root
    }

    private fun setUpSavedStory(uri: Uri) {
        binding.apply {
            val mimeType =
                if (arguments?.getString("saved_story_type")?.equals(SavedStoriesAdapter.TYPE_IMAGE)
                        ?: true
                )
                    "image/png" else "video/mp4"
            val audio =
                if (arguments?.getString("saved_story_type")?.equals(SavedStoriesAdapter.TYPE_IMAGE)
                        ?: true
                ) 0 else 1

            shareApp1.setOnClickListener {
                sharedViewModel.shareSavedStory(WHATSAPP, uri, mimeType, audio)
            }
            shareApp2.setOnClickListener {
                sharedViewModel.shareSavedStory(INSTAGRAM, uri, mimeType, audio)
            }
            shareApp3.setOnClickListener {
                sharedViewModel.shareSavedStory(FACEBOOK, uri, mimeType, audio)
            }
            shareApp4.setOnClickListener {
                sharedViewModel.shareSavedStory(FACEBOOK_MESSENGER, uri, mimeType, audio)
            }
            bgMore.setOnClickListener {
                sharedViewModel.shareSavedStory(MORE, uri, mimeType, audio)
            }

        }
    }

    private fun changeTheme() {
        binding.apply {
            root.setBackgroundColor(Color.parseColor("#ffffff"))
            appCompatTextView.setTextColor(Color.parseColor("#000000"))
            appCompatTextView2.setTextColor(Color.parseColor("#000000"))
            appCompatTextView3.setTextColor(Color.parseColor("#000000"))
            shareApp1Text.setTextColor(Color.parseColor("#000000"))
            shareApp2Text.setTextColor(Color.parseColor("#000000"))
            shareApp3Text.setTextColor(Color.parseColor("#000000"))
            shareApp4Text.setTextColor(Color.parseColor("#000000"))
            shareAppMore.visibility = View.INVISIBLE
            shareAppMoreBlack.visibility = View.VISIBLE
            bgMore.visibility = View.VISIBLE
            close.setImageResource(R.drawable.ic_back_black)

        }
    }

    private fun handleBackPress() {
        val callback: OnBackPressedCallback =
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    sharedViewModel.resumeStory()
                }
            }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)
    }

    private suspend fun Story.onIconClick(packageName: String? = null) {
        if (this is Story.ContentStory) {
            val contentOutput = this@ShareStoryFragment.renderingContext.export(this.bobbleStory)
            sharedViewModel.share(packageName, contentOutput.getOrNull())
        } else if (this is Story.VerticalStory) {
            val contentOutput = this@ShareStoryFragment.renderingContext.export(this.bobbleStory)
            sharedViewModel.share(packageName, contentOutput.getOrNull())
        }
    }

    private suspend fun setUpStory(story: Story) {
        if (story !is Story.ContentStory && story !is Story.VerticalStory)
            return
        with(binding) {
            shareApp1.setOnClickListener {
                lifecycleScope.launch { story.onIconClick(WHATSAPP) }
            }
            shareApp2.setOnClickListener {
                lifecycleScope.launch { story.onIconClick(INSTAGRAM) }
            }
            shareApp3.setOnClickListener {
                lifecycleScope.launch { story.onIconClick(FACEBOOK) }
            }
            shareApp4.setOnClickListener {
                lifecycleScope.launch { story.onIconClick(FACEBOOK_MESSENGER) }
            }
            if (sharedViewModel.getSource() == StoryUIController.Source.VERTICAL_STORIES.ordinal) {
                bgMore.setOnClickListener { lifecycleScope.launch { story.onIconClick(MORE) } }
            } else {
                shareAppMore.setOnClickListener { lifecycleScope.launch { story.onIconClick(MORE) } }
            }
            if (story is Story.ContentStory) {
                (storyContainer.layoutParams as ConstraintLayout.LayoutParams).dimensionRatio =
                    story.bobbleStory.aspectRatio

                bobbleContent.contentRenderingContext = renderingContext
                bobbleContent.setPlayableContent(
                    story.bobbleStory,
                    story.contentMetadata?.apply {
                        showLoader = false
                    }
                ).collect()
            } else if (story is Story.VerticalStory) {
                (storyContainer.layoutParams as ConstraintLayout.LayoutParams).dimensionRatio =
                    story.bobbleStory.aspectRatio

                bobbleContent.contentRenderingContext = renderingContext
                bobbleContent.setPlayableContent(
                    story.bobbleStory,
                    story.contentMetadata?.apply {
                        showLoader = false
                    }
                ).collect()
            }
        }
    }

    private suspend fun listenToStoryStatus() {
        sharedViewModel.currentStory.collectLatest {
            when (it) {
                is Resource.Success -> findNavControllerSafely(R.id.shareStoryFragment)?.popBackStack()
                is Resource.Sharing -> setUpStory(it.data)
                else -> {}
            }
        }
    }

    private suspend fun listenToVerticalStoryStatus() {
        sharedViewModel.verticalStoriesList.collectLatest {
            when (it) {
                is Resource.Success -> findNavControllerSafely(R.id.shareStoryFragment)?.popBackStack()
                else -> {}
            }
        }
    }

    private suspend fun listenToShowToast() {
        sharedViewModel.showToast.collectLatest { message ->
            GeneralUtils.showToast(
                activity,
                getString(R.string.story_app_not_installed)
            )
        }
    }
    private suspend fun listenToShareIntent() {
        sharedViewModel.shareIntent.collectLatest { intent ->
            startActivity(intent)
        }
    }

    override fun onPause() {
        super.onPause()
        if (sharedViewModel.getSource() != StoryUIController.Source.SAVED_STORIES.ordinal) {
            binding.bobbleContent.pause()
        }
    }

    override fun onResume() {
        super.onResume()
        if (sharedViewModel.getSource() != StoryUIController.Source.SAVED_STORIES.ordinal) {
            binding.bobbleContent.play()
        } else {
            binding.bobbleContentVideo.start()
        }
    }
}