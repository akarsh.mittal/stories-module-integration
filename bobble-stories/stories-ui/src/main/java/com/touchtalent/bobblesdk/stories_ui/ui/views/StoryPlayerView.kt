package com.touchtalent.bobblesdk.stories_ui.ui.views

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.viewbinding.ViewBinding
import com.touchtalent.bobblesdk.content_core.interfaces.ContentRenderingContext
import com.touchtalent.bobblesdk.content_core.interfaces.stories.StoryAdapter
import com.touchtalent.bobblesdk.content_core.model.Story
import com.touchtalent.bobblesdk.content_core.sdk.ContentCoreSDK
import com.touchtalent.bobblesdk.core.utils.setClickListenerWithPosition
import com.touchtalent.bobblesdk.stories_ui.domain.enums.TYPE_DOWNLOAD_STORY
import com.touchtalent.bobblesdk.stories_ui.ui.stories.ContentStoryAdapter
import com.touchtalent.bobblesdk.stories_ui.ui.stories.GridStoryAdapter
import com.touchtalent.bobblesdk.stories_ui.ui.stories.VerticalStoryViewAdapter
import kotlinx.coroutines.Job
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class StoryPlayerView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null
) : ConstraintLayout(context, attrs) {

    private var activeAdapterIdentifier: String? = null
    private var activeView: ViewBinding? = null
    private var activePlayJob: Job? = null
    private var activeStory: Story? = null
    private var isPausingStory = false
    private var adapter: StoryAdapter? = null
    private var oldAdapter: StoryAdapter? = null

    private val scope = MainScope()
    var playerListener: StoryPlayerListener? = null
    var onMoreOptionsClick: ((view: View, story: Story) -> Unit)? = null


    init {
        setupClickListener()
    }

    private fun setupClickListener() {
        setClickListenerWithPosition { x, _ ->
            val xPercentage = x / width
            when {
                isPausingStory -> {
                    resumeStory()
                    isPausingStory = false
                }

                xPercentage < 0.3 -> playerListener?.onPreviousClicked()
                xPercentage > 0.7 && xPercentage < 1 -> playerListener?.onNextClicked(true)
            }
        }

        setOnLongClickListener {
            pauseStory()
            isPausingStory = true
            return@setOnLongClickListener false
        }
    }

    suspend fun loadStory(story: Story, contentRenderingContext: ContentRenderingContext) {
        playerListener?.onStoryStatusUpdate(0)
        oldAdapter = adapter
        adapter = resolveStoryAdapter(story)?.apply {
            renderingContext = contentRenderingContext
            onClose = { playerListener?.onClose() }
            onShare = { playerListener?.onShare(it) }
            onDownload = { playerListener?.onDownload(it) }
            retryLoading = {
                playStory(story)
            }
        }
        if (adapter is GridStoryAdapter) {
            (adapter as GridStoryAdapter).onViewMoreClick = {
                playerListener?.onViewMoreClick()
            }
        }
        val identifier = adapter?.getIdentifier()
        if (activeAdapterIdentifier != identifier) {
            activeView?.let { oldAdapter?.onDestroyView(it, activeStory) }
            activeView = adapter?.onCreateView(this)?.apply {
                removeAllViews()
                addView(root)
            }
            activeAdapterIdentifier = adapter?.getIdentifier()
        }
        activeStory = story
        playStory(story)
    }

    private fun playStory(story: Story) {
        activeView?.let { binding ->
            activePlayJob?.cancel()
            activePlayJob = scope.launch {
                adapter?.play(binding, story)
                    ?.collectLatest {
                        playerListener?.onStoryStatusUpdate(it)
                        if (it == 100)
                            playerListener?.onNextClicked(false)
                    }

            }
        }
    }

    suspend fun playVerticalStory(story: Story, contentRenderingContext: ContentRenderingContext) {
        activeView?.let { binding ->
            adapter?.play(binding, story)
                ?.collectLatest {}
        }
    }

    suspend fun loadVerticalStory(story: Story, contentRenderingContext: ContentRenderingContext) {
        adapter = resolveStoryAdapter(story)?.apply {
            renderingContext = contentRenderingContext
            onShare = { playerListener?.onShare(it) }
            onDownload = { playerListener?.onDownload(it) }
        }
        if (adapter is VerticalStoryViewAdapter) {
            (adapter as VerticalStoryViewAdapter).playerListener = playerListener
            (adapter as VerticalStoryViewAdapter).onMoreOptionsClick =
                { view: View, story1: Story -> onMoreOptionsClick?.invoke(view, story1) }
            (adapter as VerticalStoryViewAdapter).onDownloadClick =
                { playerListener?.onButtonClick(story, TYPE_DOWNLOAD_STORY, null) }
        }
        activeView = adapter?.onCreateView(this)?.apply {
            removeAllViews()
            addView(root)
        }
        activeView?.let { binding ->
            adapter?.setStaticContent(binding, story)
        }
    }

    fun pauseStory() {
//        val adapter = activeStory?.let { resolveStoryAdapter(it) }
        activeView?.let { adapter?.pause(it, activeStory) }
    }

    private fun resumeStory() {
        adapter = if (activeStory !is Story.AddOnStory) {
            activeStory?.let { resolveStoryAdapter(it) }
        } else adapter
        activeView?.let { adapter?.resume(it, activeStory) }
    }

    fun togglePlayback(isPlaying: Boolean) {
        val adapter = activeStory?.let {
            resolveStoryAdapter(it)
        }
        activeView?.let { activeView ->
            if (isPlaying) {
                adapter?.pause(activeView)
            } else adapter?.resume(activeView)
        }
    }

    private fun resolveStoryAdapter(story: Story): StoryAdapter? {
        return when (story) {
            is Story.ContentStory -> ContentStoryAdapter()
            is Story.SummaryStory -> GridStoryAdapter()
            is Story.AddOnStory -> ContentCoreSDK.contentCoreConfig?.storyConfig?.adsAdapterFactory?.newAdapter()
            is Story.VerticalStory -> VerticalStoryViewAdapter()
            else -> null
        }
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        scope.cancel()
    }

    abstract class StoryPlayerListener {
        open fun onStoryStatusUpdate(progress: Int) {}
        open fun onNextClicked(manualClick: Boolean) {}
        open fun onPreviousClicked() {}
        open fun onPlaybackToggled() {}
        open fun onClose() {}
        open fun onDownload(story: Story) {}
        open fun onShare(story: Story) {}
        open fun onButtonClick(story: Story, type: String, url: String? = null) {}
        open fun muteVideo() {}
        open fun submitFeedback(feedback: String) {}
        open fun onViewMoreClick() {}
    }
}