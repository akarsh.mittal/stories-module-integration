package com.touchtalent.bobblesdk.stories_ui.ui.views

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import com.touchtalent.bobblesdk.content_core.interfaces.BobbleContent
import com.touchtalent.bobblesdk.content_core.interfaces.ContentRenderingContext
import com.touchtalent.bobblesdk.content_core.model.ContentMetadata
import com.touchtalent.bobblesdk.stories_ui.databinding.ViewStoriesBinding
import kotlinx.coroutines.flow.Flow

class ContentStoryView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null
) : ConstraintLayout(context, attrs) {

    private val binding = ViewStoriesBinding.inflate(LayoutInflater.from(context), this)

    private var _bobbleContent: BobbleContent? = null

    var crossTapListener: (() -> Unit)? = null
        set(value) {
            binding.close.setOnClickListener { value?.invoke() }
            field = value
        }

    var downloadListener: (()-> Unit)? = null
        set(value) {
            binding.storyShareBar.downloadListener = value
            field = value
        }

    var shareListener: (() -> Unit)? = null
        set(value) {
            binding.storyShareBar.shareListener = value
            field = value
        }

    fun setShareCount(value: String){
        binding.storyShareBar.shareStoryDescription.text = value
    }

    fun setPlayableContent(
        bobbleContent: BobbleContent,
        renderingContext: ContentRenderingContext,
        contentMetadata: ContentMetadata?
    ): Flow<Int> {
        this._bobbleContent = bobbleContent
        binding.bobbleContent.contentRenderingContext = renderingContext
        return binding.bobbleContent.setPlayableContent(bobbleContent, contentMetadata)
    }

    fun onViewDestroy(){
        binding.bobbleContent.onViewRecycled()
    }

    fun resume() {
        binding.bobbleContent.play()
    }

    fun pause() {
        binding.bobbleContent.pause()
    }
}