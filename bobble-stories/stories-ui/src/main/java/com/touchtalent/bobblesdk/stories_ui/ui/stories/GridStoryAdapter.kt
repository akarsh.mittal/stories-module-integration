package com.touchtalent.bobblesdk.stories_ui.ui.stories

import android.content.res.Resources
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.cardview.widget.CardView
import androidx.viewbinding.ViewBinding
import com.google.android.flexbox.FlexboxLayout
import com.touchtalent.bobblesdk.content_core.interfaces.stories.StoryAdapter
import com.touchtalent.bobblesdk.content_core.model.ContentMetadata
import com.touchtalent.bobblesdk.content_core.model.Story
import com.touchtalent.bobblesdk.content_core.sdk.BobbleContentView
import com.touchtalent.bobblesdk.core.utils.ViewUtil
import com.touchtalent.bobblesdk.core.utils.renderUrl
import com.touchtalent.bobblesdk.stories.data.pojo.AnimatedNonPersonalisedImageStory
import com.touchtalent.bobblesdk.stories_ui.databinding.GridStoriesBinding
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.takeWhile


class GridStoryAdapter : StoryAdapter() {

    private var currentIndex = 0
    var onViewMoreClick: (() -> Unit)? = null

    override fun getIdentifier(): String = "grid_content"

    override fun onCreateView(parent: ViewGroup): ViewBinding {
        val binding = GridStoriesBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        binding.close.setOnClickListener { onClose?.invoke() }
        onViewMoreClick?.let {
            binding.tvViewMore.visibility = View.VISIBLE
            binding.tvViewMore.setOnClickListener {
                onViewMoreClick?.invoke()
            }
        }
        return binding
    }

    override fun onDestroyView(viewBinding: ViewBinding, story: Story?) {}

    override suspend fun play(viewBinding: ViewBinding, story: Story): Flow<Int> {
        if (story !is Story.SummaryStory) error("Only SummaryStory allowed")
        if (viewBinding !is GridStoriesBinding) error("Only GridStoriesBinding allowed")
        val noOfItems = when (story.stories.size) {
            1 -> 1
            2 -> 2
            3 -> 2
            4 -> 2
            else -> 3
        }

        viewBinding.root.setOnClickListener {}
        viewBinding.flexboxlayout.removeAllViews()

        // add bobbleContentView on flexbox layout
        story.stories.forEach { bobbleStory ->
            val cardView = CardView(viewBinding.root.context)
            val bobbleContentView = BobbleContentView(viewBinding.root.context)
            renderingContext?.let { bobbleContentView.contentRenderingContext = it }

            val pMargin: Int = ViewUtil.dpToPx(12f, viewBinding.root.context)
            val margin: Int = ViewUtil.dpToPx(4f, viewBinding.root.context)
            val screenWidth: Int = Resources.getSystem().displayMetrics.widthPixels - 2 * pMargin

            val aspectRatio = bobbleStory.aspectRatio.split(":").map { it.toFloat() }
            val width = (screenWidth - margin * noOfItems * 2) / noOfItems
            val height = (width / aspectRatio[0] * aspectRatio[1]).toInt()

            val param = FlexboxLayout.LayoutParams(
                width,
                height
            )
            param.setMargins(margin, margin, margin, margin)
            cardView.layoutParams = param
            cardView.setCardBackgroundColor(Color.TRANSPARENT)
            cardView.radius = ViewUtil.dpToPx(8f, viewBinding.root.context).toFloat()
            // if video stories render the preview url
            if (bobbleStory is AnimatedNonPersonalisedImageStory) {
                val imageView = ImageView(viewBinding.root.context)
                bobbleStory.getPreviewUrl()?.let { imageView.renderUrl(it) }
                cardView.addView(imageView)
                cardView.addView(bobbleContentView)
            }
            //else render the bobble content
            else {
                bobbleContentView.setContent(bobbleStory, null)
                cardView.addView(bobbleContentView)
            }

            cardView.setOnClickListener {
                onShare?.invoke(Story.ContentStory(bobbleStory, null))
            }
            viewBinding.flexboxlayout.addView(cardView)
        }

        return flow {
            emit(100)
            //play the video story one by one
            story.stories.forEachIndexed { index, bobbleStory ->
                if (bobbleStory !is AnimatedNonPersonalisedImageStory) return@forEachIndexed
                val view: BobbleContentView =
                    (viewBinding.flexboxlayout.getChildAt(index) as CardView).getChildAt(1) as BobbleContentView
                currentIndex = index
                view.setPlayableContent(bobbleStory, ContentMetadata(mute = true))
                    .takeWhile { it != 100 }.collect()
                view.visibility = View.GONE
            }
        }
    }

    override fun resume(viewBinding: ViewBinding, story: Story?) {
        if (viewBinding !is GridStoriesBinding) error("Only GridStoriesBinding allowed")
        kotlin.runCatching {
            ((viewBinding.flexboxlayout.getChildAt(currentIndex) as CardView).getChildAt(1) as BobbleContentView).play()
        }
    }

    override fun pause(viewBinding: ViewBinding, story: Story?) {
        if (viewBinding !is GridStoriesBinding) error("Only GridStoriesBinding allowed")
        kotlin.runCatching {
            ((viewBinding.flexboxlayout.getChildAt(currentIndex) as CardView).getChildAt(1) as BobbleContentView).pause()
        }
    }


}