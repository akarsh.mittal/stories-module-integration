package com.touchtalent.bobblesdk.stories_ui.sdk

import android.content.Context
import androidx.annotation.Keep
import com.touchtalent.bobblesdk.content_core.interfaces.stories.StoryUIModule
import com.touchtalent.bobblesdk.core.config.BobbleCoreConfig
import com.touchtalent.bobblesdk.core.interfaces.BobbleModule
import com.touchtalent.bobblesdk.stories_ui.StoryDataControllerImpl
import com.touchtalent.bobblesdk.stories_ui.StoryUIControllerImpl

@Keep
object BobbleStoryUiSDK : BobbleModule(), StoryUIModule {
    lateinit var applicationContext: Context

    override fun getStoryUiController() = StoryUIControllerImpl

    override fun initialise(applicationContext: Context, config: BobbleCoreConfig) {
        this.applicationContext = applicationContext
    }

    override fun getCodeName(): String = "story-ui"
    override fun getStoryDataController() = StoryDataControllerImpl
}