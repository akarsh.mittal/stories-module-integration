package com.touchtalent.bobblesdk.stories_ui.domain.data

data class StoryStatus(val noOfStories:Int, val currentStory:Int, val progress:Int)