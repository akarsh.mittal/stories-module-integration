package com.touchtalent.bobblesdk.stories_ui.ui.stories

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.touchtalent.bobblesdk.content_core.interfaces.ContentRenderingContext
import com.touchtalent.bobblesdk.content_core.model.Story
import com.touchtalent.bobblesdk.stories_ui.databinding.ItemSearchResultBinding

class SearchResultAdapter(
    val onClick: (Int) -> Unit,
    val renderingContext: ContentRenderingContext
) :
    ListAdapter<Story, SearchResultAdapter.SearchResultViewholder>(SEARCH_RESULT_DIFFUTIL) {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): SearchResultAdapter.SearchResultViewholder {
        return SearchResultViewholder(
            ItemSearchResultBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(
        holder: SearchResultAdapter.SearchResultViewholder,
        position: Int
    ) {
        holder.bind(getItem(position))
    }

    companion object {
        private val SEARCH_RESULT_DIFFUTIL =
            object : DiffUtil.ItemCallback<Story>() {
                override fun areItemsTheSame(
                    oldItem: Story,
                    newItem: Story
                ): Boolean {
                    return oldItem.id == newItem.id
                }

                override fun areContentsTheSame(
                    oldItem: Story,
                    newItem: Story
                ): Boolean {
                    return oldItem.id == newItem.id
                }

            }
    }

    inner class SearchResultViewholder(val binding: ItemSearchResultBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Story) {
            if (item is Story.VerticalStory) {
                binding.contentView.contentRenderingContext = renderingContext
                item.contentMetadata?.showLoader = false
                binding.contentView.setContent(item.bobbleStory, item.contentMetadata)
                binding.root.setOnClickListener {
                    onClick.invoke(layoutPosition)
                }
            }
        }
    }
}