package com.touchtalent.bobblesdk.stories_ui.ui.stories

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.touchtalent.bobblesdk.stories.data.model.api.Trends
import com.touchtalent.bobblesdk.stories_ui.R
import com.touchtalent.bobblesdk.stories_ui.databinding.ItemSearchSuggestionBinding

class SearchSuggestionAdapter(val onClick: (Trends?) -> Unit) :
    ListAdapter<Trends, SearchSuggestionAdapter.SearchSuggestionViewholder>(
        SEARCH_SUGGESTION_DIFFUTIL
    ) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchSuggestionViewholder {
        return SearchSuggestionViewholder(
            ItemSearchSuggestionBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: SearchSuggestionViewholder, position: Int) {
        holder.bind(getItem(position))
    }


    override fun getItemCount(): Int {
        return if (currentList.size > 5)
            5
        else
            currentList.size
    }

    companion object {
        const val TYPE_SAVED_KEYWORD = 0
        const val TYPE_SERVER_KEYWORD = 1

        private val SEARCH_SUGGESTION_DIFFUTIL =
            object : DiffUtil.ItemCallback<Trends>() {
                override fun areItemsTheSame(
                    oldItem: Trends,
                    newItem: Trends
                ): Boolean {
                    return oldItem.keyword.equals(newItem.keyword)
                }

                override fun areContentsTheSame(
                    oldItem: Trends,
                    newItem: Trends
                ): Boolean {
                    return oldItem == newItem
                }

            }
    }

    inner class SearchSuggestionViewholder(val binding: ItemSearchSuggestionBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Trends) {
            if (item.type == TYPE_SERVER_KEYWORD) {
                binding.icon.setImageResource(R.drawable.ic_search_trending)
            } else {
                binding.icon.setImageResource(R.drawable.ic_search_history)
            }
            binding.text.text = item.keyword
            binding.root.setOnClickListener { onClick.invoke(item) }
        }
    }
}