package com.touchtalent.bobblesdk.stories_ui.ui.stories

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewbinding.ViewBinding
import com.touchtalent.bobblesdk.content_core.interfaces.stories.StoryAdapter
import com.touchtalent.bobblesdk.content_core.model.Story
import com.touchtalent.bobblesdk.stories.data.model.api.BobbleStoryImpl
import com.touchtalent.bobblesdk.stories_ui.databinding.VerticalStoryAdapterViewBinding
import com.touchtalent.bobblesdk.stories_ui.ui.activity.formatNumberWithSuffix
import com.touchtalent.bobblesdk.stories_ui.ui.views.StoryPlayerView
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.emptyFlow
import kotlinx.coroutines.launch


class VerticalStoryViewAdapter : StoryAdapter() {

    var playerListener: StoryPlayerView.StoryPlayerListener? = null
    var onMoreOptionsClick: ((view: View, story: Story) -> Unit)? = null
    var onDownloadClick: (() -> Unit)? = null
    private var job: Job? = null


    override fun getIdentifier(): String = "vertical-story"

    override fun onCreateView(parent: ViewGroup): ViewBinding {
        val binding = VerticalStoryAdapterViewBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )

        binding.bobbleContent.setOnClickListener {
            if ((job?.isActive ?: false) == true) {
                job?.cancel()
                onDownloadClick?.invoke()
            } else {
                job = GlobalScope.launch {
                    delay(500)
                    playerListener?.muteVideo()
                }
            }
        }

        return binding
    }

    override fun onDestroyView(viewBinding: ViewBinding, story: Story?) {
        if (viewBinding !is VerticalStoryAdapterViewBinding) error("Only VerticalStoryContentBinding allowed")
        viewBinding.bobbleContent.onViewRecycled()
    }

    override suspend fun play(viewBinding: ViewBinding, story: Story): Flow<Int>? {
        if (story !is Story.VerticalStory) error("Only VerticalContentStory allowed")
        if (viewBinding !is VerticalStoryAdapterViewBinding) error("Only VerticalStoryContentBinding allowed")

        viewBinding.image.visibility = View.VISIBLE
        renderingContext?.let {
            viewBinding.bobbleContent.contentRenderingContext = it
            viewBinding.bobbleContent.setPlayableContent(
                story.bobbleStory,
                story.contentMetadata
            ).collectLatest {
                if (it == 1) {
                    viewBinding.image.visibility = View.GONE
                }
            }
        }
        return emptyFlow()
    }

    override fun setStaticContent(viewBinding: ViewBinding, story: Story) {
        if (story !is Story.VerticalStory) error("Only VerticalContentStory allowed")
        if (viewBinding !is VerticalStoryAdapterViewBinding) error("Only VerticalStoryContentBinding allowed")

        if (story.bobbleStory is BobbleStoryImpl &&
            !(story.bobbleStory as BobbleStoryImpl).ctaDetails?.items.isNullOrEmpty()
        ) {
            viewBinding.bottomBar.setCtaButtons(
                (story.bobbleStory as BobbleStoryImpl).ctaDetails?.items,
                story.bobbleStory.noOfShares?.formatNumberWithSuffix() ?: "0"
            )
        } else {
            viewBinding.bottomBar.setDefaultButtons(
                story.bobbleStory.noOfShares?.formatNumberWithSuffix() ?: "0"
            )
        }
        viewBinding.bottomBar.playerListener = { type: String, url: String? ->
            playerListener?.onButtonClick(story, type, url)
        }
        viewBinding.bottomBar.moreOptionsClick = {
            onMoreOptionsClick?.invoke(it, story)
        }
        renderingContext?.let {
            viewBinding.bobbleContent.contentRenderingContext = it
            viewBinding.image.contentRenderingContext = it
            viewBinding.image.setContent(story.bobbleStory, story.contentMetadata)
        }
    }

    override fun resume(viewBinding: ViewBinding, story: Story?) {
        if (viewBinding !is VerticalStoryAdapterViewBinding) error("Only VerticalStoryContentBinding allowed")
        viewBinding.bobbleContent.play()
    }

    override fun pause(viewBinding: ViewBinding, story: Story?) {
        if (viewBinding !is VerticalStoryAdapterViewBinding) error("Only VerticalStoryContentBinding allowed")
        viewBinding.bobbleContent.pause()
    }

}