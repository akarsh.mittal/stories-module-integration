package com.touchtalent.bobblesdk.stories_ui.ui.views

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View
import androidx.core.content.ContextCompat
import com.touchtalent.bobblesdk.core.utils.BLog
import com.touchtalent.bobblesdk.stories_ui.R

class CustomProgressBarView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {

    private val primaryColor: Int
    private val secondaryColor: Int
    private val radius: Float

    var progress: Int = 0
        set(value) {
            field = value
            invalidate()
        }

    private val paint = Paint().apply {
        isAntiAlias = true
    }

    init {
        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.CustomProgressBarView)
        primaryColor = typedArray.getColor(
            R.styleable.CustomProgressBarView_cpbvPrimaryColor,
            ContextCompat.getColor(context, R.color.stories_progress_primary_color)
        )
        secondaryColor = typedArray.getColor(
            R.styleable.CustomProgressBarView_cpbvSecondaryColor,
            ContextCompat.getColor(context, R.color.stories_progress_secondary_color)
        )
        radius = typedArray.getDimension(
            R.styleable.CustomProgressBarView_cpbvRadius,
            context.resources.getDimension(R.dimen.stories_progress_radius)
        )
        typedArray.recycle()
    }

    override fun onDraw(canvas: Canvas) {
        paint.color = primaryColor
        canvas.drawRoundRect(
            paddingLeft.toFloat(),
            paddingTop.toFloat(),
            width.toFloat() - paddingLeft - paddingRight,
            height.toFloat() - paddingTop - paddingBottom,
            radius,
            radius,
            paint
        )
        if (progress > 2) {
            paint.color = secondaryColor
            canvas.drawRoundRect(
                paddingLeft.toFloat(),
                paddingTop.toFloat(),
                (width - paddingLeft - paddingRight) * progress / 100F,
                height.toFloat() - paddingTop - paddingBottom,
                radius,
                radius,
                paint
            )
        }
    }
}