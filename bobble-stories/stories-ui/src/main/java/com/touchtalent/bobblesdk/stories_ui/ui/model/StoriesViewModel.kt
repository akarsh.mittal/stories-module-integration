package com.touchtalent.bobblesdk.stories_ui.ui.model

import android.content.Context
import android.content.Intent
import android.net.Uri
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.touchtalent.bobblesdk.content_core.interfaces.stories.BobbleStory
import com.touchtalent.bobblesdk.content_core.interfaces.stories.StoryUIController
import com.touchtalent.bobblesdk.content_core.model.Story
import com.touchtalent.bobblesdk.content_core.sdk.BobbleContentOutput
import com.touchtalent.bobblesdk.core.BobbleCoreSDK
import com.touchtalent.bobblesdk.core.utils.FileUtil
import com.touchtalent.bobblesdk.core.utils.GeneralUtils
import com.touchtalent.bobblesdk.stories.data.datastore.BobbleStoriesDataStore
import com.touchtalent.bobblesdk.stories.data.model.api.Trends
import com.touchtalent.bobblesdk.stories.data.pojo.AnimatedNonPersonalisedImageStory
import com.touchtalent.bobblesdk.stories.data.pojo.ImageStory
import com.touchtalent.bobblesdk.stories.data.pojo.MusicalImageStory
import com.touchtalent.bobblesdk.stories.data.room.SearchSuggestions
import com.touchtalent.bobblesdk.stories.events.StoryEvents
import com.touchtalent.bobblesdk.stories_ui.R
import com.touchtalent.bobblesdk.stories_ui.domain.data.ShareIntentFactory
import com.touchtalent.bobblesdk.stories_ui.domain.data.StoriesRepositoryImpl
import com.touchtalent.bobblesdk.stories_ui.domain.data.StoryStatus
import com.touchtalent.bobblesdk.stories_ui.domain.enums.DownloadStates
import com.touchtalent.bobblesdk.stories_ui.domain.enums.Resource
import com.touchtalent.bobblesdk.stories_ui.domain.enums.SavedStory
import com.touchtalent.bobblesdk.stories_ui.domain.enums.VerticalStoryResource
import com.touchtalent.bobblesdk.stories_ui.domain.interfaces.StoriesRepository
import com.touchtalent.bobblesdk.stories_ui.ui.activity.StoriesUtil
import com.touchtalent.bobblesdk.stories_ui.ui.activity.isAppInstalled
import com.touchtalent.bobblesdk.stories_ui.ui.fragments.SavedStoriesFragment
import com.touchtalent.bobblesdk.stories_ui.ui.stories.SavedStoriesAdapter
import com.touchtalent.bobblesdk.stories_ui.ui.stories.SearchSuggestionAdapter
import com.touchtalent.bobblesdk.stories_ui.ui.stories.SearchSuggestionAdapter.Companion.TYPE_SERVER_KEYWORD
import com.touchtalent.bobblesdk.stories_ui.utils.MORE
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.StateFlow
import org.json.JSONArray
import org.json.JSONObject

class StoriesViewModel : ViewModel() {

    companion object {
        const val STORY_ID = "storyId"
        private const val IS_FROM_KEYBOARD = "isFromKeyboard"
        private const val SCREEN_NAME = "screenName"
        private const val NOTIFICATION_TEXT = "notification_text"
        const val SOURCE = "source"
        const val SEARCH_STRING = "searchString"

        fun getLaunchIntent(
            source: StoryUIController.Source,
            isFromKeyboard: Boolean,
            screenName: String?,
            storyId: Int,
            searchString: String? = null,
            notificationText: String? = null
        ): Intent {
            val intent = Intent()
            intent.putExtra(IS_FROM_KEYBOARD, isFromKeyboard)
            intent.putExtra(SCREEN_NAME, screenName)
            intent.putExtra(SOURCE, source.ordinal)
            intent.putExtra(STORY_ID, storyId)
            intent.putExtra(SEARCH_STRING, searchString)
            intent.putExtra(NOTIFICATION_TEXT, notificationText)
            return intent
        }
    }

    val VERTICAL_STORY_API_LIMIT = 10
    var verticalStoryNextToken: String? = null
    private val _currentStory = MutableStateFlow<Resource<Story>>(Resource.Loading())
    val currentStory: StateFlow<Resource<Story>> = _currentStory

    private val _preCacheStory = MutableSharedFlow<BobbleStory>()
    val preCacheStory: SharedFlow<BobbleStory> = _preCacheStory

    private val _storyStatus = MutableSharedFlow<StoryStatus>()
    val storyStatus: SharedFlow<StoryStatus> = _storyStatus

    private val _isDownloaded = MutableSharedFlow<DownloadStates>()
    val isDownloaded: SharedFlow<DownloadStates> = _isDownloaded

    private val _shareIntent = MutableSharedFlow<Intent>()
    val shareIntent: SharedFlow<Intent> = _shareIntent

    private val _verticalStoriesList = MutableStateFlow<Resource<List<Story>>>(Resource.Loading())
    val verticalStoriesList: StateFlow<Resource<List<Story>>> = _verticalStoriesList

    private val _savedStoriesList = MutableStateFlow<List<SavedStory>?>(null)
    val savedStoriesList: StateFlow<List<SavedStory>?> = _savedStoriesList

    private val repository: StoriesRepository = StoriesRepositoryImpl()

    private val _trendingSearches =
        MutableStateFlow<Resource<List<Trends>?>>(Resource.Loading())
    val trendingSearches: StateFlow<Resource<List<Trends>?>> = _trendingSearches
    var trendingSearchesList: List<Trends>? = null

    private val _headCreation = MutableStateFlow<Boolean>(false)
    val headCreation: SharedFlow<Boolean> = _headCreation

    private var _showToast: MutableSharedFlow<String> =
        MutableSharedFlow()
    var showToast: SharedFlow<String> = _showToast

    private var currentStoryIndex = -1
    private var listOfStories = ArrayList<Story>()
    private var isPlaying: Boolean = false
    private var landingStoryId: Int? = null
    private var fromKeyboard: Boolean = false
    var screenName: String? = null
    private var fromIcon: Boolean = false
    private var intent: Intent? = null
    private var source: Int? = null

    private var searchedKeyword: String = ""
    private var notificationText: String? = null

    private var currentStoryStatus = StoryStatus(
        noOfStories = 0,
        currentStory = 0,
        progress = 0
    )

    fun getSource(): Int? {
        return source
    }

    fun getSearchedKeyword(): String {
        return searchedKeyword
    }

    fun setData(intent: Intent) {
        this.intent = intent
        fromIcon = intent.getIntExtra(
            SOURCE,
            StoryUIController.Source.OTHERS.ordinal
        ) == StoryUIController.Source.ICON.ordinal
        fromKeyboard = intent.getBooleanExtra(IS_FROM_KEYBOARD, false)
        screenName = intent.getStringExtra(SCREEN_NAME)
        landingStoryId = intent.getIntExtra(STORY_ID, -1)
        source = intent.getIntExtra(SOURCE, StoryUIController.Source.ICON.ordinal)
        notificationText = intent.getStringExtra(NOTIFICATION_TEXT)
        if (intent.getStringExtra(SEARCH_STRING)?.isEmpty() == false) {
            searchedKeyword = intent.getStringExtra(SEARCH_STRING) ?: ""
            loadSearchResults(Trends(TYPE_SERVER_KEYWORD, searchedKeyword))
        }
        if (source == StoryUIController.Source.NOTIFICATION.ordinal) {
            logNotificationClicked()
        }
        loadStories()

    }

    private fun logNotificationClicked() = viewModelScope.launch {
        StoryEvents.logSOTDNotificationClicked(
            storyId = landingStoryId,
            text = notificationText
        )
    }

    fun loadStories() = viewModelScope.launch {
        if (source == StoryUIController.Source.VERTICAL_STORIES.ordinal) {
            _verticalStoriesList.emit(Resource.Loading())

            var searchedStory: Story? = null
            landingStoryId?.let {
                repository.getStoryById(it)
                    .onSuccess {
                        searchedStory = it
                    }
                    .onFailure {
                        searchedStory = null
                    }
            }
            repository.getListOfVerticalStories(
                verticalStoryNextToken,
                VERTICAL_STORY_API_LIMIT,
                intent?.getStringExtra(SEARCH_STRING)
            )
                .onSuccess {
                    val verticalStoryList = ArrayList<Story>()
                    searchedStory?.let {
                        verticalStoryList.add(it)
                    }
                    verticalStoryList.addAll(it.first)
                    _verticalStoriesList.emit(VerticalStoryResource.FirstLoad(verticalStoryList))
                    verticalStoryNextToken = it.second
                    listOfStories.addAll(verticalStoryList)
                }
                .onFailure {
                    _verticalStoriesList.emit(Resource.Error(it))
                }
        } else if (source != StoryUIController.Source.SEARCH.ordinal && source != StoryUIController.Source.SEARCH_RESULTS.ordinal) {
            _currentStory.emit(Resource.Loading())
            repository.getListOfStories()
                .onSuccess {
                    setCurrentStoryStatus(StoryStatus(it.size, 0, 0))
                    startStories(it)
                }
                .onFailure {
                    _currentStory.emit(Resource.Error(it))
                }
        }
    }


    fun resumeStory() = viewModelScope.launch {
        if (source == StoryUIController.Source.VERTICAL_STORIES.ordinal
            || source == StoryUIController.Source.SEARCH.ordinal
            || source == StoryUIController.Source.SEARCH_RESULTS.ordinal
        ) {
            _verticalStoriesList.emit(VerticalStoryResource.FirstLoad(listOfStories))
        } else {
            val currentStory = listOfStories[currentStoryIndex]
            _currentStory.emit(Resource.Success(currentStory))
        }
    }

    private suspend fun setCurrentStoryStatus(storyStatus: StoryStatus) {
        currentStoryStatus = storyStatus
        _storyStatus.emit(storyStatus)
    }

    private suspend fun startStories(list: List<Story>) {
        listOfStories.addAll(list)
        val index = list.indices.find { idx ->
            val story = list[idx]
            if (story !is Story.ContentStory)
                return@find false
            story.bobbleStory.id == landingStoryId
        } ?: 0
        landingStoryId = list[index].id
        loadStory(index)
    }

    private suspend fun loadStory(index: Int, manualClick: Boolean = false) {
        isPlaying = true
        val currentStory = listOfStories[index]
        val switchFrom =
            if (index - currentStoryIndex == 1) "next" else if (index - currentStoryIndex == -1) "previous" else ""
        val prevStory = listOfStories.getOrNull(index - 1)
        val nextStory = listOfStories.getOrNull(index + 1)
        cacheNextStory(nextStory)
        _currentStory.emit(Resource.Success(currentStory))
        currentStoryIndex = index

        if (currentStory is Story.ContentStory) {
            BobbleStoriesDataStore.addStoryViewed(currentStory.bobbleStory.id)
        }
        BobbleStoriesDataStore.setLastViewedTime(System.currentTimeMillis())
        StoryEvents.logStoryViewed(
            fromIcon = fromIcon,
            fromKeyboard = fromKeyboard,
            isManual = manualClick,
            switchType = switchFrom,
            screenName = screenName,
            story = currentStory,
            prevStory = prevStory,
            nextStory = nextStory,
            bgStoryId = landingStoryId ?: 0,
            audio = if (currentStory is Story.ContentStory && currentStory.bobbleStory is ImageStory) 0 else 1,
            myStory = 0,
            verticalCounter = null,
            stod = 1
        )
    }

    private suspend fun cacheNextStory(nextStory: Story?) {
        if (nextStory is Story.ContentStory)
            _preCacheStory.emit(nextStory.bobbleStory)
        if (nextStory is Story.SummaryStory) {
            nextStory.stories.forEach {
                _preCacheStory.emit(it)
            }
        }
        if (nextStory is Story.AddOnStory) {
            nextStory.preCache(viewModelScope)
        }
    }

    fun updateStoryStatus(progress: Int) = viewModelScope.launch {
        setCurrentStoryStatus(StoryStatus(listOfStories.size, currentStoryIndex, progress))
    }

    fun showNextStory(manualClick: Boolean) = viewModelScope.launch {
        if (currentStoryIndex < listOfStories.size - 1) {
            loadStory(currentStoryIndex + 1, manualClick)
        }
    }

    fun showPreviousStory() = viewModelScope.launch {
        if (currentStoryIndex > 0) {
            loadStory(currentStoryIndex - 1, manualClick = true)
        }
    }

    fun download(contentOutput: BobbleContentOutput?, story: Story? = null) {
        viewModelScope.launch {
            val currentStory = if (story != null) story else listOfStories[currentStoryIndex]
            if (StoriesUtil().getDownloadedImageWithId(currentStory.id)) {
                _isDownloaded.emit(DownloadStates.Exists)
                return@launch
            }
            if (StoriesUtil().getDownloadedVideoWithId(currentStory.id)) {
                _isDownloaded.emit(DownloadStates.Exists)
                return@launch
            }
            if (contentOutput == null) {
                _isDownloaded.emit(DownloadStates.Failed)
                return@launch
            }

            var stod_placement = 1;
            val isSuccess = withContext(Dispatchers.IO) {
                if (currentStory is Story.ContentStory) {
                    stod_placement = 1
                    return@withContext when (currentStory.bobbleStory) {
                        is AnimatedNonPersonalisedImageStory -> FileUtil.saveVideoToGallery(
                            contentOutput.localPath, "Stories", currentStory.id
                        )

                        is MusicalImageStory -> FileUtil.saveVideoToGallery(
                            contentOutput.localPath,
                            "Stories",
                            currentStory.id
                        )

                        is ImageStory -> FileUtil.saveImageToGallery(
                            contentOutput.localPath,
                            "Stories",
                            currentStory.id
                        )

                        else -> false
                    }
                } else if (currentStory is Story.VerticalStory) {
                    stod_placement = 0
                    return@withContext when (currentStory.bobbleStory) {
                        is AnimatedNonPersonalisedImageStory -> FileUtil.saveVideoToGallery(
                            contentOutput.localPath, "Stories", currentStory.id
                        )

                        is MusicalImageStory -> FileUtil.saveVideoToGallery(
                            contentOutput.localPath,
                            "Stories",
                            currentStory.id
                        )

                        is ImageStory -> FileUtil.saveImageToGallery(
                            contentOutput.localPath,
                            "Stories",
                            currentStory.id
                        )

                        else -> false
                    }
                } else
                    return@withContext false
            }
            _isDownloaded.emit(if (isSuccess) DownloadStates.Success else DownloadStates.Failed)

            StoryEvents.logStoryDownloaded(
                fromIcon = fromIcon,
                fromKeyboard = fromKeyboard,
                screenName = screenName,
                story = currentStory,
                bgStoryId = landingStoryId ?: 0,
                stod_placement
            )
        }

    }

    @JvmOverloads
    fun share(packageName: String? = null, contentOutput: BobbleContentOutput?) {
        val context = BobbleCoreSDK.applicationContext
        viewModelScope.launch {
            val resource = currentStory.value
            if (resource !is Resource.Sharing || (resource.data !is Story.ContentStory && resource.data !is Story.VerticalStory)) {
                return@launch
            }

            if (contentOutput != null && !packageName.isNullOrEmpty() && (packageName == MORE || isAppInstalled(
                    context,
                    packageName
                ))
            ) {
                val shareIntent: Intent?
                if (resource.data is Story.ContentStory) {
                    shareIntent = ShareIntentFactory.getIntent(
                        packageName,
                        contentOutput?.shareUri,
                        resource.data.bobbleStory.shareText,
                        contentOutput?.mimeType
                    )
                    _shareIntent.emit(shareIntent)

                    viewModelScope.launch {
                        StoryEvents.logStoryShared(
                            fromIcon = fromIcon,
                            fromKeyboard = fromKeyboard,
                            screenName = screenName,
                            story = resource.data,
                            sharedPackage = packageName ?: "more",
                            flow = listOfStories[currentStoryIndex].type,
                            bgStoryId = landingStoryId ?: 0,
                            audio = if (resource.data.bobbleStory is ImageStory) 0 else 1,
                            stod_flow = 1
                        )
                    }
                } else if (resource.data is Story.VerticalStory) {
                    shareIntent = ShareIntentFactory.getIntent(
                        packageName,
                        contentOutput?.shareUri,
                        resource.data.bobbleStory.shareText,
                        contentOutput?.mimeType
                    )
                    viewModelScope.launch {
                        StoryEvents.logStoryShared(
                            fromIcon = fromIcon,
                            fromKeyboard = fromKeyboard,
                            screenName = screenName,
                            story = resource.data,
                            sharedPackage = packageName,
                            flow = null,
                            bgStoryId = landingStoryId ?: 0,
                            audio = if (resource.data.bobbleStory is ImageStory) 0 else 1,
                            stod_flow = 0
                        )
                    }
                    _shareIntent.emit(shareIntent)
                }
            } else {
                _showToast.emit(packageName.orEmpty())
            }
        }
    }

    fun shareSavedStory(packageName: String?, uri: Uri, mimeType: String, audio: Int) {
        val context = BobbleCoreSDK.applicationContext
        viewModelScope.launch {
            if (!packageName.isNullOrEmpty() && (packageName == MORE || isAppInstalled(
                    context,
                    packageName
                ))
            ) {
                val shareIntent = ShareIntentFactory.getIntent(
                    packageName,
                    uri,
                    "Check out this amazing story here: MakeMyBobble.in/appinvite",
                    mimeType
                )
                _shareIntent.emit(shareIntent)
                viewModelScope.launch {
                    StoryEvents.logSavedStoryShared(
                        screenName = screenName,
                        sharedPackage = packageName,
                        audio = audio,
                    )
                }
            } else {
                _showToast.emit(packageName.orEmpty())
            }
        }
    }

    fun close() {
        val currentStory = listOfStories[currentStoryIndex]
        viewModelScope.launch {
            StoryEvents.logStoryClosed(
                fromIcon = fromIcon,
                fromKeyboard = fromKeyboard,
                isManual = true,
                screenName = screenName,
                story = currentStory,
                bgStoryId = landingStoryId ?: 0,
                storyIndex = currentStoryIndex
            )
        }
    }

    fun shareStory(story: Story, pos: Int? = null) {
        viewModelScope.launch {
            _currentStory.emit(Resource.Sharing(story))
            _verticalStoriesList.emit(Resource.Sharing(listOfStories))
            if (source != StoryUIController.Source.VERTICAL_STORIES.ordinal
                && source != StoryUIController.Source.SEARCH.ordinal
                && source != StoryUIController.Source.SEARCH_RESULTS.ordinal
            ) {
                StoryEvents.logShareIconClicked(
                    fromIcon = fromIcon,
                    fromKeyboard = fromKeyboard,
                    screenName = screenName,
                    story = story,
                    bgStoryId = landingStoryId ?: 0,
                    flow = listOfStories[currentStoryIndex].type
                )
            } else {
                StoryEvents.logShareIconClicked(
                    fromIcon = fromIcon,
                    fromKeyboard = fromKeyboard,
                    screenName = screenName,
                    story = story,
                    bgStoryId = landingStoryId ?: 0,
                    flow = null
                )
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        if (fromKeyboard) BobbleCoreSDK.getAppController().sendOpenKeyboardIntent(intent)
    }

    fun loadNewStories() {
        viewModelScope.launch {
            repository.getListOfVerticalStories(
                verticalStoryNextToken,
                VERTICAL_STORY_API_LIMIT,
                intent?.getStringExtra(SEARCH_STRING)
            )
                .onSuccess {
                    if (it.first.size > 0) {
                        _verticalStoriesList.emit(VerticalStoryResource.SubsequentLoad(it.first))
                        verticalStoryNextToken = it.second
                        listOfStories.addAll(it.first)
                    }
                }
                .onFailure {
                    _verticalStoriesList.emit(Resource.Error(it))
                }
        }
    }

    fun loadTrendingSearches() {
        viewModelScope.launch {
            var dbList: List<SearchSuggestions>? = null
            var serverList: List<Trends>? = null
            var mergedList = ArrayList<Trends>()
            val tasks = listOf(
                async { dbList = repository.getTopSavedSearches() },
                async {
                    repository.getTrendingSearches()
                        .onSuccess {
                            serverList = it.trends
                        }
                        .onFailure {
                            serverList = null
                        }
                }
            )
            tasks.awaitAll()
            if (dbList != null && (dbList?.size ?: 0) > 0) {
                dbList?.map {
                    mergedList.add(
                        Trends(
                            SearchSuggestionAdapter.TYPE_SAVED_KEYWORD,
                            it.keyword
                        )
                    )
                }
            }
            if (mergedList.size < 5 && serverList != null && (serverList?.size ?: 0) > 0) {
                serverList?.map {
                    mergedList.add(
                        Trends(
                            SearchSuggestionAdapter.TYPE_SERVER_KEYWORD,
                            it.keyword
                        )
                    )
                }
            }
            val finalList = mergedList.distinctBy { it.keyword }
            if (finalList.size > 0) {
                _trendingSearches.emit(Resource.Success(finalList))
                trendingSearchesList = finalList
            } else {
                _trendingSearches.emit(Resource.Error(Throwable()))
            }
        }

    }

    fun loadSearches(text: String) {
        searchedKeyword = text
        viewModelScope.launch {
            var dbList: List<SearchSuggestions>? = null
            var serverList: List<Trends>? = null
            val mergedList = ArrayList<Trends>()
            val tasks = listOf(
                async {
                    dbList = repository.getSavedSearches(text)
                },
                async {
                    repository.getSearches(text)
                        .onSuccess {
                            serverList = it.suggestions
                        }
                        .onFailure {
                            serverList = null
                        }
                }
            )
            tasks.awaitAll()
            if (dbList != null && (dbList?.size ?: 0) > 0) {
                dbList?.map {
                    mergedList.add(Trends(SearchSuggestionAdapter.TYPE_SAVED_KEYWORD, it.keyword))
                }
            }
            if (mergedList.size < 5 && serverList != null && (serverList?.size ?: 0) > 0) {
                serverList?.map {
                    mergedList.add(Trends(SearchSuggestionAdapter.TYPE_SERVER_KEYWORD, it.keyword))
                }
            }
            val finalList = mergedList.distinctBy { it.keyword }
            if (finalList.size > 0) {
                _trendingSearches.emit(Resource.Success(finalList))
            } else {
                _trendingSearches.emit(Resource.Error(Throwable()))
            }
        }
    }

    fun loadSearchResults(trend: Trends?) {
        searchedKeyword = trend?.keyword ?: ""
        viewModelScope.launch {
            _verticalStoriesList.emit(Resource.Loading())
            repository.getListOfVerticalStories(
                null, VERTICAL_STORY_API_LIMIT,
                searchString = searchedKeyword
            ).onSuccess {
                trend?.keyword?.let {
                    launch { repository.insertSearchKeyword(it) }
                }
                _verticalStoriesList.emit(VerticalStoryResource.FirstLoad(it.first))
                verticalStoryNextToken = it.second
                listOfStories.addAll(it.first)
                StoryEvents.logSearchResultsSuccess(
                    screenName,
                    trend?.keyword,
                    (trend?.type ?: 1) == SearchSuggestionAdapter.TYPE_SERVER_KEYWORD
                )
            }.onFailure {
                _verticalStoriesList.emit(Resource.Error(it))
            }
        }
    }

    fun loadDownloadedStories() {
        if (_savedStoriesList.value == null) {

            viewModelScope.launch {
                val imagesList = StoriesUtil().getDownloadedImageBobbleStories().map {
                    SavedStory(SavedStoriesAdapter.TYPE_IMAGE, it)
                }
                val videosList = StoriesUtil().getDownloadedVideoBobbleStories().map {
                    SavedStory(SavedStoriesAdapter.TYPE_VIDEO, it)
                }
                // setting index to first video story in final list
                SavedStoriesFragment.videoPlayIndex = imagesList.size
                val list: List<SavedStory> = imagesList.plus(videosList)
                _savedStoriesList.emit(list)
            }
        }
    }

    fun storagePermissionDenied() {
        viewModelScope.launch {
            _savedStoriesList.emit(null)
        }
    }

    fun logSearchFailed() {
        viewModelScope.launch {
            StoryEvents.logSearchResultsFailure(screenName, searchedKeyword)
        }
    }

    fun logStoryViewed(position: Int) {
        if (position < listOfStories.size) {
            val story = listOfStories[position]
            if (story is Story.VerticalStory) {
                viewModelScope.launch {
                    StoryEvents.logStoryViewed(
                        fromIcon = fromIcon,
                        fromKeyboard = fromKeyboard,
                        isManual = true,
                        screenName = screenName,
                        story = listOfStories[position],
                        bgStoryId = landingStoryId ?: 0,
                        nextStory = null,
                        prevStory = null,
                        switchType = null,
                        audio = if (story.bobbleStory is ImageStory) 0 else 1,
                        verticalCounter = position,
                        stod = 0,
                        myStory = 0
                    )
                }
            }
        }
    }

    fun logStoryViewedEvent(data: List<Story>? = null) {
        if (source == StoryUIController.Source.SAVED_STORIES.ordinal) {
            viewModelScope.launch {
                StoryEvents.logStoryViewedEvent(screenName = screenName)
            }
        }
        if (source == StoryUIController.Source.SEARCH.ordinal
            || source == StoryUIController.Source.SEARCH_RESULTS.ordinal
        ) {
            viewModelScope.launch {
                val storiesArray = JSONArray()
                data?.map {
                    val storyObject = JSONObject()
                    storyObject.put("story_id", it.id)
                    if (it is Story.VerticalStory && it.bobbleStory is ImageStory) {
                        if ((it.bobbleStory as ImageStory).headDetails.size > 0) {
                            storyObject.put("heads", (it.bobbleStory as ImageStory).headDetails)
                        }
                    }
                    storiesArray.put(storyObject)
                }
                StoryEvents.logStoryViewedEvent(screenName, storiesArray)
            }
        }
    }

    fun submitfeedback(feedback: String, context: Context?) {
        viewModelScope.launch {
            repository.submitFeedback(feedback, "reportStory")
                .onSuccess {
                    context?.let {
                        GeneralUtils.showToast(it, it.getString(R.string.feedback_sent))
                    }
                }
        }
    }

    fun setHeadCreation() {
        viewModelScope.launch {
            _headCreation.emit(true)
        }
    }
}