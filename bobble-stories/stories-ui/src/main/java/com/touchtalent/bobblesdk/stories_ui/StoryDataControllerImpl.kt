package com.touchtalent.bobblesdk.stories_ui

import com.touchtalent.bobblesdk.content_core.interfaces.stories.StoriesDataController
import com.touchtalent.bobblesdk.content_core.model.Story
import com.touchtalent.bobblesdk.stories_ui.domain.data.StoriesRepositoryImpl
import com.touchtalent.bobblesdk.stories_ui.domain.interfaces.StoriesRepository

object StoryDataControllerImpl : StoriesDataController {
    private val storyRepository: StoriesRepository by lazy {
        StoriesRepositoryImpl()
    }

    override suspend fun getListOfStoriesWithoutAds(): Result<List<Story>> {
        return storyRepository.getListOfStoriesWithoutAds()
    }

    override suspend fun getListOfVerticalStories(
        nextToken: String?,
        limit: Int,
        searchString: String?
    ): Result<Pair<List<Story>, String?>> {
        return storyRepository.getListOfVerticalStories(nextToken, limit, searchString)
    }


}