package com.touchtalent.bobblesdk.stories_ui.domain.enums

sealed class DownloadStates {
    object Success : DownloadStates()
    object Failed : DownloadStates()
    object Exists : DownloadStates()
}
