package com.touchtalent.bobblesdk.stories_ui.domain.enums

sealed class VerticalStoryResource<T>(data : T) : Resource.Success<T>(data) {
    class FirstLoad<T>(val storyData: T) : VerticalStoryResource<T>(storyData)
    class SubsequentLoad<T>(val storyData: T) : VerticalStoryResource<T>(storyData)
}