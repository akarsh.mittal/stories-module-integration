package com.touchtalent.bobblesdk.stories_ui.ui.fragments

import android.content.Context
import android.content.Intent
import android.media.AudioManager
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.viewpager2.widget.ViewPager2.OnPageChangeCallback
import com.touchtalent.bobblesdk.content_core.interfaces.ContentRenderingContext
import com.touchtalent.bobblesdk.content_core.interfaces.stories.StoryUIController
import com.touchtalent.bobblesdk.content_core.model.Story
import com.touchtalent.bobblesdk.core.BobbleCoreSDK
import com.touchtalent.bobblesdk.core.interfaces.head.BobbleHeadCreator
import com.touchtalent.bobblesdk.core.interfaces.head.HeadModule
import com.touchtalent.bobblesdk.core.utils.GeneralUtils
import com.touchtalent.bobblesdk.stories.data.datastore.BobbleStoriesDataStore
import com.touchtalent.bobblesdk.stories.data.exception.VerticalStoriesError
import com.touchtalent.bobblesdk.stories_ui.R
import com.touchtalent.bobblesdk.stories_ui.databinding.FragmentVerticalStoriesBinding
import com.touchtalent.bobblesdk.stories_ui.domain.enums.*
import com.touchtalent.bobblesdk.stories_ui.ui.activity.BobbleStoriesActivity
import com.touchtalent.bobblesdk.stories_ui.ui.activity.findNavControllerSafely
import com.touchtalent.bobblesdk.stories_ui.ui.model.StoriesViewModel
import com.touchtalent.bobblesdk.stories_ui.ui.stories.VerticalStoryAdapter
import com.touchtalent.bobblesdk.stories_ui.ui.views.StoryPlayerView
import com.touchtalent.bobblesdk.stories_ui.utils.*
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch


class VerticalStoriesFragment : Fragment() {

    private val model: StoriesViewModel by activityViewModels()
    private lateinit var binding: FragmentVerticalStoriesBinding
    private var viewpagerAdapter: VerticalStoryAdapter? = null
    private lateinit var renderingContext: ContentRenderingContext
    private var job: Job? = null
    private var mediaVolume: Int = 0
    private var adapterPosition = 0

    companion object {
        val REQUEST_CODE_CREATE_HEAD = 1000
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentVerticalStoriesBinding.inflate(inflater, container, false)
        renderingContext = (activity as BobbleStoriesActivity).renderingContext
        setUpViewpagerScrollListener()
        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                launch { listenToStoryList() }
                launch { listenToShowToast() }
                launch { listenToShareIntent() }
                launch { listenToDownloadStatus() }
                launch { listenToHeadFlow() }
                job = launch {
                    delay(2000)
                    setSwipeAnimation()
                }
            }
        }
        binding.buttonBack.setOnClickListener {
            val navHostFragment =
                activity?.supportFragmentManager?.findFragmentById(R.id.story_nav_host_fragment)
            if (navHostFragment?.childFragmentManager?.backStackEntryCount == 0) {
                activity?.finish()
            } else {
                findNavControllerSafely(R.id.verticalStoriesFragment)?.navigateUp()
            }
        }
        val audio = activity?.getSystemService(Context.AUDIO_SERVICE) as? AudioManager
        mediaVolume = audio?.getStreamVolume(AudioManager.STREAM_MUSIC) ?: 0

        return binding.root
    }

    private fun setUpViewpagerScrollListener() {
        binding.viewPager.registerOnPageChangeCallback(object : OnPageChangeCallback() {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
                if (position != 0) {
                    job?.cancel()
                    binding.swipeAnimation.visibility = View.GONE
                }
            }

            override fun onPageSelected(position: Int) {
                model.logStoryViewed(position)
                if (model.getSource() == StoryUIController.Source.SEARCH.ordinal)
                    return
                if (position == (viewpagerAdapter?.itemCount ?: 0) - 2) {
                    model.loadNewStories()
                }
            }
        })
    }


    private suspend fun listenToStoryList() {
        model.verticalStoriesList.collect { result ->
            when (result) {
                is Resource.Success -> {
                    if (result is VerticalStoryResource.FirstLoad) {
                        showStories(result.data)
                    } else {
                        addStories(result.data)
                    }
                }
                is Resource.Loading -> showLoading()
                is Resource.Error -> {
                    BobbleCoreSDK.getAppController()
                        .logException("VerticalStoriesError", VerticalStoriesError(result.error))
                }
                else -> {}
            }
        }
    }

    private fun showStories(data: List<Story>) {
        if (viewpagerAdapter == null) {
            binding.storyLoaderView.visibility = View.GONE
            viewpagerAdapter = VerticalStoryAdapter(this, renderingContext)
            setupViewListeners()
            binding.viewPager.adapter = viewpagerAdapter
            binding.viewPager.offscreenPageLimit = 1
            arguments?.let {
                if (it.getInt("position", -1) != -1) {
                    binding.viewPager.post {
                        binding.viewPager.setCurrentItem(it.getInt("position", 0), false)
                    }
                }
            }
            if (adapterPosition != 0) {
                viewpagerAdapter?.startPosition = adapterPosition
                binding.viewPager.post {
                    binding.viewPager.setCurrentItem(adapterPosition, false)
                }
            }
            viewpagerAdapter?.addAllStories(data)

        }
    }

    private fun showLoading() {
        binding.storyLoaderView.visibility = View.VISIBLE
    }

    private fun addStories(data: List<Story>) {
        viewpagerAdapter?.addAllStories(data)
    }

    private fun setupViewListeners() {
        viewpagerAdapter?.setStoryListener(object : StoryPlayerView.StoryPlayerListener() {
            override fun onButtonClick(story: Story, type: String, url: String?) {
                when (type) {
                    TYPE_SHARE_STORY_WHATSAPP -> {
                        lifecycleScope.launch {
                            val contentOutput =
                                renderingContext.export((story as Story.VerticalStory).bobbleStory)
                            model.shareStory(story)
                            model.share(WHATSAPP, contentOutput.getOrNull())
                        }
                    }
                    TYPE_SHARE_STORY -> {
                        model.shareStory(story)
                        findNavControllerSafely(R.id.verticalStoriesFragment)?.navigate(R.id.action_verticalStoriesFragment_to_shareStoryFragment)
                        viewpagerAdapter = null
                        adapterPosition = binding.viewPager.currentItem
                    }
                    TYPE_DOWNLOAD_STORY -> {
                        lifecycleScope.launch {
                            if (story is Story.VerticalStory)
                                model.download(
                                    renderingContext.export(story.bobbleStory).getOrNull(), story
                                )
                        }
                    }
                    TYPE_OPEN_DEEPLINK,
                    TYPE_OPEN_WEB_VIEW,
                    TYPE_OPEN_WEB_BROWSER -> {
                        openIntent(url)
                    }
                    TYPE_EDIT_STORY -> {
                        activity?.let {
                            BobbleCoreSDK.getModule(HeadModule::class.java)?.getHeadCreator()
                                ?.newBuilder(it)
                                ?.setScreenName(model.screenName ?: "app_story_screen")
                                ?.setInitialSource(BobbleHeadCreator.InitialSource.CAMERA)
                                ?.setKeyboardView(false)
                                ?.startActivityForResult(it, REQUEST_CODE_CREATE_HEAD)
                        }

                    }
                }
            }

            override fun muteVideo() {
                val audio = activity?.getSystemService(Context.AUDIO_SERVICE) as? AudioManager
                val volume = audio?.getStreamVolume(AudioManager.STREAM_MUSIC)
                if (volume == 0) {
                    audio.setStreamVolume(AudioManager.STREAM_MUSIC, mediaVolume, 0)
                    lifecycleScope.launch {
                        binding.muteIcom.setImageResource(R.drawable.ic_unmute)
                        binding.muteIcom.visibility = View.VISIBLE
                        delay(500)
                        binding.muteIcom.visibility = View.GONE
                    }
                } else {
                    audio?.setStreamVolume(AudioManager.STREAM_MUSIC, 0, 0)
                    lifecycleScope.launch {
                        binding.muteIcom.setImageResource(R.drawable.ic_mute)
                        binding.muteIcom.visibility = View.VISIBLE
                        delay(500)
                        binding.muteIcom.visibility = View.GONE
                    }
                }
            }

            override fun submitFeedback(feedback: String) {
                model.submitfeedback(feedback, context)
            }
        })
    }

    private fun openIntent(url: String?) {
        val intent = Intent(
            Intent.ACTION_VIEW,
            Uri.parse(url)
        )
        startActivity(intent)
    }

    private suspend fun listenToShowToast() {
        model.showToast.collectLatest { message ->
            GeneralUtils.showToast(
                activity,
                getString(R.string.story_app_not_installed)
            )
        }
    }

    private suspend fun listenToShareIntent() {
        model.shareIntent.collectLatest { intent ->
            startActivity(intent)
        }
    }

    private suspend fun listenToDownloadStatus() {
        model.isDownloaded.collectLatest {
            when (it) {
                is DownloadStates.Success -> GeneralUtils.showToast(
                    activity,
                    getString(R.string.story_saved_to_gallery)
                )
                is DownloadStates.Failed -> GeneralUtils.showToast(
                    activity,
                    getString(R.string.story_failed_to_save)
                )
                is DownloadStates.Exists -> GeneralUtils.showToast(
                    activity,
                    getString(R.string.story_exists)
                )
            }
        }
    }

    private suspend fun listenToHeadFlow() {
        model.headCreation.collect {
            if (it) {
                viewpagerAdapter?.notifyItemRangeChanged(binding.viewPager.currentItem - 2, 5)
            }
        }
    }

    private suspend fun setSwipeAnimation() {
        val count = BobbleStoriesDataStore.getSwipeAnimationCount()
        if (count < 2) {
            binding.swipeAnimation.visibility = View.VISIBLE
            BobbleStoriesDataStore.setSwipeAnimationCount(count + 1)
            delay(5000)
            binding.swipeAnimation.visibility = View.GONE
        } else {
            binding.swipeAnimation.visibility = View.GONE
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        viewpagerAdapter = null
        job?.cancel()
    }
}