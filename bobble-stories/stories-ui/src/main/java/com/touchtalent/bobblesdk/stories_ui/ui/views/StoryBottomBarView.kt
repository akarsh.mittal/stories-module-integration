package com.touchtalent.bobblesdk.stories_ui.ui.views

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import com.touchtalent.bobblesdk.stories_ui.databinding.ViewStoryBottomBarBinding

class StoryBottomBarView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null
) : ConstraintLayout(context, attrs) {
    private val binding = ViewStoryBottomBarBinding.inflate(LayoutInflater.from(context), this)
    val shareStoryDescription
        get() = binding.shareStoryDescription
    var shareListener: (() -> Unit)? = null
        set(value) {
            binding.shareButton.setOnClickListener { shareListener?.invoke() }
            field = value
        }
    var downloadListener: (() -> Unit)? = null
        set(value) {
            binding.downloadButton.setOnClickListener { downloadListener?.invoke() }
            field = value
        }
}