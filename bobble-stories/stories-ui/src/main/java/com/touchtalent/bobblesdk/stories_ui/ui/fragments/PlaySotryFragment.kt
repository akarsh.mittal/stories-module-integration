package com.touchtalent.bobblesdk.stories_ui.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import com.touchtalent.bobblesdk.content_core.interfaces.ContentRenderingContext
import com.touchtalent.bobblesdk.content_core.interfaces.stories.StoryUIController
import com.touchtalent.bobblesdk.content_core.model.Story
import com.touchtalent.bobblesdk.core.BobbleCoreSDK
import com.touchtalent.bobblesdk.core.utils.GeneralUtils
import com.touchtalent.bobblesdk.stories.data.exception.StoryOfTheDayError
import com.touchtalent.bobblesdk.stories_ui.R
import com.touchtalent.bobblesdk.stories_ui.databinding.FragmentPlayStoryBinding
import com.touchtalent.bobblesdk.stories_ui.domain.enums.DownloadStates
import com.touchtalent.bobblesdk.stories_ui.domain.enums.Resource
import com.touchtalent.bobblesdk.stories_ui.ui.activity.BobbleStoriesActivity
import com.touchtalent.bobblesdk.stories_ui.ui.model.StoriesViewModel
import com.touchtalent.bobblesdk.stories_ui.ui.views.StoryPlayerView
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class PlayStoryFragment : Fragment() {

    private val model: StoriesViewModel by activityViewModels()
    private lateinit var binding: FragmentPlayStoryBinding
    private lateinit var renderingContext: ContentRenderingContext

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentPlayStoryBinding.inflate(inflater, container, false)
        renderingContext = (activity as BobbleStoriesActivity).renderingContext
        setupViewListeners()

        viewLifecycleOwner.lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.RESUMED) {
                launch { listenToCurrentStory() }
                launch { listenToCacheStory() }
                launch { listenToProgress() }
                launch { listenToDownloadStatus() }
            }
        }
        return binding.root
    }

    private suspend fun listenToCacheStory() {
        model.preCacheStory.collectLatest { renderingContext.preCacheContent(it) }
    }

    private suspend fun listenToDownloadStatus() {
        model.isDownloaded.collectLatest {
            if (it is DownloadStates.Success) {
                GeneralUtils.showToast(
                    activity,
                    getString(R.string.story_saved_to_gallery)
                )
            } else if (it is DownloadStates.Failed) {
                GeneralUtils.showToast(
                    activity,
                    getString(R.string.story_failed_to_save)
                )
            } else if (it is DownloadStates.Exists) {
                GeneralUtils.showToast(
                    activity,
                    getString(R.string.story_exists)
                )
            }
        }
    }

    override fun onPause() {
        super.onPause()
        kotlin.runCatching {
            binding.storyPlayerView.pauseStory()
        }
    }

    private fun setupViewListeners() = with(binding) {
        storyPlayerView.playerListener = object : StoryPlayerView.StoryPlayerListener() {
            override fun onStoryStatusUpdate(progress: Int) {
                model.updateStoryStatus(progress)
            }

            override fun onNextClicked(manualClick: Boolean) {
                model.showNextStory(manualClick)
            }

            override fun onPreviousClicked() {
                model.showPreviousStory()
            }

            override fun onPlaybackToggled() {
                binding.storyPlayerView.pauseStory()
            }

            override fun onShare(story: Story) {
                model.shareStory(story)
            }

            override fun onDownload(story: Story) {
                lifecycleScope.launch {
                    if (story is Story.ContentStory)
                        model.download(renderingContext.export(story.bobbleStory).getOrNull())
                }
            }

            override fun onClose() {
                model.close()
                activity?.finish()
            }

            override fun onViewMoreClick() {
                activity?.let {
                    val intent = StoriesViewModel.getLaunchIntent(
                        StoryUIController.Source.VERTICAL_STORIES,
                        false,
                        model.screenName,
                        -1
                    )
                    intent.setClass(it, BobbleStoriesActivity::class.java)
                    it.finish()
                    it.startActivity(intent)
                }
            }
        }
    }

    private suspend fun listenToCurrentStory() {
        model.currentStory.collectLatest { result ->
            when (result) {
                is Resource.Success -> showStory(result.data)
                is Resource.Error -> showError(result.error)
                is Resource.Loading -> showLoading()
                is Resource.Sharing -> openShareScreen()
            }
        }
    }

    private fun openShareScreen() {
        if (findNavController().currentDestination?.id == R.id.playSotryFragment)
            findNavController().navigate(R.id.action_playSotryFragment_to_shareStoryFragment)
    }

    private fun showLoading() {
        binding.storyLoaderView.visibility = View.VISIBLE
    }

    private fun showError(error: Throwable) {
        binding.storyLoaderView.visibility = View.GONE
        with(binding.storyErrorView) {
            visibility = View.VISIBLE
            setRetryClickListener {
                this.visibility = View.GONE
                model.loadStories()
            }
            setOnCloseListener { activity?.finish() }
        }
        BobbleCoreSDK.getAppController()
            .logException("StoryOfTheDayError", StoryOfTheDayError(error))
    }

    private fun showStory(story: Story) = with(binding) {
        lifecycleScope.launch {
            storyLoaderView.visibility = View.GONE
            storyPlayerView.loadStory(story, renderingContext)
        }
    }

    private suspend fun listenToProgress() {
        model.storyStatus.collectLatest { status ->
            with(binding) {
                storiesProgressView.setStoriesCount(status.noOfStories)
                storiesProgressView.setProgress(status.currentStory, status.progress)
            }
        }
    }
}