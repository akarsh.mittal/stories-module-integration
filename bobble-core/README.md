# Module bobble-core

<b>Bobble Core</b> module is the parent module for all modules. It provides interfaces for
intercommunication among modules.

## Responsibilities of this modules:

1. Providing a <b>single class for initialising all modules</b>.
2. Provide <b>general utility classes</b>, which are required by all modules, like GeneralUtils,
   ContextUtils, BLog, FileUtils, etc
3. Acts as a communication module for other modules by <b>holding necessary interfaces and POJO
   models</b>.

## Key highlights regarding usage of the module:

1. All modules are required to provide a implementation
   of [BobbleModule](com.touchtalent.bobblesdk.core.interfaces.BobbleModule) which acts as an entry
   point for that module. <b>This class will be synonymous
   to [Application](android.app.Application)</b>
   class for an module which provides a global initialisation support.
2. <b>[BobbleCoreSDK](com.touchtalent.bobblesdk.core.BobbleCoreSDK)</b> class acts an entry point
   for all dependencies on other app or parent app. <i>Checkout the function and variables available
   in the class.</i>

## Guidelines for creating a new module using bobble-core:

> Following guidelines are only for documentation purposes. Use of automated tool is recommended to create a new module. Refer [here](https://gitlab.com/BobbleAndroid/bobble-android-keyboard/-/wikis/Creating-new-bobble-module) for more details

Guidelines:

1. <b>Package naming nomenclature</b> for new module: `com.touchtalent.bobblesdk.{feature}`.
   E.g: `com.touchtalent.bobblesdk.content`
2. Create a `object` class with the nomenclature : `{Feature}SDK`. E.g: `BobbleCoreSDK`
   , `BobbleContentSDK` which implements `BobbleModule` :

   ```kotlin
   object BobbleContentSDK : BobbleModule() {
   
        lateinit var applicationContext: Context
            
        //Called from Application.onCreate(). Initialisation work can be done here.
        override fun initialise(applicationContext: Context, config: BobbleCoreConfig) {
            this.applicationContext = applicationContext
        }
            
        //Handle the user config if this module is concerned with any key.
        override fun handleUserConfig(response: JSONObject) {
        }
   }
   ```
3. Register your module in [bobble-core] by <b>adding it's fully qualified package name
   in `listOfBobbleModule` in `BobbleCoreSDK`</b>. The module will be initialised automatically
   when [BobbleCoreSDK](com.touchtalent.bobblesdk.core.BobbleCoreSDK) is initialised by the parent
   app. It uses reflection under the hood, so modules missing from gradle can be ignored.
   ```kotlin
   private val listOfBobbleModule = listOf(
       "com.touchtalent.bobblesdk.content.sdk.BobbleContentSDK",
   )
   ```
4. Use available functions from [BobbleCoreSDK](com.touchtalent.bobblesdk.core.BobbleCoreSDK) to
   <b>perform specific interactions with the parent app</b>, like fetching current active language,
   getting version params, perform vibration action, etc
5. If you need to <b>communicate between 2 modules</b>, but does not want hard dependency on the
   module, for e.g: content module may/may not depend on the head module, consider adding interfaces
   and required Models in `com.touchtalent.bobblesdk.core.interfaces`
   and `com.touchtalent.bobblesdk.core.model` packages respectively. Scroll down for more
   information on tight coupling and loose coupling

## Guidelines for using `BobbleCoreSDK` in parent app:

1. Create a new implementation of the
   interface <b>[CrossAppInterface](com.touchtalent.bobblesdk.core.interfaces.CrossAppInterface)</b>
   in your app.
2. <b>[Initialise BobbleCoreSDK](com.touchtalent.bobblesdk.core.BobbleCoreSDK.initialise)</b>
   in [Application.onCreate()](android.app.Application.onCreate)

   ```kotlin
   class CustomCrossAppInterface : CrossAppInterface {
       //Override all abstract function providing their implementation
   }
   class com.example.storymodulesampleapp.BobbleApp : Application() {
       override fun onCreate(context: Context) {
           val bobbleCoreConfig: BobbleCoreConfig = BobbleCoreConfig(CustomCrossAppInterface())
           /**
            * Add other module's config to bobbleCoreConfig here, like BobbleContentConfig, BobbleHeadConfig, etc
            */
           config.okHttpClient = okhttp3.OkHttpClient()
           BobbleCoreSDK.initialise(getApplicationContext(), config)
       }
   }
   ```
3.
Call <b>[BobbleCoreSDK.runPeriodicUpdaters(forced: Boolean)](com.touchtalent.bobblesdk.core.BobbleCoreSDK.runPeriodicUpdaters)</b>
wherever required. Ideally called on every keyboard close action and every App launch action (
Preferably from background thread).

4.
Call <b>[BobbleCoreSDK.handleUserConfig(forced: Boolean)](com.touchtalent.bobblesdk.core.BobbleCoreSDK.handleUserConfig)</b>
in existing `SuccessResponseParseUtil` to allow other modules to process the config parameters

## Handling user configs (server config) for modules

The core module is responsible to <b>provide access to user config to all other modules</b>. Core
module acts as a bridge to transfer the whole payload from parent app to the main module for
isolated processing.

User config callbacks are dispatched
through [BobbleModule.handleUserConfig](com.touchtalent.bobblesdk.core.interfaces.BobbleModule.handleUserConfig)
to each module.

<i>Parent app</i>

```kotlin
object SuccessResponseParseUtil {
   fun handleConfigResponse(response: JSONObject) {
      BobbleCoreSDK.handleUserConfig(response)
   }
}
```

<i>Module</i>

```kotlin
class BobbleHeadSDK : BobbleModule {

   override fun initialise(applicationContext: Context, config: BobbleCoreConfig) {

   }

   override fun handleUserConfig(response: JSONObject) {
      response.handleAsJSONObject("headModuleSettings") {
         // Handle all keys related to head module
      }
   }

}
```

<b>Each module should handle/store/fetch the config within itself</b>. If a config is required
outside the scope of module (e.g - value in config key required in parent app), the processing of
the key might still be in the module and its value exposed from module via an API.

Its recommended to have whole payload for a single module wrapped in a single key from backend. E.g-

```json
{
   "superAppSettings": {
      "key1": "value1",
      "key2": "value2"
   }
}
```

## Handling customisable configs for modules

The core module is responsible to handle customisable config (NOT user config) for each module.
Config are parameters used to customise features in a module as per the need of the parent app. For
e.g - Default watermark for content module will be different for Bobble and Bharat keyboards, hence
needs to be provided the parent app and not inside the module.

bobble-core solves this problem by accepting config for all modules at a single place. </b>Parent
apps can set their values and modules can get their value to get the work done</b>. For e.g -
refer [BobbleCoreConfig.contentConfig](com.touchtalent.bobblesdk.core.config.BobbleCoreConfig.contentConfig)
which accepts various customisable parameters for content module set by parent app and used by
content module

<i>Parent app</i>

```kotlin
class com.example.storymodulesampleapp.BobbleApp : Application {
   fun onCreate(bundle: Bundle) {
      val contentConfig: BobbleContentConfig = BobbleContentConfig().also {
         it.seededWatermark = R.drawable.default_watermark
      }
      val coreConfig: BobbleCoreConfig = BobbleCoreConfig().also {
         it.contentConfig = contentConfig
      }
      BobbleCoreSDK.initialise(this, coreConfig)
   }
}
```

<i>Module</i>

```kotlin
object BobbleStickerWatermarkManager {
   fun applyDefaultWatermark(sticker: Sticker) {
      val defaultWatermark = BobbleCoreSDK.config.contentConfig?.seededWatermark
      //Use watermark
   }
}
```

## Handle API params within module

Most APIs share a <b>common set of parameters such as clientId, deviceId, etc</b> which are
dependent on the parent app and not accessible from modules.
[ApiParamsBuilder](com.touchtalent.bobblesdk.core.builders.ApiParamsBuilder) bridges this gap.

Parent apps are required to provide implementation of this interface and return an instance
via `BobbleCoreSDK.getAppController().getApiParamsBuilder()`

Module can fetch the instance by the same function and <b>create a HashMap of commonly used params
to be directly used in API calls</b>, without caring about the logic (key and value extraction) of
these params. Its necessary for these parameters to be in sync with backend. Any discrepancy in
consistency should be reported to the backend and fixed - for e.g - same API param with different
keys in 2 different APIs

<i>Parent app</i>

```kotlin
class BobbleApiParamBuilder : ApiParamsBuilder() {
   override fun populateDeviceIdParams(hashMap: HashMap<String, String>, key: String?) {
      hashMap[key ?: "deviceId"] = fetchDeviceId()
   }

   override fun populateClientIdParams(hashMap: HashMap<String, String>, key: String?) {
      hashMap[key ?: "clientId"] = BuildConfig.CLIENT_ID
   }

   override fun populateVersionParams(hashMap: HashMap<String, String>) {
      hashMap["appVersion"] = getAppVersion()
      hashMap["sdkVersion"] = Build.VERSION.RELEASE
   }
   // More implementations
}
```

<i>Module</i>

```kotlin
class QueryParamsAdder : Interceptor {
   override fun intercept(chain: Interceptor.Chain): Response {
      var request = chain.request()
      val apiParams: HashMap<String, String> = BobbleCoreSDK.getApiParamsBuilder()
         .withDeviceId()
         .withClientId()
         .withVersion()
         .withDeviceManufacturer()
         .build()

      val url = request.url().newBuilder().also {
         apiParams.keys.forEach { key ->
            it.addQueryParameter(key, apiParams[key])
         }
      }.build()

      request = request.newBuilder().url(url).build()
      return chain.proceed(request)
   }
}
```

## Tight coupling Vs Loose coupling

In some cases, we might want to the modules to be plug-n-play, i.e have a single control to
add/remove the module. For e.g - A single codebase of SDKs having multiple flavors. Some flavors
want AI features but some doesn't support AI features due to APK size restrictions. In this case the
whole module code should be removed from APK of that flavor, disabling the functionality using
BuildConfig keys won't work. bobble-core solves this problem with the concept of tight coupling and
loose coupling.

1. <b>Tight coupling</b> - The module is integrated the with parent app in such a way that, if its
   gradle dependency is removed, the parent app fails to compile since it directly accesses the
   classes from within the module. For e.g -

   ```kotlin
   package com.touchtalent.bobbleapp.parent_app
   
   import com.touchtalent.bobblesdk.head.BobbleHeadSDK
   
   fun deleteHead(headId: Long) {
       BobbleHeadSDK.getHeadManager().deleteHead(headId)
   }
   ```

   Since, [BobbleHeadSDK] is present inside the head-module, removing its dependency will
   compilation errors.

2. <b>Loose coupling</b> - The module is integrated with the parent app in such a way that, even if
   its gradle dependency is removed from gradle, the parent app looses the functionality but still
   continues to compile and be fully operational. This can be achieved if the API of the module are
   exposed through interfaces. The core module will contain the interfaces which will be used by the
   parent app. Their implementation would be fetch by the core-module
   using [Reflection](https://www.geeksforgeeks.org/reflection-in-java/).

   Core module provides utility classes for accessing/instantiating classes via Reflection
   with [DependencyResolver](com.touchtalent.bobblesdk.core.DependencyResolver)

   The head module could be designed this way:

   <i>File present in core module</i>

   ```kotlin
   package com.touchtalent.bobblesdk.core.interfaces
   
   interface BobbleHeadModule {
       fun getHeadManager(): BobbleHeadManager
   }
   interface BobbleHeadManager {
       fun deleteHead(headId: Long)
   }
   ```

   <i>File present in head module</i>

   ```kotlin
   package com.touchtalent.bobblesdk.head.sdk
   
   // Dependent only on core module
   import com.touchtalent.bobblesdk.core.interfaces.BobbleHeadModule
   import com.touchtalent.bobblesdk.core.interfaces.BobbleHeadManager
   
   class BobbleHeadSDK : BobbleModule, BobbleHeadModule {
       override fun initialise(applicationContext: Context, config: BobbleCoreConfig) {
   
       }
   
       override fun getHeadManager(): BobbleHeadManager = BobbleHeadManagerImpl
   }
   
   object BobbleHeadManagerImpl : BobbleHeadManager {
       override fun deleteHead(headId: Long) {
           //Delete head implementation
       }
   }
   ```

   <i>File present in parent app</i>

   ```kotlin
   package com.touchtalent.bobbleapp.keyboard
   
   // Dependent only on core module, no dependency on head module
   import com.touchtalent.bobblesdk.core.interfaces.BobbleHeadModule
   import com.touchtalent.bobblesdk.core.interfaces.BobbleHeadManager
   import com.touchtalent.bobblesdk.core.BobbleCoreSDK
   
   fun deleteHead(headId: Long) {
       // This is where magic happens. getModule() uses reflection underneath
       // If the module is not included in the gradle, 
       // getModule will return null which parent app will gracefully handle
       val headModule: BobbleHeadModule =
           BobbleCoreSDK.getModule(BobbleHeadModule::class.java) ?: return
       headModule.deleteHead(headId)
   }
   ```

### Using View class in XML - Loose coupling

Above example works fine for business logic related code. What if a loosely coupled module needs to
expose a View to the parent. `BobbleCoreSDK.getModule()` cannot be used in XML files. Referencing
View classes from module directly in XML will lead to compilation issues.

[OptionalDependencyWrapperView](com.touchtalent.bobblesdk.core.views.OptionalDependencyWrapperView)
provides support for referencing Views in XML via Reflection. E.g -

<i>Core module</i>

```kotlin
interface LocationSearchView {
   fun setSourceIcon(iconUrl: String?)
   fun setHint(hintText: String?)
   fun setActionListener(listener: SearchActionListener?)
}
```

<i> Location search module </i>

```kotlin
class LocationSearchViewImpl : RelativeLayout, LocationSearchView {
   override fun setSourceIcon(iconUrl: String?) {}
   override fun setHint(hintText: String?) {}
   override fun setActionListener(listener: SearchActionListener?) {}
}
```

<i> Parent module </i>

```xml

<com.touchtalent.bobblesdk.core.views.OptionalDependencyWrapperView
        android:id="@+id/location_search_view" android:layout_width="match_parent"
        android:layout_height="wrap_content"
        app:odwClass="com.touchtalent.bobblesdk.location_search.LocationSearchViewImpl" />
```

```kotlin
fun showLocationSearchView() {
   val locationSearchView: LocationSearchView? =
      binding.locationSearchView.getDependentView(LocationSearchView::class.java)
   locationSearchView?.let {
      it.setSourceIcon("https://example.com/image.png")
   }
}
```

# Package com.touchtalent.bobblesdk.core

This package contains classes that act as a entry point to the core module.

# Package com.touchtalent.bobblesdk.core.adapters

Utility RecyclerView adapters for common use-cases.

# Package com.touchtalent.bobblesdk.core.api

Utility classes for API calls.

# Package com.touchtalent.bobblesdk.core.builders

Utility builder classes.

# Package com.touchtalent.bobblesdk.core.cache

Utility classes for disk cache.

# Package com.touchtalent.bobblesdk.core.config

Contains config models for other modules.

# Package com.touchtalent.bobblesdk.core.constants

Contains constants related class.

# Package com.touchtalent.bobblesdk.core.enums

Contains enums to be used across modules.

# Package com.touchtalent.bobblesdk.core.executor

Contains classes for threading related solutions.

# Package com.touchtalent.bobblesdk.core.interfaces

Contains interfaces for other modules. These interfaces define the functionalities provided by these
modules

# Package com.touchtalent.bobblesdk.core.interfaces.ai_intent

Interface definitions for AI intent prediction module

# Package com.touchtalent.bobblesdk.core.interfaces.content

Interface definitions for bobble-content module

# Package com.touchtalent.bobblesdk.core.interfaces.fonts

Interface definitions for unified font management

# Package com.touchtalent.bobblesdk.core.interfaces.head

Interface definitions for bobble-head module

# Package com.touchtalent.bobblesdk.core.interfaces.location_search

Interface definitions for location-search module

# Package com.touchtalent.bobblesdk.core.interfaces.logger

Classes for Event logging.

# Package com.touchtalent.bobblesdk.core.interfaces.promotional_offers

Interface definitions for promotional-offers module

# Package com.touchtalent.bobblesdk.core.model

POJO models for modules which needs to be shared across other modules

# Package com.touchtalent.bobblesdk.core.moshi

Utility classes and adapters for Moshi

# Package com.touchtalent.bobblesdk.core.pojo

POJO classes used by core-module internally

# Package com.touchtalent.bobblesdk.core.room

Utility classes and converter classes for Room

# Package com.touchtalent.bobblesdk.core.utils

Utility classes for file management, bitmap manipulation, context management, etc

# Package com.touchtalent.bobblesdk.core.views

Contains utility View classes

# Package com.touchtalent.bobblesdk.core.interfaces.ai_predictions

Interface definitions for ai-predictions module
