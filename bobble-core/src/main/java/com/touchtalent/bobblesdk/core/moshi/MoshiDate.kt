package com.touchtalent.bobblesdk.core.moshi

import com.squareup.moshi.JsonQualifier


/**
 * created by anil_
 * on 12/04/23
 * at 5:58 PM
 *
 * Denotes that the annotated element represents a date in the format *yyyy-MM-dd*
 *
 */

@Retention(AnnotationRetention.RUNTIME)
@JsonQualifier
annotation class MoshiDate
