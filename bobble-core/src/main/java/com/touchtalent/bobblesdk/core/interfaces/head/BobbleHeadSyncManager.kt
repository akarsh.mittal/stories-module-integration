package com.touchtalent.bobblesdk.core.interfaces.head

import com.touchtalent.bobblesdk.core.model.BobbleHead
import io.reactivex.Completable
import io.reactivex.Single

/**
 * Interface to manage head sync within *bobble-head* module
 */
interface BobbleHeadSyncManager {
    /**
     * Sync any un-synced data to server.
     * @return [Completable] to sync data. Complete event is sent as soon as a single head gets
     * downloaded from server along with resources, or there is nothing to sync
     */
    fun sync(): Completable

    /**
     * Download a connection's head and add it to database given by [characterSyncId].
     * @param characterSyncId Character sync id of the head to be downloaded
     * @param characterName Character name the head to be downloaded
     * @param downloadSource Source where head is being downloaded from,
     * e.g - *head_share_notification*, *connection_welcome_notification*, etc
     * @return [Single] to download the head
     */
    fun downloadConnectionHead(
        characterSyncId: Long,
        characterName: String,
        downloadSource: String
    ): Single<BobbleHead>

    /**
     * Download a connection's head and add it to database given by [connectionCharacterShareKey]
     * and [connectionCharacterShareId].
     * @param connectionCharacterShareKey Connection character share key of the head to be downloaded
     * @param connectionCharacterShareId Connection character id key of the head to be downloaded
     * @param downloadSource Source where head is being downloaded from,
     * e.g - *head_share_notification*, *connection_welcome_notification*, etc
     * @return [Single] to download the head
     */
    fun downloadConnectionHead(
        connectionCharacterShareKey: String,
        connectionCharacterShareId: String,
        downloadSource: String
    ): Single<BobbleHead>

    /**
     * Get a shareable link for the given head.
     * @param bobbleHead Head that needs to be shared
     * @return [Single] to fetch the URL
     */
    fun getShareableLink(bobbleHead: BobbleHead): Single<String>

    /**
     * Reset sync. Called upon user-logout to reset sync status of heads
     */
    fun resetSync()

}