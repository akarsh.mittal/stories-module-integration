package com.touchtalent.bobblesdk.core.moshi

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

/**
 * Moshi adapter to convert date-time string (in the format *yyyy-MM-dd HH:mm:ss*, timezone UTC) to
 * time in millis and vice versa
 * @see DateTime
 */
class DateTimeAdapter {

    private val format: DateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH).also {
        it.timeZone = TimeZone.getTimeZone("UTC")
    }

    /**
     * @suppress
     */
    @ToJson
    fun toJson(@DateTime timeInMillis: Long?): String? {
        return timeInMillis?.let { format.format(Date(it)) }
    }

    /**
     * @suppress
     */
    @FromJson
    @DateTime
    fun fromJson(formattedTime: String?): Long? {
        return formattedTime?.let { format.parse(it)?.time }
    }


    /**
     * @suppress
     */
    @ToJson
    fun toJson(@DateTime timeInMillis: Long): String {
        return timeInMillis.let { format.format(Date(it)) }
    }

    /**
     * @suppress
     */
    @FromJson
    @DateTime
    fun fromJson(formattedTime: String): Long {
        return formattedTime.let { format.parse(it)?.time ?: throw java.lang.Exception() }
    }

}