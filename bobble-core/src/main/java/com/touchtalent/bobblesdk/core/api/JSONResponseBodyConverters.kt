package com.touchtalent.bobblesdk.core.api

import okhttp3.ResponseBody
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Converter
import java.io.IOException

/**
 * Created by MarcinOz on 2016-04-21.
 * Copyright (C) 2016 OKE Poland Sp. z o.o. All rights reserved.
 */
class JSONResponseBodyConverters private constructor() {
    internal class JSONObjectResponseBodyConverter : Converter<ResponseBody, JSONObject?> {
        @Throws(IOException::class)
        override fun convert(value: ResponseBody): JSONObject {
            return JSONObject(value.string())
        }

        companion object {
            val INSTANCE = JSONObjectResponseBodyConverter()
        }
    }

    internal class JSONArrayResponseBodyConverter : Converter<ResponseBody, JSONArray?> {
        @Throws(IOException::class)
        override fun convert(value: ResponseBody): JSONArray {
            return JSONArray(value.string())
        }

        companion object {
            val INSTANCE = JSONArrayResponseBodyConverter()
        }
    }
}