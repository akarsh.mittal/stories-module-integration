package com.touchtalent.bobblesdk.core.utils;

import android.util.Log;

import com.androidnetworking.error.ANError;

/**
 * Utility function as a replacement of {@link Log}. Logs are disabled in release builds.
 */

public final class BLog {

    private static boolean IS_LOGGING_ENABLED = false;
    private static String TAG = "BobbleConstants";

    private BLog() {
        // This class is not publicly instantiable
    }

    public static void enableLogging() {
        IS_LOGGING_ENABLED = true;
    }

    public static void disableLogging() {
        IS_LOGGING_ENABLED = false;
    }

    public static void setTag(String tag) {
        if (tag == null) {
            return;
        }
        TAG = tag;
    }

    public static void d(String tag, String message) {
        if (IS_LOGGING_ENABLED) {
            if (message == null)
                message = "";
            Log.d(tag, message);
        }
    }

    public static void d(String tag, String message, Throwable e) {
        if (IS_LOGGING_ENABLED) {
            if (message == null)
                message = "";
            Log.d(tag, message, e);
        }
    }

    public static void d(String message) {
        if (IS_LOGGING_ENABLED) {
            if (message == null)
                message = "";
            Log.d(TAG, message);
        }
    }

    public static void e(String message) {
        if (IS_LOGGING_ENABLED) {
            if (message == null)
                message = "";
            Log.e(TAG, message);
        }
    }

    public static void e(String tag, String message) {
        if (IS_LOGGING_ENABLED) {
            if (message == null)
                message = "";
            Log.e(tag, message);
        }
    }

    public static void e(String tag, String message, Throwable e) {
        if (IS_LOGGING_ENABLED) {
            if (message == null)
                message = "";
            Log.e(tag, message, e);
            if (e instanceof ANError) {
                e(tag, "message: " + e.getMessage());
                e(tag, "code: " + ((ANError) e).getErrorCode());
                e(tag, "body: " + ((ANError) e).getErrorBody());
                e(tag, "errorDetail: " + ((ANError) e).getErrorDetail());
            }
        }
    }

    public static void i(String message) {
        if (IS_LOGGING_ENABLED) {
            if (message == null)
                message = "";
            Log.i(TAG, message);
        }
    }

    public static void i(String tag, String message) {
        if (IS_LOGGING_ENABLED) {
            if (message == null)
                message = "";
            Log.i(tag, message);
        }
    }

    public static void w(String message) {
        if (IS_LOGGING_ENABLED) {
            if (message == null)
                message = "";
            Log.w(TAG, message);
        }
    }

    public static void w(String tag, String message) {
        if (IS_LOGGING_ENABLED) {
            if (message == null)
                message = "";
            Log.w(tag, message);
        }
    }

    public static void w(String tag, String message, Throwable e) {
        if (IS_LOGGING_ENABLED) {
            if (message == null)
                message = "";
            Log.w(tag, message, e);
            if (e instanceof ANError) {
                w(tag, "message: " + e.getMessage());
                w(tag, "code: " + ((ANError) e).getErrorCode());
                w(tag, "body: " + ((ANError) e).getErrorBody());
                w(tag, "errorDetail: " + ((ANError) e).getErrorDetail());
            }
        }
    }

    public static void wtf(String message) {
        if (IS_LOGGING_ENABLED) {
            if (message == null)
                message = "";
            Log.wtf(TAG, message);
        }
    }

    public static void wtf(String tag, String message) {
        if (IS_LOGGING_ENABLED) {
            if (message == null)
                message = "";
            Log.wtf(tag, message);
        }
    }

    public static void printStackTrace(Throwable e) {
        if (e != null) {
            if (e instanceof ANError) {
                w("BLOG", "message: " + e.getMessage());
                w("BLOG", "code: " + ((ANError) e).getErrorCode());
                w("BLOG", "body: " + ((ANError) e).getErrorBody());
                w("BLOG", "errorDetail: " + ((ANError) e).getErrorDetail());
            }
            w("BLOG", "STACK TRACE", e);
        }
    }

    public static void warnMissingEventParam(String eventName, String fieldName) {
        w("BobbleEvents", "Missing \"" + fieldName + "\" for \"" + eventName + "\"");
    }
}
