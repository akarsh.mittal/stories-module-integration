package com.touchtalent.bobblesdk.core.cache

/**
 * Convenient function to create cache using [CacheGenerator]
 */
fun cache(block: CacheGenerator.() -> Unit): String {
    val cacheGenerator = CacheGenerator()
    block.invoke(cacheGenerator)
    return cacheGenerator.getCacheKey()
}