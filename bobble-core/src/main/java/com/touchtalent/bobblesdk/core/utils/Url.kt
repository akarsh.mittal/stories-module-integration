package com.touchtalent.bobblesdk.core.utils

import android.webkit.MimeTypeMap
import java.net.URLDecoder


/**
 * created by anil_
 * on 21/03/23
 * at 3:49 PM
 */

typealias Url = String

/**
 * returns name of file without extension from Url or original string if string is empty or does not contains
 * extension (.)
 */
fun Url.getFileNameWithoutExtension(): String {
    return getFileNameFromUrl().substringBeforeLast(".")
}

/**
 * gets FileName from Url or original string if file delimiter not found
 */
fun Url.getFileNameFromUrl(): String {
    return substringAfterLast("/")
}

/**
 * Gets comma separated values after url decoding string
 * returns empty list if none found
 */
fun Url.urlDecodeAndGetCommaSeparatedValues(): List<String> {
    return kotlin.runCatching { URLDecoder.decode(this, "UTF-8") }.getOrDefault("").split(",")
}


fun Url.getMimeTypeFromUrl(): String? {
    val fileExtension = MimeTypeMap.getFileExtensionFromUrl(this)
    return MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileExtension)
}

fun Url.getMimeFolderFromUrl(): String {
    val fileExtension = MimeTypeMap.getFileExtensionFromUrl(this)
    return MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileExtension)?.substringBefore("/")
        ?: "others"
}