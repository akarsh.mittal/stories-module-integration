package com.touchtalent.bobblesdk.core.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

/**
 * Wrapper of recycler view that automates visibility of an ErrorView on top the recyclerView
 * whenever the count of RecyclerView items is zero
 */
public class CommonEmptyRecyclerView extends EmptyRecyclerView  {
    private View emptyView;
    private int mHeaderSize = 0;

    final private RecyclerView.AdapterDataObserver observer = new RecyclerView.AdapterDataObserver() {
        @Override
        public void onChanged() {
            checkIfEmpty();
        }

        @Override
        public void onItemRangeInserted(int positionStart, int itemCount) {
            checkIfEmpty();
        }

        @Override
        public void onItemRangeRemoved(int positionStart, int itemCount) {
            checkIfEmpty();
        }
    };

    public CommonEmptyRecyclerView(Context context) {
        super(context);
    }

    public CommonEmptyRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CommonEmptyRecyclerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void checkIfEmpty() {
        if (emptyView != null && getAdapter() != null) {
            final boolean emptyViewVisible = getAdapter().getItemCount() == mHeaderSize;
            emptyView.setVisibility(emptyViewVisible ? View.VISIBLE : View.GONE);
            setVisibility(emptyViewVisible ? View.GONE : View.VISIBLE);
            getRecycledViewPool().clear();
        }
    }

    @Override
    public void setAdapter(RecyclerView.Adapter adapter) {
        final RecyclerView.Adapter oldAdapter = getAdapter();
        if (oldAdapter != null) {
            oldAdapter.unregisterAdapterDataObserver(observer);
        }
        super.setAdapter(adapter);
        if (adapter != null) {
            adapter.registerAdapterDataObserver(observer);
        }

        checkIfEmpty();
    }

    @Override
    public void setEmptyView(View emptyView) {
        this.emptyView = emptyView;
        checkIfEmpty();
    }

    @Override
    public void setLoaderView(View LoaderView) {
    }

    @Override
    public View getEmptyView() {
        return this.emptyView;
    }

    public void setHeaderSize(int headerSize) {
        mHeaderSize = headerSize;
    }

}