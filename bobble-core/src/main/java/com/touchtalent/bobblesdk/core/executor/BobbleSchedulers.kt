package com.touchtalent.bobblesdk.core.executor

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Common class to be used to for all Rx scheduler based requirements. To be deprecated soon in
 * favour of coroutines dispatchers
 */
object BobbleSchedulers {

    private val contentScheduler =
        Schedulers.from(BobbleCore.getInstance().executorSupplier.forContentTasks())

    private val commonScheduler =
        Schedulers.from(BobbleCore.getInstance().executorSupplier.forCommonThreadTasks())

    /**
     * Scheduler to be used for any content related task which involves high number of Bitmap
     * processing. This scheduler is backed by a executor pool of fixed number of threads with
     * higher priority
     */
    fun content() = contentScheduler

    /**
     * Common scheduler to be used for any general task which needs to be done on worker thread.
     * This scheduler is backed by a executor pool of fixed number of threads.
     */
    fun common() = commonScheduler


    /**
     * Scheduler to be used for general IO (network or disk usage) related task.
     */
    @JvmStatic
    fun io() = Schedulers.io()

    /**
     * Scheduler to be used for main thread operations.
     */
    @JvmStatic
    fun main(): Scheduler = AndroidSchedulers.mainThread()
}