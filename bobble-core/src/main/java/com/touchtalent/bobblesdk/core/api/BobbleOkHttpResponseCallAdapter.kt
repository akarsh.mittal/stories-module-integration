package com.touchtalent.bobblesdk.core.api

import retrofit2.Call
import retrofit2.CallAdapter
import java.lang.reflect.Type

/**
 * @see BobbleCallAdapterFactory
 */
class BobbleOkHttpResponseCallAdapter<T : Any>(
    private val successType: Type,
) : CallAdapter<T, Call<OkHttpResponse<T>>> {

    override fun responseType(): Type = successType

    override fun adapt(call: Call<T>): Call<OkHttpResponse<T>> {
        return BobbleOkHttpCall(call)
    }
}