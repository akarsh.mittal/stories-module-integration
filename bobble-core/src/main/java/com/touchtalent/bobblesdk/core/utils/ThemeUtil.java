package com.touchtalent.bobblesdk.core.utils;

import android.content.Context;

/**
 * Utility class for theme and resolution related actions
 */
public final class ThemeUtil {

    private ThemeUtil() {
        // This utility class is not publicly instantiable.
    }

    /**
     * Get resolution of the device - hdpi, xhdpi, xxhdpi, etc
     *
     * @param context Application context
     * @return Resolution of the device
     */
    public static String getScreenResolution(Context context) {
        float screenDensity = context.getResources().getDisplayMetrics().densityDpi;
        if (screenDensity > 640) {
            return "xxxhdpi";
        }
        if (screenDensity > 480) {
            return "xxhdpi";
        }
        if (screenDensity > 320) {
            return "xhdpi";
        }
        if (screenDensity > 240) {
            return "hdpi";
        }
        if (screenDensity > 160) {
            return "mdpi";
        }
        return "ldpi";
    }
}
