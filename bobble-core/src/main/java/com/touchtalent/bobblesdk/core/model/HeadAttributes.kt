package com.touchtalent.bobblesdk.core.model

/**
 * POJO to hold expressions and accessory data used by native code to process a head
 * @property accessoryAndWigsArrayString JSON metadata for wigs and accessories
 * @property expressionString JSON metadata for expressions
 */

class HeadAttributes {
    var accessoryAndWigsArrayString:String? = null
    var expressionString:String? = null
}