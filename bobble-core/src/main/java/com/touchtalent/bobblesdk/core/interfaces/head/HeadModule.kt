package com.touchtalent.bobblesdk.core.interfaces.head

/**
 * Base interface for *bobble-head* module
 */
interface HeadModule {
    /**
     * Check whether head creation and management is supported and enabled
     */
    fun isEnabled(): Boolean

    /**
     * Check if user has ever created a head. This might be true even if no head currently exists
     * in the database - if user created and deleted the head
     * @return true if user has created any head, false otherwise
     */
    fun isAnyHeadCreated(): Boolean

    /**
     * Get instance of head management tools
     * @see BobbleHeadManager
     */
    fun getHeadManager(): BobbleHeadManager

    /**
     * Get instance of head creator tools
     * @see BobbleHeadCreator
     */
    fun getHeadCreator(): BobbleHeadCreator

    /**
     * Get instance of head sync management tools
     * @see BobbleHeadSyncManager
     */
    fun getHeadSyncManager(): BobbleHeadSyncManager

    /**
     * Get instance of head compatibility management tools
     * @see BobbleHeadCompatibilityManager
     */
    fun getCompatibilityManager(): BobbleHeadCompatibilityManager
}