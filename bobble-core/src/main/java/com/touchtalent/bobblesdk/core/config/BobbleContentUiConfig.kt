package com.touchtalent.bobblesdk.core.config

/**
 * Config class for *content-ui* module
 * @property disableStoreIfNoHeadCreated Flag to disable store if user has not created any head
 * @property shareUrlFetcher Utility function to modify the shareUrl while sharing a content.
 * The parent might want to add prefix/suffix to the share URL to make it more personalised
 * @property onContentShare Callback when a content is shared. The client may want to keep a record
 * of the number of content being shared or activate any campaign related to the content.
 * @property enableDeepLinkPack if false, content pack sharing options are disabled in the store.
 */
class BobbleContentUiConfig {
    var disableStoreIfNoHeadCreated = true
    var enableDeepLinkPack = true
    var shareUrlFetcher: ((String) -> String?)? = null
    var onContentShare: ((contentId: Int) -> Unit)? = null
}
