package com.touchtalent.bobblesdk.core.utils

import org.json.JSONArray
import org.json.JSONObject

/**
 * Utility function which helps to handle keys of user config as a [JSONObject] with less boilerplate code.
 * The function ensure that each config is handled in as isolated try catch so that any abnormality
 * in any config doesn't affect other configs.
 *
 * @param name Key of the config
 * @param callback Callback to handle the JSONObject value, if successfully parsed
 */
inline fun JSONObject.handleAsJSONObject(name: String, callback: (jsonObject: JSONObject) -> Unit) {
    if (this.has(name)) {
        try {
            val jsonObject = this.getJSONObject(name)
            callback.invoke(jsonObject)
        } catch (e: Exception) {
            BLog.printStackTrace(e)
        }
    }
}

/**
 * Utility function which helps to handle keys of user config as a [JSONArray] with less boilerplate code.
 * The function ensure that each config is handled in as isolated try catch so that any abnormality
 * in any config doesn't affect other configs.
 *
 * @param name Key of the config
 * @param callback Callback to handle the JSONArray value, if successfully parsed
 */
inline fun JSONObject.handleAsJSONArray(name: String, callback: (jsonObject: JSONArray) -> Unit) {
    if (this.has(name)) {
        try {
            val jsonArray = this.getJSONArray(name)
            callback.invoke(jsonArray)
        } catch (e: Exception) {
            BLog.printStackTrace(e)
        }
    }
}

/**
 * Utility function which helps to handle keys of user config as a [Long] with less boilerplate code.
 * The function ensure that each config is handled in as isolated try catch so that any abnormality
 * in any config doesn't affect other configs.
 *
 * @param name Key of the config
 * @param callback Callback to handle the long value, if successfully parsed
 */
inline fun JSONObject.handleAsLong(name: String, callback: (longValue: Long) -> Unit) {
    if (this.has(name)) {
        try {
            val jsonObject = this.getLong(name)
            callback.invoke(jsonObject)
        } catch (e: Exception) {
            BLog.printStackTrace(e)
        }
    }
}

/**
 * Utility function which helps to handle keys of user config as a [Float] with less boilerplate code.
 * The function ensure that each config is handled in as isolated try catch so that any abnormality
 * in any config doesn't affect other configs.
 *
 * @param name Key of the config
 * @param callback Callback to handle the long value, if successfully parsed
 */
inline fun JSONObject.handleAsFloat(name: String, callback: (floatValue: Float) -> Unit) {
    if (this.has(name)) {
        try {
            val jsonObject = this.getDouble(name)
            callback.invoke(jsonObject.toFloat())
        } catch (e: Exception) {
            BLog.printStackTrace(e)
        }
    }
}

/**
 * Utility function which helps to handle keys of user config as a [Boolean] with less boilerplate code.
 * The function ensure that each config is handled in as isolated try catch so that any abnormality
 * in any config doesn't affect other configs.
 *
 * @param name Key of the config
 * @param callback Callback to handle the boolean value, if successfully parsed
 */
inline fun JSONObject.handleAsBoolean(name: String, callback: (booleanValue: Boolean) -> Unit) {
    if (this.has(name)) {
        try {
            val jsonObject = this.getBoolean(name)
            callback.invoke(jsonObject)
        } catch (e: Exception) {
            BLog.printStackTrace(e)
        }
    }
}

/**
 * Utility function which helps to handle keys of user config as a [String] with less boilerplate code.
 * The function ensure that each config is handled in as isolated try catch so that any abnormality
 * in any config doesn't affect other configs.
 *
 * @param name Key of the config
 * @param callback Callback to handle the String value, if successfully parsed
 */
inline fun JSONObject.handleAsString(name: String, callback: (stringValue: String) -> Unit) {
    if (this.has(name)) {
        try {
            val jsonObject = this.getString(name)
            callback.invoke(jsonObject)
        } catch (e: Exception) {
            BLog.printStackTrace(e)
        }
    }
}

/**
 * Utility function which helps to handle keys of user config as a [Int] with less boilerplate code.
 * The function ensure that each config is handled in as isolated try catch so that any abnormality
 * in any config doesn't affect other configs.
 *
 * @param name Key of the config
 * @param callback Callback to handle the integer value, if successfully parsed
 */
inline fun JSONObject.handleAsInt(name: String, callback: (intValue: Int) -> Unit) {
    if (this.has(name)) {
        try {
            val jsonObject = this.getInt(name)
            callback.invoke(jsonObject)
        } catch (e: Exception) {
            BLog.printStackTrace(e)
        }
    }
}