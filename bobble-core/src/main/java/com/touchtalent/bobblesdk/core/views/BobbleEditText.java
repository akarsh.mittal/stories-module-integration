package com.touchtalent.bobblesdk.core.views;


import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;

/**
 * Created by aamir on 08/03/18.
 */

/**
 * This class implements custom text view that can be used in custom views inside keyboard.
 */
public class BobbleEditText extends androidx.appcompat.widget.AppCompatEditText {
    private int mOldSelectionStart;
    private int mOldSelectionEnd;
    private SelectionUpdateListener mSelectionUpdateListener;
    private TextChangeListener mTextChangeListener;
    private EditTextImeBackListener mOnImeBack;

    public BobbleEditText(Context context) {
        super(context);
    }

    public BobbleEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BobbleEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }



    @Override
    public InputConnection onCreateInputConnection(EditorInfo outAttrs) {
        return super.onCreateInputConnection(outAttrs);
    }

    public void setSelectionUpdateListener(SelectionUpdateListener selectionUpdateListener) {
        this.mSelectionUpdateListener = selectionUpdateListener;
    }

    public void setTextChangeListener(TextChangeListener textChangeListener) {
        this.mTextChangeListener = textChangeListener;
    }

    @Override
    protected void onSelectionChanged(int selStart, int selEnd) {
        super.onSelectionChanged(selStart, selEnd);

        if (mSelectionUpdateListener != null) {
            mSelectionUpdateListener.onSelectionUpdate(selStart, selEnd, mOldSelectionStart, mOldSelectionEnd);
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                mOldSelectionEnd = getSelectionEnd();
                mOldSelectionStart = getSelectionStart();
                break;
        }

        return super.onTouchEvent(event);
    }

    @Override
    public void setText(CharSequence text, BufferType type) {
        super.setText("", type);
        super.setText(text, type);
        if (mTextChangeListener != null) {
            mTextChangeListener.onTextChange(text, type);
        }
    }

    @Override
    public boolean onKeyPreIme(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            if (mOnImeBack != null) {
                mOnImeBack.onImeBack(this);
            }
        }
        return super.onKeyPreIme(keyCode, event);
    }

    public void setOnEditTextImeBackListener(EditTextImeBackListener listener) {
        mOnImeBack = listener;
    }

    public interface EditTextImeBackListener {
        void onImeBack(BobbleEditText ctrl);
    }
}
