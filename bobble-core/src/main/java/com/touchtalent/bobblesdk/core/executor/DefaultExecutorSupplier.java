package com.touchtalent.bobblesdk.core.executor;

import android.os.Process;

import java.util.concurrent.Executor;
import java.util.concurrent.ThreadFactory;

/**
 * Created by Prashant Gupta on 24-09-2016.
 */

public class DefaultExecutorSupplier implements ExecutorSupplier {

    public static final int DEFAULT_MAX_NUM_THREADS = Runtime.getRuntime().availableProcessors();
    public static final int DEFAULT_MAX_NUM_THREADS_GIF = 2;

    private final Executor mMainThreadExecutor;
    //Common Thread Executor to execute all Background task and replacing Task.callInBackground
    private final CommonThreadPoolExecutor mCommonThreadExecutor;
    //Common Thread Executor to execute all Foreground tasks related to content creation
    private final CommonThreadPoolExecutor mContentThreadExecutor;

    public DefaultExecutorSupplier() {
        ThreadFactory backgroundPriorityThreadFactory = new PriorityThreadFactory(Process.THREAD_PRIORITY_BACKGROUND);
        mMainThreadExecutor = new MainThreadExecutor();
        mCommonThreadExecutor = new CommonThreadPoolExecutor(DEFAULT_MAX_NUM_THREADS, backgroundPriorityThreadFactory);
        mContentThreadExecutor = new CommonThreadPoolExecutor(DEFAULT_MAX_NUM_THREADS, new PriorityThreadFactory(Process.THREAD_PRIORITY_DEFAULT));
    }

    @Override
    public Executor forMainThreadTasks() {
        return mMainThreadExecutor;
    }

    @Override
    public CommonThreadPoolExecutor forCommonThreadTasks() {
        return mCommonThreadExecutor;
    }

    @Override
    public CommonThreadPoolExecutor forContentTasks() {
        return mContentThreadExecutor;
    }
}