package com.touchtalent.bobblesdk.core.views;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.viewpager.widget.ViewPager;
import androidx.viewpager.widget.ViewPager.OnPageChangeListener;

import com.touchtalent.bobblesdk.core.BobbleCoreSDK;
import com.touchtalent.bobblesdk.core.R;
import com.touchtalent.bobblesdk.core.utils.BLog;
import com.touchtalent.bobblesdk.core.utils.ViewUtil;

import java.util.Locale;

/**
 * Created by amitshekhar on 14/04/15.
 */
public class PagerSlidingTabStrip extends HorizontalScrollView {

    public static final String NEW = "new";
    private int previousTabPosition = 0;
    private int mSelectedTextColorCode = 0xFF00B9B7;
    private int mUnSelectedTextColorCode = 0x75000000;
    private final float mArrowWidth = ViewUtil.dpToPx(13.45f, getContext());
    private final float mArrowHeight = ViewUtil.dpToPx(5.89f, getContext());
    private int indicatorType = 0; //Refer @attr/BobbleContentView_bcvNavigationType

    public void setIndicatorType(int indicatorType) {
        this.indicatorType = indicatorType;
    }

    private final LinearLayout.LayoutParams defaultTabLayoutParams;
    private final LinearLayout.LayoutParams expandedTabLayoutParams;

    public interface TabListener {
        void clickedContentPack(int position);
    }

    TabListener mTabListener;

    public void setListener(TabListener tabListener) {
        mTabListener = tabListener;
    }

    // @formatter:off
    private static final int[] ATTRS = new int[]{
            android.R.attr.textSize,
            android.R.attr.textColor
    };
    // @formatter:on
    private final LinearLayout tabsContainer;

    public interface TabBackgroundProvider {
        View getPageTabBackground(int position);
    }

    private final PageListener pageListener = new PageListener();
    public OnPageChangeListener delegatePageListener;

    public interface TabCustomViewProvider {
        View getPageTabCustomView(int position);
    }

    private ViewPager pager;

    private int tabCount;

    private int currentPosition = 0;
    private float currentPositionOffset = 0f;
    private int selectedPosition = 0;
    private float indicatorRadius = 10f;


    private final Paint indicatorPaint;
    private final Paint dividerPaint;

    private boolean checkedTabWidths = false;

    private int indicatorColor = Color.TRANSPARENT;
    private int underlineColor = 0x1A000000;
    private int dividerColor = 0x1A000000;

    private boolean shouldExpand = false;
    private boolean textAllCaps = true;
    private boolean shouldShowIndicatorAtTop = true;

    private int scrollOffset = 52;
    private int indicatorHeight = 0; //changing it to 0 from  8;
    private int underlineHeight = 0; //changing it to 0 from  2;
    private int dividerPadding = 12;
    private int tabPadding = 24;
    private int dividerWidth = 1;
    public PagerSlidingListener mPagerSlidingListener;

    private boolean backgroundDividerDrawn;
    private int selectedPosX = 0;

    private int tabTextSize = 12;
    private int tabTextColor = 0xFF666666;
    private Typeface tabTypeface = null;
    private int tabTypefaceStyle = Typeface.BOLD;

    private int lastScrollX = 0;

    private int tabBackgroundResId = R.drawable.background_tab;

    private Locale locale;
    TabCustomViewProvider tabCustomViewProvider;
    private RedDotImageRemoveListener redDotImageRemoveListener;

    public PagerSlidingTabStrip(Context context) {
        this(context, null);
    }

    public PagerSlidingTabStrip(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public PagerSlidingTabStrip(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        setFillViewport(true);
        setWillNotDraw(false);

        tabsContainer = new LinearLayout(context);
        tabsContainer.setOrientation(LinearLayout.HORIZONTAL);
        tabsContainer.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        addView(tabsContainer);

        DisplayMetrics dm = getResources().getDisplayMetrics();

        scrollOffset = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, scrollOffset, dm);
        indicatorHeight = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, indicatorHeight, dm);
        underlineHeight = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, underlineHeight, dm);
        dividerPadding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dividerPadding, dm);
        tabPadding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, tabPadding, dm);
        dividerWidth = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dividerWidth, dm);
        tabTextSize = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, tabTextSize, dm);

        // get system attrs (android:textSize and android:textColor)

        TypedArray a = context.obtainStyledAttributes(attrs, ATTRS);

        tabTextSize = a.getDimensionPixelSize(0, tabTextSize);
        tabTextColor = a.getColor(1, tabTextColor);

        a.recycle();

        // get custom attrs

        a = context.obtainStyledAttributes(attrs, R.styleable.PagerSlidingTabStrip);

        indicatorColor = a.getColor(R.styleable.PagerSlidingTabStrip_pstIndicatorColor, indicatorColor);
        underlineColor = a.getColor(R.styleable.PagerSlidingTabStrip_pstUnderlineColor, underlineColor);
        dividerColor = a.getColor(R.styleable.PagerSlidingTabStrip_pstDividerColor, dividerColor);
        indicatorHeight = a.getDimensionPixelSize(R.styleable.PagerSlidingTabStrip_pstIndicatorHeight, indicatorHeight);
        underlineHeight = a.getDimensionPixelSize(R.styleable.PagerSlidingTabStrip_pstUnderlineHeight, underlineHeight);
        dividerPadding = a.getDimensionPixelSize(R.styleable.PagerSlidingTabStrip_pstDividerPadding, dividerPadding);
        tabPadding = a.getDimensionPixelSize(R.styleable.PagerSlidingTabStrip_pstTabPaddingLeftRight, tabPadding);
        tabBackgroundResId = a.getResourceId(R.styleable.PagerSlidingTabStrip_pstTabBackground, tabBackgroundResId);
        shouldExpand = a.getBoolean(R.styleable.PagerSlidingTabStrip_pstShouldExpand, shouldExpand);
        scrollOffset = a.getDimensionPixelSize(R.styleable.PagerSlidingTabStrip_pstScrollOffset, scrollOffset);
        textAllCaps = a.getBoolean(R.styleable.PagerSlidingTabStrip_pstTextAllCaps, textAllCaps);
        shouldShowIndicatorAtTop = a.getBoolean(R.styleable.PagerSlidingTabStrip_pstShouldShowIndicatorAtTop, shouldShowIndicatorAtTop);
        indicatorType = a.getInt(R.styleable.PagerSlidingTabStrip_pstIndicatorType, 0);

        mSelectedTextColorCode = a.getColor(R.styleable.PagerSlidingTabStrip_pstTextSelectedColor, mSelectedTextColorCode);
        mUnSelectedTextColorCode = a.getColor(R.styleable.PagerSlidingTabStrip_pstTextUnSelectedColor, mUnSelectedTextColorCode);

        a.recycle();

        indicatorPaint = new Paint();
        indicatorPaint.setAntiAlias(true);
        indicatorPaint.setColor(indicatorColor);
        indicatorPaint.setStyle(Style.FILL);

        dividerPaint = new Paint();
        dividerPaint.setAntiAlias(true);
        dividerPaint.setStrokeWidth(dividerWidth);

        defaultTabLayoutParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
        expandedTabLayoutParams = new LinearLayout.LayoutParams(0, LayoutParams.MATCH_PARENT, 1.0f);

        if (locale == null) {
            locale = getResources().getConfiguration().locale;
        }
    }

    public void setViewPager(ViewPager pager) {
        this.pager = pager;

        if (pager.getAdapter() == null) {
            throw new IllegalStateException("ViewPager does not have adapter instance.");
        }

        pager.setOnPageChangeListener(pageListener);

        notifyDataSetChanged();
    }

    public void setOnPageChangeListener(OnPageChangeListener listener) {
        this.delegatePageListener = listener;
    }


    public void setOnRedDotImageRemoveListener(RedDotImageRemoveListener redDotImageRemoveListener) {
        this.redDotImageRemoveListener = redDotImageRemoveListener;
    }

    public void notifyDataSetChanged() {

        tabsContainer.removeAllViews();

        if (pager == null || pager.getAdapter() == null)
            return;

        tabCount = pager.getAdapter().getCount();

        final TabBackgroundProvider tabBackgroundProvider;

        if (pager.getAdapter() instanceof TabBackgroundProvider) {
            tabBackgroundProvider = (TabBackgroundProvider) pager.getAdapter();
        } else {
            tabBackgroundProvider = null;
        }

        if (pager.getAdapter() instanceof TabCustomViewProvider) {
            tabCustomViewProvider = (TabCustomViewProvider) pager.getAdapter();
        } else {
            tabCustomViewProvider = null;
        }

        for (int i = 0; i < tabCount; i++) {
            final View customView = (tabCustomViewProvider != null) ? tabCustomViewProvider.getPageTabCustomView(i) : null;
            final View backgroundView;

            if (tabBackgroundProvider != null) {
                backgroundView = tabBackgroundProvider.getPageTabBackground(i);
                // backgroundView.setAlpha(0.1f);
            } else {
                backgroundView = null;
            }

            if (pager.getAdapter() != null) {
                addTab(i, pager.getAdapter().getPageTitle(i), customView, backgroundView);
            }
        }

        updateTabStyles();

        checkedTabWidths = false;

        getViewTreeObserver().addOnGlobalLayoutListener(new OnGlobalLayoutListener() {

            @SuppressWarnings("deprecation")
            @SuppressLint("NewApi")
            @Override
            public void onGlobalLayout() {

                getViewTreeObserver().removeOnGlobalLayoutListener(this);

                currentPosition = pager.getCurrentItem();
                scrollToChild(currentPosition, 0);
            }
        });

    }

    public void notifyItemChanged(int position) {
        if (pager == null || pager.getAdapter() == null)
            return;

        tabCount = pager.getAdapter().getCount();

        if (position < 0 || position >= tabCount)
            return;

        final TabBackgroundProvider tabBackgroundProvider;

        if (pager.getAdapter() instanceof TabBackgroundProvider) {
            tabBackgroundProvider = (TabBackgroundProvider) pager.getAdapter();
        } else {
            tabBackgroundProvider = null;
        }

        if (pager.getAdapter() instanceof TabCustomViewProvider) {
            tabCustomViewProvider = (TabCustomViewProvider) pager.getAdapter();
        } else {
            tabCustomViewProvider = null;
        }

        final View customView = (tabCustomViewProvider != null) ? tabCustomViewProvider.getPageTabCustomView(position) : null;
        final View backgroundView;

        if (tabBackgroundProvider != null) {
            backgroundView = tabBackgroundProvider.getPageTabBackground(position);
            // backgroundView.setAlpha(0.1f);
        } else {
            backgroundView = null;
        }

        if (pager.getAdapter() != null) {
            tabsContainer.removeViewAt(position);
            addTab(position, pager.getAdapter().getPageTitle(position), customView, backgroundView);
        }

        updateTabStyles();
    }

    private void setSelectedPosition(final int position) {
        if (selectedPosition >= 0 && selectedPosition < tabsContainer.getChildCount()) {
            tabsContainer.getChildAt(selectedPosition).setSelected(false);
        }

        selectedPosition = position;

        if (position >= 0 && position < tabsContainer.getChildCount()) {
            tabsContainer.getChildAt(position).setSelected(true);
        }
    }

    private void addTab(final int position, final CharSequence title, final View customView, final View backgroundView) {

        if ((title == null || title.length() == 0) && customView == null) {
            throw new IllegalStateException("PagerSlidingTabStrip requires that tabs have either a (non-zero length) title, a custom view, or both.");
        }

        if (customView != null) {
            final ViewParent customViewParent = customView.getParent();

            if (customViewParent instanceof ViewGroup) {
                final ViewGroup viewGroup = (ViewGroup) customViewParent;
                viewGroup.removeView(customView);
            }
        }

        if (backgroundView != null) {
            final ViewParent backgroundViewParent = backgroundView.getParent();

            if (backgroundViewParent instanceof ViewGroup) {
                final ViewGroup viewGroup = (ViewGroup) backgroundViewParent;
                viewGroup.removeView(backgroundView);
            }
        }

        final TabLayout tabLayout = new TabLayout(getContext());
        tabLayout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT));

        if (backgroundView != null) {
            tabLayout.setBackgroundView(backgroundView);
        }

        if (title != null && title.length() > 0) {
            final TextView textView = new TextView(getContext());
            textView.setText(title);
            textView.setGravity(Gravity.CENTER);
            textView.setSingleLine();
            tabLayout.setTextView(textView);
        }

        if (customView != null) {
            if (customView != null) {
                if (customView.getTag() != null && customView.getTag().equals(NEW)) {
                    tabLayout.setNewTagImageView();
                }
                tabLayout.setCustomView(customView);
            }
        }

        tabLayout.setFocusable(true);
        tabLayout.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                try {
                    if (pager.getAdapter().getCount() > position) {

                        if (mTabListener != null) {
                            mTabListener.clickedContentPack(position);
                        }
                        pager.setCurrentItem(position);

                        if (mPagerSlidingListener != null) {
                            mPagerSlidingListener.getCurrentTabPosition(position);
                        }
                    }
                } catch (Exception e) {
                    BLog.printStackTrace(e);
                }
            }
        });

        tabLayout.setSelected(position == selectedPosition);

        tabsContainer.addView(tabLayout, position);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void updateTabStyles() {

        for (int i = 0; i < tabCount; i++) {

            final TabLayout tabLayout = (TabLayout) tabsContainer.getChildAt(i);

            tabLayout.setLayoutParams(defaultTabLayoutParams);
            //tabLayout.setBackgroundResource(tabBackgroundResId);

            if (shouldExpand) {
                tabLayout.setPadding(0, 0, 0, 0);
            } else {
                tabLayout.setPadding(tabPadding, 0, tabPadding, 0);
            }

            if (tabLayout.getTextView() != null) {

                TextView textView = tabLayout.getTextView();
                textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, tabTextSize);
                textView.setTypeface(tabTypeface, tabTypefaceStyle);
                textView.setTextColor(tabTextColor);
                // setAllCaps() is only available from API 14, so the upper case is made manually if we are on a
                // pre-ICS-build
                if (textAllCaps) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
                        textView.setAllCaps(true);
                    } else {
                        textView.setText(textView.getText().toString().toUpperCase(locale));
                    }
                }
            }
        }

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        if (!shouldExpand || MeasureSpec.getMode(widthMeasureSpec) == MeasureSpec.UNSPECIFIED) {
            return;
        }

        int myWidth = getMeasuredWidth();
        int childWidth = 0;
        for (int i = 0; i < tabCount; i++) {
            childWidth += tabsContainer.getChildAt(i).getMeasuredWidth();
        }

        if (!checkedTabWidths && childWidth > 0 && myWidth > 0) {

            if (childWidth <= myWidth) {
                for (int i = 0; i < tabCount; i++) {
                    tabsContainer.getChildAt(i).setLayoutParams(expandedTabLayoutParams);
                }
            }

            checkedTabWidths = true;
        }
    }

    private void scrollToChild(int position, int offset) {

        if (tabCount == 0) {
            return;
        }

        int newScrollX = tabsContainer.getChildAt(position).getLeft() + offset;

        if (position > 0 || offset > 0) {
            newScrollX -= scrollOffset;
        }

        if (newScrollX != lastScrollX) {
            lastScrollX = newScrollX;
            scrollTo(newScrollX, 0);
        }

    }

    @Override
    protected void onDraw(final Canvas canvas) {
        super.onDraw(canvas);
        if (indicatorType == 0){
            onDrawBar(canvas);
        }else if (indicatorType == 1){
            onDrawArrow(canvas);
        }
    }

    private void onDrawArrow(Canvas canvas){

        if (isInEditMode() || tabCount == 0) {
            return;
        }

        final int height = getHeight();

        // draw divider
        final Rect clipBounds = canvas.getClipBounds();

        dividerPaint.setColor(dividerColor);

        // default: line below current tab
        View currentTab = tabsContainer.getChildAt(currentPosition);
        float lineLeft = currentTab.getLeft();
        float lineRight = currentTab.getRight();

        // if there is an offset, start interpolating left and right coordinates between current and next tab
        if (currentPositionOffset > 0f && currentPosition < tabCount - 1) {

            View nextTab = tabsContainer.getChildAt(currentPosition + 1);
            final float nextTabLeft = nextTab.getLeft();
            final float nextTabRight = nextTab.getRight();

            lineLeft = (currentPositionOffset * nextTabLeft + (1f - currentPositionOffset) * lineLeft);
            lineRight = (currentPositionOffset * nextTabRight + (1f - currentPositionOffset) * lineRight);

        }

        int midX = (int) (lineLeft + lineRight) / 2;
        Path path = new Path();
        path.setFillType(Path.FillType.EVEN_ODD);
        path.moveTo(midX - mArrowWidth / 2, height);
        path.lineTo(midX + mArrowWidth / 2, height);
        path.lineTo(midX, height - mArrowHeight);
        path.close();

        canvas.drawPath(path, indicatorPaint);
        if (midX > 0) {
            indicatorPaint.setAntiAlias(true);
            indicatorPaint.setStrokeWidth(1);
            canvas.drawLine(0, height, midX - mArrowWidth / 2 - 1, height, dividerPaint);
            canvas.drawLine(midX - mArrowWidth / 2 - 1, height, midX, height - mArrowHeight - 1, dividerPaint);
            canvas.drawLine(midX, height - mArrowHeight - 1, midX + mArrowWidth / 2 + 1, height, dividerPaint);
            canvas.drawLine(midX + mArrowWidth / 2 + 1, height, clipBounds.right, height, dividerPaint);
        }
    }

    private void onDrawBar(Canvas canvas){

        if (isInEditMode() || tabCount == 0) {
            return;
        }

        final int height = getHeight();

        // draw divider

        dividerPaint.setColor(dividerColor);

        final int tabDividerCount = (backgroundDividerDrawn) ? tabCount : tabCount - 1;

        for (int i = 0; i < tabDividerCount; i++) {
            View tab = tabsContainer.getChildAt(i);
            canvas.drawLine(tab.getRight(), dividerPadding, tab.getRight(), height - dividerPadding, dividerPaint);
        }

        // default: line below current tab
        View currentTab = tabsContainer.getChildAt(currentPosition);
        float lineLeft = currentTab.getLeft();
        float lineRight = currentTab.getRight();

        // if there is an offset, start interpolating left and right coordinates between current and next tab
        if (currentPositionOffset > 0f && currentPosition < tabCount - 1) {

            View nextTab = tabsContainer.getChildAt(currentPosition + 1);
            final float nextTabLeft = nextTab.getLeft();
            final float nextTabRight = nextTab.getRight();

            lineLeft = (currentPositionOffset * nextTabLeft + (1f - currentPositionOffset) * lineLeft);
            lineRight = (currentPositionOffset * nextTabRight + (1f - currentPositionOffset) * lineRight);
        }

        if (Build.VERSION.SDK_INT >= 21) {
            if (shouldShowIndicatorAtTop) {
                canvas.drawRoundRect(lineLeft, 0, lineRight, indicatorHeight, indicatorRadius, indicatorRadius, indicatorPaint);
            } else {
                canvas.drawRoundRect(lineLeft, height - indicatorHeight, lineRight, height, indicatorRadius, indicatorRadius, indicatorPaint);
            }
        } else {
            if (shouldShowIndicatorAtTop) {
                canvas.drawRect(lineLeft, 0, lineRight, indicatorHeight, indicatorPaint);
            } else {
                canvas.drawRect(lineLeft, height - indicatorHeight, lineRight, height, indicatorPaint);
            }
        }

        // draw underline
        canvas.drawRect(0, 0, tabsContainer.getWidth(), underlineHeight, indicatorPaint);
    }


    public int getSelectedPosX() {
        return selectedPosX;
    }

    private class PageListener implements OnPageChangeListener {

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            currentPosition = position;
            currentPositionOffset = positionOffset;

            try {
                scrollToChild(position, (int) (positionOffset * tabsContainer.getChildAt(position).getWidth()));
            } catch (Exception e) {
                BobbleCoreSDK.logException("PagerSlidingTabStrip", e);
            }

            invalidate();

            if (delegatePageListener != null) {
                delegatePageListener.onPageScrolled(position, positionOffset, positionOffsetPixels);
            }

        }

        @Override
        public void onPageScrollStateChanged(int state) {
            if (state == ViewPager.SCROLL_STATE_IDLE) {
                scrollToChild(pager.getCurrentItem(), 0);

            }
            if (delegatePageListener != null) {
                delegatePageListener.onPageScrollStateChanged(state);
            }
        }


        @Override
        public void onPageSelected(int position) {
            setSelectedPosition(position);
            BLog.e("position page strip ", "position page strip  " + position);

            if (delegatePageListener != null) {
                delegatePageListener.onPageSelected(position);
            }
            if (selectedPosition <= tabCount) {
                try {
                    TabLayout tabLayoutPresent = (TabLayout) tabsContainer.getChildAt(position);
                    TabLayout tabLayoutPrevious = (TabLayout) tabsContainer.getChildAt(previousTabPosition);
                    selectedPosX = position * tabLayoutPresent.getWidth();

                    View view = tabLayoutPresent.getCustomView();
                    if (view instanceof ImageView) {
                        ImageView imageView = (ImageView) view;
                        imageView.clearColorFilter();
                        tabLayoutPresent.setCustomView(imageView);
                    } else if (view instanceof LinearLayout) {
                        View childView = ((LinearLayout) view).getChildAt(0);
                        if (childView != null && childView instanceof ImageView) {
                            ImageView imageView = (ImageView) childView;
                            imageView.clearColorFilter();
                            tabLayoutPresent.setCustomView(view);
                        } else if (childView != null && childView instanceof TextView) {
                            TextView textView = (TextView) childView;
                            //imageView.clearColorFilter();
                            // imageView.setAlpha(0.2f);


                            textView.setTextColor(mSelectedTextColorCode);

                            tabLayoutPresent.setCustomView(view);
                        }
                    } else if (view instanceof TextView) {
                        TextView textView = (TextView) view;
                        textView.setAlpha(1.0f);
                        tabLayoutPresent.setCustomView(textView);
                    }

                    View viewPrevious = tabLayoutPrevious.getCustomView();
                    if (viewPrevious instanceof ImageView) {
                        ImageView imageViewPrevious = (ImageView) viewPrevious;
                        ColorMatrix matrix = new ColorMatrix();
                        matrix.setSaturation(0);
                        ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
                        imageViewPrevious.setColorFilter(filter);
                        tabLayoutPrevious.setCustomView(imageViewPrevious);
                        tabLayoutPrevious.removeNewTagImageView();
                        if (redDotImageRemoveListener != null) {
                            redDotImageRemoveListener.removeImage(position);
                        }
                    } else if (viewPrevious instanceof LinearLayout) {
                        View childView = ((LinearLayout) viewPrevious).getChildAt(0);
                        if (childView != null && childView instanceof ImageView) {
                            ImageView imageViewPrevious = (ImageView) childView;
                            ColorMatrix matrix = new ColorMatrix();
                            matrix.setSaturation(0);
                            ColorMatrixColorFilter filter = new ColorMatrixColorFilter(matrix);
                            imageViewPrevious.setColorFilter(filter);
                            tabLayoutPrevious.setCustomView(viewPrevious);
                            tabLayoutPrevious.removeNewTagImageView();
                            if (redDotImageRemoveListener != null) {
                                redDotImageRemoveListener.removeImage(position);
                            }
                        } else if (childView != null && childView instanceof TextView) {
                            TextView textViewPrevious = (TextView) childView;
                            textViewPrevious.setTextColor(mUnSelectedTextColorCode);
                            tabLayoutPrevious.setCustomView(viewPrevious);
                        }

                    } else if (viewPrevious instanceof TextView) {
                        TextView textViewPrevious = (TextView) viewPrevious;
                        //  textViewPrevious.setTextColor(getResources().getColor(R.color.blue_common_dialog_positive));
                        tabLayoutPrevious.setCustomView(viewPrevious);
                    }


                    previousTabPosition = selectedPosition;
                } catch (Exception e) {
                    BLog.printStackTrace(e);
                }
            }
        }
    }

    private static final class TabLayout extends ViewGroup {

        private View backgroundView;
        private View customView;
        private TextView textView;
        private ImageView newImageView;

        public TabLayout(final Context context) {
            super(context);
            setClipToPadding(false);
        }

        public View getBackgroundView() {
            return backgroundView;
        }

        public void setBackgroundView(final View backgroundView) {

            if (this.backgroundView != null) {
                removeView(this.backgroundView);
            }

            this.backgroundView = backgroundView;

            if (backgroundView != null && backgroundView.getParent() != this) {
                addView(backgroundView, 0);
            }

        }

        public void setNewTagImageView() {
            newImageView = new ImageView(getContext());
            newImageView.setBackgroundDrawable(getContext().getResources().getDrawable(R.drawable.ic_new_cat));
        }

        public void removeNewTagImageView() {
            if (this.newImageView != null) {
                removeView(this.newImageView);
                this.newImageView = null;
            }
        }

        public View getCustomView() {
            return customView;
        }

        public void setCustomView(final View customView) {

            if (this.customView != null) {
                removeView(this.customView);
            }

            if (this.newImageView != null) {
                removeView(this.newImageView);
            }

            this.customView = customView;

            if (customView != null && customView.getParent() != this) {
                if (textView == null) {
                    addView(customView);
                    if (newImageView != null) {
                        addView(newImageView);
                    }
                } else {
                    addView(customView, indexOfChild(textView));
                }
            }

        }

        public TextView getTextView() {
            return textView;
        }

        public void setTextView(final TextView textView) {

            if (this.textView != null) {
                removeView(this.textView);
            }

            this.textView = textView;

            if (textView != null && textView.getParent() != this) {
                addView(textView);
            }

        }

        @Override
        public boolean shouldDelayChildPressedState() {
            return false;
        }

        @Override
        protected void onMeasure(final int widthMeasureSpec, final int heightMeasureSpec) {

            int maxHeight = 0;
            int maxWidth = 0;

            final int count = getChildCount();

            for (int i = 0; i < count; i++) {

                final View child = getChildAt(i);

                if (child.getVisibility() != GONE && child != backgroundView) {
                    measureChild(child, widthMeasureSpec, heightMeasureSpec);

                    maxWidth = Math.max(maxWidth, child.getMeasuredWidth());
                    maxHeight = Math.max(maxHeight, child.getMeasuredHeight());
                }
            }

            maxWidth = Math.max(maxWidth + getPaddingLeft() + getPaddingRight(), getSuggestedMinimumWidth());
            maxHeight = Math.max(maxHeight + getPaddingTop() + getPaddingBottom(), getSuggestedMinimumHeight());

            final int measuredWidth = resolveSize(maxWidth, widthMeasureSpec);
            final int measuredHeight = resolveSize(maxHeight, heightMeasureSpec);

            if (backgroundView != null) {
                backgroundView.measure(MeasureSpec.makeMeasureSpec(measuredWidth, MeasureSpec.EXACTLY), MeasureSpec.makeMeasureSpec(measuredHeight, MeasureSpec.EXACTLY));
            }

            setMeasuredDimension(measuredWidth, measuredHeight);
        }

        @Override
        protected void onLayout(final boolean changed, final int left, final int top, final int right, final int bottom) {

            final int innerLeft = getPaddingLeft();
            final int innerRight = right - left - getPaddingRight();
            final int innerTop = getPaddingTop();
            final int innerBottom = bottom - top - getPaddingBottom();

            final int innerWidth = innerRight - innerLeft;
            final int innerHeight = innerBottom - innerTop;

            final int count = getChildCount();

            for (int i = 0; i < count; i++) {

                final View child = getChildAt(i);

                if (child.getVisibility() != GONE && child != backgroundView) {

                    final int width = child.getMeasuredWidth();
                    final int height = child.getMeasuredHeight();

                    final int childLeft = innerLeft + (innerWidth - width) / 2;
                    final int childRight = childLeft + width;
                    final int childTop = innerTop + (innerHeight - height) / 2;
                    final int childBottom = childTop + height;

                    child.layout(childLeft, childTop, childRight, childBottom);
                }
            }

            if (backgroundView != null) {
                backgroundView.layout(0, 0, right - left, bottom - top);
            }
        }
    }

    public void setIndicatorColor(int indicatorColor) {
        this.indicatorColor = indicatorColor;
        indicatorPaint.setColor(indicatorColor);
        invalidate();
    }

    public void setIndicatorRadius(float radius) {
        this.indicatorRadius = radius;
        invalidate();
    }

    public void setIndicatorColorResource(int resId) {
        this.indicatorColor = getResources().getColor(resId);
        invalidate();
    }

    public int getIndicatorColor() {
        return this.indicatorColor;
    }

    public void setIndicatorHeight(int indicatorLineHeightPx) {
        this.indicatorHeight = indicatorLineHeightPx;
        invalidate();
    }

    public int getIndicatorHeight() {
        return indicatorHeight;
    }

    public void setUnderlineColor(int underlineColor) {
        this.underlineColor = underlineColor;
        invalidate();
    }

    public void setUnderlineColorResource(int resId) {
        this.underlineColor = getResources().getColor(resId);
        invalidate();
    }

    public int getUnderlineColor() {
        return underlineColor;
    }

    public void setDividerColor(int dividerColor) {
        this.dividerColor = dividerColor;
        invalidate();
    }

    public void setDividerColorResource(int resId) {
        this.dividerColor = getResources().getColor(resId);
        invalidate();
    }

    public int getDividerColor() {
        return dividerColor;
    }

    public void setBackgroundDividerDrawn(final boolean backgroundDividerDrawn) {
        this.backgroundDividerDrawn = backgroundDividerDrawn;
    }

    public boolean isBackgroundDividerDrawn() {
        return backgroundDividerDrawn;
    }

    public void setUnderlineHeight(int underlineHeightPx) {
        this.underlineHeight = underlineHeightPx;
        invalidate();
    }

    public int getUnderlineHeight() {
        return underlineHeight;
    }

    public void setDividerPadding(int dividerPaddingPx) {
        this.dividerPadding = dividerPaddingPx;
        invalidate();
    }

    public int getDividerPadding() {
        return dividerPadding;
    }

    public void setScrollOffset(int scrollOffsetPx) {
        this.scrollOffset = scrollOffsetPx;
        invalidate();
    }

    public void selectedPage(int position) {

        if (pageListener != null) {
            pageListener.onPageSelected(position);
        }

    }

    public int getScrollOffset() {
        return scrollOffset;
    }

    public void setShouldExpand(boolean shouldExpand) {
        this.shouldExpand = shouldExpand;
        requestLayout();
    }

    public boolean getShouldExpand() {
        return shouldExpand;
    }

    public boolean isTextAllCaps() {
        return textAllCaps;
    }

    public void setAllCaps(boolean textAllCaps) {
        this.textAllCaps = textAllCaps;
    }

    public void setTextSize(int textSizePx) {
        this.tabTextSize = textSizePx;
        updateTabStyles();
    }

    public int getTextSize() {
        return tabTextSize;
    }

    public void setTextColor(int textColor) {
        this.tabTextColor = textColor;
        updateTabStyles();
    }

    public void setTextColorResource(int resId) {
        this.tabTextColor = getResources().getColor(resId);
        updateTabStyles();
    }

    public int getTextColor() {
        return tabTextColor;
    }

    public void setTypeface(Typeface typeface, int style) {
        this.tabTypeface = typeface;
        this.tabTypefaceStyle = style;
        updateTabStyles();
    }

    public void setTabBackground(int resId) {
        this.tabBackgroundResId = resId;
    }

    public int getTabBackground() {
        return tabBackgroundResId;
    }

    public void setTabPaddingLeftRight(int paddingPx) {
        this.tabPadding = paddingPx;
        updateTabStyles();
    }

    public int getTabPaddingLeftRight() {
        return tabPadding;
    }

    public void setShouldShowIndicatorAtTop(boolean val) {
        this.shouldShowIndicatorAtTop = val;
    }

    public boolean getShouldShowIndicatorAtTop() {
        return this.shouldShowIndicatorAtTop;
    }

    @Override
    public void onRestoreInstanceState(Parcelable state) {
        SavedState savedState = (SavedState) state;
        super.onRestoreInstanceState(savedState.getSuperState());
        currentPosition = savedState.currentPosition;
        setSelectedPosition(currentPosition);
        requestLayout();
    }

    @Override
    public Parcelable onSaveInstanceState() {
        Parcelable superState = super.onSaveInstanceState();
        SavedState savedState = new SavedState(superState);
        savedState.currentPosition = currentPosition;
        return savedState;
    }

    static class SavedState extends BaseSavedState {
        int currentPosition;

        public SavedState(Parcelable superState) {
            super(superState);
        }

        private SavedState(Parcel in) {
            super(in);
            currentPosition = in.readInt();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            super.writeToParcel(dest, flags);
            dest.writeInt(currentPosition);
        }

        public static final Parcelable.Creator<SavedState> CREATOR = new Parcelable.Creator<SavedState>() {
            @Override
            public SavedState createFromParcel(Parcel in) {
                return new SavedState(in);
            }

            @Override
            public SavedState[] newArray(int size) {
                return new SavedState[size];
            }
        };
    }

    public void setPageListener(PagerSlidingListener pagerSlidingListener) {
        mPagerSlidingListener = pagerSlidingListener;
    }

    public interface PagerSlidingListener {
        void getCurrentTabPosition(int position);
    }

    public interface RedDotImageRemoveListener {
        void removeImage(int position);
    }
}

