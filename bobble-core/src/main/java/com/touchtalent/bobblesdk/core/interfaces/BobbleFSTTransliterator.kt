package com.touchtalent.bobblesdk.core.interfaces

import androidx.annotation.Keep

/**
 * Interface for a transliterator - Transliteration stands for conversion of a language written in
 * English script to native script. e.g - "namaste" -> "नमस्ते"
 */
@Keep
interface BobbleFSTTransliterator {

    /**
     * Initialises the transliterator with given resource folder - [modelPath]
     * @throws Exception if folder doesn't exists or files are missing
     */
    @Throws(Exception::class)
    fun init(modelPath: String)

    /**
     * Process transliteration for the given [query]
     * @throws Exception if not successfully initialised
     * @return transliteration text
     */
    @Throws(Exception::class)
    fun getTransliteration(query: String): String

    /**
     * Release resources held by the transliterator
     * @throws Exception
     */
    @Throws(Exception::class)
    fun deleteNative()

}