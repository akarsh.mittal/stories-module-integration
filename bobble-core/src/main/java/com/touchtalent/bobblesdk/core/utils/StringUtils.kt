package com.touchtalent.bobblesdk.core.utils


/**
 * created by anil_
 * on 05/03/23
 * at 10:14 PM
 */

fun String.isValidJsonUrl(): Boolean {
    return isNotEmpty() && substring(lastIndexOf(".")) == ".json"
}