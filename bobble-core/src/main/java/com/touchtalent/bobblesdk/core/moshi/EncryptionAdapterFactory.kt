package com.touchtalent.bobblesdk.core.moshi

import android.util.Base64
import com.squareup.moshi.*
import com.touchtalent.bobblesdk.core.BobbleCoreSDK
import java.lang.reflect.Type

/**
 * Moshi adapter to convert a POJO to encrypted string instead of JSON for POST APIs.
 * The adapter only supports serialisation and does not support deserialization
 *
 * @see EncryptedBase64
 */
class EncryptionAdapterFactory : JsonAdapter.Factory {
    override fun create(type: Type, annotations: Set<Annotation>, moshi: Moshi): JsonAdapter<Any>? {
        val delegateAnnotations = Types.nextAnnotations(annotations, EncryptedBase64::class.java)
            ?: return null
        val delegateAdapter: JsonAdapter<Any> = moshi.adapter(type, delegateAnnotations)
        val nonNullStringAdapter = moshi.adapter(String::class.java)
        return EncryptionAdapter(delegateAdapter, nonNullStringAdapter)
    }

    class EncryptionAdapter(private val delegateAdapter: JsonAdapter<Any>, private val nonNullStringAdapter: JsonAdapter<String>) : JsonAdapter<Any>() {
        override fun toJson(writer: JsonWriter, value: Any?) {
            val data = delegateAdapter.toJson(value)
            val encryptedData = BobbleCoreSDK.getAppController()
                .getEncryptionManager()
                .encrypt(data)
                .getOrThrow()
            val base64 = Base64.encode(encryptedData, Base64.DEFAULT)
            nonNullStringAdapter.toJson(writer, String(base64))
        }

        override fun fromJson(reader: JsonReader): Any? {
            throw java.lang.UnsupportedOperationException()
        }
    }
}