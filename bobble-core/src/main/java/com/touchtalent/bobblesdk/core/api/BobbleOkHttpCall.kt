package com.touchtalent.bobblesdk.core.api

import okhttp3.Request
import okio.Timeout
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * @see BobbleCallAdapterFactory
 */
class BobbleOkHttpCall<T>(private val delegate: Call<T>) : Call<OkHttpResponse<T>> {

    override fun enqueue(callback: Callback<OkHttpResponse<T>>) {
        return delegate.enqueue(object : Callback<T> {
            override fun onResponse(call: Call<T>, response: Response<T>) {
                val okHttpResponse = OkHttpResponse(
                    call.request(),
                    BobbleResult.success(response)
                )
                callback.onResponse(this@BobbleOkHttpCall, Response.success(okHttpResponse))
            }

            override fun onFailure(call: Call<T>, t: Throwable) {
                val okHttpResponse = OkHttpResponse<T>(
                    call.request(),
                    BobbleResult.failure(t)
                )
                callback.onResponse(this@BobbleOkHttpCall, Response.success(okHttpResponse))
            }
        })
    }

    override fun clone(): Call<OkHttpResponse<T>> = BobbleOkHttpCall(delegate)
    override fun isExecuted(): Boolean = delegate.isExecuted
    override fun cancel() = delegate.cancel()
    override fun isCanceled() = delegate.isCanceled
    override fun request(): Request = delegate.request()
    override fun timeout(): Timeout = delegate.timeout()

    override fun execute(): Response<OkHttpResponse<T>> {
        throw UnsupportedOperationException("BobbleCall doesn't support execute")
    }
}