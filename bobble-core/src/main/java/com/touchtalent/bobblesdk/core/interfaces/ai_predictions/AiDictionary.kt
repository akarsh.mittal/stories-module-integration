package com.touchtalent.bobblesdk.core.interfaces.ai_predictions

import com.touchtalent.bobblesdk.core.enums.DictionaryType
import java.io.File

/**
 * AiDictionary interface to provide ai based predictions
 */
interface AiDictionary {

    /**
     * Initialises the dictionary.
     * @param aiFolderResource Folder containing models, JSONs, etc for the predictions
     * @param dictionaryType Dictionary type
     */
    fun init(
        aiFolderResource: File,
        dictionaryType: DictionaryType?
    )

    /**
     * Check if the dictionary is successfully loaded.
     * @return true if loaded, false otherwise
     */
    fun isLoaded(): Boolean

    /**
     * Close the dictionary to release resources.
     */
    fun close()

    /**
     * @return resource folder path used by the dictionary
     */
    fun getResourcePath(): String

    /**
     * Process the n-grams and predict the next word.
     * @param previousWords List of n-grams which upon joining with a space should represent the
     * sentence
     * @return List of predictions sorted in the order of confidence (Highest confidence word at
     * lowest index)
     */
    fun getPredictions(previousWords: Array<String>): List<String>
}