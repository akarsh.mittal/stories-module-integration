package com.touchtalent.bobblesdk.core.webViewPreCache

val filesExtensionsToMimeTypes = hashMapOf(
    "png" to "image/png",
    "webp" to "image/webp",
    "jpg" to "image/jpeg",
    "jpeg" to "image/jpeg",
    "js" to "application/javascript",
    "css" to "text/css",
    "woff" to "font/woff",
    "woff2" to "font/woff2"
)

/**
 * store url of assets that are cached
 */
val webCachedAssets = mutableSetOf<String>()

const val WEB_ASSETS_CACHE_KEY = "web_assets"
const val WEB_ASSETS_DIRECTORY = "webAssets"