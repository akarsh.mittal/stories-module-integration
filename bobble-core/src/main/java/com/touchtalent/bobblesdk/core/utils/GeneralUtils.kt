package com.touchtalent.bobblesdk.core.utils

import android.app.Activity
import android.app.ActivityManager
import android.content.Context
import android.content.ContextWrapper
import android.content.res.Configuration
import android.os.Build
import android.os.Looper
import android.os.SystemClock
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.touchtalent.bobblesdk.core.BobbleCoreSDK
import com.touchtalent.bobblesdk.core.BuildConfig
import com.touchtalent.bobblesdk.core.R
import com.touchtalent.bobblesdk.core.api.BobbleResult
import com.touchtalent.bobblesdk.core.executor.BobbleSchedulers
import io.reactivex.Completable
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.HttpException
import retrofit2.Response
import java.util.*

/**
 * Utility class for generic use cases like showing toast, fetching random placeholder color, etc
 */
object GeneralUtils {

    @Deprecated(
        "Deprecated in favour of kotlin's default extension function",
        replaceWith = ReplaceWith(expression = "objects?.toList() ?: emptyList()")
    )
    @JvmStatic
    fun getArrayInListFormat(objects: IntArray?): List<Int> {
        val list: MutableList<Int> = ArrayList()
        if (objects != null && objects.isNotEmpty()) {
            for (`object` in objects) {
                list.add(`object`)
            }
        }
        return list
    }

    @Deprecated(
        "Deprecated in favour of ContextUtils.isDarkMode",
        replaceWith = ReplaceWith(
            expression = "ContextUtils.isDarkMode(context)",
            imports = arrayOf("com.touchtalent.bobblesdk.core.utils.ContextUtils")
        )
    )
    @JvmStatic
    fun isSystemInDarkMode(context: Context): Boolean {
        val nightModeFlags = context.resources.configuration.uiMode and
                Configuration.UI_MODE_NIGHT_MASK
        return nightModeFlags == Configuration.UI_MODE_NIGHT_YES
    }

    /**
     * Checks if the context is valid to be used with Glide
     */
    @JvmStatic
    fun isValidContextForGlide(context: Context?): Boolean {
        if (context == null) {
            return false
        }
        var activity: Context? = context
        while (activity is ContextWrapper && activity !is AppCompatActivity) {
            if ((context as ContextWrapper).baseContext == context)
                break
            activity = activity.baseContext
        }
        if (activity is Activity) {
            return !activity.isDestroyed && !activity.isFinishing
        }
        return true
    }

    private var mLastClickTime = SystemClock.elapsedRealtime()
    private const val CLICK_TIME_INTERVAL: Long = 1200

    @Deprecated("Use StressProofClickListener instead")
    @JvmStatic
    fun canProceedWithClick(): Boolean {
        val now = SystemClock.elapsedRealtime()
        if (now - mLastClickTime < CLICK_TIME_INTERVAL) {
            return false
        }
        mLastClickTime = now
        return true
    }

    /**
     * Use to show toast in a safe environment avoiding android version specific crashes.
     * @param context Context for toast
     * @param message Message to be shown
     */
    @JvmStatic
    fun showToast(context: Context?, message: String?) {
        if (Looper.getMainLooper() != Looper.myLooper()) {
            Completable.fromRunnable {
                showToast(context, message)
            }.subscribeOn(BobbleSchedulers.main())
                .subscribe()
            return
        }
        try {
            if (Build.VERSION.SDK_INT == 25 || Build.VERSION.SDK_INT == 24) {
                return
            }
            if (isValidContextForGlide(context))
                Toast.makeText(
                    context,
                    message,
                    Toast.LENGTH_SHORT
                ).show()
        } catch (ignored: Exception) {
        }
    }

    /**
     * Use to show toast in a safe environment avoiding android version specific crashes.
     * @param context Context for toast
     * @param message Message to be shown
     */
    @JvmStatic
    fun showLongToast(context: Context?, message: String?) {
        if (Looper.getMainLooper() != Looper.myLooper()) {
            Completable.fromRunnable {
                showToast(context, message)
            }.subscribeOn(BobbleSchedulers.main())
                .subscribe()
            return
        }
        try {
            if (isValidContextForGlide(context))
                Toast.makeText(
                    context,
                    message,
                    Toast.LENGTH_LONG
                ).show()
        } catch (ignored: Exception) {
            ignored.printStackTrace()
        }
    }

    /**
     * Get a random color for placeholder.
     * @param context Context to resolve light/dark mode
     * @return Random color int to be used as a placeholder
     */
    @JvmStatic
    fun getRandomDrawableColor(context: Context): Int {
        // placeholderColors should not cached, because change in dark mode is reflected through context
        val placeHolderColors =
            context.resources.getIntArray(R.array.bobble_core_placeholder_colors)
        val id = Random().nextInt(placeHolderColors.size)
        return placeHolderColors[id]
    }

    /**
     * Converts any object to a JSONObject. Make sure the object is annotated with @JsonClass
     */
    inline fun <reified T> T.toJsonObject(): JSONObject {
        return JSONObject(BobbleCoreSDK.moshi.adapter(T::class.java).toJson(this))
    }

    /**
     * Converts any List to a JSONArray. Make sure all objects in the list are annotated with @JsonClass
     */
    inline fun <reified T : List<Any>> T.toJsonArray(): JSONArray {
        return JSONArray(BobbleCoreSDK.moshi.adapter(T::class.java).toJson(this))
    }

    /**
     * Helper function to convert a Object into JSON string
     */
    inline fun <reified T> T.stringify(): String {
        return BobbleCoreSDK.moshi.adapter(T::class.java).toJson(this)
    }

    /**
     * Converts Retrofit [Response] to kotlin [Result]
     */
    fun <T> Response<T>.toResult(): Result<T> {
        return if (isSuccessful) {
            body()?.let { Result.success(it) }
                ?: Result.failure(HttpException(this))
        } else Result.failure(HttpException(this))
    }

    fun <T> Response<T>.toBobbleResult(): BobbleResult<T> {
        return if (isSuccessful) {
            body()?.let { BobbleResult.success(it) }
                ?: BobbleResult.failure(HttpException(this))
        } else BobbleResult.failure(HttpException(this))
    }

    fun totalRamMemorySize(context: Context): DeviceType {
        val mi = ActivityManager.MemoryInfo()
        val activityManager = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        activityManager.getMemoryInfo(mi)
        val ramRange = mi.totalMem.toDouble() / (1024 * 1024 * 1024)
        return if (ramRange < 3) {
            DeviceType.LOW_END_DEVICE
        } else if (ramRange <= 3) {
            DeviceType.MID_END_DEVICE
        } else
            DeviceType.HIGH_END_DEVICE
    }


    enum class DeviceType {
        LOW_END_DEVICE,
        MID_END_DEVICE,
        HIGH_END_DEVICE
    }
}

/**
 * Function to be used when a unexpected point is reached in the code.
 * Will log it to firebase as non-fatal and crash in debug mode
 */
inline fun <reified T> createError(throwable: Throwable, logFirebase: Boolean = false): Result<T> {
    if (logFirebase)
        BobbleCoreSDK.getAppController().logException("createError", throwable)
    if (BuildConfig.DEBUG)
        throw throwable
    else return Result.failure(throwable)
}

/**
 * Converts any List to a JSONArray. Make sure all objects in the list are annotated with @JsonClass
 */
inline fun <reified T : Any> String.convert(): T? {

    return try {
        BobbleCoreSDK.moshi.adapter(T::class.java).fromJson(this)
    } catch (ex :Exception){
        return null
    }
}

/**
 * Function to concatenate items of given [lists] and return a single list
 */
fun <T> concatenateUnique(lists: List<List<T>>): List<T> {
    val set = mutableSetOf<T>()
    lists.forEach { list ->
        set.addAll(list)
    }
    return set.toList()
}

/**
 * Utility function to log time taken by a given code [block]. Logs are sent to logcat with the [tag] passed
 */
inline fun <T> logTime(tag: String, block: () -> T): T {
    val start = SystemClock.uptimeMillis()
    val result = block()
    BLog.d(tag, "Time taken: ${SystemClock.uptimeMillis() - start}ms")
    return result
}

/**
 * Utility function to run a piece of code [callback] only in Debug variant
 */
inline fun onlyOnDebug(callback: () -> Unit) {
    if (BuildConfig.DEBUG)
        callback()
}

/**
 * Utility function to run a piece of code [callback] only in Release variant
 */
inline fun onlyOnRelease(callback: () -> Unit) {
    if (!BuildConfig.DEBUG)
        callback()
}

/**
 * Utility function to log debug messages to console in Debug mode.
 * It is preferred over [BLog] as it is a inline function that takes callback as a method. So the
 * extra processing taken by the code to generate logs is avoided on Release builds
 */
inline fun logDebug(
    tag: String,
    msgCallback: () -> String,
    errCallback: (() -> Throwable?) = { null }
) {
    if (!BuildConfig.DEBUG)
        return
    errCallback.invoke()?.let {
        Log.d(tag, msgCallback(), it)
    } ?: run {
        Log.d(tag, msgCallback())
    }
}

/**
 * Utility function to log warning messages to console in Debug mode.
 * It is preferred over [BLog] as it is a inline function that takes callback as a method. So the
 * extra processing taken by the code to generate logs is avoided on Release builds
 */
inline fun logWarning(
    tag: String,
    msgCallback: () -> String,
    errCallback: (() -> Throwable?) = { null }
) {
    if (!BuildConfig.DEBUG)
        return
    errCallback.invoke()?.let {
        Log.w(tag, msgCallback(), it)
    } ?: run {
        Log.w(tag, msgCallback())
    }
}

/**
 * Utility function to log error messages to console in Debug mode.
 * It is preferred over [BLog] as it is a inline function that takes callback as a method. So the
 * extra processing taken by the code to generate logs is avoided on Release builds
 */
inline fun logError(
    tag: String,
    msgCallback: () -> String,
    errCallback: (() -> Throwable?) = { null }
) {
    if (!BuildConfig.DEBUG)
        return
    errCallback.invoke()?.let {
        Log.e(tag, msgCallback(), it)
    } ?: run {
        Log.e(tag, msgCallback())
    }
}

fun JSONArray.toStringList(): List<String> {
    val list = arrayListOf<String>()
    for (i in 0 until this.length()) {
        list.add(this.getString(i))
    }
    return list
}
