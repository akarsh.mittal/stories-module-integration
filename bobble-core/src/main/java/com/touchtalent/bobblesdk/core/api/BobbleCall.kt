package com.touchtalent.bobblesdk.core.api

import com.touchtalent.bobblesdk.core.utils.GeneralUtils.toResult
import okhttp3.Request
import okio.Timeout
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * @see BobbleCallAdapterFactory
 */
class BobbleCall<T>(private val delegate: Call<T>) : Call<Result<T>> {

    override fun enqueue(callback: Callback<Result<T>>) {
        return delegate.enqueue(object : Callback<T> {
            override fun onResponse(call: Call<T>, response: Response<T>) {
                callback.onResponse(this@BobbleCall, Response.success(response.toResult()))
            }

            override fun onFailure(call: Call<T>, t: Throwable) {
                callback.onResponse(this@BobbleCall, Response.success(Result.failure(t)))
            }
        })
    }

    override fun clone(): Call<Result<T>> = BobbleCall(delegate)
    override fun isExecuted(): Boolean = delegate.isExecuted
    override fun cancel() = delegate.cancel()
    override fun isCanceled() = delegate.isCanceled
    override fun request(): Request = delegate.request()
    override fun timeout(): Timeout = delegate.timeout()

    override fun execute(): Response<Result<T>> {
        throw UnsupportedOperationException("BobbleCall doesn't support execute")
    }
}