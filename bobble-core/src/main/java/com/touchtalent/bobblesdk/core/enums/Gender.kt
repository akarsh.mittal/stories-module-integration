package com.touchtalent.bobblesdk.core.enums

/**
 * Enum for gender.
 * @property value String counterpart of the gender
 */
enum class Gender(val value: String) {
    MALE("male"), FEMALE("female"), UNISEX("unisex");

    companion object {
        /**
         * Get the [Gender] from a given string.
         * @param name String to resolve - "all" and "unisex" both are considered as [UNISEX],
         * "male" for [MALE], "female" for [FEMALE]. Any other string will default to [MALE]
         */
        fun from(name: String): Gender {
            return if (name.equals("unisex", ignoreCase = true) ||
                name.equals("all", ignoreCase = true)
            ) UNISEX
            else if (name.equals("female", ignoreCase = true))
                FEMALE
            else MALE
        }
    }
}