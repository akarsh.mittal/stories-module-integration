@file:Suppress("unused")

package com.touchtalent.bobblesdk.core.interfaces.logger

import android.util.Log
import com.google.gson.JsonObject
import com.google.gson.JsonParseException
import com.touchtalent.bobblesdk.core.BobbleCoreSDK
import io.reactivex.annotations.CheckReturnValue
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

/**
 * Utility class for logging events using Builder pattern.
 */
open class EventBuilder {
    private var screenName: String? = null
    private var eventAction: String? = null
    private var eventName: String? = null
    protected var jsonObject: JSONObject? = null
    protected var enableDebugging = false

    private var eventLabel: String? = null

    data class EventData(
        val screenName: String?,
        val eventAction: String?,
        val eventName: String,
        val eventLabel: String?,
    )

    /**
     * Set event name
     *
     * @param eventName Event name for the event.
     * @return Builder for chaining calls.
     */
    open fun withEventName(eventName: String) = apply {
        this.eventName = eventName
    }

    /**
     * Enable printing events to logcat with tag "BobbleEvent.Builder" for debugging purposes.
     * Should not be used on prod builds
     */
    fun enableDebugging() = apply {
        enableDebugging = true
    }

    /**
     * Set event action
     *
     * @param eventAction Event action for the event.
     * @return Builder for chaining calls.
     */
    @CheckReturnValue
    open fun withEventAction(eventAction: String) = apply {
        this.eventAction = eventAction
    }

    /**
     * Set screen name
     *
     * @param screenName Screen name for the event.
     * @return Builder for chaining calls.
     */
    @CheckReturnValue
    open fun withScreenName(screenName: String) = apply {
        this.screenName = screenName
    }

    @CheckReturnValue
    open fun withEventLabel(eventLabel: String?) = apply {
        this.eventLabel = eventLabel
    }

    /**
     * Add a String key-value param to the label
     *
     * @param key   Key for the param
     * @param value Value for the param, the param is omitted if value is @null
     * @return Builder for chaining calls.
     */
    @CheckReturnValue
    fun addLabelParam(key: String, value: String?) = apply {
        internalAddLabelParam(key, value)
    }

    /**
     * Add a Set as an String:JsonArray key:value pair in the label
     *
     * @param key      Key for the param
     * @param set      List which need to be flattened and appended to the JsonArray value, the param is omitted if value is @null
     * @param modifier Modifier to modify item in list to a json object
     * @return Builder for chaining calls.
     */
    open fun <T> addLabelSetParam(
        key: String,
        set: Set<T>?,
        modifier: JSONObjectModifier<T>
    ): EventBuilder {
        if (set == null) return this
        var array: JSONArray? = null
        try {
            for (item in set) {
                if (array == null) array = JSONArray()
                if (item != null) {
                    val `object` = JSONObject()
                    modifier.populate(item, `object`)
                    array.put(`object`)
                }
            }
            createJSONObject().put(key, array)
        } catch (e: JsonParseException) {
            return this
        }
        return this
    }

    private fun createJSONObject(): JSONObject {
        return jsonObject ?: JSONObject().also { jsonObject = it }
    }


    /**
     * Add a Integer key-value param to the label
     *
     * @param key   Key for the param
     * @param value Value for the param, the param is omitted if value is @null
     * @return Builder for chaining calls.
     */
    @CheckReturnValue
    fun addLabelParam(key: String, value: Int?) = apply {
        internalAddLabelParam(key, value)
    }

    /**
     * Add a Float key-value param to the label
     *
     * @param key   Key for the param
     * @param value Value for the param, the param is omitted if value is @null
     * @return Builder for chaining calls.
     */
    fun addLabelParam(key: String, value: Float?) = apply {
        internalAddLabelParam(key, value)
    }

    /**
     * Add a Integer key-value param to the label
     *
     * @param key                  Key for the param
     * @param value                Value for the param, the param is omitted if value is @null
     * @param defaultValueToIgnore Default value for the param, for which the label is ignored
     * @return Builder for chaining calls.
     */
    @CheckReturnValue
    fun addLabelParam(key: String, value: Int?, defaultValueToIgnore: Int) = apply {
        if (value == null || value == defaultValueToIgnore) return this
        internalAddLabelParam(key, value)
    }

    /**
     * Add a String key-value param to the label
     *
     * @param key                  Key for the param
     * @param value                Value for the param, the param is omitted if value is @null
     * @param defaultValueToIgnore Default value for the param, for which the label is ignored
     * @return Builder for chaining calls.
     */
    @CheckReturnValue
    fun addLabelParam(key: String, value: String?, defaultValueToIgnore: String) = apply {
        if (value == null || value == defaultValueToIgnore) return this
        internalAddLabelParam(key, value)
    }

    /**
     * Add a List as an String:JSONArray key:value pair in the label
     *
     * @param key      Key for the param
     * @param list     List which need to be flattened and appended to the JSONArray value, the param is omitted if value is @null
     * @param modifier Modifier which contains logic for modifying the each flattened item (T) of list into corresponding JSONObject.
     * @return Builder for chaining calls.
     */
    @CheckReturnValue
    fun <T> addLabelListParam(
        key: String,
        list: List<T>?,
        modifier: JSONObjectModifier<T>
    ): EventBuilder {
        if (list == null) return this
        var array: JSONArray? = null
        try {
            for (item in list) {
                if (array == null) array = JSONArray()
                if (item != null) {
                    val `object` = JSONObject()
                    modifier.populate(item, `object`)
                    array.put(`object`)
                }
            }
            jSONObject.put(key, array)
        } catch (e: JSONException) {
            return this
        }
        return this
    }


    /**
     * Add a List as an String:JSONArray key:value pair in the label
     *
     * @param key      Key for the param
     * @param list     List which need to be flattened and appended to the JSONArray value, the param is omitted if value is @null
     * @param modifier Modifier which contains logic for modifying the each flattened item (T) of list into corresponding JSONObject.
     * @return Builder for chaining calls.
     */
    @CheckReturnValue
    fun <T, K> addLabelListParam(
        key: String,
        list: List<T>?,
        function: Function<T, K>
    ): EventBuilder {
        if (list == null) return this
        var array: JSONArray? = null
        try {
            for (item in list) {
                if (array == null) array = JSONArray()
                if (item != null) {
                    array.put(function.apply(item))
                }
            }
            jSONObject.put(key, array)
        } catch (e: JSONException) {
            return this
        }
        return this
    }

    /**
     * Add a List as an String:JSONArray key:value pair in the label
     *
     * @param key      Key for the param
     * @param list     List which need to be flattened and appended to the JSONArray value, the param is omitted if value is @null
     * @return Builder for chaining calls.
     */
    @CheckReturnValue
    fun <T> addLabelListParam(key: String, list: List<T>?): EventBuilder {
        if (list == null) return this
        var array: JSONArray? = null
        try {
            for (item in list) {
                if (array == null) array = JSONArray()
                if (item != null) {
                    array.put(item)
                }
            }
            jSONObject.put(key, array)
        } catch (e: JSONException) {
            return this
        }
        return this
    }

    /**
     * Add a Set as an String:JSONArray key:value pair in the label
     *
     * @param key      Key for the param
     * @param set     List which need to be flattened and appended to the JSONArray value, the param is omitted if value is @null
     * @return EventBuilder for chaining calls.
     */
    fun <T> addLabelSetParam(key: String, set: HashSet<T>?): EventBuilder {
        if (set == null) return this
        var array: JSONArray? = null
        try {
            for (item in set) {
                if (array == null) array = JSONArray()
                if (item != null) {
                    array.put(item)
                }
            }
            jSONObject.put(key, array)
        } catch (e: JSONException) {
            return this
        }
        return this
    }

    /**
     * Add an object as an String:JSONObject key:value pair in the label
     *
     * @param key      Key for the param
     * @param item     Object which needs to be added as a param. The param is omitted if value is @null
     * @param modifier Modifier which contains logic for modifying the object into corresponding JSONObject.
     * @return EventBuilder for chaining calls.
     */
    fun <T> addLabelObjectParam(
        key: String,
        item: T?,
        modifier: JSONObjectModifier<T>
    ): EventBuilder {
        if (item == null) return this
        try {
            val `object` = JSONObject()
            modifier.populate(item, `object`)
            jSONObject.put(key, `object`)
        } catch (ignore: JSONException) {
        }
        return this
    }

    /**
     * Add an String:JSONObject key:value pair in the label
     *
     * @param key      Key for the param
     * @param modifier Modifier which contains logic for modifying the object into corresponding JSONObject.
     * @return EventBuilder for chaining calls.
     */
    fun addLabelObjectParam(
        key: String,
        modifier: JSONModifier
    ): EventBuilder {
        try {
            val `object` = JSONObject()
            modifier.populate(`object`)
            jSONObject.put(key, `object`)
        } catch (ignore: JSONException) {
        }
        return this
    }

    /**
     * Add a Long key-value param to the label
     *
     * @param key   Key for the param
     * @param value Value for the param, the param is omitted if value is @null
     * @return EventBuilder for chaining calls.
     */
    @CheckReturnValue
    fun addLabelParam(key: String, value: Long?) = apply {
        internalAddLabelParam(key, value)
    }

    /**
     * Add a Boolean key-value param to the label. The value is added as either "true" or "false"
     *
     * @param key   Key for the param
     * @param value Value for the param, the param is omitted if value is @null
     * @return EventBuilder for chaining calls.
     */
    @CheckReturnValue
    fun addLabelParam(key: String, value: Boolean?) = apply {
        internalAddLabelParam(key, value?.let { if (it) 1 else 0 })
    }

    /**
     * Adds the session id with key "session_id" to the event payload.
     * @param keyboardView Is the event generated from keyboard
     * @return EventBuilder for chaining
     */
    @CheckReturnValue
    fun withSessionId(keyboardView: Boolean?) = apply {
        keyboardView?.let { internalAddLabelParam("session_id", BobbleCoreSDK.getAppController().getSessionId(it)) }
    }

    /**
     * Adds current package name with key "package_name" to the event payload.
     * @param keyboardView Is the event generated from keyboard
     * @return EventBuilder for chaining
     */
    @CheckReturnValue
    fun withPackageName(keyboardView: Boolean?) = apply {
        keyboardView?.let {
            internalAddLabelParam(
                "package_name",
                BobbleCoreSDK.getCurrentPackageName(it)
            )
        }
    }

    /**
     * Triggers the event.
     */
    open fun log() {
        if (enableDebugging) {
            try {
                Log.d("EventBuilder", "$screenName, $eventAction, $eventName,$eventLabel")
                if (jsonObject != null) {
                    Log.d("EventBuilder", jsonObject!!.toString(4))
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }
        BobbleCoreSDK.logEvent(
            screenAt = screenName,
            eventType = eventAction,
            eventAction = eventName,
            data = if (eventLabel == null) (if (jsonObject == null) "" else jsonObject.toString()) else eventLabel
        )
    }

    fun build(): EventData {
        return EventData(
            screenName,
            eventAction,
            eventName ?: throw java.lang.Exception("eventName cannot be null"),
            if (eventLabel == null) (if (jsonObject == null) "" else jsonObject.toString()) else eventLabel
        )
    }

    /**
     * Triggers the event.
     */
    open fun logMultiple() {
        if (enableDebugging) {
            try {
                Log.d("EventBuilder", "$screenName, $eventAction, $eventName")
                if (jsonObject != null) {
                    Log.d("EventBuilder", jsonObject!!.toString(4))
                }
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }
        BobbleCoreSDK.logEvent(
            screenName,
            eventAction,
            eventName,
            if (jsonObject == null) "" else jsonObject.toString()
        )
    }

    private fun internalAddLabelParam(key: String, value: Any?) {
        try {
            jSONObject.put(key, value)
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }

    /**
     * Add a [JSONArray] to the label.
     *
     * @param key   Key for the param
     * @param jsonArray Value for the param, the param is omitted if value is @null
     * @return EventBuilder for chaining calls.
     */
    fun addLabelParam(key: String, jsonArray: JSONArray) = apply {
        internalAddLabelParam(key, jsonArray)
    }

    protected val jSONObject: JSONObject
        get() {
            if (jsonObject == null) jsonObject = JSONObject()
            return jsonObject!!
        }
}

/**
 * Modifies an item [T] to a [JSONObject] for events
 */
interface JSONObjectModifier<T> {
    /**
     * @param item Item to be converted
     * @param jsonObject JSONObject to be populated
     */
    @Throws(JSONException::class)
    fun populate(item: T, jsonObject: JSONObject)
}

/**
 * Builder for a [JSONObject]. The implementor might use combination of various variables to create
 * a [JSONObject]
 */
interface JSONModifier {
    /**
     * @param jsonObject JSONObject to be populated
     */
    @Throws(JSONException::class)
    fun populate(jsonObject: JSONObject)
}

/**
 * Converts a given object [T] into another [K]. Can be used to flatten list of objects to list of
 * primitives
 */
interface Function<T, K> {
    /**
     * Apply the conversion of [T] to [K]
     */
    fun apply(item: T): K
}