package com.touchtalent.bobblesdk.core.moshi

import com.squareup.moshi.JsonQualifier

/**
 * Denotes that the annotated element represents a HashMap of string (identifier - "dark", "light",
 * "default") to color ("#RGB" or "#AARRGGBB"). Used for dark/light support in colors.
 */
@Retention(AnnotationRetention.RUNTIME)
@JsonQualifier
annotation class HexColorMap