package com.touchtalent.bobblesdk.core.pojo

/**
 * Data class for user credentials, used where login is supported by the parent app.
 * @property accessToken Access token of the user
 * @property userId User id of the user
 */
data class UserCredentials(val accessToken: String, val userId: String)