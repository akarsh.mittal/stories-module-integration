package com.touchtalent.bobblesdk.core.interfaces.location_search

import android.widget.EditText

/**
 * Interface for view to facilitate a UI to search for location with user-typed text
 * @property superAppId Source id, used for events. e.g - super app id
 * @property source Source, used for event. e.g - "super-app"
 * @property flow Source, used for event. e.g - "dynamic_icon", "partner_services", "suggestion_pill"
 */
interface LocationSearchView {
    var superAppId: Int?
    var source: String?
    var flow : String?

    /**
     * Set source icon for the locations.
     * @param iconUrl URL for the source icon
     */
    fun setSourceIcon(iconUrl: String?)

    /**
     * Set hint on the input box
     * @param hintText hint to be set
     */
    fun setHint(hintText: String?)

    /**
     * Set current location. Used to localise the results
     * @param currentLatitude Latitude of current location
     * @param currentLongitude Longitude of current location
     * @param state Current state
     * @param state Current city
     */
    fun setCurrentLocation(
        currentLatitude: Double,
        currentLongitude: Double,
        state: String?,
        city: String?
    )

    /**
     * Set listener for callbacks
     * @param listener Listener for callbacks
     * @see SearchActionListener
     */
    fun setActionListener(listener: SearchActionListener?)

    /**
     * Get the edittext used for searching location
     * @return the edittext used for searching location
     */
    fun getEditText(): EditText
}

/**
 * Interface for click events during location search
 */
interface SearchActionListener {
    /**
     * Callback when back button is clicked
     */
    fun onBackPress()

    /**
     * Callback when a detection location is clicked by the user
     * @param lat Latitude of the destination chosen by the user
     * @param lang Longitude of the destination chosen by the user
     * @param address Complete address of the destination chosen by the user
     */
    fun onLocationSearch(lat: Double, lang: Double, address: String?)

    /**
     * Callback when location search was successful but failed to resolve its co-ordinates upon
     * user click
     */
    fun onLocationSearchFailed()
}