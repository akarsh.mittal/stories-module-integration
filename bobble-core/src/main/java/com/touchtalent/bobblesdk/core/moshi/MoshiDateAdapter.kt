package com.touchtalent.bobblesdk.core.moshi

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import java.text.SimpleDateFormat
import java.util.*


/**
 * created by anil_
 * on 12/04/23
 * at 5:58 PM
 */

class MoshiDateAdapter {
    private val dateFormat: SimpleDateFormat by lazy {
        SimpleDateFormat(
            "yyyy-MM-dd", Locale.ENGLISH
        )
    }

    @ToJson
    fun toJson(@MoshiDate moshiDate: Date?): String? {
        return moshiDate?.let {
            dateFormat.format(it)
        }
    }

    @FromJson
    @MoshiDate
    fun fromJson(dateString: String?): Date? {
        return dateString?.let {
            dateFormat.parse(it)
        }
    }
}