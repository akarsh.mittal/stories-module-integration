package com.touchtalent.bobblesdk.core.utils;

import java.io.Closeable;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import kotlin.Deprecated;

/**
 * Created by amitshekhar on 16/11/15.
 */
@Deprecated(message = "Deprecated in favor of kotlin extension functions which provide .use to close a buffer after use")
public final class CloseUtil {

    private CloseUtil() {
        // This class is not publicly instantiable.
    }

    public static final void close(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {


            }
        }
    }


    public static final void close(Socket closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {


            }
        }
    }


    public static final void close(ServerSocket closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (IOException e) {


            }
        }
    }
}