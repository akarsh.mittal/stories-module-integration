package com.touchtalent.bobblesdk.core.utils

import android.content.Context
import android.webkit.MimeTypeMap
import androidx.work.*
import com.touchtalent.bobblesdk.core.BobbleCoreSDK
import com.touchtalent.bobblesdk.core.cache.cache
import com.touchtalent.bobblesdk.core.utils.ConfigResourceDownloader.Companion.getConfigs
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.File
import java.util.concurrent.TimeUnit

/**
 * Utility class to facilitate downloading of resources/assets present in configs.
 * Features:
 * 1. Config might contain a list of URLs of assets that can be downloaded
 * 2. Each resource is downloaded independently. Partial success is supported.
 * 3. Retry mechanism in case of download failure
 * 4. Downloads happen asynchronously i.e fire and forget strategy
 * 5. Clean old config assets that are not is use
 * 6. List of downloaded files can be queried via [getConfigs]
 *
 * The files are downloaded in the local storage at this path:
 * /data/user_de/0/<package_name>/files/core/configs/<configName>/<index>/<fileNameMD5>.<extension>
 *
 */
open class ConfigResourceDownloader(
    appContext: Context,
    val params: WorkerParameters
) : CoroutineWorker(appContext, params) {

    companion object {
        private const val TAG = "ResourceDownloadWorker"
        private const val MAX_RETRY = 3
        private const val INPUT_URL = "input_url"
        private const val INPUT_CONFIG_KEY = "input_config_key"
        private const val INPUT_INDEX = "input_index"

        /**
         * Sets a download request for the given config key and list of assets
         * P.S: This function returns instantly irrespective of download success/failure.
         * This function works on fire and forget strategy.
         * @param configKey Config key which would be used as a key for storing the asset in files
         * @param assets List of assets that need to be downloaded and cached
         */
        suspend fun downloadConfig(
            worker: Class<out ListenableWorker>,
            configKey: String,
            assetsToDownload: List<String>,
        ) = withContext(Dispatchers.Default) {
            val assets = assetsToDownload.filter { it.isNotEmpty() && it.isNotBlank() }.toTypedArray()
            deleteExtraConfigs(configKey, assets.size)
            assets.forEachIndexed { index, url ->
                val request = OneTimeWorkRequest
                    .Builder(worker)
                    .setBackoffCriteria(BackoffPolicy.EXPONENTIAL, 5, TimeUnit.MINUTES)
                    .setConstraints(
                        Constraints.Builder()
                            .setRequiredNetworkType(NetworkType.CONNECTED)
                            .build()
                    )
                    .setInputData(
                        Data.Builder().putString(INPUT_URL, url)
                            .putString(INPUT_CONFIG_KEY, configKey)
                            .putInt(INPUT_INDEX, index)
                            .build()
                    )
                    .build()
                val workManager = BobbleCoreSDK.getWorkManager() ?: return@withContext
                workManager.enqueueUniqueWork(
                    configKey + index,
                    ExistingWorkPolicy.REPLACE,
                    request
                )
            }
        }

        private suspend fun deleteExtraConfigs(configKey: String, totalAssetCount: Int) =
            withContext(Dispatchers.IO) {
                val folder = FileUtil.join(BobbleCoreSDK.rootDirNonBootAware, "configs", configKey)
                if (!FileUtil.exists(folder))
                    return@withContext
                // Delete any folder whose name exceeds total count
                File(folder).listFiles { file ->
                    val index = file.name.toIntOrNull() ?: return@listFiles false
                    if (index >= totalAssetCount)
                        return@listFiles true
                    return@listFiles false
                }?.forEach { FileUtil.delete(it) }
            }

        /**
         * Retrieve the list of cached files. Returns only those files, which are cached.
         * If download requests didn't complete or didn't succeed, will return empty list
         * @param configKey Config key which was used to download the files
         * @return List of local file paths, if exists, empty list otherwise
         */
        suspend fun getConfigs(configKey: String): List<String> = withContext(Dispatchers.IO) {
            val folder = FileUtil.join(BobbleCoreSDK.rootDirNonBootAware, "configs", configKey)
            if (!FileUtil.exists(folder))
                return@withContext emptyList()
            val folders = File(folder).listFiles() ?: return@withContext emptyList()
            // Map folder names to first file within that folder
            return@withContext folders.map { return@map if (it.exists()) it else null }.mapNotNull {
                it?.listFiles()?.getOrNull(0)?.absolutePath
            }
        }
    }

    override suspend fun doWork(): Result = withContext(Dispatchers.IO) {
        val configKey: String =
            params.inputData.getString(INPUT_CONFIG_KEY) ?: return@withContext Result.failure()
        val url: String =
            params.inputData.getString(INPUT_URL)?.trim() ?: return@withContext Result.failure()
        if (url.isEmpty() || url.isBlank() || url == "null")
            return@withContext Result.failure()
        val index: Int = params.inputData.getInt(INPUT_INDEX, -1)
        logDebug(TAG, { "Downloading url: $url for configKey: $configKey[$index]" })
        val rootFolder = FileUtil.join(BobbleCoreSDK.rootDirNonBootAware, "configs")
        val folder = FileUtil.join(rootFolder, configKey, index)
        val cacheKey = cache { process(url) }
        val extension = MimeTypeMap.getFileExtensionFromUrl(url)
        if (extension.isNullOrEmpty())
            return@withContext Result.failure()
        val fileName = "$cacheKey.$extension"
        val file = FileUtil.join(folder, fileName)
        logDebug(TAG, { "Expected file path: $file" })
        if (FileUtil.exists(file)) {
            logDebug(TAG, { "File already present, returning" })
            return@withContext success()
        }
        val tmpFolder = FileUtil.createDirAndGetPath(rootFolder, "tmp")
        logDebug(TAG, { "File not found, downloading at $tmpFolder" })
        val downloadedFile = ResourceDownloader.downloadResourcesSuspend(url, destDir = tmpFolder)
        if (downloadedFile.first == null) {
            logDebug(TAG, { "$url download failed" })
            return@withContext failure()
        }
        logDebug(TAG, { "$url download succeeded, deleting old folder: $folder" })
        FileUtil.delete(folder)
        if (!FileUtil.mkdirs(folder))
            return@withContext failure()
        logDebug(TAG, { "Moving ${downloadedFile.first} to $file" })
        if (!FileUtil.rename(downloadedFile.first, file))
            return@withContext failure()
        onDownloadSuccess(configKey, index, file)
        return@withContext success()
    }

    fun success(): Result {
        return Result.success()
    }

    fun failure(): Result {
        if (runAttemptCount > MAX_RETRY)
            return Result.failure()
        return Result.retry()
    }

    open suspend fun onDownloadSuccess(configKey: String, index: Int, filePath: String) {
    }
}