package com.touchtalent.bobblesdk.core.api

import okhttp3.MediaType
import okhttp3.RequestBody
import retrofit2.Converter
import java.io.IOException

/**
 * Created by MarcinOz on 2016-04-21.
 * Copyright (C) 2016 OKE Poland Sp. z o.o. All rights reserved.
 */
class JSONRequestBodyConverter<T> private constructor() : Converter<T, RequestBody> {
    @Throws(IOException::class)
    override fun convert(value: T): RequestBody {
        return RequestBody.create(MEDIA_TYPE, value.toString())
    }

    companion object {
        val INSTANCE = JSONRequestBodyConverter<Any>()
        private val MEDIA_TYPE = MediaType.parse("text/plain; charset=UTF-8")
    }
}