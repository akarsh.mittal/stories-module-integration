package com.touchtalent.bobblesdk.core.interfaces.promotional_offers

import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout

/**
 * View encapsulating the offers and advertisement section to be plugged inside keyboard.
 * [load] needs to be called to render the view
 * @property keyboardOpenListener Lambda function to receive callback for keyboard icon click
 */
abstract class BobblePromotionalOffersView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null
) : FrameLayout(context, attrs) {
    open var keyboardOpenListener: (() -> Unit)? = null

    /**
     * Render the view based on [config] parameters
     */
    abstract fun load(config: BobbleOffersViewConfig)
}

/**
 * Config rules defining parameters for [BobblePromotionalOffersView] to load
 */
class BobbleOffersViewConfig {
    /**
     * Placement id for BobbleNativeAds (in-house ads)
     */
    var placementId: String? = null

    /**
     * Package name of current app, used for events
     */
    var packageName: String? = null

    /**
     * true to enable the advertisement, false otherwise
     */
    var enableAds: Boolean = false

    /**
     * Banner id to scroll to after data is loaded
     */
    var offerIdDeeplink: String? = null
}

