package com.touchtalent.bobblesdk.core.cache;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;

import com.touchtalent.bobblesdk.core.BobbleCoreSDK;
import com.touchtalent.bobblesdk.core.BuildConfig;
import com.touchtalent.bobblesdk.core.executor.BobbleCore;
import com.touchtalent.bobblesdk.core.utils.BLog;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.Callable;

/**
 * Created by amitshekhar on 06/08/15.
 */
public class DiskLruImageCache {

    private DiskLruCache mDiskCache;
    private Bitmap.CompressFormat mCompressFormat = Bitmap.CompressFormat.PNG;
    private int mCompressQuality = 70;
    private final int VALUE_COUNT = 1;
    private int APP_VERSION;
    private File diskCacheDir;
    private int mDiskCacheSize;

    public DiskLruImageCache(Context context, String uniqueName, int diskCacheSize,
                             Bitmap.CompressFormat compressFormat, int quality) {
        try {
            mDiskCacheSize = diskCacheSize;
            diskCacheDir = getDiskCacheDir(context, uniqueName);
            APP_VERSION = BobbleCoreSDK.getVersion();
            mCompressFormat = compressFormat;
            mCompressQuality = quality;
            init();
        } catch (Exception e) {
            BLog.printStackTrace(e);
        }
    }

    private void init() {
        try {
            mDiskCache = DiskLruCache.open(diskCacheDir, APP_VERSION, VALUE_COUNT, mDiskCacheSize);
        } catch (Exception e) {
            BLog.printStackTrace(e);
        }
    }

    private boolean writeBitmapToFile(Bitmap bitmap, DiskLruCache.Editor editor)
            throws IOException {
        OutputStream out = null;
        try {
            out = new BufferedOutputStream(editor.newOutputStream(0), DiskUtils.IO_BUFFER_SIZE);
            return bitmap.compress(mCompressFormat, mCompressQuality, out);
        } finally {
            if (out != null) {
                out.close();
            }
        }
    }

    private File getDiskCacheDir(Context context, String uniqueName) {

        // Check if media is mounted or storage is built-in, if so, try and use external cache dir
        // otherwise use internal cache dir
        String cachePath;

        try {
            cachePath = Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState()) ||
                    !DiskUtils.isExternalStorageRemovable() ?
                    DiskUtils.getExternalCacheDir(context).getPath() :
                    context.getCacheDir().getPath();
        } catch (Exception e) {
            cachePath = context.getCacheDir().getPath();
            BLog.printStackTrace(e);
        }


        return new File(cachePath + File.separator + uniqueName);
    }

    public void put(String key, Bitmap data) {

        DiskLruCache.Editor editor = null;
        try {
            editor = mDiskCache.edit(key);
            if (editor == null) {
                return;
            }

            if (writeBitmapToFile(data, editor)) {
                mDiskCache.flush();
                editor.commit();
                if (BuildConfig.DEBUG) {
                    BLog.d("cache_test_DISK_", "image put on disk cache " + key);
                }
            } else {
                editor.abort();
                if (BuildConfig.DEBUG) {
                    BLog.d("cache_test_DISK_", "ERROR on: image put on disk cache " + key);
                }
            }
        } catch (IOException e) {
            if (BuildConfig.DEBUG) {
                BLog.d("cache_test_DISK_", "ERROR on: image put on disk cache " + key);
            }
            try {
                mDiskCache.delete();
                init();
            } catch (Exception e1) {
                BLog.printStackTrace(e1);
            }
            try {
                if (editor != null) {
                    editor.abort();
                }
            } catch (IOException ignored) {
            }
        }

    }

    public void forceUpdateJournal(String key) {
        DiskLruCache.Editor editor = null;
        try {
            editor = mDiskCache.edit(key);
            if (editor == null) {
                return;
            }
            mDiskCache.flush();
            editor.commit();
            if (BuildConfig.DEBUG) {
                BLog.d("cache_test_DISK_", "image put on disk cache " + key);
            }
        } catch (IOException e) {
            if (BuildConfig.DEBUG) {
                BLog.d("cache_test_DISK_", "ERROR on: image put on disk cache " + key);
            }
            try {
                mDiskCache.delete();
                init();
            } catch (Exception e1) {
                BLog.printStackTrace(e1);
            }
            try {
                if (editor != null) {
                    editor.abort();
                }
            } catch (IOException ignored) {
            }
        }
    }

    public Bitmap getBitmap(String key) {

        Bitmap bitmap = null;
        DiskLruCache.Snapshot snapshot = null;
        try {

            if (mDiskCache.isClosed()) {
                return null;
            }
            snapshot = mDiskCache.get(key);
            if (snapshot == null) {
                return null;
            }
            final InputStream in = snapshot.getInputStream(0);
            if (in != null) {
                final BufferedInputStream buffIn =
                        new BufferedInputStream(in, DiskUtils.IO_BUFFER_SIZE);
                bitmap = BitmapFactory.decodeStream(buffIn);
            }
        } catch (IOException e) {
            try {
                mDiskCache.delete();
                init();
            } catch (Exception e1) {
                BLog.printStackTrace(e1);
            }
            BLog.printStackTrace(e);
        } finally {
            if (snapshot != null) {
                snapshot.close();
            }
        }

        if (BuildConfig.DEBUG) {
            BLog.d("cache_test_DISK_", bitmap == null ? "" : "image read from disk " + key);
        }

        return bitmap;

    }

    public boolean containsKey(String key) {

        boolean contained = false;
        DiskLruCache.Snapshot snapshot = null;
        try {
            if (!mDiskCache.isClosed()) {
                snapshot = mDiskCache.get(key);
                contained = snapshot != null;
            }
        } catch (IOException e) {
            BLog.printStackTrace(e);
        } finally {
            if (snapshot != null) {
                snapshot.close();
            }
        }

        return contained;

    }

    public void clearCache() {
        if (BuildConfig.DEBUG) {
            BLog.d("cache_test_DISK_", "disk cache CLEARED");
        }
        try {
            mDiskCache.delete();
        } catch (IOException e) {
            BLog.printStackTrace(e);
        }
    }

    public void clearCacheWithoutClosingDisk() {
        if (BuildConfig.DEBUG) {
            BLog.d("cache_test_DISK_", "disk cache CLEARED");
        }
        try {
            mDiskCache.deleteAndDoNotCloseDisk();
        } catch (Exception e) {
            BLog.printStackTrace(e);
        }
    }

    public void deleteForParticularKey(String key) {
        try {
            mDiskCache.remove(key);
        } catch (IOException e) {
            BLog.printStackTrace(e);
        }
    }

    public DiskLruCache getDiskCache() {
        return mDiskCache;
    }

    public File getCacheFolder() {
        if (mDiskCache != null) {
            return mDiskCache.getDirectory();
        } else {
            //if mDiskCache is null, we try to init cache in background and return empty file
            //for now, because we don't app to crash if we don't have cache dir.
            BobbleCore.getInstance().getExecutorSupplier().forCommonThreadTasks().executeCommonTask(new Callable() {
                @Override
                public Object call() {
                    init();
                    return null;
                }
            });
            return new File("");
        }
    }

}

