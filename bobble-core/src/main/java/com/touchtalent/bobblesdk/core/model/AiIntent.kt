package com.touchtalent.bobblesdk.core.model

data class AiIntent(
    var type: String,
    var details: List<SubAiIntent>,
    var typeConfidence: Float
)