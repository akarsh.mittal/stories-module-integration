package com.touchtalent.bobblesdk.core.interfaces.head

import com.touchtalent.bobblesdk.core.model.BobbleHead
import io.reactivex.Single

/**
 * [BobbleHeadCompatibilityManager] provides interface to facilitate migration from deprecated
 * architecture
 */
interface BobbleHeadCompatibilityManager {
    /**
     * @param bobbleHeads List of BobbleHeads to be inserted. All BobbleHeads are considered to be
     * multiple types a single head character.
     * @param setDefault Whether this head should be set default
     * @param defaultHeadType Which head type is preferred for this head character - RT or server
     *
     * @return [BobbleHead.characterId] for the head character created by the module. Depends on
     * [defaultHeadType] whether [BobbleHead.characterId] is of RT head or server head
     */
    fun addBobbleHeads(
        bobbleHeads: List<BobbleHead>,
        setDefault: Boolean,
        defaultHeadType: Int?
    ): Single<Long>

    /**
     * Utility function to scale face feature points for deprecated RT heads.
     * @param facePointMap Serialised JSON for original face feature point map
     * @param widthHeight Pair of width and height of the original face image
     * @param chinPointRatio Pair of Ratio of chin point (x-axis) over width (faceOverNeckX) and
     * Ratio of chin point (y-axis) over height (faceOverNeckY)
     * @return Serialised JSON for face feature point map after modifications
     */
    fun scaleFaceFeaturePointsForDeprecatedRTHeads(
        facePointMap: String?,
        widthHeight: Pair<Int, Int>,
        chinPointRatio: Pair<Float, Float>
    ): String
}
