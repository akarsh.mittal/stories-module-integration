package com.touchtalent.bobblesdk.core.cache

import java.nio.ByteBuffer
import java.security.MessageDigest

/**
 * Class to generate a cache key for in-memory objects. The cache key depends on the parameters in
 * the object that makes it unique.
 *
 * Based on builder pattern. The object whose cache key needs to be generated needs to implement the
 * interface [Cacheable]
 *
 * E.g usage:
 *
 * ```
 * class Sample(val id:Int, val sample2:Sample2): Cacheable{
 *      override fun generateCacheKey(source: CacheGenerator){
 *          source.process(id)
 *              .process(sample2)
 *      }
 * }
 *
 * class Sample2(id:Int): Cacheable{
 *      override fun generateCacheKey(source: CacheGenerator){
 *          source.process(id)
 *      }
 * }
 * val cacheGenerator = CacheGenerator()
 * val sample = Sample(1, Sample2(2))
 * val otherMetadata = MetaData()
 * val cacheKey = cacheGenerator.process(sample)
 *      .process(otherMetaData)
 *      .getCacheKey()
 * ```
 *
 */
class CacheGenerator {

    private val messageDigest = MessageDigest.getInstance("SHA-1")
    private val buffer = ByteBuffer.allocate(8)

    fun process(cacheable: Cacheable?) = apply {
        cacheable?.generateCacheKey(this)
    }

    fun process(string: String?) = apply {
        string?.let {
            messageDigest.update(it.toByteArray())
        }
    }

    fun process(integer: Int?) = apply {
        integer?.let {
            buffer.putInt(it)
            messageDigest.update(buffer.array())
            buffer.clear()
        }
    }

    fun process(float: Float?) = apply {
        float?.let {
            buffer.putFloat(it)
            messageDigest.update(buffer.array())
            buffer.clear()
        }
    }

    fun process(boolean: Boolean?) = apply {
        boolean?.let {
            messageDigest.update(byteArrayOf(if (it) 1 else 0))
        }
    }

    fun process(hashMap: Map<*, *>?) = apply {
        hashMap?.let {
            val serialized = StringBuilder()
            for ((key, value) in it.entries) {
                serialized.append(key)
                serialized.append(value)
            }
            messageDigest.update(serialized.toString().toByteArray())
        }
    }

    fun processList(cacheList: List<Cacheable?>?) = apply {
        cacheList?.forEach { element ->
            process(element)
        }
    }

    fun processIntegerList(cacheList: List<Int?>?) = apply {
        cacheList?.forEach { element ->
            process(element)
        }
    }

    fun process(list: List<String?>?) = apply {
        list?.let {
            messageDigest.update(it.toString().toByteArray())
        }
    }

    fun getCacheKey(): String {
        val bytes = messageDigest.digest()
        val result = StringBuilder(bytes.size * 2)
        bytes.forEach {
            val i = it.toInt()
            result.append(HEX_CHARS[i shr 4 and 0x0f])
            result.append(HEX_CHARS[i and 0x0f])
        }
        return result.toString()
    }

    companion object {
        private const val HEX_CHARS = "0123456789ABCDEF"
    }
}