package com.touchtalent.bobblesdk.core.config

import androidx.annotation.DrawableRes

/**
 * Config class for *super-app-module* module
 * @property defaultTopBarIconEnabled Sets whether top bar should be enabled by default or not
 */
class BobbleSuperAppConfig {
    var defaultTopBarIconEnabled = true

    @DrawableRes
    var errorImageResource = 0

    @DrawableRes
    var smallNotificationIconResource = 0
}
