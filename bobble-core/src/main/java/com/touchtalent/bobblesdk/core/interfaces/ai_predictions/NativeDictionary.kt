package com.touchtalent.bobblesdk.core.interfaces.ai_predictions

import java.util.*

/**
 * Interface to bridge the implementation of BinaryDictionary implemented in native code to handle
 * .dict files.
 * @param filename File path of the .dict file or the file containing the dictionary contents
 * @param offset Offset index within the file, where the dictionary content starts from
 * @param length Length of the dictionary contents within the file
 * @param useFullEditDistance Whether the dictionary should use full edit distance
 * @param locale Locale supported by this dictionary
 * @param dictType Type of the dictionary - *main*, *ai*, *user*, etc
 */
abstract class NativeDictionary(
    filename: String,
    offset: Long,
    length: Long,
    useFullEditDistance: Boolean,
    locale: Locale?,
    dictType: String
) {
    /**
     * Get bi-grams of the given [word]
     * @param word Word to search dictionary for bi-grams
     * @param isBeginningOfSentence Is the word first word of the sentence
     * @return list of bi-grams present in the dictionary
     */
    abstract fun getBiGrams(word: String, isBeginningOfSentence: Boolean): List<String>

    /**
     * @return total words present in the dictionary.
     */
    abstract fun getWordCount(): Int

    /**
     * Checks whether a given [word] is present within the dictionary
     * @param word Word to check
     * @return true if word exists, false otherwise
     */
    abstract fun isInDictionary(word: String): Boolean

    /**
     * Close the dictionary
     */
    abstract fun close()
}