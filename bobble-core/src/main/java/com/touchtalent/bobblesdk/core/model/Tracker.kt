package com.touchtalent.bobblesdk.core.model

import androidx.annotation.Keep
import com.androidnetworking.common.Priority
import com.rx2androidnetworking.Rx2AndroidNetworking
import com.squareup.moshi.JsonClass
import com.touchtalent.bobblesdk.core.BobbleCoreSDK
import com.touchtalent.bobblesdk.core.constants.CommonConstants.CHROME_USER_AGENT
import com.touchtalent.bobblesdk.core.executor.BobbleSchedulers
import com.touchtalent.bobblesdk.core.utils.BLog
import io.reactivex.Completable
import io.reactivex.schedulers.Schedulers
import java.util.*

/**
 * POJO class for 3rd party trackers such as DCM, DoubleVerify, etc. This trackers are occasionally
 * required by 3rd party client for tracking impressions or shares in an external database. Client
 * needs to trigger these URLs upon such events
 * @property type Type of the tracker, denotes how to handle the [url].
 * Can be "direct_url" - Trigger the url directly
 * @property url URL of the tracker to be triggered.
 */
@Keep
@JsonClass(generateAdapter = true)
class Tracker {
    var type: String? = null
    var url: String? = null

    companion object {
        /**
         * Log multiple trackers at once.
         * @param trackers list of trackers to be logged
         */
        fun logMultiple(trackers: List<Tracker?>?) {
            Completable.fromAction {
                val advId = BobbleCoreSDK.getAdvertisingId()
                val isEnable = BobbleCoreSDK.isLimitAdTrackingEnabled()
                trackers?.let {
                    it.forEach { impressionTracker -> impressionTracker?.log(advId, isEnable) }
                }
            }.subscribeOn(BobbleSchedulers.common())
                .subscribe()
        }
    }

    private fun log(brandId: String, isLatEnable: Boolean) {
        if (type != null && url != null && type == "direct_url") {
            BLog.d(
                "ImpressionTracker",
                "Logging direct_url : ${applyPlaceholders(url, brandId, isLatEnable)}"
            )
            var d = Rx2AndroidNetworking.get(applyPlaceholders(url, brandId, isLatEnable))
                .setPriority(Priority.HIGH)
                .setUserAgent(CHROME_USER_AGENT)
                .build()
                .stringCompletable
                .subscribeOn(Schedulers.io())
                .subscribe()
        }
    }

    private fun applyPlaceholders(
        url: String?,
        advertisementId: String,
        isLatEnable: Boolean
    ): String? {
        return url
            ?.replace("__MAID__", advertisementId)
            ?.replace("__UUID__", UUID.randomUUID().toString())
            ?.replace("__TFCD__", "0")
            ?.replace("__LAT__", if (isLatEnable) "1" else "0")
            ?.replace("__CLICK_ID__", UUID.randomUUID().toString())
    }
}

