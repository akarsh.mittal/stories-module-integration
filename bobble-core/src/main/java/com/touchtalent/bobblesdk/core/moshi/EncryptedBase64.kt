package com.touchtalent.bobblesdk.core.moshi

import com.touchtalent.bobblesdk.core.interfaces.EncryptionManager
import com.touchtalent.bobblesdk.core.BobbleCoreSDK
import com.squareup.moshi.JsonQualifier


/**
 * Denotes that the annotated element should be encrypted using [EncryptionManager] retrieved from
 * [BobbleCoreSDK.getAppController]. Converts encrypted information to Base64
 */
@Retention(AnnotationRetention.RUNTIME)
@JsonQualifier
annotation class EncryptedBase64
