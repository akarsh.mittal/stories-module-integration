package com.touchtalent.bobblesdk.core.utils

import android.content.Context
import java.io.File
import java.io.FileInputStream
import java.io.IOException
import java.nio.channels.FileChannel
import java.nio.charset.Charset

class FileUtilKotlin {

    companion object {
        fun loadJson(fileName: String, context: Context): String? {
            return if (!fileName.startsWith("/")) {
                loadJsonFromAsset(context, fileName)
            } else {
                loadJsonFromFile(fileName)
            }
        }

        private fun loadJsonFromAsset(context: Context, assetName: String): String? {
            val json: String?
            try {
                val inputStream = context.assets.open(assetName)
                val size = inputStream.available()
                val buffer = ByteArray(size)
                inputStream.read(buffer)
                inputStream.close()
                json = String(buffer)
                return json
            } catch (ex: IOException) {
                ex.printStackTrace()
            }
            return null
        }

        private fun loadJsonFromFile(fileName: String): String? {
            try {
                val file = File(fileName)
                val stream = FileInputStream(file)
                val jsonStr: String
                val fc = stream.channel
                val bb = fc.map(FileChannel.MapMode.READ_ONLY, 0, fc.size())
                jsonStr = Charset.defaultCharset().decode(bb).toString()
                stream.close()
                return jsonStr
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return null
        }

    }


}