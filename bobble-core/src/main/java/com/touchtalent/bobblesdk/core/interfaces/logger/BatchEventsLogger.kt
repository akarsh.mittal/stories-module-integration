package com.touchtalent.bobblesdk.core.interfaces.logger

import org.json.JSONArray
import org.json.JSONObject

/**
 * Utility class for logging events in batch. Create a custom implementation of this class for each
 * use-case by implementing the function [flush]
 * @param buffer Size of each individual batch
 */
abstract class BatchEventsLogger(val buffer: Int = 5) {

    private val eventModelList = mutableListOf<JSONObject>()

    /**
     * Create new instance of [EventBuilder]. Calling [EventBuilder.log] of this builder would add
     * the event details to a queue instead of logging it. Event params like screen name, event name,
     * event action can be ignored, only event label params should be built using this.
     */
    fun newEvent(): EventBuilder {
        return BatchEventBuilder(this)
    }

    private fun addEvent(jsonObject: JSONObject) {
        eventModelList.add(jsonObject)
        if (eventModelList.size >= buffer)
            flushInternal()
    }

    /**
     * Called when aggregating events has been completed. Logs any pending events in the queue
     */
    fun flushInternal() {
        if (eventModelList.isEmpty())
            return
        val jsonArray = JSONArray()
        eventModelList.forEach { jsonArray.put(it) }
        eventModelList.clear()
        flush(jsonArray)
    }

    /**
     * Callback received with the [jsonArray] of the event label of multiple events.
     * The implementation must take care of logging an event with the [jsonArray] as its event label
     */
    abstract fun flush(jsonArray: JSONArray)

    /**
     * Extension class for [EventBuilder] which adds the logged events to buffer of [batchEventsLogger]
     * to be flushed when max count has been achieved. screenName, eventName, eventAction for this
     * builder are disabled since they need to be set on [BatchEventsLogger] instead.
     */
    internal class BatchEventBuilder(private val batchEventsLogger: BatchEventsLogger) :
        EventBuilder() {

        override fun withEventAction(eventAction: String): EventBuilder {
            throw Error("eventAction is not available for individual item of batched events")
        }

        override fun withEventName(eventName: String): EventBuilder {
            throw Error("eventName is not available for individual item of batched events")
        }

        override fun withScreenName(screenName: String): EventBuilder {
            throw Error("screenName is not available for individual item of batched events")
        }

        override fun log() {
            batchEventsLogger.addEvent(jSONObject)
        }

        override fun logMultiple() {
            batchEventsLogger.addEvent(jSONObject)
        }
    }
}