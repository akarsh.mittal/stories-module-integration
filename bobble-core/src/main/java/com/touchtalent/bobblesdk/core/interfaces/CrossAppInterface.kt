package com.touchtalent.bobblesdk.core.interfaces

import android.app.Activity
import android.content.Intent
import android.view.View
import com.androidnetworking.error.ANError
import com.google.gson.Gson
import com.touchtalent.bobblesdk.core.BobbleCoreSDK
import com.touchtalent.bobblesdk.core.builders.ApiParamsBuilder
import com.touchtalent.bobblesdk.core.config.BobbleCoreConfig
import com.touchtalent.bobblesdk.core.deeplink.DeeplinkInterface
import com.touchtalent.bobblesdk.core.enums.AppElements
import com.touchtalent.bobblesdk.core.enums.KeyboardDeepLink
import com.touchtalent.bobblesdk.core.interfaces.logger.EventBuilder
import com.touchtalent.bobblesdk.core.pojo.UserCredentials
import okhttp3.OkHttpClient

/**
 * Interface to bridge communication between parent app and module.
 *
 * *For parent apps:*
 * Parent apps are required to provide implementation for this interface and call [BobbleCoreSDK.initialise]
 * passing the instance via [BobbleCoreConfig]
 */
interface CrossAppInterface {
    /**
     * Get the base url for the app (this might include the current version as well). The URL
     * should not contain trailing slash. For e.g - https://api.bobble.ai/v4
     * @return Base url of the app
     */
    fun baseUrl(): String

    /**
     * Get the base log url for the app (this might include the current version as well). The URL
     * should not contain trailing slash. For e.g - https://log-api.bobbleapp.me/v4
     * @return Base log url of the app
     */
    fun logUrl(): String

    /**
     * Get the base url for the app, but without version, as new APIs might have a different version.
     * The URL should not contain trailing slash. For e.g - https://api.bobble.ai
     * @return Base url of the app without version
     */
    fun baseUrlWithoutVersion(): String

    /**
     * Get the base bobblification URL of the app. (Must include version as well) (Should not
     * contain trailing slash). For e.g - https://bobblification.bobbleapp.me/v4
     */
    fun bobblificationUrl(): String


    /**
     * Get the base url of content Suggestion module.(Must include version as well)
     * (Should no contain trailing slash)
     *  e.g - https://api.mintkeyboard.com/v2
     */
    fun contentSuggestionBaseURL(): String {
        return baseUrl()
    }

    /**
     * Get login credentials like [UserCredentials.accessToken] and [UserCredentials.userId] used
     * for sync purposes by the module
     * @return [UserCredentials] if user is logged in, null otherwise
     */
    fun getLoginCredentials(): UserCredentials?

    /**
     * Handles network error and looks for generic error codes like deviceNotRegistered,
     * accessTokenExpired, etc and takes necessary actions to resolve the issue.
     * @param anError Error to be checked
     * @param reference Source of the error, for logging purpose
     */
    fun handleGeneralANErrorResponse(anError: ANError, reference: String)

    /**
     * Return the global instance of GSON
     */
    @Deprecated(
        message = "GSON has been deprecated because of its limited capabilities to handle Kotlin data classes. Use Moshi instead",
        replaceWith = ReplaceWith(
            expression = "BobbleCoreSDK.moshi"
        )
    )
    fun getGson(): Gson

    /**
     * Get the app invite link
     * @return App invite link if exists, empty string otherwise
     */
    fun getAppInviteLink(): String

    /**
     * Get the current active language locale of the app/keyboard.
     * @return If keyboard component exists, current language of the keyboard, app language otherwise
     */
    fun getActiveLanguageLocale(): String
    fun getActiveKBLanguageLocales(): Set<String> {
        return emptySet()
    }

    fun getActiveKBLanguageCode(merged: Boolean): String {
        return "en"
    }

    fun getActiveLanguageId(): String

    fun getActiveLanguageLayoutId(): String


    /**
     * Whether it is safe to log events and make API calls to server.
     * @return true if privacy policy has been accepted/acknowledged, false otherwise
     */
    fun canLogEvents(): Boolean

    /**
     * Log an exception to a 3rd party logging service, if available. For e.g - Crashlytics
     * @param tag Tag for the exception
     * @param exception Exception to log
     */
    fun logException(tag: String, exception: Throwable)

    /**
     * Fetch the current appVersion
     */
    fun getVersion(): Int

    /**
     * Performs haptic feedback. The implementation takes care of custom vibration settings like
     * enabled/disabled, intensity, etc
     */
    fun performVibration(view: View?)

    /**
     * Convert text in cool font to basic font
     * @param otfText Text in cool font
     * @return Text in basic font
     */
    fun getBasicFont(otfText: String): String

    /**
     * Send broadcast to open keyboard. Generally called in [Activity.onDestroy] to force open
     * keyboard.
     *
     * *For parent app* : The implementation should take care of sending the broadcast only for
     * [sourceIntent] originated from keyboard and ignore intents from application. The parent app
     * is responsible to add all [Intent.getExtras] in the broadcast event, so that deep link params
     * are transferred properly to the keyboard.
     *
     * *For modules* : This function should be called from every activities onDestroy to ensure
     * reopening of keyboard after an activity (launched from keyboard) has been closed
     * @param sourceIntent Source intent of the activity
     *
     * This function must be used in conjunction with [modifyActivityIntentForKeyboard] to achieve
     * smooth opening/closing of keyboard while launching an activity from keyboard
     */
    fun sendOpenKeyboardIntent(sourceIntent: Intent?)

    /**
     * Modify the intent to add extras that indicate that the activity is launched from keyboard.
     *
     * *For parent app* - The extras added here must be handled in [sendOpenKeyboardIntent].
     * Keyboard must be closed forcefully when this function is called. [KeyboardDeepLink] should
     * also be appended in the intent extras, which should be handled by the Keyboard
     *
     * *For module* - Call this function when launching any activity from keyboard. Add a custom
     * [KeyboardDeepLink] if the keyboard needs to be opened at exact same view after the activity
     * being closed.
     *
     * @param intent Intent being used by the module to launch activity
     * @param deepLink Deeplink - to be used when keyboard re-opens after activity close.
     */
    fun modifyActivityIntentForKeyboard(intent: Intent, @KeyboardDeepLink deepLink: Int?)

    /**
     * Get the GAID of the user.
     */
    fun getAdvertisingId(): String

    /**
     * Get whether the user has opted out for ad tracking.
     */
    fun isLimitAdTrackingEnabled(): Boolean

    /**
     * Get current package, used for events
     * @param isKeyboardView whether this function is being called from keyboard or app
     * @return Package name of current parent app if [isKeyboardView] is true, package name of self
     * otherwise
     */
    fun getCurrentPackageName(isKeyboardView: Boolean): String

    /**
     * Get session id, used for events
     * @param isKeyboardView whether this function is being called from keyboard or app
     * @return Session id of the keyboard if [isKeyboardView] is true, session id of app otherwise
     */
    fun getSessionId(isKeyboardView: Boolean): String

    @Deprecated(
        "okHttp has been moved to [BobbleCoreConfig]",
        replaceWith = ReplaceWith(
            expression = "BobbleCoreSDK.okHttpClient",
            imports = arrayOf("com.touchtalent.bobblesdk.core.BobbleCoreSDK")
        )
    )
    fun getOkHttpClient(): OkHttpClient?

    /**
     * *For modules* - Should not be used directly, use [EventBuilder] instead.
     *
     * *For parent app* - The parent app must take care of logging this to BobbleEventLogger and
     * other event aggregators
     *
     * Log events from following parameters:
     * @param eventAction Event action
     * @param eventType Event type
     * @param screenAt Screen at which event was generated
     * @param data Serialised JSON for event label
     * @param logMultiple Should this be logged to multiple sources, or just BobbleEventLogger
     */
    fun logEvents(
        eventType: String?,
        eventAction: String?,
        screenAt: String?,
        data: String?,
        logMultiple: Boolean,
    )

    /**
     * Run a piece of code in boot-aware, i.e, when the system has been unlocked. The code is run
     * instantly if the device is already unlocked or doesn't support boot-aware mode.
     * @param runnable Runnable to be executed
     */
    fun runInBootAwareMode(runnable: Runnable)

    /**
     * Get a user-visible name of the parent app
     */
    fun getAppName(): String

    /**
     * Return a new instance of [ApiParamsBuilder]
     * @see [ApiParamsBuilder]
     */
    fun getApiParamsBuilder(): ApiParamsBuilder

    fun getApiKey(): String

    fun getEncryptionManager(): EncryptionManager

    /**
     * Utility function to access client specific string tokens such as client id, device id, app version, etc.
     * @see [AppElements]
     */
    fun getAppElement(element: AppElements): String? {
        return null
    }

    /**
     * Provides interface controller for logging in user
     */
    fun getLoginInterface(): OtherModuleLoginInterface? {
        return null
    }

    /**
     * Handle deeplink utils like prefetching , downloading for pre processing etc
     */
    suspend fun getDeeplinkPrefetchInterface(): DeeplinkInterface? {
        return null
    }

    fun getAppInviteMessageAndLink(): String?

    /**
     * forceSyncEvents for submodules
     */
    fun forceSyncEvents()

    /**
     * provides webview cache settings response config
     */
    fun getWebViewCacheConfigSettingsInterface() : WebViewCacheConfigSettingsInterface?
}
