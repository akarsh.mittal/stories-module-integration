package com.touchtalent.bobblesdk.core.deeplink

import android.content.Context
import com.touchtalent.bobblesdk.core.moshi.DeeplinkUrl


/**
 * created by anil_
 * on 06/04/23
 * at 4:04 PM
 *
 * Interface for Deeplink PreOperations
 * like downloading and type conversion
 */


interface DeeplinkInterface {
    suspend fun preDownload(deeplinkUrl: DeeplinkUrl)
    suspend fun preConversion(urlList: List<DeeplinkUrl>)
    suspend fun openDeepLink(
        context: Context,
        deepLinkUrl: String,
        placementString: String
    )
}