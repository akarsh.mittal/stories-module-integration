package com.touchtalent.bobblesdk.core.moshi

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import java.text.SimpleDateFormat
import java.util.*


/**
 * created by anil_
 * on 21/01/23
 * at 4:33 PM
 */

class TimeAdapter {
    private val timeFormat: SimpleDateFormat by lazy {
        SimpleDateFormat(
            "HH:mm", Locale.ENGLISH
        )
    }

    @ToJson
    fun toJson(@Time time: Date?): String? {
        return time?.let {
            timeFormat.format(time)
        }
    }

    @FromJson
    @Time
    fun fromJson(timeString: String?): Date? {
        return timeString?.let {
            timeFormat.parse(timeString)
        }
    }


}