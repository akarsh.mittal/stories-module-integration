package com.touchtalent.bobblesdk.core.interfaces.head

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import androidx.annotation.DimenRes
import androidx.annotation.DrawableRes
import androidx.constraintlayout.widget.ConstraintLayout
import com.touchtalent.bobblesdk.core.DependencyResolver

/**
 * View to facilitate a UI for creation of new heads and management (edit, delete) of existing heads.
 * Call [updateEventParams] for correct event tracking
 * @see DependencyResolver.newChooseHeadView
 */
abstract class ChooseHeadView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    /**
     * Set listener to listen for actions like triggering head creation or close this UI.
     * @param listener Listener to listen for user actions
     */
    abstract fun setChooseHeadInterface(listener: ChooseHeadViewListener)

    /**
     * Update event params, used for events.
     * @param isFromKeyboard Whether the UI is shown in keyboard
     * @param sourcePackId Id of source content pack where this UI is visible
     * @param screenName Current screen name
     */
    abstract fun updateEventParams(isFromKeyboard: Boolean, sourcePackId: Int, screenName: String)

    /**
     * Theme parameters to initialise this UI with
     * @property primaryColor Primary color of the UI. Used for CTA color
     * @property topBarColor Color for top bar
     * @property backgroundColor Background color of the panel
     * @property itemBackgroundColor Background color of the heads in the panel
     * @property titleTextColor Text color of the title
     * @property itemTextColor Text color of the head names
     * @property borderColor Border color of the head card
     * @property isFromKeyboard Whether the UI is visible in keyboard
     */
    class ThemeParams {
        var primaryColor = Color.GREEN
        var topBarColor = Color.GRAY
        var backgroundColor = Color.WHITE
        var itemBackgroundColor = Color.WHITE
        var titleTextColor = Color.BLACK
        var itemTextColor = Color.BLACK
        var borderColor = Color.TRANSPARENT
        var isFromKeyboard: Boolean? = null
    }

    /**
     * Listener for actions in [ChooseHeadView]
     */
    interface ChooseHeadViewListener {
        /**
         * Callback when close button is clicked by user
         */
        fun closeChooseHeadUi()

        /**
         * Callback when create new head is clicked by user
         */
        fun openCameraView()
    }

    sealed class Type {
        class BrandedType(@DrawableRes val logo: Int, @DimenRes val viewHeight: Int) : Type()

        object NonBrandedType : Type()
    }
}