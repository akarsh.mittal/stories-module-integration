package com.touchtalent.bobblesdk.core.utils

sealed class Optional<out T> {
    class Value<out T>(val element: T) : Optional<T>()
    object None : Optional<Nothing>()
}