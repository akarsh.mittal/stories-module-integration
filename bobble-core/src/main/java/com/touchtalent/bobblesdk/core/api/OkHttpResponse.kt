package com.touchtalent.bobblesdk.core.api

import okhttp3.Request
import retrofit2.Response

/**
 * Response data type for Retrofit API interfaces. This response type helps to get the original
 * okhttp3.Request along with the complete Response object.
 * This response type ensures that no exception is being thrown. All unknown exceptions are wrapped
 * in [result]
 **/
class OkHttpResponse<T>(val request: Request, val result: BobbleResult<Response<T>>)