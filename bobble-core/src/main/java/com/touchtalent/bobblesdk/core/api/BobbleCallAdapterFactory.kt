package com.touchtalent.bobblesdk.core.api

import retrofit2.Call
import retrofit2.CallAdapter
import retrofit2.Retrofit
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type

/**
 * Utility [CallAdapter] for Retrofit to handle errors and deliver responses with data type [Result].
 * Handles all types of errors such as CertificateExpired, InternetNotConnect which are otherwise
 * not handled by the default CallAdapter of Retrofit. Helps in reducing boilerplate in the code
 */
class BobbleCallAdapterFactory : CallAdapter.Factory() {
    companion object {
        fun create() = BobbleCallAdapterFactory()
    }

    override fun get(
        returnType: Type,
        annotations: Array<out Annotation>,
        retrofit: Retrofit
    ): CallAdapter<*, *>? {
        // suspend functions wrap the response type in `Call`
        if (Call::class.java != getRawType(returnType)) {
            return null
        }

        // check first that the return type is `ParameterizedType`
        check(returnType is ParameterizedType) {
            "return type must be parameterized as Call<Result<Foo>> or Call<Result<out Foo>>"
        }

        // get the response type inside the `Call` type
        val responseType = getParameterUpperBound(0, returnType)
        // if the response type is not ApiResponse then we can't handle this type, so we return null
        val rawType = getRawType(responseType)
        if (rawType != Result::class.java &&
            rawType != OkHttpResponse::class.java &&
            rawType != BobbleResult::class.java
        ) {
            return null
        }

        // the response type is ApiResponse and should be parameterized
        check(responseType is ParameterizedType) { "Response must be parameterized as ApiResponse<Foo> or ApiResponse<out Foo>" }
        val successBodyType = getParameterUpperBound(0, responseType)
        return when (rawType) {
            Result::class.java ->
                BobbleCallAdapter<Any>(successBodyType)
            BobbleResult::class.java ->
                BobbleResultCallAdapter<Any>(successBodyType)
            OkHttpResponse::class.java ->
                BobbleOkHttpResponseCallAdapter<Any>(successBodyType)
            else -> null
        }
    }
}