package com.touchtalent.bobblesdk.core.config

import com.touchtalent.bobblesdk.core.interfaces.CrossAppInterface
import com.touchtalent.bobblesdk.core.interfaces.fonts.FontManager
import okhttp3.OkHttpClient

/**
 * Config class for *bobble-core* module to be used across modules
 * @property crossAppInterface Implementation of [CrossAppInterface] provided by the parent app for
 * inter-app operations.
 * @property okHttpClient Global instance of [OkHttpClient] provided by the parent app.
 * @property fontManager [FontManager] to maintain a unified font repository across all modules.
 * @property contentConfig Config for *bobble-content* module
 * @property headConfig Config for *bobble-head* module
 * @property superAppConfig Config for *super-app-module* module.
 */
class BobbleCoreConfig(
    val crossAppInterface: CrossAppInterface
) {
    var okHttpClient: OkHttpClient? = null
    var fontManager: FontManager? = null
    var headConfig: BobbleHeadConfig? = null
    var superAppConfig: BobbleSuperAppConfig? = null
    var contentUiConfig: BobbleContentUiConfig? = null
    var staticContentConfig: BobbleStaticContentConfig? = null
    var animatedContentConfig: BobbleAnimatedContentConfig? = null
}