package com.touchtalent.bobblesdk.core.interfaces

import android.app.Application
import android.content.Context
import androidx.annotation.CallSuper
import com.touchtalent.bobblesdk.core.BobbleCoreSDK
import com.touchtalent.bobblesdk.core.config.BobbleCoreConfig
import org.json.JSONObject
import java.io.File

/**
 * Base class for entry point of all modules which provides basic interactions with the app.
 *
 * Interactions available:
 * 1. [handleUserConfig]
 * 2. [registerPeriodicUpdater]
 * 3. [supportsBootAwareMode]
 * 4. [resetLoginCredentials]
 *
 * Properties available - [rootDir], [cacheDir]
 */
abstract class BobbleModule {

    /**
     * Root dir for all file storage solutions for individual modules. The value is populated by
     * [BobbleCoreSDK.initialise] and is equivalent to [Context.getFilesDir] + [File.separator] + [getCodeName]
     */
    lateinit var rootDir: String

    /**
     * Root dir for all cache storage solutions for individual modules. The value is populated by
     * [BobbleCoreSDK.initialise] and is equivalent to [Context.getCacheDir] + [File.separator] + [getCodeName]
     */
    lateinit var cacheDir: String

    private val periodicUpdaters = mutableListOf<PeriodicUpdater>()

    /**
     * Callback received from [Application.onCreate]. To be used to initialise global parameters
     *
     * @param applicationContext Application context of app.
     * @param config App level config as well as Module level config. This config could be used to
     * make features/resources for module customisable for different apps.
     */
    abstract fun initialise(applicationContext: Context, config: BobbleCoreConfig)

    /**
     * Callback received from [Application.onCreate]. To be used to seed default values. Threading
     * must be taken care of as this is called from main thread
     */
    open fun seedDefaultValues() {}

    /**
     * Callback received when a new user config data is available in SuccessResponseParseUtil.
     * The module must handle the module specific params from the whole payload
     * @param response Complete config response payload
     */
    open fun handleUserConfig(response: JSONObject) {}

    /**
     * Get codename for the module. Used to name folders specific to this module.
     * Nomenclature guidelines - small-case-separated-with-dashes. e.g - bobble-content
     * @return codename of the module
     */
    abstract fun getCodeName(): String

    /**
     * Used to register [PeriodicUpdater] for the modules. PeriodicUpdaters are typically run during
     * KB close event.
     * @see [PeriodicUpdater]
     */
    fun registerPeriodicUpdater(periodicUpdater: PeriodicUpdater) {
        periodicUpdaters.add(periodicUpdater)
    }

    /**
     * *For parent apps* - This must be called at the keyboard close event or application open, to
     * check for any updates in every module
     */
    @CallSuper
    open fun runPeriodicUpdaters(forced: Boolean) {
        periodicUpdaters.forEach { it.update(forced) }
    }

    /**
     * Return whether the module supports boot-aware mode or not
     */
    open fun supportsBootAwareMode() = false

    /**
     * Callback received when the user has logged out from parent app. This callback must be used to
     * invalidate sync status (if any), for e.g - head sync
     */
    open fun resetLoginCredentials() {}
}