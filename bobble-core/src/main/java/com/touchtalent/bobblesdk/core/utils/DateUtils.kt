package com.touchtalent.bobblesdk.core.utils

import java.text.SimpleDateFormat
import java.util.*


/**
 * created by anil_
 * on 06/03/23
 * at 11:46 AM
 */

fun SimpleDateFormat.formatOrNull(date: Date?): String? {
    return kotlin.runCatching {
        date?.let {
            return@runCatching format(it)
        }
    }.getOrNull()
}


fun SimpleDateFormat.parseOrNull(time: String?): Date? {
    return kotlin.runCatching {
        time?.let {
            if (it.isNotEmpty())
                return@runCatching parse(it)
            else
                return@runCatching null
        }
    }.getOrNull()
}