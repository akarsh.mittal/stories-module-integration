package com.touchtalent.bobblesdk.core.api

import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Streaming
import retrofit2.http.Url

interface CoreApiService {
    @GET
    @Streaming
    suspend fun download(@Url url: String): Response<ResponseBody>
}