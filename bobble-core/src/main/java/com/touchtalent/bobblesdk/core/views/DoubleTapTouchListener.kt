package com.touchtalent.bobblesdk.core.views

import android.content.Context
import android.os.Handler
import android.os.Looper
import android.os.SystemClock
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.View
import android.view.ViewConfiguration

/**
 * Utility [View.OnTouchListener] which can be used to detect double taps
 */
abstract class DoubleTapTouchListener(val context: Context) : View.OnTouchListener {

    private var isDoubleTapActive = true
    private val longClickTimeout: Long = ViewConfiguration.getLongPressTimeout().toLong()

    val gestureListener = object : GestureDetector.SimpleOnGestureListener() {

        override fun onDown(e: MotionEvent): Boolean {
            return true
        }

        override fun onSingleTapConfirmed(e: MotionEvent): Boolean {
            onClick()
            return true
        }

        override fun onDoubleTap(e: MotionEvent): Boolean {
            onDoubleTap()
            return true
        }

        override fun onLongPress(e: MotionEvent) {
            onLongClick()
        }
    }

    private val gestureDetector = GestureDetector(context, gestureListener)

    private var lastClickTime = 0L
    private var isUpEventPending = false

    private val handler = Handler(Looper.getMainLooper())

    override fun onTouch(v: View?, event: MotionEvent): Boolean {
        if (isDoubleTapActive)
            return gestureDetector.onTouchEvent(event)
        if (event?.action == MotionEvent.ACTION_DOWN) {
            isUpEventPending = true
            lastClickTime = SystemClock.elapsedRealtime()
            handler.postDelayed({
                isUpEventPending = false
                onLongClick()
            }, longClickTimeout)
        }
        if (event?.action == MotionEvent.ACTION_UP || event?.action == MotionEvent.ACTION_CANCEL) {
            handler.removeCallbacksAndMessages(null)
        }
        if (event?.action == MotionEvent.ACTION_UP && isUpEventPending)
            onClick()
        return true
    }

    abstract fun onClick()
    open fun onLongClick() {}
    abstract fun onDoubleTap()

    fun setDoubleTapEnabled(enable: Boolean) {
        isDoubleTapActive = enable
        if (isDoubleTapActive)
            gestureDetector.setOnDoubleTapListener(gestureListener)
        else gestureDetector.setOnDoubleTapListener(null)
    }
}