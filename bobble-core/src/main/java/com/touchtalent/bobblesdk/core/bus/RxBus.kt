package com.touchtalent.bobblesdk.core.bus

import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

@Deprecated("Use Kotlin shared flows instead")
class RxBus {
    private val bus = PublishSubject.create<Any>()

    fun send(o: Any) {
        bus.onNext(o)
    }

    fun toObservable(): Observable<Any> {
        return bus
    }
}