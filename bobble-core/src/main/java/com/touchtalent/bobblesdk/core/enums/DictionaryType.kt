package com.touchtalent.bobblesdk.core.enums

/**
 * Specifies the dictionary type
 */
enum class DictionaryType {
    /**
     * Main dictionary type
     */
    MAIN,

    /**
     * Merged dictionary type; the dictionary represents macaronic language
     */
    MERGED
}