package com.touchtalent.bobblesdk.core.interfaces.promotional_offers

import io.reactivex.Single

/**
 * Interface for bobble-promotional-offer module
 */
interface PromotionalOffersModule {
    /**
     * Fetches whether the offers view is enabled.
     */
    fun getIsEnabled(): Single<Boolean>

    /**
     * Checks if the banner has been updated since last user visit.
     * @param id Id of the banner to check
     * @param lastUpdated Last updated timestamp of the banner
     * @return [Single] to fetch whether the banner was updated
     */
    fun isUpdated(id: String, lastUpdated: Long): Single<Boolean>
}
