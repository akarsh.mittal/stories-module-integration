package com.touchtalent.bobblesdk.core.views;


public interface SelectionUpdateListener {
    void onSelectionUpdate(int newStart, int newEnd, int oldStart, int oldEnd);
}