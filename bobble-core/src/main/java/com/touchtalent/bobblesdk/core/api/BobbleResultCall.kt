package com.touchtalent.bobblesdk.core.api

import com.touchtalent.bobblesdk.core.utils.GeneralUtils.toBobbleResult
import okhttp3.Request
import okio.Timeout
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * @see BobbleCallAdapterFactory
 */
class BobbleResultCall<T>(private val delegate: Call<T>) : Call<BobbleResult<T>> {

    override fun enqueue(callback: Callback<BobbleResult<T>>) {
        return delegate.enqueue(object : Callback<T> {
            override fun onResponse(call: Call<T>, response: Response<T>) {
                callback.onResponse(this@BobbleResultCall, Response.success(response.toBobbleResult()))
            }

            override fun onFailure(call: Call<T>, t: Throwable) {
                callback.onResponse(this@BobbleResultCall, Response.success(BobbleResult.failure(t)))
            }
        })
    }

    override fun clone(): Call<BobbleResult<T>> = BobbleResultCall(delegate)
    override fun isExecuted(): Boolean = delegate.isExecuted
    override fun cancel() = delegate.cancel()
    override fun isCanceled() = delegate.isCanceled
    override fun request(): Request = delegate.request()
    override fun timeout(): Timeout = delegate.timeout()

    override fun execute(): Response<BobbleResult<T>> {
        throw UnsupportedOperationException("BobbleCall doesn't support execute")
    }
}