package com.touchtalent.bobblesdk.core.bus

import com.touchtalent.bobblesdk.core.BobbleCoreSDK
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.launch

object LanguageChangeFlow {
    private val _events = MutableSharedFlow<Boolean>()
    val events = _events.asSharedFlow()

    fun onReactedToLogin(languageLocalChanged: Boolean) {
        try {
            BobbleCoreSDK.applicationScope.launch {
                _events.emit(languageLocalChanged)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}