package com.touchtalent.bobblesdk.core.views

import android.content.Context
import android.graphics.Canvas
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.view.ViewTreeObserver
import androidx.appcompat.widget.AppCompatImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestBuilder
import com.touchtalent.bobblesdk.core.interfaces.GlideRequestListener
import com.touchtalent.bobblesdk.core.utils.GeneralUtils
import com.touchtalent.bobblesdk.core.utils.ViewUtil

private const val MINIMUM_PERCENTAGE_FOR_IMPRESSION = 50

/**
 * ImageView to provide callbacks for impression. The impression is logged only and only if the
 * view is user-visible at a percentage higher than [MINIMUM_PERCENTAGE_FOR_IMPRESSION].
 *
 * This function can be used in two modes - externally and internally
 *
 * A image can be loaded internally by using [setImageUrl] function which loads the image via Glide
 * and registers impression only on successful loading of the image. This is the default use-case of
 * this class. Glide options customisation is available, check [setImageUrl]
 *
 * A image can be loaded externally by using default methods [setImageResource], [setImageBitmap], etc
 * In this case, success of loading of image is neglected and impression is generated as soon as the
 * view is visible in viewport. Set [shouldLoadExternally] to true to enable this mode
 *
 * If this view is reused multiple times for a same image, only 1 event gets triggered. To enable
 * multiple events use [reset] to reset last impression tracker
 *
 * @property onImpression Lambda function to dispatch callback for impression
 */
open class ImpressionImageView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defaultStyle: Int = 0,
) : AppCompatImageView(context, attrs, defaultStyle) {

    private var canLogEvent = true
    private var isLoadComplete = true

    var shouldLoadExternally = false

    private var onGlobalLayoutListener = ViewTreeObserver.OnGlobalLayoutListener {
        logImpression()
    }

    private val onPreDrawListener = ViewTreeObserver.OnPreDrawListener {
        logImpression()
        return@OnPreDrawListener true
    }

    var onImpression: (() -> Unit)? = null

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        logImpression()
    }

    /**
     * Resets the status to NOT LOGGED for new impression events to be generated
     */
    fun reset() {
        canLogEvent = true
        isLoadComplete = false
    }

    /**
     * Sets the image URL which would be loaded via Glide. Impression is triggered only after image
     * is successfully loaded.
     *
     *@param url URL to load
     * @param requestBuilder Modifier for caller to customise Glide parameters
     */
    //TODO change fun name
    fun setImageUrl(
        url: Any?,
        setPlaceholder: Boolean = true,
        requestBuilder: ((RequestBuilder<Drawable>) -> RequestBuilder<Drawable>)? = null
    ) {
        reset()
        var request = Glide.with(this)
            .load(url)
        if (setPlaceholder)
            request =
                request.placeholder(ColorDrawable(GeneralUtils.getRandomDrawableColor(context)))
        requestBuilder?.let { request = it.invoke(request) }
        request.listener(object : GlideRequestListener() {
            override fun onComplete(success: Boolean) {
                isLoadComplete = success
            }
        }).into(this)
    }

    private fun logImpression() {
        // This function is being from onDraw() and onScrollChangeListener(). The sequence of
        // conditions being executed is paramount for performance and visibility percentage should
        // be the last item to be evaluated
        if (canLogEvent &&
            (isLoadComplete || shouldLoadExternally) &&
            onImpression != null &&
            ViewUtil.getVisiblePercentage(this) >= MINIMUM_PERCENTAGE_FOR_IMPRESSION
        ) {
            onImpression?.invoke()
            canLogEvent = false
        }
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        viewTreeObserver.addOnGlobalLayoutListener(onGlobalLayoutListener)
        viewTreeObserver.addOnPreDrawListener(onPreDrawListener)
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        viewTreeObserver.removeOnGlobalLayoutListener(onGlobalLayoutListener)
        viewTreeObserver.removeOnPreDrawListener(onPreDrawListener)
    }

}
