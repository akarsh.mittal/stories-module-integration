package com.touchtalent.bobblesdk.core.moshi

import com.squareup.moshi.JsonQualifier

@Retention(AnnotationRetention.RUNTIME)
@JsonQualifier
annotation class Deeplink
