package com.touchtalent.bobblesdk.core

import android.annotation.TargetApi
import android.content.*
import android.os.Build
import androidx.core.os.UserManagerCompat
import androidx.work.WorkManager
import com.google.gson.Gson
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import com.touchtalent.bobblesdk.core.BobbleCoreSDK.config
import com.touchtalent.bobblesdk.core.BobbleCoreSDK.crossAppInterface
import com.touchtalent.bobblesdk.core.BobbleCoreSDK.listOfBobbleModule
import com.touchtalent.bobblesdk.core.BobbleCoreSDK.moshi
import com.touchtalent.bobblesdk.core.BobbleCoreSDK.okHttpClient
import com.touchtalent.bobblesdk.core.builders.ApiParamsBuilder
import com.touchtalent.bobblesdk.core.config.BobbleCoreConfig
import com.touchtalent.bobblesdk.core.enums.KeyboardDeepLink
import com.touchtalent.bobblesdk.core.interfaces.BobbleModule
import com.touchtalent.bobblesdk.core.interfaces.ConfigHandler
import com.touchtalent.bobblesdk.core.interfaces.CrossAppInterface
import com.touchtalent.bobblesdk.core.interfaces.fonts.FontManager
import com.touchtalent.bobblesdk.core.moshi.*
import com.touchtalent.bobblesdk.core.utils.BLog
import com.touchtalent.bobblesdk.core.utils.FileUtil
import com.touchtalent.bobblesdk.core.utils.onlyOnDebug
import io.reactivex.plugins.RxJavaPlugins
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import okhttp3.OkHttpClient
import org.json.JSONObject

/**
Bobble Core module is the parent module for all modules. It provides interfaces for intercommunication among modules.

Responsibilities of this modules:

1. Providing a single class for initialising all modules.
2. Provide general utility classes, which are required by all modules, like GeneralUtils, ContextUtils, BLog, FileUtils, etc
3. Acts as a communication module for other modules by holding necessary interfaces and POJO models.

Guidelines for modules dependent on [BobbleCoreSDK]:

1. All modules are required to implement [BobbleModule] which acts as an bridge for that module.
2. All modules using bobble-core module are required to register itself in core module by adding its fully qualified class name in [listOfBobbleModule].
3. Use of automated tool is recommended to create a new module. Refer [here](https://gitlab.com/BobbleAndroid/bobble-android-keyboard/-/wikis/Creating-new-bobble-module) for more details

Data points available via [BobbleCoreSDK]:

1. [crossAppInterface]
2. [okHttpClient]
3. [config]
4. [moshi]

Other app interactions like user config, periodic updaters, etc are available via the interface [BobbleModule]
 */
object BobbleCoreSDK : BobbleModule() {

    private val listOfBobbleModule = listOf(
        "com.touchtalent.bobblesdk.content.sdk.BobbleStaticContentSDK",
        "com.touchtalent.bobblesdk.headcreation.sdk.BobbleHeadSDK",
        "com.touchtalent.bobblesdk.bobble_transliteration.sdk.TransliterationSDK",
        "com.touchtalent.bobbleapp.nativeapi.sdk.BobbleNativeApiSDK",
        "com.touchtalent.bobblesdk.intent.sdk.BobbleIntentSDK",
        "com.touchtalent.super_app_module.SuperAppSDK",
        "com.touchtalent.bobblesdk.intentprediction.IntentPredictionSDK",
        "com.touchtalent.bobblesdk.ai_predictions.sdk.BobbleAIPredictionSDK",
        "com.touchtalent.bobblesdk.promotional_offers.PromotionalOffersSDK",
        "com.touchtalent.bobblesdk.bobble_native_ads.sdk.BobbleNativeAdsSDK",
        "com.touchtalent.bobblesdk.default_prompts.sdk.DefaultPromptSDK",
        "com.touchtalent.bobblesdk.poptext.sdk.PopTextSdk",
        "com.touchtalent.bobblesdk.content_suggestions.module.ContentSuggestionSDK",
        "com.touchtalent.bobblesdk.bigmoji.sdk.BigmojiSDK",
        "com.touchtalent.bobblesdk.moviegif.sdk.MovieGifSDK",
        "com.touchtalent.bobblesdk.animation_processor.sdk.AnimationProcessorSDK",
        "com.touchtalent.bobblesdk.animated_stickers.sdk.AnimatedStickersSDK",
        "com.touchtalent.bobblesdk.cre_ui.sdk.ContentRecommendationSDK",
        "com.touchtalent.bobblesdk.stories.sdk.BobbleStorySDK",
        "com.touchtalent.bobblesdk.stories_ui.sdk.BobbleStoryUiSDK",
        "com.touchtalent.bobblesdk.content.ui.sdk.BobbleContentUiSDK",
        "com.touchtalent.bobblesdk.content_activity.sdk.ContentActivitySDK",
        "com.touchtalent.bobblesdk.cre_banners.sdk.ContentBannersSdk",
        // All content modules to be placed above ContentCoreSDK
        "com.touchtalent.bobblesdk.content_core.sdk.ContentCoreSDK",
        "com.mint.watermark_injection.WatermarkInjectorSDK"
    )

    @JvmStatic
    val modules = mutableListOf<BobbleModule>()

    @JvmStatic
    public lateinit var applicationContext: Context

    /**
     * Interface which provides provides basic app level data and interactions like modifyActivityIntentForKeyboard, getVersion, getVersion, runInBootAwareMode, etc
     */
    lateinit var crossAppInterface: CrossAppInterface

    /**
     * Provides configuration parameters for individual parameters as well as app level parameters.
     */
    lateinit var config: BobbleCoreConfig

    /**
     * Base okHttpClient used by the parent app. All modules are required to use this client for all network operations to maintain a single instance if okHTTP across app.
     */
    lateinit var okHttpClient: OkHttpClient

    /**
     *  Global [Moshi] instance which provides access to basic adapters such as [ColorAdapter], [DateTimeAdapter], [KotlinRegexAdapter], etc
     */
    val moshi: Moshi by lazy {
        Moshi.Builder()
            .addLast(DeeplinkLocalAdapter())
            .add(ColorAdapter())
            .add(DateTimeAdapter())
            .add(KotlinRegexAdapter())
            .add(StringWrapperAdapter())
            .add(TimeAdapter())
            .add(MoshiDateAdapter())
            .add(KotlinJsonAdapterFactory())
            .add(EncryptionAdapterFactory())
            .build()
    }

    val applicationScope = CoroutineScope(Dispatchers.Default + SupervisorJob())

    lateinit var rootDirNonBootAware: String
    lateinit var cacheDirNonBootAware: String

    private val configHandlers = mutableListOf<ConfigHandler>()

    /**
     * Initialises the core module and all other modules within [listOfBobbleModule] via Reflection.
     */
    override fun initialise(applicationContext: Context, config: BobbleCoreConfig) {
        this.applicationContext = applicationContext.applicationContext
        this.config = config
        this.crossAppInterface = config.crossAppInterface
        okHttpClient = config.okHttpClient ?: OkHttpClient()
        val contextForDir =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N && supportsBootAwareMode()) {
                applicationContext.createDeviceProtectedStorageContext()
            } else {
                applicationContext
            }
        rootDir = FileUtil.join(contextForDir.filesDir, getCodeName())
        cacheDir = FileUtil.join(contextForDir.cacheDir, getCodeName())
        rootDirNonBootAware = FileUtil.join(applicationContext.filesDir, getCodeName())
        cacheDirNonBootAware = FileUtil.join(applicationContext.cacheDir, getCodeName())
        initRxJava()
        initBobbleModules()
    }

    private fun initRxJava() {
        val oldErrorHandler = RxJavaPlugins.getErrorHandler()
        RxJavaPlugins.setErrorHandler { throwable: Throwable ->
            BLog.e(
                "BobbleCoreSDK",
                "RxCrash",
                throwable
            )
            oldErrorHandler?.accept(throwable)
        }
    }

    override fun supportsBootAwareMode() = true

    fun initBobbleModules() {
        for (className in listOfBobbleModule) {
            val module = DependencyResolver.getObjectInstance<BobbleModule>(className)
            module?.let {
                if (module.supportsBootAwareMode())
                    initModule(module)
                else crossAppInterface.runInBootAwareMode { initModule(module) }
            }
        }
    }

    private fun initModule(module: BobbleModule) {
        modules.add(module)
        val contextForDir =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N && module.supportsBootAwareMode()) {
                applicationContext.createDeviceProtectedStorageContext()
            } else {
                applicationContext
            }
        module.rootDir = FileUtil.join(contextForDir.filesDir, module.getCodeName())
        module.cacheDir = FileUtil.join(contextForDir.cacheDir, module.getCodeName())
        module.initialise(applicationContext, config)
        module.seedDefaultValues()
    }

    /**
     * Gets the module by its interface class. This is required for loosely-coupled modules where
     * the module might not be available during compile-time and only its interface might be available
     * for compilation. The parent app must use this function to get a reference to the module's base
     * class and communicate via interface functions instead of directing accessing classes from the
     * dependent module.
     */
    @JvmStatic
    @Suppress("unchecked_cast")
    fun <T> getModule(className: Class<T>): T? {
        for (module in modules) {
            if (className.isAssignableFrom(module.javaClass)) {
                return module as? T
            }
        }
        return null
    }

    /**
     * Returns the default [SharedPreferences] instance from the underlying storage context.
     */
    @JvmStatic
    @TargetApi(Build.VERSION_CODES.N)
    fun getDefaultSharedPreferences(
        prefName: String?,
        mode: Int
    ): SharedPreferences {
        try {
            val storageContext: Context
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                // All N devices have split storage areas. Migrate the existing preferences into the new
                // device encrypted storage area if that has not yet occurred.
                storageContext = applicationContext.createDeviceProtectedStorageContext()
                if (!storageContext.moveSharedPreferencesFrom(applicationContext, prefName)) {
                    BLog.d("BobbleCore", "Failed to migrate shared preferences")
                }
            } else {
                storageContext = applicationContext
            }
            return storageContext.getSharedPreferences(prefName, mode)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return applicationContext.getSharedPreferences(prefName, mode)
    }

    /**
     * Get [WorkManager] instance. Returns null when device is in "locked" state and work manager
     * cannot be initialised
     */
    fun getWorkManager(): WorkManager? {
        return if (UserManagerCompat.isUserUnlocked(applicationContext))
            WorkManager.getInstance(applicationContext)
        else null
    }

    /**
     * To be called by the parent app from keyboard close callback to trigger periodic updater of
     * each module
     * @param forced true if periodic updater needs to be forcefully updated
     */
    override fun runPeriodicUpdaters(forced: Boolean) {
        super.runPeriodicUpdaters(forced)
        for (module in modules)
            module.runPeriodicUpdaters(forced)
    }

    /**
     * To be called by the parent app from SuccessResponseParseUtil to pass user config payload to
     * other modules
     * @param response User config [JSONObject] payload
     */
    override fun handleUserConfig(response: JSONObject) {
        for (module in modules) {
            try {
                module.handleUserConfig(response)
            } catch (e: Exception) {
                BLog.printStackTrace(e)
            }
        }
        applicationScope.launch {
            configHandlers.forEach {
                it.handleConfig(response)
            }
        }
    }

    fun registerConfigHandler(configHandler: ConfigHandler) {
        configHandlers.add(configHandler)
    }

    override fun getCodeName() = "bobble-core"

    override fun resetLoginCredentials() {
        modules.forEach { it.resetLoginCredentials() }
    }

    /**
     * Get instance of universal [FontManager]
     */
    fun getFontDownloader() = config.fontManager

    /**
     * Get instance of universal [CrossAppInterface]
     */
    fun getAppController() = crossAppInterface

    @Deprecated(
        message = "Direct access to BobbleCore has been deprecated.",
        replaceWith = ReplaceWith("BobbleCoreSDK.getAppController().logEvents(eventType, eventAction, screenAt, data, logMultiple)")
    )
    fun logEvent(
        eventType: String?,
        eventAction: String?,
        screenAt: String?,
        data: String?,
        logMultiple: Boolean = false
    ) = crossAppInterface.logEvents(eventType, eventAction, screenAt, data, logMultiple)

    @Deprecated(
        message = "Direct access to BobbleCore has been deprecated.",
        replaceWith = ReplaceWith("BobbleCoreSDK.getAppController().getAppName()")
    )
    @JvmStatic
    fun getAppName() = crossAppInterface.getAppName()

    @Deprecated(
        message = "Direct access to BobbleCore has been deprecated.",
        replaceWith = ReplaceWith("BobbleCoreSDK.getAppController().getAdvertisingId()")
    )
    fun getAdvertisingId(): String {
        return crossAppInterface.getAdvertisingId()
    }

    @Deprecated(
        message = "Direct access to BobbleCore has been deprecated.",
        replaceWith = ReplaceWith("BobbleCoreSDK.getAppController().isLimitAdTrackingEnabled()")
    )
    fun isLimitAdTrackingEnabled(): Boolean {
        return crossAppInterface.isLimitAdTrackingEnabled()
    }

    @Deprecated(
        message = "Direct access to BobbleCore has been deprecated.",
        replaceWith = ReplaceWith("BobbleCoreSDK.getAppController().getApiParamsBuilder()")
    )
    @JvmStatic
    fun getApiParamsBuilder(): ApiParamsBuilder = crossAppInterface.getApiParamsBuilder()

    @Deprecated(
        message = "Direct access to BobbleCore has been deprecated.",
        replaceWith = ReplaceWith("BobbleCoreSDK.getAppController().canLogEvents()")
    )
    @JvmStatic
    fun canLogEvents() = crossAppInterface.canLogEvents()

    @Deprecated(
        message = "Direct access to BobbleCore has been deprecated.",
        replaceWith = ReplaceWith("BobbleCoreSDK.getAppController().logException(tag, exception)")
    )
    @JvmStatic
    fun logException(tag: String, exception: Throwable) =
        crossAppInterface.logException(tag, exception)

    @Deprecated(
        message = "Direct access to BobbleCore has been deprecated.",
        replaceWith = ReplaceWith("BobbleCoreSDK.getAppController().getCurrentPackageName(isKeyboardView)")
    )
    @JvmStatic
    fun getCurrentPackageName(isKeyboardView: Boolean): String =
        crossAppInterface.getCurrentPackageName(isKeyboardView)

    @Deprecated(
        message = "Direct access to BobbleCore has been deprecated.",
        replaceWith = ReplaceWith("BobbleCoreSDK.getAppController().getSessionId(isKeyboardView)")
    )
    @JvmStatic
    fun getSessionId(isKeyboardView: Boolean): String =
        crossAppInterface.getSessionId(isKeyboardView)

    @Deprecated(
        message = "Direct access to BobbleCore has been deprecated.",
        replaceWith = ReplaceWith("BobbleCoreSDK.getAppController().getBasicFont(otfText)")
    )
    @JvmStatic
    fun getBasicFont(otfText: String) = crossAppInterface.getBasicFont(otfText)

    @Deprecated(
        message = "Direct access to BobbleCore has been deprecated.",
        replaceWith = ReplaceWith("BobbleCoreSDK.getAppController().sendOpenKeyboardIntent(sourceIntent)")
    )
    @JvmStatic
    fun sendOpenKeyboardIntent(sourceIntent: Intent?) =
        crossAppInterface.sendOpenKeyboardIntent(sourceIntent)

    @JvmOverloads
    @Deprecated(
        message = "Direct access to BobbleCore has been deprecated.",
        replaceWith = ReplaceWith("BobbleCoreSDK.getAppController().modifyActivityIntentForKeyboard(intent, deepLink)")
    )
    @JvmStatic
    fun modifyActivityIntentForKeyboard(intent: Intent, @KeyboardDeepLink deepLink: Int? = null) =
        crossAppInterface.modifyActivityIntentForKeyboard(intent, deepLink)

    @Deprecated(
        message = "Direct access to BobbleCore has been deprecated.",
        replaceWith = ReplaceWith("BobbleCoreSDK.getAppController().getVersion()")
    )
    @JvmStatic
    fun getVersion() = crossAppInterface.getVersion()

    @Deprecated(
        message = "Direct access to BobbleCore has been deprecated.",
        replaceWith = ReplaceWith("BobbleCoreSDK.getAppController().appVersion()")
    )
    fun appVersion() = crossAppInterface.getVersion()

    @Deprecated(
        message = "GSON has been deprecated because of its limited capabilities to handle Kotlin data classes. Use Moshi instead",
        replaceWith = ReplaceWith(
            expression = "BobbleCoreSDK.moshi"
        )
    )
    fun getGson(): Gson {
        return crossAppInterface.getGson()
    }

    fun getAllModules(): List<BobbleModule> {
        return modules
    }

}