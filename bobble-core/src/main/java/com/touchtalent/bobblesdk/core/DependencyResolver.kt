package com.touchtalent.bobblesdk.core

import android.content.Context
import android.util.AttributeSet
import androidx.annotation.Keep
import com.touchtalent.bobblesdk.core.interfaces.BobbleFSTTransliterator
import com.touchtalent.bobblesdk.core.interfaces.ai_predictions.NativeDictionary
import com.touchtalent.bobblesdk.core.interfaces.head.ChooseHeadView
import com.touchtalent.bobblesdk.core.interfaces.head.HeadFloatingIcon
import com.touchtalent.bobblesdk.core.interfaces.promotional_offers.BobblePromotionalOffersView
import com.touchtalent.bobblesdk.core.utils.BLog
import java.util.*

/**
 * This class is responsible for resolving dependencies via Reflection.
 * Any module might be used by either tight coupling or loose coupling:
 * 1. Tight coupling - The parent app will require the module during compile time. If the module
 * is not present (implementation from gradle removed), the parent app will fail to compile
 * 2. Loose coupling -The parent app will <b>NOT</b> require the module during compile time but only
 * during run-time (optional). The parent app should be configured in such a way that the module
 * might/might not be present during run-time. This enables plug-n-play nature of a module, where
 * removing a module is as simple as removing dependency its from build.gradle
 */
@Keep
object DependencyResolver {

    class Dependency(
        var className: String,
        var constructor: Array<Class<*>> = arrayOf()
    )

    private val TRANSLITERATION = Dependency(
        "com.touchtalent.bobbleapp.translitrationG2pfst.BobbleFSTTransliteratorImpl",
    )

    private val CHOOSE_HEAD_VIEW = Dependency(
        "com.touchtalent.bobblesdk.headcreation.custom.ChooseHeadViewImpl",
        arrayOf(
            Context::class.java,
            ChooseHeadView.ThemeParams::class.java
        )
    )

    private val BOBBLE_PROMOTIONAL_OFFERS_VIEW = Dependency(
        "com.touchtalent.bobblesdk.promotional_offers.sdk.views.BobblePromotionalOffersViewImpl",
        arrayOf(
            Context::class.java,
            AttributeSet::class.java
        )
    )

    private val HEAD_FLOATING_ICON = Dependency(
        "com.touchtalent.bobblesdk.headcreation.sdk.views.HeadFloatingIconImpl",
        arrayOf(Context::class.java)
    )

    private val NATIVE_DICTIONARY = Dependency(
        "com.android.inputmethod.latin.NativeDictionaryImpl",
        arrayOf(
            String::class.java,
            Long::class.java,
            Long::class.java,
            Boolean::class.java,
            Locale::class.java,
            String::class.java
        )
    )

    private val IMPLEMENTATION_CLASS_MAP = mapOf(
        BobbleFSTTransliterator::class.java to TRANSLITERATION,
        ChooseHeadView::class.java to CHOOSE_HEAD_VIEW,
        HeadFloatingIcon::class.java to HEAD_FLOATING_ICON,
        NativeDictionary::class.java to NATIVE_DICTIONARY
    )

    @JvmStatic
    fun newHeadFloatingIcon(context: Context): HeadFloatingIcon? {
        return newInstance(HEAD_FLOATING_ICON, context)
    }

    @JvmStatic
    fun newBobblePromotionalOffersView(context: Context): BobblePromotionalOffersView? {
        return newInstance(BOBBLE_PROMOTIONAL_OFFERS_VIEW, context, null)
    }

    @JvmStatic
    fun newNativeDictionary(
        filename: String,
        offset: Long,
        length: Long,
        useFullEditDistance: Boolean,
        locale: Locale?,
        dictType: String
    ): NativeDictionary? {
        return newInstance(
            NATIVE_DICTIONARY, filename,
            offset,
            length,
            useFullEditDistance,
            locale,
            dictType
        )
    }

    @JvmStatic
    fun newBobbleFSTTransliterator(): BobbleFSTTransliterator? {
        return newInstance(TRANSLITERATION)
    }

    @JvmStatic
    fun newChooseHeadView(
        context: Context,
        themeParams: ChooseHeadView.ThemeParams? = null
    ): ChooseHeadView? {
        return newInstance(
            CHOOSE_HEAD_VIEW,
            context,
            themeParams
        )
    }

    /**
     * Runtime check to verify whether the implementation of given class exists.
     */
    @JvmStatic
    fun exists(classType: Class<*>): Boolean {
        val className = IMPLEMENTATION_CLASS_MAP[classType]?.className ?: return false
        try {
            Class.forName(className)
        } catch (e: Exception) {
            return false
        }
        return true
    }

    /**
     * Create a new instance of the given [Dependency] with the specified arguments.
     * @return null if class not found, or corresponding constructor not found
     */
    @Suppress("UNCHECKED_CAST")
    private fun <T> newInstance(
        dependency: Dependency,
        vararg objects: Any?
    ): T? {
        return try {
            val classObject = Class.forName(dependency.className)
            if (objects.isEmpty()) {
                classObject.newInstance() as T
            } else {
                classObject.getConstructor(*dependency.constructor).newInstance(*objects) as T
            }
        } catch (e: Exception) {
            BLog.printStackTrace(e)
            null
        }
    }

    /**
     * Fetch the kotlin object of the given class.
     */
    @Suppress("unchecked_cast")
    fun <T> getObjectInstance(className: String): T? {
        return try {
            val classObject = Class.forName(className).kotlin
            return classObject.objectInstance as? T
        } catch (e: Exception) {
            null
        }
    }
}