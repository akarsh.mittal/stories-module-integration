package com.touchtalent.bobblesdk.core.builders

import androidx.annotation.Keep
import com.touchtalent.bobblesdk.core.BobbleCoreSDK
import com.touchtalent.bobblesdk.core.interfaces.CrossAppInterface
import com.touchtalent.bobblesdk.core.utils.ThemeUtil
import java.util.*

/**
 * Utility builder class for populating API params whose key-value pairs are provided the parent app.
 *
 * _Any addition to this class must be made a generic use-case for all APIs.
 * Please discuss with backend for uniformity before adding/duplicating any existing function_
 *
 * ### For modules/libraries:
 * Provides support for basic params like clientId, deviceId, version params, etc.
 *
 * Use [build] function to obtain the [HashMap] after chaining required params. The [build] function
 * ensure to add *deviceId* = *android* by default
 *
 * ### For parent apps:
 * The parent app is responsible for deciding the default *key*, though it can be overridden by the
 * module if required.
 *
 * Create a implementation of this class and register it in core module by returning an instance in
 * [CrossAppInterface.getApiParamsBuilder]
 *
 */
@Keep
abstract class ApiParamsBuilder {

    private val hashMap = HashMap<String, String>()

    /**
     * Populates device id of the app
     * @param key Key to override the default key used by the parent app
     */
    fun withDeviceId(key: String? = null): ApiParamsBuilder {
        populateDeviceIdParams(hashMap, key)
        return this
    }

    /**
     * Populates client id of the app
     * @param key Key to override the default key used by the parent app
     */
    fun withClientId(key: String? = null): ApiParamsBuilder {
        populateClientIdParams(hashMap, key)
        return this
    }

    /**
     * Populates versions corresponding to the app. Multiple version can be added as per the parent
     * app's requirement. For e.g - appVersion, sdkVersion, bobbleSdkVersion
     */
    fun withVersion(): ApiParamsBuilder {
        populateVersionParams(hashMap)
        return this
    }

    /**
     * Populates location params like *countryCode*, *geoLocationCountryCode*, *geoLocationAdmin1*,
     * *geoipLocationCountryCode*, etc
     */
    @Deprecated(
        message = "This has been deprecated in favour of withLocationV2(). " +
            "In case server requests this params, ask them to use withLocationV2() params instead",
        replaceWith = ReplaceWith(expression = "withLocationV2()")
    )
    fun withGeoLocation(): ApiParamsBuilder {
        populateGeoLocationParams(hashMap)
        return this
    }

    /**
     * Populates location params like *cityName*, *regionName* and *countryCode*
     */
    fun withLocationV2() = apply {
        populateLocationV2Params(hashMap)
    }

    /**
     * Populates the GAID of the user
     */
    fun withAdvertisementId(): ApiParamsBuilder {
        populateAdvertisementId(hashMap)
        return this
    }

    /**
     * Populates UTM campaign related params. For e.g: *utmCampaign*, *utmMedium*, *utmContent*,
     * *utmTerm*
     */
    fun withUtmCampaign(): ApiParamsBuilder {
        populateUtmCampaignParams(hashMap)
        return this
    }

    /**
     * Populates the parameter *locale*, which is equivalent to current keyboard language's locale
     */
    fun withLocale(): ApiParamsBuilder {
        populateLocaleParams(hashMap)
        return this
    }

    /**
     * Populates the parameter *languageCode*, which is equivalent to current keyboard language's locale
     */
    fun withLanguageCode(): ApiParamsBuilder {
        populateLanguageCodeParams(hashMap)
        return this
    }

    /**
     * Populates the current timezone of the user
     */
    fun withTimeZone(): ApiParamsBuilder {
        hashMap["timezone"] = getTimeZone()
        return this
    }

    /**
     * Populates the resolution of user's device in dpi format, e.g - hdpi, xhdpi, ect
     */
    fun withResolution(): ApiParamsBuilder {
        hashMap["resolution"] = ThemeUtil.getScreenResolution(BobbleCoreSDK.applicationContext)
        return this
    }

    /**
     * Populates the exact latitude and longitude of the user (last known location)
     */
    fun withGPSCoordinates() = apply {
        populateGPSCoordinates(hashMap)
    }

    private fun getTimeZone(): String {
        val calendar = Calendar.getInstance(TimeZone.getDefault())
        val zone = calendar.timeZone
        return zone.id ?: ""
    }

    /**
     * Populates the device manufacturer of the user's device
     */
    fun withDeviceManufacturer() = apply {
        populateDeviceManufacturer(hashMap)
    }

    /**
     * Populates whether limit ad tracking is enabled by the user or not
     */
    fun withLimitAdTracking() = apply {
        populateLimitAdTracking(hashMap)
    }

    /**
     * Populates cellular data like *internetServiceProvider*, *networkType*, *cellularNetworkCellId* and *cellularNetworkLocationAreaCode*
     */
    fun withCellularData() = apply {
        populateCellularData(hashMap)
    }

    /**
     * Build the final hashmap for API requests. This by default adds the *deviceType* as per
     * backend's guidelines
     */
    fun build(): HashMap<String, String> {
        hashMap["deviceType"] = "android"
        return hashMap
    }

    abstract fun populateDeviceIdParams(hashMap: HashMap<String, String>, key: String?)
    abstract fun populateClientIdParams(hashMap: HashMap<String, String>, key: String?)
    abstract fun populateVersionParams(hashMap: HashMap<String, String>)
    abstract fun populateGeoLocationParams(hashMap: HashMap<String, String>)
    abstract fun populateUtmCampaignParams(hashMap: HashMap<String, String>)
    abstract fun populateDeviceManufacturer(hashMap: HashMap<String, String>)
    abstract fun populateAdvertisementId(hashMap: HashMap<String, String>)
    abstract fun populateLocationV2Params(hashMap: HashMap<String, String>)
    abstract fun populateGPSCoordinates(hashMap: HashMap<String, String>)
    abstract fun populateLimitAdTracking(hashMap: HashMap<String, String>)
    abstract fun populateCellularData(hashMap: HashMap<String, String>)

    open fun getAuthorisationKeyAndValue(): Pair<String, String>? {
        val accessToken = BobbleCoreSDK.getAppController().getLoginCredentials()?.accessToken?.ifEmpty { null }
        return accessToken?.let { "Authorization" to "Bearer $it" }
    }

    open fun populateLanguageCodeParams(hashMap: HashMap<String, String>) {}

    open fun populateUserAgent(hashMap: HashMap<String, String>) {}
    open fun populateDeviceLanguage(hashMap: HashMap<String, String>) {}
    open fun populateDeviceModel(hashMap: HashMap<String, String>) {}

    private fun populateLocaleParams(hashMap: HashMap<String, String>) {
        hashMap["locale"] = BobbleCoreSDK.crossAppInterface.getActiveLanguageLocale()
    }

    fun withDeviceModel() = apply {
        populateDeviceModel(hashMap)
    }

    fun withUserAgent() = apply {
        populateUserAgent(hashMap)
    }

    fun withDeviceLanguage() = apply {
        populateDeviceLanguage(hashMap)
    }
}