package com.touchtalent.bobblesdk.core.moshi

import com.squareup.moshi.JsonQualifier


/**
 * created by anil_
 * on 21/01/23
 * at 4:32 PM
 */


/**
 * Denotes annotated element is String in the format of "HH:mm"
 */

@Retention(AnnotationRetention.RUNTIME)
@JsonQualifier
annotation class Time()
