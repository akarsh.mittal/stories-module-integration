package com.touchtalent.bobblesdk.core.room

import androidx.room.TypeConverter
import com.google.gson.reflect.TypeToken
import com.touchtalent.bobblesdk.core.BobbleCoreSDK
import com.touchtalent.bobblesdk.core.utils.It
import java.lang.reflect.Type
import java.util.*

/**
 * Room converter for general use cases.
 * 1. Time in millis to [Date]
 * 2. HashMap<String, String> serialisation
 * 3. List<String> serialisation
 */
class GeneralConverters {

    /**
     * @suppress
     */
    @TypeConverter
    fun fromTimestamp(value: Long?): Date? {
        return value?.let { Date(it) }
    }

    /**
     * @suppress
     */
    @TypeConverter
    fun dateToTimestamp(date: Date?): Long? {
        return date?.time
    }

    /**
     * @suppress
     */
    @TypeConverter
    fun hashMapToString(hashMap: HashMap<String, String>?): String? {
        if (hashMap == null)
            return ""
        return BobbleCoreSDK.getGson().toJson(hashMap)
    }

    /**
     * @suppress
     */
    @TypeConverter
    fun stringToHashMap(string: String?): HashMap<String, String> {
        if (It.isEmpty(string))
            return HashMap()
        val type: Type = object : TypeToken<HashMap<String, String>>() {}.type
        return BobbleCoreSDK.getGson().fromJson(string, type)
    }

    /**
     * @suppress
     */
    @TypeConverter
    fun listOfStringToString(list: List<String?>?): String {
        if (list == null)
            return ""
        return BobbleCoreSDK.getGson().toJson(list)
    }

    /**
     * @suppress
     */
    @TypeConverter
    fun stringToListOfString(string: String?): List<String> {
        if (It.isEmpty(string))
            return ArrayList()
        val type: Type = object : TypeToken<List<String>>() {}.type
        return BobbleCoreSDK.getGson().fromJson(string, type)
    }
}
