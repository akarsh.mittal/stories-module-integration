package com.touchtalent.bobblesdk.core.views;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.ProgressBar;

import androidx.core.content.ContextCompat;

import com.touchtalent.bobblesdk.core.R;


public class ThemedProgressBar extends ProgressBar {

    public ThemedProgressBar(Context context) {
        super(context);
        init(null);
    }

    public ThemedProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public ThemedProgressBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.ThemedProgressBar, 0, 0);
        int indeterminateDrawable = typedArray.getResourceId(R.styleable.ThemedProgressBar_tpbIndeterminateDrawable, 0);
        int color = typedArray.getColor(R.styleable.ThemedProgressBar_tpbPrimaryColor, Color.TRANSPARENT);
        typedArray.recycle();
        Drawable progressDrawable;
        if (color == Color.TRANSPARENT)
            return;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setIndeterminateTintList(ColorStateList.valueOf(color));
        }
        if (indeterminateDrawable != 0) {
            progressDrawable = ContextCompat.getDrawable(getContext(), indeterminateDrawable);
            setIndeterminateDrawable(progressDrawable);
        }
    }
}
