package com.touchtalent.bobblesdk.core.utils

import android.annotation.SuppressLint
import android.app.Activity
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewParent
import androidx.annotation.MainThread
import androidx.core.view.doOnAttach
import androidx.lifecycle.*
import com.touchtalent.bobblesdk.core.BobbleCoreSDK

/**
 * Utility function to set a click listener and listen for the co-ordinates on which the user has
 * clicked.
 */
@SuppressLint("ClickableViewAccessibility")
fun View.setClickListenerWithPosition(listener: (x: Float, y: Float) -> Unit) {
    val lastTouchDownXY = FloatArray(2)
    // the purpose of the touch listener is just to store the touch X,Y coordinates
    setOnTouchListener { _, event -> // save the X,Y coordinates
        if (event.actionMasked == MotionEvent.ACTION_DOWN) {
            lastTouchDownXY[0] = event.x
            lastTouchDownXY[1] = event.y
        }
        // let the touch event pass on to whoever needs it
        false
    }

    setOnClickListener { // retrieve the stored coordinates
        val x: Float = lastTouchDownXY[0]
        val y: Float = lastTouchDownXY[1]
        listener(x, y)
    }
}

/**
 * Recursively checks all parents to find a parent that implements [ViewModelStoreOwner]
 * This is useful in cases when a Activity <-> Fragment relation needs to achieved in Views.
 * The parent view becomes the [ViewModelStoreOwner] for the child view so that its state can be
 * maintained while switching between other child views
 */
fun View.getParentViewModelStoreOwner(): ViewModelStoreOwner? {
    var currentParent = parent
    while (currentParent != null) {
        if (currentParent is ViewModelStoreOwner) {
            return currentParent
        }
        currentParent = currentParent.parent
    }
    return null
}


/**
 * View equivalent of Activity's -> by viewModels(). Helps to create [ViewModel] for [View].
 * [View] acts as UI host for keyboard, in comparison to Activity and Fragment.
 * If the given view is attached to a [Activity], the [Activity]'s [ViewModelStoreOwner] is used.
 *
 * Otherwise a new ViewModelStoreOwner is created which gets cleared on [View.onDetachedFromWindow]
 *
 * @param key Key used to create multiple view model instances of the same ViewModel
 */
@MainThread
inline fun <reified VM : ViewModel> View.viewModels(key: String? = null): Lazy<VM> = lazy {
    val parentActivity = context.parentActivity
    val parentViewModelStore = getParentViewModelStoreOwner()
    val viewModelStoreOwner = parentViewModelStore ?: parentActivity ?: kotlin.run {
        val viewModelStore = ViewModelStore()
        addOnAttachStateChangeListener(object : View.OnAttachStateChangeListener {
            override fun onViewAttachedToWindow(v: View) {
            }

            override fun onViewDetachedFromWindow(v: View) {
                viewModelStore.clear()
            }
        })
        ViewModelStoreOwner { viewModelStore }
    }
    val viewModelProvider = ViewModelProvider(viewModelStoreOwner)
    val viewModelKey = key ?: tag?.toString()
    viewModelKey?.let { viewModelProvider[it, VM::class.java] } ?: viewModelProvider[VM::class.java]
}

/**
 * Creates a lifecycle owner for a View.
 * If the given view is attached to a [Activity], the [Activity]'s [LifecycleOwner] is used.
 *
 * The given lifecycle has 3 events - OnStart, OnCreate, OnDestroy
 * OnCreate is called instantly when instantiated
 * OnStart is called when the view is attached
 * OnDestroy is called when the view detached
 */
fun View.lifecycleOwner(): Lazy<LifecycleOwner> = lazy {
    val parentActivity = context.parentActivity
    if (parentActivity != null) return@lazy parentActivity
    val lifecycleOwner = object : LifecycleOwner {
        val registry by lazy { LifecycleRegistry(this) }
        override fun getLifecycle() = registry
    }
    lifecycleOwner.lifecycle.handleLifecycleEvent(Lifecycle.Event.ON_CREATE)

    doOnAttach {
        lifecycleOwner.lifecycle.handleLifecycleEvent(Lifecycle.Event.ON_START)
    }
    val destroyListener = getParentOnDestroyListener()
    if (destroyListener != null) {
        destroyListener.addOnDestroyListener {
            lifecycleOwner.lifecycle.handleLifecycleEvent(Lifecycle.Event.ON_DESTROY)
        }
    } else {
        addOnAttachStateChangeListener(object : View.OnAttachStateChangeListener {
            override fun onViewAttachedToWindow(view: View) {}

            override fun onViewDetachedFromWindow(view: View) {
                removeOnAttachStateChangeListener(this)
                lifecycleOwner.lifecycle.handleLifecycleEvent(Lifecycle.Event.ON_DESTROY)
            }
        })
    }
    lifecycleOwner
}

private fun View.getParentOnDestroyListener(): OnDestroyListener? {
    if (this is OnDestroyListener)
        return this
    var currentParent = parent
    while (currentParent != null) {
        if (currentParent is OnDestroyListener) {
            return currentParent
        }
        currentParent = currentParent.parent
    }
    return null
}

interface OnDestroyListener {
    fun addOnDestroyListener(listener: () -> Unit)
}

/**
 * Returns the local of the [View]
 */
val View.locale: String
    get() = ContextUtils.getLocale(this.context).toString()

/**
 * Utility function to fetch [LayoutInflater] inside a [View]
 */
val View.layoutInflater: LayoutInflater
    get() = LayoutInflater.from(context)

val Int.dp: Int
    get() = ViewUtil.dpToPx(this.toFloat(), BobbleCoreSDK.applicationContext)

val Float.dp: Int
    get() = ViewUtil.dpToPx(this, BobbleCoreSDK.applicationContext)