package com.touchtalent.bobblesdk.core.executor;

import com.touchtalent.bobblesdk.core.utils.BLog;

import java.util.concurrent.Callable;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


/**
 * Created by aamir on 30/10/17.
 */

public class CommonThreadPoolExecutor extends ThreadPoolExecutor {

    public CommonThreadPoolExecutor(int maxNumThreads, ThreadFactory threadFactory) {
        super(maxNumThreads, maxNumThreads, 0, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<Runnable>(), threadFactory);
    }

    public void executeCommonTask(final Callable callable) {
            execute(new Runnable() {
                @Override
                public void run() {
                    try {
                        callable.call();
                    } catch (Exception e) {
                        BLog.printStackTrace(e);
                    }
                }
            });
    }
}
