package com.touchtalent.bobblesdk.core.interfaces.head

import android.content.Context
import android.util.AttributeSet
import androidx.annotation.ColorInt
import androidx.cardview.widget.CardView

/**
 * View to facilitate a UI for a floating icon which displays currently selected head. If no head
 * is created a camera icon is shown to create a new head. Automatically refreshes in case current
 * head is changed
 * Call [updateEventParams] for correct event tracking
 */
abstract class HeadFloatingIcon @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : CardView(context, attrs, defStyleAttr) {
    protected var listener: HeadFloatingIconListener? = null

    /**
     * Set primary color for the floating icon
     * @param colorInt New primary color
     */
    abstract fun setPrimaryColor(@ColorInt colorInt: Int)

    /**
     * Animate the icon - the icon expands to show a text for head creation and collapses back after
     * a few seconds
     */
    abstract fun animateText()

    /**
     * Update event params, us  ed for events.
     * @param isKeyboardView Whether the UI is shown in keyboard
     * @param sourcePackId Id of source content pack where this UI is visible
     * @param screenName Current screen name
     */
    abstract fun updateEventParams(isKeyboardView: Boolean, screenName: String, sourcePackId: Int?)

    /**
     * Listener for click actions
     */
    interface HeadFloatingIconListener {
        /**
         * Callback when user clicks on a head, a [ChooseHeadView] should be opened to allow user
         * to manage heads
         */
        fun onOpenChooseHeadPanel()

        /**
         * Callback when camera icon is clicked to launch head creation flow
         */
        fun onLaunchHeadCreationFlow()
    }

    /**
     * Set listener for click events
     * @see HeadFloatingIconListener
     */
    fun setOnHeadPanelOpenListener(headFloatingIconListener: HeadFloatingIconListener?) {
        this.listener = headFloatingIconListener
    }

}