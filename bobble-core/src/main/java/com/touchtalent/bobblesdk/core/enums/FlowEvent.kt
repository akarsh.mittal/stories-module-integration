package com.touchtalent.bobblesdk.core.enums

enum class FlowEvent {
    LOGIN,
    LOGOUT
}