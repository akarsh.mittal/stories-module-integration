package com.touchtalent.bobblesdk.core.api

class BobbleResult<out T> private constructor(val value: Any?) {
    class Failure(val exception: Throwable)

    /**
     * Returns `true` if this instance represents a successful outcome.
     * In this case [isFailure] returns `false`.
     */
    val isSuccess: Boolean get() = value !is Failure

    /**
     * Returns `true` if this instance represents a failed outcome.
     * In this case [isSuccess] returns `false`.
     */
    private val isFailure: Boolean get() = value is Failure

    inline fun onSuccess(callback: (result: T) -> Unit): BobbleResult<T> {
        if (isSuccess)
            callback.invoke(value as T)
        return this
    }

    inline fun onFailure(callback: (result: Throwable) -> Unit): BobbleResult<T> {
        if (!isSuccess)
            callback.invoke((value as Failure).exception)
        return this
    }

    fun getOrNull(): T? {
        return if (isSuccess) value as T else null
    }

    fun getOrThrow(): T? {
        return if (isSuccess) value as T else throw (value as Failure).exception
    }

    inline fun <K> mapOrDefault(map: (value: T) -> K, default: () -> K): K {
        return if (isSuccess)
            map(value as T)
        else default()
    }

    companion object {
        fun <T> success(value: T): BobbleResult<T> {
            return BobbleResult(value)
        }

        fun <T> failure(error: Throwable): BobbleResult<T> {
            return BobbleResult(Failure(error))
        }
    }
}