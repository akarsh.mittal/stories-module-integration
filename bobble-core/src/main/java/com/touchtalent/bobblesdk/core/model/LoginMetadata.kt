package com.touchtalent.bobblesdk.core.model

data class LoginMetadata(val accessToken:String?, val refreshToken:String?,val expiresIn:Long?,
                         val tokenType:String?,val phoneNumber:String?,val countryCode:Long?)
