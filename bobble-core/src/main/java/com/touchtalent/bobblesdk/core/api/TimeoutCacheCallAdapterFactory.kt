package com.touchtalent.bobblesdk.core.api

import com.touchtalent.bobblesdk.core.utils.FileLruCache
import retrofit2.Call
import retrofit2.CallAdapter
import retrofit2.Retrofit
import java.lang.reflect.ParameterizedType
import java.lang.reflect.Type

class TimeoutCacheCallAdapterFactory(
    private val parametersToCache: List<String>,
    val maxSize: Long,
    val cachePath: String,
) : CallAdapter.Factory() {

    private val fileLruCache by lazy { FileLruCache(maxSize, cachePath) }

    private var timeoutInMs: Long? = null

    fun setTimeout(timeoutInMs: Long) {
        this.timeoutInMs = timeoutInMs
    }

    override fun get(
        returnType: Type,
        annotations: Array<Annotation>,
        retrofit: Retrofit
    ): CallAdapter<*, *>? {
        if (getRawType(returnType) != Call::class.java) {
            return null
        }
        val responseType = getParameterUpperBound(0, returnType as ParameterizedType)
        val nonNullTimeoutInMs = timeoutInMs ?: return null
        val responseConverter = retrofit.responseBodyConverter<Any?>(responseType, annotations)
        val delegate = retrofit.nextCallAdapter(this, returnType, annotations)
        return object : CallAdapter<Any?, Call<Any?>> {
            override fun responseType(): Type {
                return delegate.responseType()
            }

            override fun adapt(call: Call<Any?>): Call<Any?> {
                return TimeoutCacheCall(
                    responseConverter,
                    responseType,
                    call,
                    nonNullTimeoutInMs,
                    parametersToCache,
                    fileLruCache
                )
            }
        }
    }
}