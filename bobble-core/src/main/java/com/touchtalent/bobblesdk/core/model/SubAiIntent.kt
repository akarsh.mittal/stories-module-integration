package com.touchtalent.bobblesdk.core.model

data class SubAiIntent(
    val category: String?,
    val subCategory: String?,
    val brands: List<String>,
    val items: List<String>,
    val confidence: Float
)