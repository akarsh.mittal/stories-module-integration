package com.touchtalent.bobblesdk.core.prefs

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringSetPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import com.touchtalent.bobblesdk.core.BobbleCoreSDK
import com.touchtalent.bobblesdk.core.utils.ResourceDownloader
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map

/**
 * Datastore to facilitate storing cache group keys and corresponding set of paths required for LRU cache
 * implementation in [ResourceDownloader]
 */
internal object ResourcesCacheDatastore {

    private val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = "resource_cache")

    /**
     * Creates dynamic keys for datastore based on [groupKey], stores a set of directory paths
     */
    private fun resourceCacheGroupMapKey(groupKey: String) =
        stringSetPreferencesKey("cache_group_$groupKey")

    suspend fun addPathToCacheGroup(groupKey: String, path: String) {
        if (!getCachePathMap(groupKey).contains(path)) {
            BobbleCoreSDK.applicationContext.dataStore.edit { cacheMap ->
                val cacheGroupMapKey = resourceCacheGroupMapKey(groupKey)
                val cacheGroupMap = cacheMap[cacheGroupMapKey] ?: emptySet()
                if (!cacheGroupMap.contains(path)) {
                    val mutableSet = cacheGroupMap.toMutableSet()
                    mutableSet.add(path)
                    cacheMap[cacheGroupMapKey] = mutableSet
                }
            }
        }
    }

    suspend fun getCachePathMap(key: String): Set<String> {
        return BobbleCoreSDK.applicationContext.dataStore.data
            .map { cacheGroupMap ->
                cacheGroupMap[resourceCacheGroupMapKey(key)] ?: emptySet()
            }.catch { emit(emptySet()) }
            .first()
    }

}