package com.touchtalent.bobblesdk.core.enums

enum class PermissionName {
    CONTACTS,
    LOCATION,
    NOTIFICATION,
   //Mock permission to refresh the carousel when user comes from settings.
    SETTINGS_PAGE
}