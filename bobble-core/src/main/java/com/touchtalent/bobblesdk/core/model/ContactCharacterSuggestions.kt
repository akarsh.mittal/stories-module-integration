package com.touchtalent.bobblesdk.core.model

data class ContactCharacterSuggestions(
    val name: String?,
    val source: String,
    val gender: String,
    val imageUrl: String,
    val serverId: Long,
    val title: String,
    val buttonText: String
)