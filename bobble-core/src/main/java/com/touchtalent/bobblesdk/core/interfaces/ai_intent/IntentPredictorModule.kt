package com.touchtalent.bobblesdk.core.interfaces.ai_intent

/**
 * Base interface for *intent-prediction* module for all interactions.
 * This module is designed to support loose coupling
 */
interface IntentPredictorModule {
    fun newAiIntentPredictor(): AiIntentEngine
}