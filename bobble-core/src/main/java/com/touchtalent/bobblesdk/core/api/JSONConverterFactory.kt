package com.touchtalent.bobblesdk.core.api

import okhttp3.RequestBody
import okhttp3.ResponseBody
import org.json.JSONArray
import org.json.JSONObject
import retrofit2.Converter
import retrofit2.Retrofit
import java.lang.reflect.Type

/**
 * Created by MarcinOz on 2016-04-21.
 * Copyright (C) 2016 OKE Poland Sp. z o.o. All rights reserved.
 */
class JSONConverterFactory private constructor() : Converter.Factory() {
    override fun requestBodyConverter(type: Type, parameterAnnotations: Array<Annotation>, methodAnnotations: Array<Annotation>, retrofit: Retrofit): Converter<*, RequestBody>? {
        return if (type === JSONObject::class.java || type === JSONArray::class.java) {
            JSONRequestBodyConverter.INSTANCE
        } else null
    }

    override fun responseBodyConverter(type: Type, annotations: Array<Annotation>, retrofit: Retrofit): Converter<ResponseBody, *>? {
        if (type === JSONObject::class.java) {
            return JSONResponseBodyConverters.JSONObjectResponseBodyConverter.INSTANCE
        }
        return if (type === JSONArray::class.java) {
            JSONResponseBodyConverters.JSONArrayResponseBodyConverter.INSTANCE
        } else null
    }

    companion object {
        fun create(): JSONConverterFactory {
            return JSONConverterFactory()
        }
    }
}