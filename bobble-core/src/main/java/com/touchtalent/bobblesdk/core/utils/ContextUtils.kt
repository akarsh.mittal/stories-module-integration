package com.touchtalent.bobblesdk.core.utils

import android.content.Context
import android.content.ContextWrapper
import android.content.res.Configuration
import android.view.ContextThemeWrapper
import android.view.LayoutInflater
import androidx.annotation.StyleRes
import androidx.appcompat.app.AppCompatActivity
import java.util.*

/**
 * Utility class to handle context related operations such as fetching locale from context,
 * modifying context as per theme, etc
 */
object ContextUtils {

    /**
     * Check whether the given [context] is configured for dark mode
     * @return true if [context] is in dark mode
     */
    @JvmStatic
    fun isDarkMode(context: Context): Boolean {
        return context.resources.configuration.uiMode and Configuration.UI_MODE_NIGHT_MASK == Configuration.UI_MODE_NIGHT_YES
    }

    /**
     * Resolve a map of (locale ->  item [T]) to a item [T] based on given context.
     * Following precedence is followed for resolving locale:
     * 1. Check if exact locale exists in the map
     * 2. Extract language from the locale and check if it exists
     * 3. Check if "default" locale exists
     * 4. Check if "en" locale exists
     * 5. return null
     * @return item [T] based on above precedence
     */
    fun <T> Map<String, T>.resolveLocale(context: Context): T? {
        val currentLocale = getLocale(context)
        this.forEach { entry ->
            if (entry.key.equals(currentLocale.toString(), ignoreCase = true))
                return entry.value
            if (entry.key.equals(currentLocale.language, ignoreCase = true))
                return entry.value
        }
        return this["default"] ?: this["en"]
    }

    /**
     * Resolve a map of (theme ->  item [T]) to a item [T] based on given context.
     * Following keys are supported - "dark", "light", "default"
     * @return item [T] based on the context - "dark" or "light". "default" would be used as a
     * fallback
     */
    fun <T> Map<String, T>.resolveTheme(context: Context): T? {
        val isDarkMode = isDarkMode(context)
        return this[if (isDarkMode) "dark" else "light"] ?: this["light"] ?: this["default"]
    }

    /**
     * Resolve a map of (theme ->  item [T]) to a item [T] based on given dark mode.
     * @param isDarkMode true if "dark" mode must be resolved
     * @return item [T] based on [isDarkMode] - "dark" if true, "light" otherwise.If "dark" or
     * "light" is missing, "default" would be used as a fallback
     */
    fun <T> Map<String, T>.resolveTheme(isDarkMode: Boolean): T? {
        return this[if (isDarkMode) "dark" else "light"] ?: this["light"] ?: this["default"]
    }

    /**
     * Fetch the locale from given [context]
     * @param context Context to fetch locale from
     * @return locale of the [context]
     */
    fun getLocale(context: Context): Locale {
        return context.resources.configuration.locale ?: Locale.getDefault()
    }

    /**
     * Check if the given context is in portrait orientation
     * @param context Context to be checked
     * @return true if context is in portrait mode
     */
    fun isOrientationPortrait(context: Context): Boolean {
        val orientation = context.resources.configuration.orientation
        return orientation != Configuration.ORIENTATION_LANDSCAPE
    }

    /**
     * Fetch the language from given [context]
     * @param context Context to fetch language from
     * @return language of the [context]
     */
    fun getLanguage(context: Context): String {
        return getLocale(context).language
    }

    /**
     * Change the theme of the context based on [isDarkTheme]
     * @param isDarkTheme Whether the context is required to be in dark mode
     * @return New context instance with modified UI
     */
    fun Context.changeTheme(isDarkTheme: Boolean) =
        Modifier(this).updateUiMode(isDarkTheme)
            .modify()
    /**
     * Change the language of the context based on [languageCode]
     * @param languageCode New Language code to be used
     * @return New context instance with modified UI
     */
    fun Context.changeLocale(locale: String) =
        Modifier(this).updateLocale(locale.toLocale() ?: Locale.ENGLISH)
            .modify()


    /**
     * Change the locale of the context based on [locale]
     * @param locale the current active locale is required to be used.
     * @return New context instance with Modified locale.
     */
    fun Context.changeLocale(locale: Locale) =
        Modifier(this).updateLocale(locale).modify()

    /**
     * Utility builder class to modify parameters of a context such as UI mode, locale, theme, etc
     * @param context Context to modify
     * @return New context instance with modified parameters
     */
    class Modifier(private val context: Context) {
        private var nightMode: Boolean? = null
        private var locale: Locale? = null

        @StyleRes
        private var theme: Int? = null

        /**
         * Change light/dark mode of the context
         * @param nightMode true if dark mode is required, false otherwise
         * @return Same [Modifier] instance for chaining
         */
        fun updateUiMode(nightMode: Boolean): Modifier {
            this.nightMode = nightMode
            return this
        }

        /**
         * Change locale of the context
         * @param locale new locale to set
         * @return Same [Modifier] instance for chaining
         */
        fun updateLocale(locale: Locale): Modifier {
            this.locale = locale
            return this
        }

        /**
         * Add a theme/style to the existing context
         * @param theme style resource of the theme to add
         * @return Same [Modifier] instance for chaining
         */
        fun updateTheme(@StyleRes theme: Int): Modifier {
            this.theme = theme
            return this
        }

        /**
         * Get the modified context after applying changes
         */
        fun modify(): Context {
            val config = Configuration(context.resources.configuration)
            nightMode?.let {
                val mask =
                    if (it) Configuration.UI_MODE_NIGHT_YES else Configuration.UI_MODE_NIGHT_NO
                config.uiMode = config.uiMode or Configuration.UI_MODE_NIGHT_MASK
                config.uiMode = config.uiMode and mask
            }
            locale?.let {
                config.setLocale(it)
            } ?: run {
                config.setLocale(context.resources.configuration.locale)
            }
            val modifiedContext = context.createConfigurationContext(config)
            theme?.let {
                return ContextThemeWrapper(modifiedContext, it)
            } ?: return modifiedContext
        }
    }
}

/**
 * Utility function to check if a given context is Landscape or not
 */
fun Context.isLandscape(): Boolean {
    val orientation = resources.configuration.orientation
    return orientation == Configuration.ORIENTATION_LANDSCAPE
}

/**
 * Utility function to find the base activity of the given context, if any.
 * Contexts might be modified and wrapped inside ContextWrapper, hence context is Activity checks
 * fail many a time, this function iterates over the baseContext until and unless the root context
 * is found
 * @return instance of activity if the root context is instance of Activity
 */
val Context.parentActivity: AppCompatActivity?
    get() {
        var activity: Context? = this
        while (activity is ContextWrapper && activity !is AppCompatActivity) {
            if ((this as ContextWrapper).baseContext == this)
                break
            activity = activity.baseContext
        }
        return if (activity is AppCompatActivity) activity else null
    }

/**
 * Utility function to extract [LayoutInflater] from a [Context]
 */
val Context.layoutInflater: LayoutInflater
    get() = LayoutInflater.from(this)
