package com.touchtalent.bobblesdk.core.moshi

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson

class DeeplinkLocalAdapter {

    @ToJson
    fun toJson(@Deeplink deeplink: DeeplinkUrl?): String? {
        return deeplink
    }

    @FromJson
    @Deeplink
    fun fromJson(deeplink: String?): DeeplinkUrl? {
        return deeplink
    }
}