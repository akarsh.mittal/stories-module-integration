package com.touchtalent.bobblesdk.core.moshi

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson
import com.touchtalent.bobblesdk.core.BobbleCoreSDK
import kotlinx.coroutines.launch

typealias DeeplinkUrl = String


class DeeplinkPreDownloadAdapter {

    @ToJson
    fun toJson(@Deeplink deeplink: DeeplinkUrl?): String? {
        return deeplink
    }

    @FromJson
    @Deeplink
    fun fromJson(deeplink: String?): DeeplinkUrl? {
        deeplink?.let {
            if (it.isNotEmpty()) {
                BobbleCoreSDK.applicationScope.launch {
                    BobbleCoreSDK.getAppController().getDeeplinkPrefetchInterface()
                        ?.preDownload(it)
                }
            }
        }
        return deeplink
    }
}