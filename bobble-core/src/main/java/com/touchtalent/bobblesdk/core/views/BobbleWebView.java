package com.touchtalent.bobblesdk.core.views;

import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;

import androidx.annotation.RequiresApi;

/**
 * WebView to be used in any project that consists of Keyboard.
 * Android system has a bug where a few IME APIs doesn't work (calling them cause ANRs) on a
 * WebView when the WebView and IME belong to the same process.
 * To enable using WebView alongside keyboards, this WebView must be used as it sets a specific IME
 * option which directs the IME to avoid use of buggy APIs.
 * <p>
 * The IME must also check for privateImeOptions = BobbleBuggyEditText and disable all
 * {@link InputMethodManager}'s IPC calls
 */
public class BobbleWebView extends WebView {

    public BobbleWebView(Context context) {
        super(context);
    }

    public BobbleWebView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BobbleWebView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public BobbleWebView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public BobbleWebView(Context context, AttributeSet attrs, int defStyleAttr, boolean privateBrowsing) {
        super(context, attrs, defStyleAttr, privateBrowsing);
    }

    @Override
    public InputConnection onCreateInputConnection(EditorInfo outAttrs) {
        outAttrs.privateImeOptions = "BobbleBuggyEditText";
        return super.onCreateInputConnection(outAttrs);
    }
}

