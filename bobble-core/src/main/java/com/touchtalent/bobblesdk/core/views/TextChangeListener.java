package com.touchtalent.bobblesdk.core.views;


import android.widget.TextView;

public interface TextChangeListener {
    void onTextChange(CharSequence text, TextView.BufferType type);
}
