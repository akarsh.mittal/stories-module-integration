package com.touchtalent.bobblesdk.core.executor;

/**
 * Utility class for thread management throughout the app. Direct access to this class to be
 * deprecated soon in favour of coroutines dispatchers.
 */

public class BobbleCore {

    private static BobbleCore sInstance = null;
    private final ExecutorSupplier mExecutorSupplier;

    private BobbleCore() {
        this.mExecutorSupplier = new DefaultExecutorSupplier();
    }

    public static BobbleCore getInstance() {
        if (sInstance == null) {
            synchronized (BobbleCore.class) {
                if (sInstance == null) {
                    sInstance = new BobbleCore();
                }
            }
        }
        return sInstance;
    }

    /**
     * Get supplier class which provides executors for different use-cases
     *
     * @return Executor supplier
     */
    public ExecutorSupplier getExecutorSupplier() {
        return mExecutorSupplier;
    }

}