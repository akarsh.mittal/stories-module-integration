package com.touchtalent.bobblesdk.core.utils

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestBuilder
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.target.Target
import com.bumptech.glide.request.transition.Transition
import com.touchtalent.bobblesdk.core.interfaces.GlideRequestListener
import kotlinx.coroutines.CancellableContinuation
import kotlinx.coroutines.suspendCancellableCoroutine
import java.io.File
import kotlin.coroutines.resume

/**
 * Utility function to render a given bitmap on the ImageView using Glide.
 *
 * [bitmap] Bitmap to render
 * [memoryCache] true if memory cache needs to be enabled, disabled by default
 */
fun ImageView.renderBitmap(
    bitmap: Bitmap,
    memoryCache: Boolean = false,
    requestBuilder: ((RequestBuilder<Drawable>) -> RequestBuilder<Drawable>)? = null
) {
    var request = Glide.with(this)
        .load(bitmap)
        .skipMemoryCache(!memoryCache)

    requestBuilder?.let {
        request = it(request)
    }

    request.into(this)
}

/**
 * Utility function to load url and give callback
 * [url] Url to render
 * returns [Boolean] whether success or failure
 */
suspend fun ImageView.loadImageUrlAsync(url: String?): Boolean =
    suspendCancellableCoroutine {
        Glide.with(context)
            .load(url)
            .listener(object : GlideRequestListener() {
                override fun onComplete(success: Boolean) {
                    it.resumeIfActive(success)
                }
            })
            .into(this)
    }


fun ImageView.renderBitmapFit(bitmap: Bitmap, memoryCache: Boolean = false) {
    this.scaleType = ImageView.ScaleType.FIT_XY
    Glide.with(this)
        .load(bitmap)
        .skipMemoryCache(!memoryCache)
        .into(this)
}

/**
 * Utility function to render any object (String, Bitmap, File, etc) on the ImageView using Glide.
 *
 * [any] Object to render
 * [memoryCache] true if memory cache needs to be enabled, disabled by default
 * [storageCache] true if disk cache needs to be enabled, enabled by default
 */
fun ImageView.renderAny(any: Any, memoryCache: Boolean = false, storageCache: Boolean = true) {
    Glide.with(this)
        .load(any)
        .diskCacheStrategy(if (storageCache) DiskCacheStrategy.ALL else DiskCacheStrategy.NONE)
        .skipMemoryCache(!memoryCache)
        .into(this)
}

/**
 * Utility function to render a drawable on the ImageView using Glide.
 *
 * [drawable] Drawable to render */
fun ImageView.renderDrawable(drawable: Drawable) {
    if (GeneralUtils.isValidContextForGlide(context))
        Glide.with(this)
            .load(drawable)
            .into(this)
}

/**
 * Utility function to render a URL on the ImageView using Glide.
 *
 * [url] Drawable to render */
fun ImageView.renderUrl(
    url: String, requestBuilder: ((RequestBuilder<Drawable>) -> RequestBuilder<Drawable>)? = null
) {
    var request = Glide.with(this)
        .load(url)
    requestBuilder?.let {
        request = it(request)
    }
    request.into(this)
}

fun ImageView.renderUrlFit(url: String) {
    this.scaleType = ImageView.ScaleType.FIT_XY
    if (GeneralUtils.isValidContextForGlide(context))
        Glide.with(this)
            .load(url)
            .into(this)
}

/**
 * Utility function to render a given drawable on the ImageView using Glide with rounded corners
 *
 * [drawable] Drawable to render
 */
fun ImageView.renderRoundedDrawable(drawable: Drawable?) {
    Glide.with(this)
        .load(drawable)
        .circleCrop()
        .into(this)
}

/**
 * Utility function to render a Bitmap on a ImageView and wait until the rendering is success/failure.
 * Rendering happens on Glide's own thread and the calling thread is suspended until Glide response
 * [bitmap] Bitmap to render
 * [memoryCache] true if memory cache needs to be enabled, false by default
 *
 * @return true if rendering was successful, false otherwise
 */
suspend fun ImageView.renderBitmapSync(bitmap: Bitmap, memoryCache: Boolean = false): Boolean =
    suspendCancellableCoroutine {
        Glide.with(this)
            .load(bitmap)
            .skipMemoryCache(memoryCache)
            .listener(object : GlideRequestListener() {
                override fun onComplete(success: Boolean) {
                    it.resumeIfActive(success)
                }
            })
            .into(this)
    }

suspend fun Context.downloadBitmapSync(url: String?, memoryCache: Boolean = false, size : Int? = null): Bitmap? =
    suspendCancellableCoroutine {
        val customTarget = object : CustomTarget<Bitmap>() {
            override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                it.resumeIfActive(resource)
            }

            override fun onLoadCleared(placeholder: Drawable?) {
                it.resumeIfActive(null)
            }
        }
        Glide.with(this)
            .asBitmap()
            .load(url)
            .override(size ?: Target.SIZE_ORIGINAL)
            .skipMemoryCache(memoryCache)
            .into(customTarget)

        it.invokeOnCancellation {
            Glide.with(this).clear(customTarget)
        }
    }

suspend fun Context.downloadImageAsFileSync(url: String?): File =
    suspendCancellableCoroutine {
        val customTarget = object : CustomTarget<File>() {
            override fun onResourceReady(resource: File, transition: Transition<in File>?) {
                it.resumeIfActive(resource)
            }

            override fun onLoadCleared(placeholder: Drawable?) {
                it.cancel(Exception("Failed to load"))
            }
        }
        Glide.with(this)
            .asFile()
            .load(url)
            .into(customTarget)

        it.invokeOnCancellation {
            Glide.with(this).clear(customTarget)
        }
    }

suspend fun Context.loadDrawableSyncResult(
    url: String?,
    memoryCache: Boolean = false,
    requestBuilder: ((RequestBuilder<Drawable>) -> RequestBuilder<Drawable>)? = null
): Result<Drawable> =
    suspendCancellableCoroutine {
        val customTarget = object : CustomTarget<Drawable>() {
            override fun onResourceReady(resource: Drawable, transition: Transition<in Drawable>?) {
                it.resumeIfActive(Result.success(resource))
            }

            override fun onLoadCleared(placeholder: Drawable?) {
                it.resumeIfActive(Result.failure(Exception()))
            }

            override fun onLoadFailed(errorDrawable: Drawable?) {
                super.onLoadFailed(errorDrawable)
                errorDrawable?.let { drawable ->
                    it.resumeIfActive(Result.success(drawable))
                } ?: run {
                    it.resumeIfActive(Result.failure(Exception()))
                }

            }
        }
        var request = Glide.with(this)
            .load(url)
            .skipMemoryCache(memoryCache)
        requestBuilder?.let { request = it(request) }
        request.into(customTarget)

        it.invokeOnCancellation {
            Glide.with(this).clear(customTarget)
        }
    }

fun Context.preloadImage(url: String?) {
    if (GeneralUtils.isValidContextForGlide(this))
        Glide.with(this)
            .load(url)
            .preload()
}


fun <T> CancellableContinuation<T>.resumeIfActive(value: T) {
    if (isActive)
        resume(value)
}