package com.touchtalent.bobblesdk.core.interfaces

import java.io.File

interface EncryptionManager {
    fun encrypt(filePath: File): Result<ByteArray>
    fun encrypt(data: String): Result<ByteArray>
    fun encrypt(byteArray: ByteArray): Result<ByteArray>
}