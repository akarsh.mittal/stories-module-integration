package com.touchtalent.bobblesdk.core.api

import okhttp3.Interceptor
import okhttp3.Response

/**
 * Utility class to add common params via OkHttp interceptor.
 * Takes in a HashMap provider as input, which is used to fill the params wherever this interceptor
 * is used
 */
class ParamsAdderInterceptor(val hashmapGetter: () -> HashMap<String, String>) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val original = chain.request()
        val originalHttpUrl = original.url()
        val urlBuilder = originalHttpUrl.newBuilder()
        val queryParams = hashmapGetter()
        for ((key, value) in queryParams) {
            urlBuilder.addQueryParameter(key, value)
        }
        val url = urlBuilder.build()

        // Request customization: add request headers
        val requestBuilder = original.newBuilder()
            .url(url)
        val request = requestBuilder.build()
        return chain.proceed(request)
    }
}