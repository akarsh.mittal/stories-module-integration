package com.touchtalent.bobblesdk.core.interfaces.head

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import com.touchtalent.bobblesdk.core.enums.KeyboardDeepLink
import com.touchtalent.bobblesdk.core.interfaces.head.BobbleHeadCreator.Builder

/**
 * Interface to initiated head creation activity (uses builder pattern).
 * @see Builder
 */
abstract class BobbleHeadCreator {

    /**
     * Builder to initiate head creation activity. Use functions [startActivity],
     * [startActivityForResult] or [startActivityForResult] to launch the activity.
     * @property context Context whose locale would be used for the activity
     * @property bundle Bundle containing deeplink params, if any. E.g - source and field id could
     * be set if triggered from keyboard for deep linking to keyboard on close
     * @property screenName Screen name where it is being triggered from, used for events
     * @property keyboardDeepLink Keyboard deeplink to trigger after activity is closed,
     * if [isKeyboardView] is true
     * @property isKeyboardView true if triggered from keyboard
     * @property initialSource [InitialSource] for a head creation activity - camera or gallery
     */
    abstract class Builder(protected val context: Context) {
        protected var bundle: Bundle? = null
        protected var screenName: String? = null

        @KeyboardDeepLink
        protected var keyboardDeepLink: Int? = null
        protected var isKeyboardView = false
        protected var initialSource = InitialSource.CAMERA

        /**
         * Add Bundle containing deeplink params, if any. E.g - source and field id could
         */
        fun withAdditionalBundle(bundle: Bundle) = apply {
            this.bundle = bundle
        }

        /**
         * Set whether triggered from keyboard
         */
        fun setKeyboardView(isKeyboardView: Boolean) = apply {
            this.isKeyboardView = isKeyboardView
        }

        /**
         * Set screen name
         */
        fun setScreenName(screenName: String) = apply {
            this.screenName = screenName
        }

        /**
         * Set keyboard deeplink
         */
        fun setKeyboardDeepLink(@KeyboardDeepLink keyboardDeepLink: Int?) = apply {
            this.keyboardDeepLink = keyboardDeepLink
        }

        /**
         * Sets initial source of the head creation process
         */
        fun setInitialSource(initialSource: InitialSource) = apply {
            this.initialSource = initialSource
        }

        /**
         * Start head creation activity
         */
        abstract fun startActivity()

        /**
         * Start head creation activity with result to listen for head creation success.
         * [Activity.onActivityResult] is triggered with result = [Activity.RESULT_OK] upon
         * successful head creation
         * @param fragment Fragment to receive result callback in.
         * @param requestCode Request code for activity result callback
         */
        abstract fun startActivityForResult(fragment: Fragment, requestCode: Int)

        /**
         * Start head creation activity with result to listen for head creation success.
         * [Activity.onActivityResult] is triggered with result = [Activity.RESULT_OK] upon
         * successful head creation
         * @param activity Activity to receive result callback in.
         * @param requestCode Request code for activity result callback
         */
        abstract fun startActivityForResult(activity: Activity, requestCode: Int)

        /**
         * Get intent to start head creation activity.
         * @return Intent to launch head creation activity.
         */
        abstract fun getIntent(): Intent
    }

    /**
     * @return a new [Builder] instance
     *
     */
    abstract fun newBuilder(context: Context): Builder

    /**
     * Initial source to open head creation activity
     */
    enum class InitialSource {
        CAMERA,
        GALLERY
    }

}