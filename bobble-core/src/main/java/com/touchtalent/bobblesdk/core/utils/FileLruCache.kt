package com.touchtalent.bobblesdk.core.utils

import java.io.File

/**
 * Creates a disk LRU cache on the given [path] with max cache size [maxSize]. [get] and [put] are
 * used to fetch the data and set the data respectively. While setting the data, cache is purged to [maxSize].
 *
 * Each cache has its own cache key and is required for setting/putting the data
 */
class FileLruCache(val maxSize: Long, val path: String) {

    companion object {
        const val META_DATA_EXTENSION = ".0"
        const val FILE_EXTENSION = ".1"
    }

    internal data class CacheEntry(val filePath: String, val lastAccessedTime: Long)

    var size = FileUtil.size(path)

    /**
     * Returns [ByteArray] of the content corresponding to the given [cacheKey]
     * @return null if cache not present, [ByteArray] of the contents, if found
     */
    fun get(cacheKey: String): ByteArray? {
        val cachePath = cachePath(cacheKey)
        val cacheFile = cachePath + FILE_EXTENSION
        if (!FileUtil.exists(cacheFile)) return null
        return kotlin.runCatching {
            val bytes = FileUtil.readAllBytes(cacheFile)
            val metadataFile = cachePath + META_DATA_EXTENSION
            FileUtil.write(metadataFile, System.currentTimeMillis().toString())
            bytes
        }.onFailure {
            BLog.printStackTrace(it)
        }.getOrNull()
    }

    /**
     * Puts the given [data] into the cache with cacheKey = [cacheKey]
     * Cache is purged, if cache size exceeds [maxSize] after putting new data
     */
    fun put(cacheKey: String, data: String) = kotlin.runCatching {
        val cachePath = cachePath(cacheKey)
        val filePath = cachePath + FILE_EXTENSION
        val metadataPath = cachePath + META_DATA_EXTENSION
        var existingSize = 0L
        // Since existing file would be overridden, its size must be subtracted from total size
        if (FileUtil.exists(filePath))
            existingSize += FileUtil.size(filePath)
        FileUtil.write(filePath, data)
        // Since existing file would be overridden, its size must be subtracted from total size
        if (FileUtil.exists(metadataPath))
            existingSize += FileUtil.size(metadataPath)
        FileUtil.write(metadataPath, System.currentTimeMillis().toString())
        size += FileUtil.size(filePath) + FileUtil.size(metadataPath) - existingSize
        checkSize()
    }

    private fun checkSize() {
        if (size <= maxSize)
            return
        val files = File(path).listFiles() ?: return
        val fileMap = mutableListOf<CacheEntry>()
        files.forEach internalForEach@{
            if (it.name.endsWith(META_DATA_EXTENSION)) {
                val lastAccessedTime =
                    FileUtil.readFile(it.absolutePath)?.toLongOrNull()
                        ?: return@internalForEach
                val originalFile =
                    it.absolutePath.removeSuffix(META_DATA_EXTENSION)
                fileMap.add(CacheEntry(originalFile, lastAccessedTime))
            }
        }
        fileMap.sortBy { it.lastAccessedTime }
        if (fileMap.isNotEmpty()) {
            while (size > maxSize) {
                val lastAccessedItem = fileMap.removeLast()
                val filePathToDelete = lastAccessedItem.filePath + FILE_EXTENSION
                val fileMetaData = lastAccessedItem.filePath + META_DATA_EXTENSION
                size -= FileUtil.size(filePathToDelete) + FileUtil.size(fileMetaData)
                FileUtil.delete(filePathToDelete)
                FileUtil.delete(fileMetaData)
            }
        }
    }

    private fun cachePath(cacheKey: String): String {
        return FileUtil.join(path, cacheKey)
    }
}