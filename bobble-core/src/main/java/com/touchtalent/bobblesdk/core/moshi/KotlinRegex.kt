package com.touchtalent.bobblesdk.core.moshi

import com.squareup.moshi.JsonQualifier

/**
 * Denotes that the annotated element represents a regex pattern
 */
@Retention(AnnotationRetention.RUNTIME)
@JsonQualifier
annotation class KotlinRegex