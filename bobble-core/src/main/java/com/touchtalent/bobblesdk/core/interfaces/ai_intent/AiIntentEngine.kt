package com.touchtalent.bobblesdk.core.interfaces.ai_intent

import com.touchtalent.bobblesdk.core.model.AiIntent

interface AiIntentEngine {
    fun start(folderPath: String): Boolean
    fun process(inputText: String): List<AiIntent>
    fun close()
}