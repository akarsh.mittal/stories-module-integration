package com.touchtalent.bobblesdk.core.utils;


import android.content.ContentValues;
import android.content.Context;
import android.content.res.AssetManager;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.webkit.MimeTypeMap;

import androidx.annotation.Nullable;
import androidx.annotation.RawRes;

import com.touchtalent.bobblesdk.core.BobbleCoreSDK;

import org.jetbrains.annotations.NotNull;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.security.MessageDigest;

/**
 * Utility class to handle files
 */
public class FileUtil {

    public static final String TAG = FileUtil.class.getSimpleName();

    private static final int IO_BUFFER_SIZE = 16384;

    private FileUtil() {
        // This class is not publicly instantiable.
    }

    /**
     * Join multiple paths together with {@link File#separator}
     *
     * @param paths Paths to join together
     * @return the final path after joining
     */
    public static String join(Object... paths) {
        if (paths != null && paths.length > 0) {
            StringBuilder joinedPath = new StringBuilder(paths[0].toString());
            for (int i = 1; i < paths.length; i++) {
                joinedPath.append(File.separator);
                joinedPath.append(paths[i]);
            }
            return joinedPath.toString();
        } else return "";
    }

    /**
     * Create a new file. Parent folders will be recursively created if not-found.
     *
     * @param absPath Absolute path of the file
     * @return true if file creation succeeded, false otherwise
     */
    public static boolean create(String absPath) {
        return create(absPath, false);
    }

    /**
     * Create a new file. Parent folders will be recursively created if not-found.
     *
     * @param absPath Absolute path of the file
     * @param force   Force delete any conflicting file with same name, if any
     * @return true if file creation succeeded, false otherwise
     */
    public static boolean create(String absPath, boolean force) {
        if (TextUtils.isEmpty(absPath)) {
            return false;
        }

        if (exists(absPath)) {
            return true;
        }

        String parentPath = getParent(absPath);
        mkdirs(parentPath, force);

        try {
            File file = new File(absPath);
            return file.createNewFile();
        } catch (Exception e) {
            BLog.printStackTrace(e);
        }
        return false;
    }

    /**
     * Create a new folder. Parent folders will be recursively created if not-found.
     *
     * @param absPath Absolute path of the folder
     * @return true if folder creation succeeded, false otherwise
     */
    public static boolean mkdirs(String absPath) {
        return mkdirs(absPath, false);
    }

    /**
     * Create a new folder. Parent folders will be recursively created if not-found.
     *
     * @param absPath Absolute path of the folder
     * @param force   Force delete any conflicting file with same name, if any
     * @return true if folder creation succeeded, false otherwise
     */
    public static boolean mkdirs(String absPath, boolean force) {
        File file = new File(absPath);
        if (exists(absPath) && !isFolder(absPath)) {
            if (!force) {
                return false;
            } else {
                delete(file);
            }
        }
        try {
            file.mkdirs();
        } catch (Exception e) {
            BLog.printStackTrace(e);
        }
        return exists(file);
    }

    /**
     * Move a file from a source path to a destination path.
     *
     * @param srcPath Source path of the file
     * @param dstPath Destination path of the file
     * @return true if move succeeded, false otherwise
     */
    public static boolean move(String srcPath, String dstPath) {
        return move(srcPath, dstPath, false);
    }

    /**
     * Move a file from a source path to a destination path.
     *
     * @param srcPath Source path of the file
     * @param dstPath Destination path of the file
     * @param force   Force delete any conflicting file with same target file name, if any
     * @return true if move succeeded, false otherwise
     */
    public static boolean move(String srcPath, String dstPath, boolean force) {
        if (TextUtils.isEmpty(srcPath) || TextUtils.isEmpty(dstPath)) {
            return false;
        }

        if (!exists(srcPath)) {
            BLog.d("LanguageResourceDownloader", srcPath + " does not exist");
            return false;
        }

        if (exists(dstPath)) {
            if (!force) {
                return false;
            } else {
                delete(dstPath);
            }
        }

        try {
            File srcFile = new File(srcPath);
            File dstFile = new File(dstPath);
            return srcFile.renameTo(dstFile);
        } catch (Exception e) {
            BLog.printStackTrace(e);
        }
        return false;
    }

    /**
     * Delete a file or a folder. Folders will be deleted recursively.
     *
     * @param absPath Absolute path of the file/folder to be deleted
     * @return true if delete succeeded, false otherwise
     */
    public static boolean delete(String absPath) {
        if (TextUtils.isEmpty(absPath)) {
            return false;
        }

        File file = new File(absPath);
        return delete(file);
    }

    /**
     * Delete a file or a folder but retain a specific file/folder. Folders will be deleted recursively.
     *
     * @param absPath       Absolute path of the file/folder to be deleted
     * @param toPersistFile Absolute path of the file/folder to be retained
     * @return true if delete succeeded, false otherwise
     */
    public static boolean delete(String absPath, String toPersistFile) {
        if (TextUtils.isEmpty(absPath)) {
            return false;
        }

        File file = new File(absPath);
        return delete(file, toPersistFile);
    }

    /**
     * Delete a file or a folder. Folders will be deleted recursively.
     *
     * @param file file/folder to be deleted
     * @return true if delete succeeded, false otherwise
     */
    public static boolean delete(File file) {
        if (!exists(file)) {
            return true;
        }

        if (file.isFile()) {
            return file.delete();
        }

        boolean result = true;
        File[] files = file.listFiles();
        if (files == null) return false;
        for (int index = 0; index < files.length; index++) {
            result |= delete(files[index]);
        }
        result |= file.delete();

        return result;
    }

    /**
     * Delete a file or a folder but retain a specific file/folder. Folders will be deleted recursively.
     *
     * @param file          file/folder to be deleted
     * @param toPersistFile Absolute path of the file/folder to be retained
     * @return true if delete succeeded, false otherwise
     */
    public static boolean delete(File file, String toPersistFile) {
        if (!exists(file)) {
            return true;
        }

        if (file.isFile()) {
            return file.delete();
        }

        boolean result = true;
        File[] files = file.listFiles();
        if (files == null) return false;
        for (File value : files) {
            if (toPersistFile != null
                    && value.getAbsolutePath().equalsIgnoreCase(toPersistFile)) {
                continue;
            }
            result |= delete(value, toPersistFile);
        }

        return result;
    }

    /**
     * Check if the given path exists.
     *
     * @param absPath Absolute path of the file/folder to be searched
     * @return true if the file/folder exists, false otherwise
     */
    public static boolean exists(String absPath) {
        if (TextUtils.isEmpty(absPath)) {
            return false;
        }
        File file = new File(absPath);
        return exists(file);
    }

    /**
     * Check if the given path exists.
     *
     * @param file file/folder to be searched
     * @return true if the file/folder exists, false otherwise
     */
    public static boolean exists(File file) {
        return file != null && file.exists();
    }

    /**
     * Checks if the given childPath is a child of the parentPath
     *
     * @param childPath  Child path to be checked
     * @param parentPath Parent path to be checked against
     * @return true if childPath is a child of the parentPath, false otherwise
     */
    public static boolean childOf(String childPath, String parentPath) {
        if (TextUtils.isEmpty(childPath) || TextUtils.isEmpty(parentPath)) {
            return false;
        }
        childPath = cleanPath(childPath);
        parentPath = cleanPath(parentPath);
        return childPath.startsWith(parentPath + File.separator);
    }

    /**
     * Counts the number of file/folder in a given folder
     *
     * @param absPath Path to check
     * @return number of files/folder in the given absPath
     */
    public static int childCount(String absPath) {
        if (!exists(absPath)) {
            return 0;
        }
        File file = new File(absPath);
        File[] children = file.listFiles();
        if (children == null || children.length == 0) {
            return 0;
        }
        return children.length;
    }

    /**
     * Converts the given path into a standardised format - case, symbolic links, etc
     *
     * @param absPath Absolute path to be standardised
     * @return clean path of the given absPath
     */
    public static String cleanPath(String absPath) {
        if (TextUtils.isEmpty(absPath)) {
            return absPath;
        }
        try {
            File file = new File(absPath);
            absPath = file.getCanonicalPath();
        } catch (Exception e) {

        }
        return absPath;
    }

    /**
     * Evaluates the size, in bytes, of the given file/folder
     *
     * @param absPath Absolute path whose size needs to be known
     * @return size of the absPath
     */
    public static long size(String absPath) {
        if (absPath == null) {
            return 0;
        }
        File file = new File(absPath);
        return size(file);
    }

    /**
     * Evaluates the size, in bytes, of the given file/folder
     *
     * @param file file whose size needs to be known
     * @return size of the absPath
     */
    public static long size(File file) {
        if (!exists(file)) {
            return 0;
        }

        long length = 0;
        if (isFile(file)) {
            length = file.length();
            return length;
        }

        // Folders too consume space on disk for storing metadata, such as name, child, etc
        length += file.length();

        File[] files = file.listFiles();
        if (files == null || files.length == 0) {
            return length;
        }

        int size = files.length;
        for (int index = 0; index < size; index++) {
            File child = files[index];
            length += size(child);
        }
        return length;
    }

    /**
     * Copy a file from a source path to a destination path.
     *
     * @param srcPath Source path of the file
     * @param dstPath Destination path of the file
     * @return true if copy succeeded, false otherwise
     */
    public static boolean copy(String srcPath, String dstPath) {
        return copy(srcPath, dstPath, false);
    }

    /**
     * Copy a file from a source path to a destination path.
     *
     * @param srcPath Source path of the file
     * @param dstPath Destination path of the file
     * @param force   Force delete any conflicting file with same target file name, if any
     * @return true if copy succeeded, false otherwise
     */
    public static boolean copy(String srcPath, String dstPath, boolean force) {
        if (TextUtils.isEmpty(srcPath) || TextUtils.isEmpty(dstPath)) {
            return false;
        }

        // check if copy source equals destination
        if (srcPath.equals(dstPath)) {
            return true;
        }

        // check if source file exists or is a directory
        if (!exists(srcPath) || !isFile(srcPath)) {
            return false;
        }

        // delete old content
        if (exists(dstPath)) {
            if (!force) {
                return false;
            } else {
                delete(dstPath);
            }
        }
        if (!create(dstPath)) {
            return false;
        }

        FileInputStream in = null;
        FileOutputStream out = null;

        // get streams
        try {
            in = new FileInputStream(srcPath);
            out = new FileOutputStream(dstPath);
        } catch (Exception e) {
            BLog.printStackTrace(e);
            return false;
        }
        return copy(in, out);
    }

    /**
     * Copy data from a {@link InputStream} to a {@link OutputStream}
     *
     * @param is InputStream to copy data from
     * @param os OutputStream to copy data to
     * @return true if copy succeeded, false otherwise
     */
    public static boolean copy(InputStream is, OutputStream os) {
        if (is == null || os == null) {
            return false;
        }

        try {
            byte[] buffer = new byte[IO_BUFFER_SIZE];
            int length;
            while ((length = is.read(buffer)) != -1) {
                os.write(buffer, 0, length);
            }
            os.flush();
        } catch (Exception e) {
            BLog.printStackTrace(e);
            return false;
        } finally {
            try {
                is.close();
            } catch (Exception ignore) {
            }
            try {
                os.close();
            } catch (Exception ignore) {

            }
        }
        return true;
    }

    /**
     * Check if the given path is a file
     *
     * @param absPath Absolute path to check
     * @return true if given path exists and is a file, false otherwise
     */
    public static boolean isFile(String absPath) {
        boolean exists = exists(absPath);
        if (!exists) {
            return false;
        }

        File file = new File(absPath);
        return isFile(file);
    }

    /**
     * Check if the given File object is a file
     *
     * @param file file to check
     * @return true if given file exists and is a file, false otherwise
     */
    public static boolean isFile(File file) {
        return file != null && file.isFile();
    }

    /**
     * Check if the given path is a folder
     *
     * @param absPath Absolute path to check
     * @return true if given path exists and is a folder, false otherwise
     */
    public static boolean isFolder(String absPath) {
        boolean exists = exists(absPath);
        if (!exists) {
            return false;
        }

        File file = new File(absPath);
        return file.isDirectory();
    }


    /**
     * Get name of the file/folder
     *
     * @param file File to get name
     * @return Name of the file
     */
    @Nullable
    public final static String getName(File file) {
        return file == null ? null : getName(file.getAbsolutePath());
    }

    /**
     * Get name of the file/folder
     *
     * @param absPath Absolute path of the file to get name
     * @return Name of the file
     */
    @Nullable
    public final static String getName(String absPath) {
        if (TextUtils.isEmpty(absPath)) {
            return absPath;
        }

        String fileName = null;
        int index = absPath.lastIndexOf("/");
        if (index >= 0 && index < (absPath.length() - 1)) {
            fileName = absPath.substring(index + 1);
        }
        return fileName;
    }

    /**
     * Get parent folder of the given file/folder
     *
     * @param file File to get parent folder of
     * @return Absolute path of the parent folder
     */
    @Nullable
    public static String getParent(@Nullable File file) {
        return file == null ? null : file.getParent();
    }

    /**
     * Get parent folder of the given file/folder
     *
     * @param absPath Absolute path of the file to get parent folder of
     * @return Absolute path of the parent folder
     */
    @Nullable
    public static String getParent(String absPath) {
        if (TextUtils.isEmpty(absPath)) {
            return null;
        }
        absPath = cleanPath(absPath);
        File file = new File(absPath);
        return getParent(file);
    }

    /**
     * Remove extension from the given fileName
     *
     * @param fileName Name of the file
     * @return Name of the file without extension
     */
    public static String removeExtension(String fileName) {
        if (fileName.contains(".")) {
            return fileName.substring(0, fileName.lastIndexOf('.'));
        } else {
            return fileName;
        }
    }

    /**
     * Remove extension from the given file
     *
     * @param file File to remove extension of
     * @return Name of the file without extension
     */
    public static String getExtension(File file) {
        return file == null ? null : getExtension(file.getName());
    }

    /**
     * Get the extension of the given fileName
     *
     * @param fileName File to get extension of
     * @return Extension of the given file
     */
    public static String getExtension(String fileName) {
        if (TextUtils.isEmpty(fileName)) {
            return "";
        }

        int index = fileName.lastIndexOf('.');
        if (index < 0 || index >= (fileName.length() - 1)) {
            return "";
        }
        return fileName.substring(index + 1);
    }

    /**
     * Get MIME type of the given file
     *
     * @param file File to get MIME type of
     * @return MIME type of the file
     */
    public static String getMimeType(File file) {
        if (file == null) {
            return "*/*";
        }
        String fileName = file.getName();
        return getMimeType(fileName);
    }

    /**
     * Get MIME type of the given file
     *
     * @param fileName File name to get MIME type of
     * @return MIME type of the file
     */
    public static String getMimeType(String fileName) {
        if (TextUtils.isEmpty(fileName)) {
            return "*/*";
        }
        String extension = getExtension(fileName);
        MimeTypeMap map = MimeTypeMap.getSingleton();
        String type = map.getMimeTypeFromExtension(extension);
        if (TextUtils.isEmpty(type)) {
            return "*/*";
        } else {
            return type;
        }
    }

    /**
     * Get SHA-1 of the file
     *
     * @param absPath Absolute path of the file
     * @return SHA-1 of the file, null if error occurred
     */
    @Nullable
    public static String fileSHA1(String absPath) {
        if (TextUtils.isEmpty(absPath)) {
            return null;
        }
        File file = new File(absPath);

        if (!file.exists() || file.isDirectory()) {
            return null;
        }
        String fileSHA1 = null;
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(file);
        } catch (FileNotFoundException e1) {
            BLog.printStackTrace(e1);
            return null;
        }
        MessageDigest messageDigest;
        try {
            messageDigest = MessageDigest.getInstance("SHA-1");
            byte[] buffer = new byte[IO_BUFFER_SIZE];
            int length = 0;
            while ((length = fis.read(buffer)) > 0) {
                messageDigest.update(buffer, 0, length);
            }
            fis.close();
            fileSHA1 = SecurityUtil.bytes2Hex(messageDigest.digest());
        } catch (Exception e) {
            BLog.printStackTrace(e);
        } finally {
            try {
                fis.close();
            } catch (IOException e) {
                BLog.printStackTrace(e);
            }
        }
        if (!TextUtils.isEmpty(fileSHA1)) {
            fileSHA1 = fileSHA1 != null ? fileSHA1.trim() : null;
        }
        return fileSHA1;
    }

    /**
     * Get MD5 of the file
     *
     * @param absPath Absolute path of the file
     * @return MD5 of the file, null if error occurred
     */
    @Nullable
    public static String fileMD5(String absPath) {
        if (TextUtils.isEmpty(absPath)) {
            return null;
        }
        File file = new File(absPath);
        if (!file.exists() || file.isDirectory()) {
            return null;
        }
        String fileMD5 = null;
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(file);
        } catch (FileNotFoundException e1) {
            BLog.printStackTrace(e1);
            return null;
        }
        MessageDigest messageDigest;
        try {
            messageDigest = MessageDigest.getInstance("MD5");
            byte[] buffer = new byte[IO_BUFFER_SIZE];
            int length = 0;
            while ((length = fis.read(buffer)) > 0) {
                messageDigest.update(buffer, 0, length);
            }
            fis.close();
            fileMD5 = SecurityUtil.bytes2Hex(messageDigest.digest());
        } catch (Exception e) {
            BLog.printStackTrace(e);
        } finally {
            try {
                fis.close();
            } catch (IOException e) {
                BLog.printStackTrace(e);
            }
        }
        if (!TextUtils.isEmpty(fileMD5)) {
            fileMD5 = fileMD5 != null ? fileMD5.trim() : null;
        }
        return fileMD5;
    }

    /**
     * Write a given text to specified file - absPath
     *
     * @param absPath Absolute path of the file to write to
     * @param text    Text to be written
     * @return true, if write operation succeeded
     */
    public static final boolean write(String absPath, String text) {
        if (!create(absPath, true)) {
            return false;
        }

        FileOutputStream fos = null;
        PrintWriter pw = null;
        try {
            fos = new FileOutputStream(absPath);
            pw = new PrintWriter(fos);
            pw.write(text);
            pw.flush();
        } catch (Exception e) {

        } finally {
            CloseUtil.close(pw);
            CloseUtil.close(fos);
        }

        return true;
    }

    /**
     * Write to the specified file - absPath from the given InputStream
     *
     * @param absPath Absolute path of the file to write to
     * @param ips     InputStream to write data from
     * @return true, if write operation succeeded
     */
    public static final boolean write(String absPath, InputStream ips) {
        if (!create(absPath, true)) {
            return false;
        }

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(absPath);
            byte[] buffer = new byte[IO_BUFFER_SIZE];
            int count = ips.read(buffer);
            for (; count != -1; ) {
                fos.write(buffer, 0, count);
                count = ips.read(buffer);
            }
            fos.flush();
        } catch (Exception e) {
            //
            return false;
        } finally {
            CloseUtil.close(fos);
        }

        return true;
    }

    /**
     * Check if a file is present. This is different from {@link FileUtil#exists(File)} as it also
     * checks the name within drawable folder.
     *
     * @param context Context
     * @param name    Absolute path of the file or name of the drawable
     * @return true if exists, false otherwise
     */
    public static boolean isFilePresent(Context context, String name) {
        if (name == null) {
            return false;
        }
        if (name.startsWith("/")) {
            return exists(name);
        } else {
            return context.getResources().getIdentifier(name, "drawable", context.getPackageName()) != 0;
        }
    }

    /**
     * Read contents of a text file.
     *
     * @param absPath Absolute path of the file to be read
     * @return Data of the file if successfully read, null otherwise
     */
    @Nullable
    public static String readFile(String absPath) {
        InputStream is;
        Writer writer;
        char[] buffer;
        try {
            is = new FileInputStream(absPath);
            writer = new StringWriter();
            buffer = new char[1024];
        } catch (Exception e) {
            return null;
        }
        try {
            Reader reader = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8));
            int n;
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }
        } catch (IOException e) {
            BLog.printStackTrace(e);
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                BLog.printStackTrace(e);
            }
        }
        return writer.toString();
    }


    /**
     * Read contents of a file as a byte array.
     *
     * @param path Absolute path of the file to be read
     * @return Data of the file if successfully read, null otherwise
     * @throws Exception if failed to read
     */
    public static byte[] readAllBytes(String path) throws Exception {
        File file = new File(path);
        byte[] bytes = new byte[(int) file.length()];
        boolean success = true;
        FileInputStream fis = new FileInputStream(file);
        try {
            fis.read(bytes);
        } catch (Exception e) {
            BLog.printStackTrace(e);
            success = false;
        } finally {
            fis.close();
        }
        if (success)
            return bytes;
        throw new Exception();
    }

    /**
     * Copy a given fileName from /assets to given folder.
     *
     * @param fileName   Filename in assets to be copied
     * @param folderPath Folder path where the file needs to be copied
     * @return Path of the new file created
     * @throws Exception if any IO exception occurs
     */
    public static String convertAssetsToUri(final String fileName, String folderPath) throws Exception {
        AssetManager assetManager = BobbleCoreSDK.applicationContext.getAssets();
        InputStream is = assetManager.open(fileName);

        String filePath = folderPath + File.separator + fileName;
        File outFile = new File(folderPath, fileName);

        byte[] buffer = new byte[1024];

        FileOutputStream fos = new FileOutputStream(outFile);
        int read = 0;

        while ((read = is.read(buffer, 0, 1024)) >= 0) {
            fos.write(buffer, 0, read);
        }

        fos.flush();
        fos.close();
        is.close();
        return filePath;
    }

    /**
     * Join a set of path together using {@link File#separator} ans ensure that the directory with
     * that path exists
     *
     * @param paths List of paths to join together
     * @return Joined path of the folder so created
     */
    public static String createDirAndGetPath(Object... paths) {
        String path = join(paths);
        mkdirs(path);
        return path;
    }

    /**
     * {@code Deletes a file in directory}.
     *
     * @param DIR_PATH  the application context.
     * @param FILE_NAME the name of the file to be deleted.
     */
    public static void deleteFileFromDir(String DIR_PATH, String FILE_NAME) {
        File f = new File(DIR_PATH + "/" + FILE_NAME);
        if (f.exists()) {
            FileUtil.delete(f);
        }
    }

    /**
     * Delete files or folder based on the given fileFilter. If a directory matches the filter, the
     * whole directory is deleted, otherwise the directory is traversed and matching files are deleted
     *
     * @param file       File or directory to delete
     * @param fileFilter File filter to match specific files/folders to delete
     */
    public static void delete(String file, FileFilter fileFilter) {
        File fileObj = new File(file);
        if (fileObj.isFile()) {
            if (fileFilter.accept(fileObj))
                delete(fileObj);
        } else {
            if (fileFilter.accept(fileObj))
                delete(fileObj);
            else {
                File[] list = fileObj.listFiles();
                if (list != null) {
                    for (File subFile : list) {
                        delete(subFile.getAbsolutePath(), fileFilter);
                    }
                }
            }
        }
    }

    /**
     * Save a image to gallery. Having storage permission before Android Q is necessary for this
     * function to work. The file is saved in the public "Pictures" folder inside a directory named
     * after the application. See {@code BobbleCoreSDK.getAppController().getAppName()}
     * <p>
     * The function takes care of adding the file to MediaScanner for it to be visible in the gallery
     *
     * @param originalPath Original path of the image to be saved
     * @return true if succeeded, false otherwise
     */
    public static boolean saveImageToGallery(String originalPath) {
        return saveImageToGallery(originalPath, null, null);
    }

    public static boolean saveImageToGallery(String originalPath, String folderName, Integer fileId) {
        Context context = BobbleCoreSDK.applicationContext;
        try {
            String picturesPath = "";
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
                if (folderName != null && !folderName.isEmpty()) {
                    picturesPath = FileUtil.join(
                            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                            BobbleCoreSDK.INSTANCE.getAppController().getAppName() + " " + folderName
                    );
                } else {
                    picturesPath = FileUtil.join(
                            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                            BobbleCoreSDK.INSTANCE.getAppController().getAppName()
                    );
                }
                if (mkdirs(originalPath))
                    return false;
                String extension = FileUtil.getExtension(originalPath);
                String dstPath = "";
                if (fileId != null) {
                    dstPath = FileUtil.join(picturesPath, System.currentTimeMillis() + "_" + fileId + "." + extension);

                } else {
                    dstPath = FileUtil.join(picturesPath, "sticker_" + System.currentTimeMillis() + "." + extension);
                }
                if (!copy(originalPath, dstPath, true))
                    return false;
                final String[] SCAN_TYPES = {"image/*"};
                File photo = new File(dstPath);
                MediaScannerConnection.scanFile(context,
                        new String[]{photo.getAbsolutePath()}, SCAN_TYPES, null);
            } else {
                Uri collection = MediaStore.Images.Media.getContentUri(MediaStore.VOLUME_EXTERNAL_PRIMARY);
                File destDir;
                if (folderName != null && !folderName.isEmpty()) {
                    destDir = new File(Environment.DIRECTORY_PICTURES, BobbleCoreSDK.crossAppInterface.getAppName()
                            + " " + folderName);
                } else {
                    destDir = new File(Environment.DIRECTORY_PICTURES, BobbleCoreSDK.crossAppInterface.getAppName());
                }

                long date = System.currentTimeMillis();
                String extension = FileUtil.getExtension(originalPath);
                ContentValues newImageValues = new ContentValues();

                newImageValues.put(MediaStore.Images.Media.DATE_TAKEN, System.currentTimeMillis());
                newImageValues.put(MediaStore.Images.Media.MIME_TYPE, "image/" + extension);
                if (fileId != null) {
                    newImageValues.put(MediaStore.Images.Media.DISPLAY_NAME, date + "_" + fileId + "." + extension);
                } else {
                    newImageValues.put(MediaStore.Images.Media.DISPLAY_NAME, date + "." + extension);
                }
                newImageValues.put(MediaStore.MediaColumns.DATE_ADDED, date);
                newImageValues.put(MediaStore.MediaColumns.DATE_MODIFIED, date);
                newImageValues.put(MediaStore.MediaColumns.SIZE, FileUtil.size(originalPath));
                newImageValues.put(MediaStore.MediaColumns.RELATIVE_PATH, destDir + File.separator);
                newImageValues.put(MediaStore.Images.Media.IS_PENDING, 1);

                Uri newImageUri = context.getContentResolver().insert(collection, newImageValues);

                OutputStream outStream = context.getContentResolver().openOutputStream(newImageUri, "w");
                FileInputStream inputStream = new FileInputStream(originalPath);
                FileUtil.copy(inputStream, outStream);
                newImageValues.clear();
                newImageValues.put(MediaStore.Images.Media.IS_PENDING, 0);
                context.getContentResolver().update(newImageUri, newImageValues, null, null);
                outStream.flush();
                outStream.close();
                inputStream.close();
            }
            return true;
        } catch (Exception e) {
            BLog.printStackTrace(e);
            return false;
        }
    }

    public static boolean saveVideoToGallery(String originalPath, String folderName, Integer fileId) {
        Context context = BobbleCoreSDK.applicationContext;

        try {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
                String picturesPath = "";
                if (folderName != null && !folderName.isEmpty()) {
                    picturesPath = FileUtil.join(
                            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                            BobbleCoreSDK.INSTANCE.getAppController().getAppName() + " " + folderName
                    );
                } else {
                    picturesPath = FileUtil.join(
                            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                            BobbleCoreSDK.INSTANCE.getAppController().getAppName()
                    );
                }

                if (mkdirs(originalPath))
                    return false;
                String dstPath = FileUtil.join(picturesPath, System.currentTimeMillis() + "_" + fileId + ".mp4");
                if (!copy(originalPath, dstPath, true))
                    return false;
                final String[] SCAN_TYPES = {"video/*"};
                File photo = new File(dstPath);
                MediaScannerConnection.scanFile(context,
                        new String[]{photo.getAbsolutePath()}, SCAN_TYPES, null);
            } else {
                File videoFile = new File(originalPath);
                Uri file = Uri.fromFile(new File(originalPath));
                Uri collection = MediaStore.Video.Media.getContentUri(MediaStore.VOLUME_EXTERNAL_PRIMARY);
                File destDir;
                if (folderName != null && !folderName.isEmpty()) {
                    destDir = new File(Environment.DIRECTORY_MOVIES, BobbleCoreSDK.crossAppInterface.getAppName() + " " + folderName);

                } else {
                    destDir = new File(Environment.DIRECTORY_MOVIES, BobbleCoreSDK.crossAppInterface.getAppName());

                }
                long date = System.currentTimeMillis();
                String extension = MimeTypeMap.getFileExtensionFromUrl(file.toString());
                ContentValues newVideoValues = new ContentValues();

                newVideoValues.put(MediaStore.Video.Media.DATE_TAKEN, System.currentTimeMillis());
                newVideoValues.put(MediaStore.Video.Media.MIME_TYPE, "video/" + extension);
                newVideoValues.put(MediaStore.Video.Media.DISPLAY_NAME, date + "_" + fileId + "." + extension);
                newVideoValues.put(MediaStore.MediaColumns.DATE_ADDED, date);
                newVideoValues.put(MediaStore.MediaColumns.DATE_MODIFIED, date);
                newVideoValues.put(MediaStore.MediaColumns.RELATIVE_PATH, destDir + File.separator);
                newVideoValues.put(MediaStore.Video.Media.IS_PENDING, 1);

                Uri newVideoUri = context.getContentResolver().insert(collection, newVideoValues);

                OutputStream outStream = context.getContentResolver().openOutputStream(newVideoUri, "w");
                Files.copy(videoFile.toPath(), outStream);
                newVideoValues.clear();
                newVideoValues.put(MediaStore.Video.Media.IS_PENDING, 0);
                context.getContentResolver().update(newVideoUri, newVideoValues, null, null);
                outStream.flush();
                outStream.close();
            }
            return true;
        } catch (Exception e) {
            BLog.printStackTrace(e);
            return false;
        }
    }

    /**
     * Rename a given file
     *
     * @param srcFile Source file to be renamed
     * @param dstFile Path of the file after renaming
     * @return true if rename succeeded, false otherwise
     */
    public static boolean rename(File srcFile, File dstFile) {
        return srcFile.renameTo(dstFile);
    }

    /**
     * Rename a given file
     *
     * @param srcFile Source file to be renamed
     * @param dstFile Path of the file after renaming
     * @return true if rename succeeded, false otherwise
     */
    public static boolean rename(String srcFile, String dstFile) {
        return new File(srcFile).renameTo(new File(dstFile));
    }


    /**
     * This method proides a filename from a given url
     *
     * @param url Complete url
     * @return Filename from url
     */
    @Nullable
    public static String getFileNameFromUrl(String url) {
        if (url == null) {
            return null;
        }
        return url.substring(url.lastIndexOf('/') + 1);
    }

    public static String getRawFileContents(@RawRes int id){
        return getRawFileContents(BobbleCoreSDK.applicationContext, id);
    }

    public static String getRawFileContents(@RawRes int id, @Nullable String locale){
        Context updatedContext = ContextUtils.INSTANCE.changeLocale(BobbleCoreSDK.applicationContext, locale);
        return getRawFileContents(updatedContext, id);
    }
    /**
     * Read string data from the given raw resource
     *
     * @param id Raw resource id
     * @return Contents of the resource
     */
    @NotNull
    public static String getRawFileContents(Context context, @RawRes int id) {
        InputStream is = context.getResources().openRawResource(id);
        Writer writer = new StringWriter();
        char[] buffer = new char[1024];
        try {
            Reader reader = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8));
            int n;
            while ((n = reader.read(buffer)) != -1) {
                writer.write(buffer, 0, n);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return writer.toString();
    }


}
