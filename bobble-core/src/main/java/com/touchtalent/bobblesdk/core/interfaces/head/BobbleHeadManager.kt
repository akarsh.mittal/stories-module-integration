package com.touchtalent.bobblesdk.core.interfaces.head

import android.content.Context
import com.touchtalent.bobblesdk.core.model.BobbleHead
import com.touchtalent.bobblesdk.core.model.ContactCharacterSuggestions
import com.touchtalent.bobblesdk.core.model.HeadCategory
import io.reactivex.Observable
import io.reactivex.Single

/**
 * Interface to manage heads within the *bobble-head* module
 */
interface BobbleHeadManager {
    /**
     * Fetch [BobbleHead] by given head id
     * @param headCharacterId Head character id of the required head.
     * @param headType Preferred bobble type of the head required; if [headType] is not available,
     * head of default head type is returned as fallback
     * @return [Single] to load the head
     */
    fun getHeadById(headCharacterId: Long, headType: Int? = null): Single<BobbleHead>

    /**
     * Fetch currently selected head
     * @param gender Gender of the current head required. If null, head of default gender is used.
     * @param headType Preferred bobble type of the head required; if [headType] is not available,
     * head of default head type is returned as fallback
     * @return [Single] to load the head
     */
    fun getCurrentHead(gender: String? = null, headType: Int? = null): Single<BobbleHead>

    /**
     * Fetch the count of heads of the provided gender
     * @param gender Gender of the head count required.
     * @return [Single] to load the head count
     */
    fun getHeadsByGenderCount(gender: String): Single<Int>

    /**
     * Fetch current head with all other head types available for the same head.
     * @param gender Gender of the head to be fetched. If null, head of default gender is used.
     * @return [Single] to load the head list
     */
    fun getCurrentHeadAllVariants(gender: String? = null): Single<List<BobbleHead>>

    /**
     * Fetch observable to listen for change in current head. The id of current head is published
     * in event of any change
     * @param gender Gender of the head to be listened for.
     * @return [Observable] to listen for changes
     */
    fun getCurrentHeadChangeListener(gender: String): Observable<Long>

    /**
     * Fetch observable to listen for change in current head of any gender. Can be used to refresh
     * any view which uses current head.
     * @return [Observable] to listen for changes
     */
    fun getAnyHeadChangeListener(): Observable<Any>

    /**
     * Fetch list of [BobbleHead] for the given [Config] parameters
     * @return [Observable] to fetch the heads as per specification
     */
    fun getBobbleHeads(config: Config): Observable<List<BobbleHead>>

    /**
     * Fetch count of head characters for the given [Config] parameters
     * @return [Observable] to fetch the heads count as per specification
     */
    fun getBobbleHeadsCount(config: Config): Observable<Int>

    /**
     * Fetch count of head characters for the given [Config] parameters
     * @return [Observable] to fetch the heads count as per specification
     */
    fun getDefaultHeadType(): Int

    /**
     * Update name of the given head character
     * @param characterId Character id of the head whose name needs to be updated
     * @param name New name for the head
     */
    fun updateName(characterId: Long, name: String)

    /**
     * Update relation of the given head character
     * @param characterId Character id of the head whose name needs to be updated
     * @param relationId Id of the new relation for the head
     */
    fun updateRelation(characterId: Long, relationId: String)

    /**
     * Delete all heads and related data by the head character id
     * @param characterId Character id of the head to be deleted
     */
    fun delete(characterId: Long)

    /**
     * Change current head to the given [characterId]. The head is set selected by the gender of
     * the head represented by [characterId]
     * @param characterId Character id of the head to be set default
     */
    fun setCurrentHead(characterId: Long)

    /**
     * Check if the given [characterId] is set current.
     * @return if the given [characterId] is set current
     */
    fun isCurrentHead(characterId: Long): Boolean

    /**
     * Resolve [relationId] to user visible locale based relation.
     * @param context Context, used to resolve locale
     * @param relationId Relation id to resolve user-visible relation
     * @return User visible localised relation
     */
    fun resolveRelation(relationId: String, context: Context): String

    suspend fun getContactCharacterSuggestions(): List<ContactCharacterSuggestions>

    /**
     * Config class to search for heads
     * @property headCategories Head categories to include in the result
     * @property bobbleType Preferred head type of the heads
     * @property doNotFallbackToDefaultHeadType Whether [bobbleType] should be enforced, false if
     * default head type can be used instead
     * @see getBobbleHeads
     */
    class Config {
        var headCategories = mutableListOf<HeadCategory>()
            get() {
                return if (field.isEmpty())
                    mutableListOf(HeadCategory.SELF, HeadCategory.MASCOT, HeadCategory.FNF)
                else field
            }

        var bobbleType: Int = 0
        var doNotFallbackToDefaultHeadType: Boolean = false

        /**
         * Add [headCategories] required
         * @param headCategories List of head categories to support
         */
        fun withHeadCategories(vararg headCategories: HeadCategory): Config {
            this.headCategories = headCategories.asList().toMutableList()
            return this
        }

        /**
         * Add [bobbleType] required
         * @param bobbleType Bobble type to be preferred
         */
        fun withBobbleType(bobbleType: Int): Config {
            this.bobbleType = bobbleType
            return this
        }

        /**
         * Indicate whether default head can be used as a fallback if specified [bobbleType] is not
         * found
         */
        fun doNotFallbackToDefaultHeadType(): Config {
            this.doNotFallbackToDefaultHeadType = true
            return this
        }
    }

}
