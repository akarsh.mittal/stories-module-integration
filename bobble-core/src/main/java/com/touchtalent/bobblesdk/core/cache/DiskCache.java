package com.touchtalent.bobblesdk.core.cache;

import android.content.Context;
import android.graphics.Bitmap;

import com.touchtalent.bobblesdk.core.BobbleCoreSDK;

/**
 * Created by amitshekhar on 27/04/16.
 */
public class DiskCache {
    private static final String TAG = DiskCache.class.getSimpleName();
    private static DiskCache sInstance;
    private final Context context;
    private DiskLruImageCache diskLruImageCache = null;
    private static final int DISK_CACHE_SIZE = 1024 * 1024 * 15; // 15MB
    private static final String DISK_CACHE_SUB_DIR = "bobble_cache";

    private DiskCache() {
        context = BobbleCoreSDK.applicationContext;
    }

    public static DiskCache getInstance() {
        if (sInstance == null) {
            synchronized (DiskCache.class) {
                sInstance = new DiskCache();
            }
        }
        return sInstance;
    }

    public void initDiskLruImageCache() {
        if (diskLruImageCache == null) {
            diskLruImageCache = new DiskLruImageCache(context, DISK_CACHE_SUB_DIR, DISK_CACHE_SIZE, Bitmap.CompressFormat.PNG, 70);
        }
    }

    public void clearDiskLruImageCache() {
        if (diskLruImageCache == null) {
            initDiskLruImageCache();
        }
        diskLruImageCache.clearCacheWithoutClosingDisk();
    }

    public void deleteCacheForParticularKey(String key) {
        if (diskLruImageCache == null) {
            initDiskLruImageCache();
        }
        if (diskLruImageCache.containsKey(key)) {
            diskLruImageCache.deleteForParticularKey(key);
        }
    }

    public void addBitmapToDiskCache(String key, Bitmap bitmap) {
        if (diskLruImageCache == null) {
            initDiskLruImageCache();
        }
        if (diskLruImageCache != null && diskLruImageCache.getBitmap(key) == null) {
            diskLruImageCache.put(key, bitmap);
        }
    }

    public String getDiskCacheDirectory() {
        synchronized (DiskCache.class) {
            if (diskLruImageCache == null || diskLruImageCache.getDiskCache() == null || diskLruImageCache.getCacheFolder() == null) {
                initDiskLruImageCache();
            }
            return diskLruImageCache.getCacheFolder().getAbsolutePath();
        }
    }

    public Bitmap getBitmapFromDiskCache(String key) {
        if (diskLruImageCache == null) {
            initDiskLruImageCache();
        }
        Bitmap bitmap = null;
        if (key != null && diskLruImageCache != null) {
            bitmap = diskLruImageCache.getBitmap(key);
            return bitmap;
        }
        return null;
    }

    public boolean isBitmapPresent(String key) {
        if (diskLruImageCache == null) {
            initDiskLruImageCache();
        }

        return diskLruImageCache.containsKey(key);
    }

}
