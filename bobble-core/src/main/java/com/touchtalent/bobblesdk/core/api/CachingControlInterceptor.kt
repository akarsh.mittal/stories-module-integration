package com.touchtalent.bobblesdk.core.api

import com.touchtalent.bobblesdk.core.BobbleCoreSDK
import com.touchtalent.bobblesdk.core.utils.NetworkUtil
import okhttp3.Interceptor
import okhttp3.Response

/**
 * Interceptor that forces HTTP cache in case for all GET requests in case no internet is connected.
 * [cacheOnly] USE THIS CAREFULLY! Setting this true will not communicate from server and attempt
 *      to fetch response only from local cache.
 */
class CachingControlInterceptor(private val cacheOnly: Boolean = false) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        var request = chain.request()
        // Add Cache Control only for GET methods
        if (request.method() == "GET" &&
            (!NetworkUtil.isInternetConnected(BobbleCoreSDK.applicationContext)
                    || cacheOnly)
        ) {
            request = request.newBuilder()
                .header("Cache-Control", "only-if-cached, max-stale=${Int.MAX_VALUE}")
                .build()
        }
        return chain.proceed(request)
    }
}
