package com.touchtalent.bobblesdk.core.moshi

import com.squareup.moshi.*
import com.touchtalent.bobblesdk.core.moshi.StringWrapperAdapter.StringWrapperPojo
import okio.Buffer

/**
 * Utility Moshi Adapter class to parse a JSON Object as a String via the wrapper [StringWrapperPojo]
 */
class StringWrapperAdapter {

    data class StringWrapperPojo(val data: String) {
        override fun toString() = data
    }

    @StringWrapper
    @FromJson
    fun fromJson(reader: JsonReader?, delegate: JsonAdapter<Any>): StringWrapperPojo? {
        val jsonValue: Any? = reader?.readJsonValue()
        return delegate.toJson(jsonValue)?.let { StringWrapperPojo(it) }
    }

    @ToJson
    fun toJson(writer: JsonWriter?, @StringWrapper value: StringWrapperPojo?) {
        writer?.value(Buffer().writeUtf8(value.toString()))
    }
}