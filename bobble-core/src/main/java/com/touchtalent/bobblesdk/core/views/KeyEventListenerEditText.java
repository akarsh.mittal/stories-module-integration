package com.touchtalent.bobblesdk.core.views;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;

import com.touchtalent.bobblesdk.core.utils.BLog;

import java.lang.ref.WeakReference;

/**
 * EditText that can listen to back button clicks, handle it and dispatch events for the same.
 * Also supports listening to clicks on drawable left and drawable right
 */
public class KeyEventListenerEditText extends AppCompatEditText {

    private static final String TAG = "KeyEventListenerEditText";
    private Drawable dLeft;
    private Drawable dRight;

    private WeakReference<onDispatchKeyEventListener> onDispatchKeyEventListener;

    public KeyEventListenerEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public KeyEventListenerEditText(Context context) {
        super(context);
    }

    public void initEventListener(onDispatchKeyEventListener onDispatchKeyEventListener) {
        this.onDispatchKeyEventListener = new WeakReference<>(onDispatchKeyEventListener);
    }

    public void removeEventListener() {
        this.onDispatchKeyEventListener = null;
    }

    /**
     * Overrides the handling of the back key to move back to the
     * previous sources or dismiss the search dialog, instead of
     * dismissing the input method.
     */
    @Override
    public boolean dispatchKeyEventPreIme(KeyEvent event) {
        BLog.d(TAG, "dispatchKeyEventPreIme(" + event + ")");
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            KeyEvent.DispatcherState state = getKeyDispatcherState();
            if (state != null) {
                if (event.getAction() == KeyEvent.ACTION_DOWN
                        && event.getRepeatCount() == 0) {
                    state.startTracking(event, this);
                    return true;
                } else if (event.getAction() == KeyEvent.ACTION_UP
                        && !event.isCanceled() && state.isTracking(event)) {
                    onActionUp();
                    return true;
                }
            }
        }

        return super.dispatchKeyEventPreIme(event);
    }

    @Override
    public void setCompoundDrawables(@Nullable Drawable left, @Nullable Drawable top, @Nullable Drawable right, @Nullable Drawable bottom) {
        if(left!=null) {
            dLeft = left;

        }
        if(right!=null) {
            dRight = right;
        }
        super.setCompoundDrawables(left, top, right, bottom);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if( event.getAction() == MotionEvent.ACTION_DOWN && dLeft!=null) {
            final int x = (int)event.getX();
            final int y = (int)event.getY();

            //System.out.println("x:/y: "+x+"/"+y);
            //System.out.println("bounds: "+bounds.left+"/"+bounds.right+"/"+bounds.top+"/"+bounds.bottom);
            //check to make sure the touch event was within the bounds of the drawable
            if( dLeft.getBounds().contains(x, y)) {
                onActionUp();
            }
        }

        if( event.getAction() == MotionEvent.ACTION_DOWN && dRight!=null) {
            final int x = (int)event.getX();
            final int y = (int)event.getY();

            //System.out.println("x:/y: "+x+"/"+y);
            //System.out.println("bounds: "+bounds.left+"/"+bounds.right+"/"+bounds.top+"/"+bounds.bottom);
            //check to make sure the touch event was within the bounds of the drawable
            if( dRight.getBounds().contains(x, y)) {
                onActionUp();
            }
        }

        return super.onTouchEvent(event);
    }

    private void onActionUp() {
        if (onDispatchKeyEventListener != null && onDispatchKeyEventListener.get() != null) {
            onDispatchKeyEventListener.get().onActionUp();
        }
    }

    /**
     * Interface for back button click listener
     */
    public interface onDispatchKeyEventListener {
        /**
         * Called when back button is pressed (given this edittext is in focus)
         */
        void onActionUp();
    }
}