package com.touchtalent.bobblesdk.core.cache

/**
 * Interface that makes any object cacheable by providing a cache key for its content
 */
interface Cacheable {
    /**
     * Process any data that makes 2 instance of this object unique
     * For e.g - [source].process(id)
     */
    fun generateCacheKey(source: CacheGenerator)
}