package com.touchtalent.bobblesdk.core.utils;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

/**
 * Utility class for {@link View} related operations
 */

public final class ViewUtil {

    private ViewUtil() {
        // This utility class is not publicly instantiable.
    }

    /**
     * Convert {@link TypedValue#COMPLEX_UNIT_DIP} value to {@link TypedValue#COMPLEX_UNIT_PX}
     *
     * @param dp Value in dp
     * @return Corresponding value in pixels
     */
    public static int dpToPx(float dp, Context context) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dp * scale + 0.5f);
    }

    /**
     * Convert {@link TypedValue#COMPLEX_UNIT_PX} value to {@link TypedValue#COMPLEX_UNIT_SP}
     *
     * @param px Value in pixels
     * @return Corresponding value in sp
     */
    public static float pixelsToSp(float px, Context context) {
        float scaledDensity = context.getResources().getDisplayMetrics().scaledDensity;
        return px / scaledDensity;
    }

    /**
     * Convert {@link TypedValue#COMPLEX_UNIT_PX} value to {@link TypedValue#COMPLEX_UNIT_DIP}
     *
     * @param px Value in pixels
     * @return Corresponding value in dp
     */
    public static float PxToDp(float px, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return dp;
    }

    /**
     * Convert {@link TypedValue#COMPLEX_UNIT_SP} value to {@link TypedValue#COMPLEX_UNIT_PX}
     *
     * @param sp Value in sp
     * @return Corresponding value in pixels
     */
    public static int spToPx(float sp, Context context) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, context.getResources().getDisplayMetrics());
    }

    public static int getStatusBarHeight(Context context) {
        int result = 0;
        int resourceId = context.getResources().getIdentifier(
                "status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    /**
     * Detach a {@link View} from its parent, if attached.
     *
     * @param view View to detach from its parent
     */
    public static void detachViewFromParent(View view) {
        ViewParent parent = view.getParent();
        if (parent instanceof ViewGroup) {
            ((ViewGroup) parent).removeView(view);
        }
    }

    /**
     * Measures the percentage of visible portion of the given {@code view}
     *
     * @param view View whose visibility needs to be evaluated
     * @return Percentage in integer (0-100)
     */
    public static int getVisiblePercentage(View view) {
        if (!isViewPartiallyVisible(view)) {
            return 0;
        } else {
            Rect var2 = new Rect();
            view.getGlobalVisibleRect(var2);
            double visibleArea = var2.width() * var2.height();
            double totalArea = view.getWidth() * view.getHeight();
            return (int) (100.0D * visibleArea / totalArea);
        }
    }

    /**
     * Checks whether the given {@code view} is visible on the complete layout that has been drawn.
     * If this function returns true, it is not guaranteed that this view is user visible. Refer
     * {@link #getVisiblePercentage(View)} for actual user visibility
     *
     * @param view View whose visibility needs to be evaluated
     * @return Percentage in integer (0-100)
     */
    public static boolean isViewPartiallyVisible(View view) {
        if (view.getParent() == null) {
            return false;
        } else if (Build.VERSION.SDK_INT >= 19 && !view.isAttachedToWindow()) {
            return false;
        } else {
            Rect rect = new Rect();
            ((ViewGroup) view.getParent()).getHitRect(rect);
            return view.getGlobalVisibleRect(rect);
        }
    }

    public static boolean areViewOverlapping(View firstView ,View secondView){
        try {
            int[] firstPosition = new int[2];
            int[] secondPosition = new int[2];

            firstView.getLocationOnScreen(firstPosition);
            secondView.getLocationOnScreen(secondPosition);

            // Rect constructor parameters: left, top, right, bottom
            Rect rectFirstView = new Rect(firstPosition[0], firstPosition[1],
                    firstPosition[0] + firstView.getMeasuredWidth(), firstPosition[1] + firstView.getMeasuredHeight());
            Rect rectSecondView = new Rect(secondPosition[0], secondPosition[1],
                    secondPosition[0] + secondView.getMeasuredWidth(), secondPosition[1] + secondView.getMeasuredHeight());
            return rectFirstView.intersect(rectSecondView);
        } catch (Exception ex){
            ex.fillInStackTrace();
        }
        return false;
    }

}
