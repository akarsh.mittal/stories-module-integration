package com.touchtalent.bobblesdk.core.moshi

import com.squareup.moshi.JsonQualifier

/**
 * Used with Moshi JsonAdapter [StringWrapperAdapter] to parse a JSON Object as a String.
 */
@Retention(AnnotationRetention.RUNTIME)
@JsonQualifier
annotation class StringWrapper