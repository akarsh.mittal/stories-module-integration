package com.touchtalent.bobblesdk.core.moshi

import com.squareup.moshi.JsonQualifier

/**
 * Denotes that the annotated element represents a date time in the format *yyyy-MM-dd HH:mm:ss*
 * (TimeZone UTC).
 */
@Retention(AnnotationRetention.RUNTIME)
@JsonQualifier
annotation class DateTime