package com.touchtalent.bobblesdk.core.interfaces.ai_predictions

/**
 * Base interface for *ai-prediction* module for all interactions. This module is designed to
 * support loose coupling
 */
interface AIPredictionModule {
    /**
     * @return a new instance of [AiDictionary]
     */
    fun newAiDictionaryInstance(): AiDictionary
}
