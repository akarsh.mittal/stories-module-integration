package com.touchtalent.bobblesdk.core.moshi

import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson

/**
 * Moshi adapter to convert a regex string to a kotlin [Regex] object. The conversion provides
 * type-safety and throws exception in case of corrupt regex patterns.
 *
 * Also supports converting [List] of [String] to [List] of [Regex]
 * @see KotlinRegex
 */
class KotlinRegexAdapter {
    /**
     * @suppress
     */
    @ToJson
    fun toJson(@KotlinRegex regex: Regex): String {
        return regex.pattern
    }

    /**
     * @suppress
     */
    @FromJson
    @KotlinRegex
    fun fromJson(regexPattern: String): Regex {
        return Regex(regexPattern, RegexOption.IGNORE_CASE)
    }

    /**
     * @suppress
     */
    @ToJson
    fun toJson(@KotlinRegex regexList: List<Regex>?): List<String>? {
        return regexList?.map { toJson(it) }
    }

    /**
     * @suppress
     */
    @FromJson
    @KotlinRegex
    fun fromJson(regexPatternList: List<String>?): List<Regex>? {
        return regexPatternList?.map { fromJson(it) }
    }
}