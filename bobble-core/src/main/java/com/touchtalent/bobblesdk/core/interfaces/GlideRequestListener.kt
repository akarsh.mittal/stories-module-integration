package com.touchtalent.bobblesdk.core.interfaces

import android.graphics.drawable.Drawable
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target

/**
 * A simple listener to listen for only success and error events from Glide
 * to avoid boilerplate in main code
 */
abstract class GlideRequestListener : RequestListener<Drawable> {

    abstract fun onComplete(success: Boolean)

    override fun onLoadFailed(
        e: GlideException?,
        model: Any?,
        target: Target<Drawable>?,
        isFirstResource: Boolean
    ): Boolean {
        onComplete(false)
        return false
    }

    override fun onResourceReady(
        resource: Drawable?,
        model: Any?,
        target: Target<Drawable>?,
        dataSource: DataSource?,
        isFirstResource: Boolean
    ): Boolean {
        onComplete(true)
        return false
    }
}