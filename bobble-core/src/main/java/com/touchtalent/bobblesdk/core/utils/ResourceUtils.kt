package com.touchtalent.bobblesdk.core.utils

import android.content.Context
import java.io.*

object ResourceUtils {

    fun getResourceString(context: Context, resId: Int): String? {
        val `is` = context.resources.openRawResource(resId)
        val writer: Writer = StringWriter()
        val buffer = CharArray(1024)
        try {
            val reader: Reader = BufferedReader(InputStreamReader(`is`, "UTF-8"))
            var n: Int
            while (reader.read(buffer).also { n = it } != -1) {
                writer.write(buffer, 0, n)
            }
        } catch (e: IOException) {
            e.printStackTrace()
        } finally {
            try {
                `is`.close()
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
        return writer.toString()
    }
}