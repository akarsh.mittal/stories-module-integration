package com.touchtalent.bobblesdk.core.enums

import androidx.annotation.IntDef
import com.touchtalent.bobblesdk.core.enums.KeyboardDeepLink.Companion.CUSTOMIZATION
import com.touchtalent.bobblesdk.core.enums.KeyboardDeepLink.Companion.GIFS
import com.touchtalent.bobblesdk.core.enums.KeyboardDeepLink.Companion.STICKERS

/**
 * Enumerated annotation for representing type of deep links supported by the keyboard
 */
@IntDef(value = [STICKERS, GIFS,CUSTOMIZATION])
annotation class KeyboardDeepLink {
    companion object {
        const val STICKERS = 1
        const val GIFS = 2
        const val CUSTOMIZATION = 3

    }
}