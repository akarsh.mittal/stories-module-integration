package com.touchtalent.bobblesdk.core.config

import androidx.annotation.RawRes
import com.touchtalent.bobblesdk.core.interfaces.head.ChooseHeadView

/**
 * Config class for *bobble-head* module
 * @property seededMascotHeads Raw resource path for JSON for seeded mascots.
 * @property defaultBobbleHeadType Default bobble head type, e.g - 2101, 2301, etc
 */
class BobbleHeadConfig {
    @RawRes
    var seededMascotHeads: Int? = null
    var defaultBobbleHeadType: Int = 2101
    var userGender: String? = null
    var userAge: Int? = null
    var showRelationSelection: Boolean = true
    var headChooserType:ChooseHeadView.Type = ChooseHeadView.Type.NonBrandedType
}