package com.touchtalent.bobblesdk.core.utils

import android.content.Context
import android.graphics.*
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

/**
 * Utility class for [Bitmap] related operations
 */
object BitmapUtils {


    @JvmStatic
    @Throws(Exception::class)
    fun getBitMapFromUrlGlide(
            context: Context?,
            urlString: String?,
            width: Int = 500,
            height: Int = 500
    ): Bitmap? {
        return if (GeneralUtils.isValidContextForGlide(context))
            Glide.with(context!!).asBitmap()
                    .load(urlString)
                    .centerInside()
                    .apply(RequestOptions().override(width, height))
                    .submit()
                    .get()
        else null
    }

    /**
     * Applies shadow to the given [Bitmap].
     * @param color Color of the shadow to be applied
     * @param radius Radius of the shadow to be applied
     * @param dx Translation of the shadow w.r.t original image on x-axis
     * @param dy Translation of the shadow w.r.t original image on y-axis
     * @param blur Blur type of the shadow
     * @return New bitmap after applying the shadow. The old Bitmap is kept intact and is upon the
     * caller to recycle it
     */

    @JvmStatic
    fun applyShadow(
            bitmap: Bitmap,
            color: String?,
            radius: Float,
            dx: Int,
            dy: Int,
            blur: BlurMaskFilter.Blur?
    ): Bitmap? {
        if (radius == 0f) return bitmap
        val borderWidth = radius.toInt()
        val newBitmap = Bitmap.createBitmap(
                bitmap.width + borderWidth,
                bitmap.height + borderWidth,
                Bitmap.Config.ARGB_8888
        )
        val c = Canvas(newBitmap)
        val blurMaskFilter = BlurMaskFilter(radius, blur)
        val shadowPaint = Paint()
        shadowPaint.maskFilter = blurMaskFilter
        val shadowBitmap = bitmap.extractAlpha(shadowPaint, null)
        shadowPaint.color = Color.parseColor(color)
        c.drawBitmap(shadowBitmap, dx.toFloat(), dy.toFloat(), shadowPaint)
        val paint = Paint()
        c.drawBitmap(bitmap, borderWidth.toFloat(), borderWidth.toFloat(), paint)
        val finalBitmap =
                Bitmap.createBitmap(newBitmap, borderWidth, borderWidth, bitmap.width, bitmap.height)
        shadowBitmap.recycle()
        newBitmap.recycle()
        return finalBitmap
    }

    /**
     * Saves the [bitmap] to the given [path] in PNG format
                * @param bitmap Bitmap to be saved
     * @param path Path of the file to which the bitmap must be saved
     */

    @JvmStatic
    fun saveImage(bitmap: Bitmap?, path: String) {
        if (bitmap == null) {
            return
        }
        val photo = File(path)
        try {
            photo.parentFile?.let {
                FileUtil.mkdirs(it.absolutePath)
                photo.createNewFile()
                val fos = FileOutputStream(photo)
                bitmap.compress(Bitmap.CompressFormat.PNG, 0 /* ignored for PNG */, fos)
                fos.flush()
                fos.close()
            }
        } catch (e: IOException) {
            BLog.printStackTrace(e)
        }
        return
    }

    /**
     * Converts the bitmap to a mutable bitmap
     * @return Same bitmap if already mutable, otherwise a mutable copy of the bitmap. The original
     * bitmap is recycled.
     */
    fun Bitmap.toMutableBitmap(): Bitmap {
        return if (this.isMutable) this
        else {
            val newBitmap = this.copy(this.config, true)
            this.recycle()
            newBitmap
        }
    }

    /**
     * Read width and height from a given image file
     * @param file File path whose dimension needs to be read
     * @return Pair of width:height of the given image
     * @throws Exception if the file doesn't exists or is corrupt
     */
    fun getWidthAndHeightFromFile(file: String?): Pair<Int, Int> {
        val options = BitmapFactory.Options().also { it.inJustDecodeBounds = true }
        BitmapFactory.decodeFile(file, options)
        require(options.outHeight > 0)
        require(options.outWidth > 0)
        return options.outWidth to options.outHeight
    }


}