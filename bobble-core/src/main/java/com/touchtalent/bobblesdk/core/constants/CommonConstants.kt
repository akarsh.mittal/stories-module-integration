package com.touchtalent.bobblesdk.core.constants

import android.os.Build

/**
 * Collection of constants to be used across modules
 */
object CommonConstants {
    const val SOURCE = "source"
    const val FIELD_ID = "field_id"
    const val FROM_KEYBOARD = 0
    const val FROM_APP = 1

    const val STICKERS = "stickers"
    const val STORIES = "stories"
    const val GIFS = "gifs"
    const val MOVIE_GIFS = "movie_gifs"
    const val EMOJIS = "emojis"
    val CHROME_USER_AGENT =
        "Mozilla/5.0 (Linux; Android ${Build.VERSION.RELEASE}; ${Build.MODEL}) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/99.0.4844.58 Mobile Safari/537.36"
}