package com.touchtalent.bobblesdk.core.prefs

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.*
import androidx.datastore.preferences.preferencesDataStore
import com.touchtalent.bobblesdk.core.BobbleCoreSDK
import com.touchtalent.bobblesdk.core.utils.BLog
import com.touchtalent.bobblesdk.core.utils.logDebug
import io.reactivex.Observable
import io.reactivex.Single
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.rx2.asObservable
import kotlinx.coroutines.rx2.rxSingle
import java.lang.reflect.Type

/**
 * Extendable class to provide utility function for easy access to datastore.
 * To create a datastore file, extend this class and create variables using lazy loading utility
 * functions
 * Following data types are supported:
 * 1. [booleanData] - Boolean data type
 * 2. [intData] - Integer data type
 * 3. [floatData] - Float data type
 * 4. [complexData] - Custom POJO data type, serialisation and deserialization is taken care of,
 * POJO needs to support Moshi
 * 5. [stringSetData] - Set of string data type
 *
 * Each data class has 3 functions:
 * - put - Used to save a value to the key
 * - getOnce - Used to get a one-time value of the key
 * - getFlow - Get flow to receive realtime updates of the value. A value is emitted during collect too
 * <br>
 * E.g:
 * ```kotlin
 * object SampleDataStoreClass : BobbleDataStore("content-config") {
 *        //Boolean key sample_boolean with defaultValue false
 *        val booleanDataStore by booleanData(key = "sample_boolean", defaultValue = false)
 *        //Complex key sample_complex with defaultValue null
 *        val complexDataStore by complexData<CustomPojo>(key = "sample_complex", defaultValue = null)
 * }
 * ```
 */
open class BobbleDataStore(name: String) {

    companion object {
        const val TAG = "BobbleDataStore"
    }

    val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = name)

    // WARNING: Do not use this function for parameterized types such as List<X,Y> or Map<X,Y> use
    // [Type.complexData] instead. Type for parameterized types can be created by using Moshi's
    // utility function - [Types.newParameterizedType]
    inline fun <reified T : Any> complexData(key: String, defaultValue: T?) = lazy {
        ComplexData(BobbleCoreSDK.applicationContext.dataStore, key, defaultValue, T::class.java)
    }

    inline fun <reified T : Any> complexData(key: String, defaultValue: String?): Lazy<ComplexData<T>> = lazy {
        ComplexData(BobbleCoreSDK.applicationContext.dataStore, key, T::class.java, defaultValue)
    }

    inline fun <reified T : Any> Type.complexData(key: String, defaultValue: T?) = lazy {
        ComplexData(BobbleCoreSDK.applicationContext.dataStore, key, defaultValue, this)
    }

    fun stringData(key: String, defaultValue: String?) = lazy {
        StringData(BobbleCoreSDK.applicationContext.dataStore, key, defaultValue)
    }

    fun floatData(key: String, defaultValue: Float?) = lazy {
        FloatData(BobbleCoreSDK.applicationContext.dataStore, key, defaultValue)
    }

    fun intData(key: String, defaultValue: Int?) = lazy {
        IntData(BobbleCoreSDK.applicationContext.dataStore, key, defaultValue)
    }

    fun stringSetData(key: String, defaultValue: Set<String>?) = lazy {
        StringSetData(BobbleCoreSDK.applicationContext.dataStore, key, defaultValue)
    }

    fun booleanData(key: String, defaultValue: Boolean?) = lazy {
        BooleanData(BobbleCoreSDK.applicationContext.dataStore, key, defaultValue)
    }

    fun longData(key: String, defaultValue: Long?) = lazy {
        LongData(BobbleCoreSDK.applicationContext.dataStore, key, defaultValue)
    }

    class ComplexData<T : Any>(
        val dataStore: DataStore<Preferences>,
        val key: String,
        val defaultValue: T?,
        val type: Type
    ) {

        var defaultJsonValue: String? = null

        constructor(
            dataStore: DataStore<Preferences>,
            key: String,
            type: Type,
            defaultValue: String?,
        ) : this(dataStore, key, null, type) {
            defaultJsonValue = defaultValue
        }

        val dataKey = stringPreferencesKey(key)

        suspend fun put(value: String) {
            logDebug(TAG, { "keyUpdated: $key: $value" })
            dataStore.edit { it[dataKey] = value }
        }

        suspend fun getOnce(): T? {
            return getFlow().first()
        }

        fun getDefault(): T? {
            return defaultJsonValue?.let {
                BobbleCoreSDK.moshi.adapter<T>(type).fromJson(it)
            } ?: defaultValue
        }


        fun getFlow(): Flow<T?> {
            return dataStore.data
                .map { it[dataKey] }
                .map {
                    it?.let {
                        BobbleCoreSDK.moshi.adapter<T>(type).fromJson(it)
                    } ?: getDefault()
                }
                .catch {
                    logDebug(TAG, { "catch" }, { it })
                    emit(getDefault())
                }
        }


        fun getRxObservable(): Observable<T> = getFlow().filterNotNull().asObservable()

        suspend fun clearKey() {
            dataStore.edit {
                it.remove(key = dataKey)
            }
        }
    }


    class StringData(
        val dataStore: DataStore<Preferences>,
        key: String,
        val defaultValue: String?
    ) {
        val dataKey = stringPreferencesKey(key)

        suspend fun put(value: String) {
            dataStore.edit { it[dataKey] = value }
        }

        suspend fun getOnce(): String? {
            return getFlow().first()
        }

        fun getOnceRx(): Single<String> {
            return rxSingle {
                getFlow().first() ?: throw KotlinNullPointerException()
            }
        }

        fun getFlow(): Flow<String?> {
            return dataStore.data
                .map { it[dataKey] ?: defaultValue }
                .catch {
                    logDebug(TAG, { "catch" }, { it })
                    emit(defaultValue)
                }
        }

        fun getRxObservable(): Observable<String> = getFlow().filterNotNull().asObservable()

        suspend fun clearKey() {
            dataStore.edit {
                it.remove(key = dataKey)
            }
        }
    }

    class LongData(val dataStore: DataStore<Preferences>, key: String, val defaultValue: Long?) {
        val dataKey = longPreferencesKey(key)

        suspend fun put(value: Long) {
            dataStore.edit { it[dataKey] = value }
        }

        suspend fun getOnce(): Long? {
            return getFlow().first()
        }

        fun getOnceRx(): Single<Long> {
            return rxSingle {
                getFlow().first() ?: throw KotlinNullPointerException()
            }
        }

        fun getFlow(): Flow<Long?> {
            return dataStore.data
                .map { it[dataKey] ?: defaultValue }
                .catch {
                    logDebug(TAG, { "catch" }, { it })
                    emit(defaultValue)
                }
        }

        fun getRxObservable(): Observable<Long> = getFlow().filterNotNull().asObservable()

        suspend fun clearKey() {
            dataStore.edit {
                it.remove(key = dataKey)
            }
        }
    }

    class FloatData(val dataStore: DataStore<Preferences>, key: String, val defaultValue: Float?) {
        val dataKey = floatPreferencesKey(key)

        suspend fun put(value: Float) {
            dataStore.edit { it[dataKey] = value }
        }

        suspend fun getOnce(): Float? {
            return getFlow().first()
        }

        fun getFlow(): Flow<Float?> {
            return dataStore.data
                .map { it[dataKey] ?: defaultValue }
                .catch {
                    logDebug(TAG, { "catch" }, { it })
                    emit(defaultValue)
                }
        }

        fun getRxObservable(): Observable<Float> = getFlow().filterNotNull().asObservable()

        suspend fun clearKey() {
            dataStore.edit {
                it.remove(key = dataKey)
            }
        }
    }

    class BooleanData(
        val dataStore: DataStore<Preferences>,
        key: String,
        val defaultValue: Boolean?
    ) {
        val dataKey = booleanPreferencesKey(key)

        suspend fun put(value: Boolean) {
            dataStore.edit { it[dataKey] = value }
        }

        suspend fun getOnce(): Boolean? {
            return getFlow().first()
        }

        fun getFlow(): Flow<Boolean?> {
            return dataStore.data
                .map { it[dataKey] ?: defaultValue }
                .catch {
                    logDebug(TAG, { "catch" }, { it })
                    emit(defaultValue)
                }
        }

        fun getRxObservable(): Observable<Boolean> = getFlow().filterNotNull().asObservable()
    }

    class IntData(
        private val dataStore: DataStore<Preferences>,
        key: String,
        private val defaultValue: Int?
    ) {
        private val dataKey = intPreferencesKey(key)

        suspend fun put(value: Int) {
            dataStore.edit { it[dataKey] = value }
        }

        suspend fun getOnce(): Int? {
            return getFlow().first()
        }

        fun getFlow(): Flow<Int?> {
            return dataStore.data
                .map { it[dataKey] ?: defaultValue }
                .catch {
                    BLog.d(TAG, "catch", it)
                    emit(defaultValue)
                }
        }

        fun getRxObservable(): Observable<Int> = getFlow().filterNotNull().asObservable()

        suspend fun clearKey() {
            dataStore.edit {
                it.remove(key = dataKey)
            }
        }

    }

    class StringSetData(
        val dataStore: DataStore<Preferences>,
        key: String,
        val defaultValue: Set<String>?
    ) {
        val dataKey = stringSetPreferencesKey(key)

        suspend fun put(value: Set<String>) {
            dataStore.edit { it[dataKey] = value }
        }

        suspend fun getOnce(): Set<String>? {
            return getFlow().first()
        }

        fun getFlow(): Flow<Set<String>?> {
            return dataStore.data
                .map { it[dataKey] ?: defaultValue }
                .catch {
                    BLog.d(TAG, "catch", it)
                    emit(defaultValue)
                }
        }

        fun getRxObservable(): Observable<Set<String>> = getFlow().filterNotNull().asObservable()

        suspend fun clearKey() {
            dataStore.edit {
                it.remove(key = dataKey)
            }
        }

    }
}