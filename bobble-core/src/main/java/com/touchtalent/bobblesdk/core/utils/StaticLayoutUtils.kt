package com.touchtalent.bobblesdk.core.utils

import android.os.Build
import android.text.Layout
import android.text.StaticLayout
import android.text.TextPaint

fun newStaticLayout(text: CharSequence, textPaint: TextPaint, width: Int): StaticLayout {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        StaticLayout.Builder.obtain(
            text,
            0,
            text.length,
            textPaint,
            width
        ).setAlignment(Layout.Alignment.ALIGN_CENTER)
            .setLineSpacing(0F, 1F)
            .setIncludePad(true)
            .build()
    } else {
        StaticLayout(
            text,
            textPaint,
            width,
            Layout.Alignment.ALIGN_CENTER,
            1F,
            0F,
            true
        )
    }
}