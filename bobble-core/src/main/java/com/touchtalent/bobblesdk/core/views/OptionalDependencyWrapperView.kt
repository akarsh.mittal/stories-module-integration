package com.touchtalent.bobblesdk.core.views

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.core.content.res.getStringOrThrow
import com.touchtalent.bobblesdk.core.R

/**
 * Wrapper view to include Views from loosely coupled modules in XML.
 * Use attribute [R.styleable.OptionalDependencyWrapperView_odwClass] to specify the fully qualified
 * class name of the View to be populated. The View will be populated and added to this FrameLayout,
 * if exists. Use [getDependentView] to fetch the instance of the view
 */
class OptionalDependencyWrapperView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null
) : FrameLayout(context, attrs) {

    private var dependentView: View? = null

    init {
        val typedArray =
            context.obtainStyledAttributes(attrs, R.styleable.OptionalDependencyWrapperView)
        val className =
            typedArray.getStringOrThrow(R.styleable.OptionalDependencyWrapperView_odwClass)
        try {
            dependentView = Class.forName(className)
                .getConstructor(Context::class.java, AttributeSet::class.java)
                .newInstance(context, attrs) as View?
            addView(dependentView)
            dependentView?.layoutParams?.height = ViewGroup.LayoutParams.MATCH_PARENT
            dependentView?.layoutParams?.width = ViewGroup.LayoutParams.MATCH_PARENT
        } catch (_: Exception) {
        }
        typedArray.recycle()
    }

    override fun setVisibility(visibility: Int) {
        super.setVisibility(visibility)
        dependentView?.visibility = visibility
    }

    /**
     * Get the instance of the view, if it was successfully inflated via reflection.
     * @param interfaceType Interface class in core module that the View implements
     * @return The implementation of the view, if inflation was successful, null otherwise
     */
    fun <T> getDependentView(interfaceType: Class<T>): T? {
        return runCatching { interfaceType.cast(dependentView) }.getOrNull()
    }

    fun isDependentViewAvailable() = dependentView != null
}