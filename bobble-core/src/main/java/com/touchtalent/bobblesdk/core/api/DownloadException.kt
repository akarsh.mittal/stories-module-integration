package com.touchtalent.bobblesdk.core.api

import retrofit2.HttpException
import retrofit2.Response

/**
 * Custom Exception class that provides [downloadStats] with exception
 * @see DownloadStats
 */

class DownloadException(val error: Throwable, val downloadStats: DownloadStats? = null) :
    Exception(error) {
    constructor(errorMsg: String) : this(Exception(errorMsg))
    constructor(response: Response<*>) : this(HttpException(response))

    override val cause: Throwable
        get() = error
    override val message: String
        get() = "{\"errorMsg\":\"${super.message}\", \"bytesDownloaded\": ${downloadStats?.bytesDownloaded ?: -1}, \"totalBytesCount\": ${downloadStats?.totalBytesCount ?: -1}}"
}


/**
 * Represent intermediate status of Download
 * @property bytesDownloaded number of bytes downloaded
 * @property totalBytesCount total number of bytes a file contains, -1 in case of Unknown
 */
data class DownloadStats(
    val bytesDownloaded: Long,
    val totalBytesCount: Long
)