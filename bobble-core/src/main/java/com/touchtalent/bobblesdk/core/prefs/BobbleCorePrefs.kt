package com.touchtalent.bobblesdk.core.prefs

import android.content.Context
import android.content.SharedPreferences
import com.touchtalent.bobblesdk.core.BobbleCoreSDK

/**
 * @suppress ignore documentation since this is internal class
 */
object BobbleCorePrefs {
    private const val PREF_NAME = "core_prefs"
    private var editor: SharedPreferences.Editor
    private var sharedPreferences: SharedPreferences =
        BobbleCoreSDK.applicationContext.getSharedPreferences(
            PREF_NAME,
            Context.MODE_PRIVATE
        )

    private const val LAST_TIME_API_CALLED = "lat_api_"

    init {
        editor = sharedPreferences.edit()
    }

    fun getLastCalledTime(id: String): Long {
        return sharedPreferences.getLong("${LAST_TIME_API_CALLED}_$id", 0)
    }

    fun putLastCalledTime(id: String, lastCalledTime: Long) {
        editor.putLong("${LAST_TIME_API_CALLED}_$id", lastCalledTime).apply()
    }

}