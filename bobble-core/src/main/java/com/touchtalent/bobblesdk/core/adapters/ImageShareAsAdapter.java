package com.touchtalent.bobblesdk.core.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Handler;
import android.os.Looper;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.touchtalent.bobblesdk.core.R;
import com.touchtalent.bobblesdk.core.databinding.ItemBobbleCoreShareBinding;
import com.touchtalent.bobblesdk.core.views.StressProofClickListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Implementation of {@link RecyclerView.Adapter} to create a "Share to app" panel. Populates the
 * recycler view with icon of the app and its name. Initialise this with the {@link Intent} which
 * needs to be queried for. Make sure to add the &lt;queries&gt; tag in manifest for the corresponding
 * Intent.
 * The app list is fetched asynchronously and a progress bar might be required to while the data is
 * being loaded.
 * <p>Use {@link ImageShareAsAdapter#setSharingInterface(SharingOptionInterface)} to listen for changes</p>
 */
public class ImageShareAsAdapter extends RecyclerView.Adapter<ImageShareAsAdapter.ViewHolder> {
    protected static final String TAG = "ImageShareAsAdapter";
    private final PackageManager mPackageManager;
    private final Handler mDelayedHandler = new Handler(Looper.getMainLooper());
    private final SparseArray<Disposable> disposables = new SparseArray<>();
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();
    private final Intent intent;
    private final boolean includeSaveInGallery;
    private final View.OnAttachStateChangeListener detachListener = new View.OnAttachStateChangeListener() {
        @Override
        public void onViewAttachedToWindow(View v) {

        }

        @Override
        public void onViewDetachedFromWindow(View v) {
            onDestroy();
        }
    };
    protected Context context;
    private SharingOptionInterface sharingOptionInterface;
    private List<ResolveInfo> dataList;

    /**
     * @param context              Context
     * @param intent               Intent with type and data set which would be used to query list of apps
     * @param includeSaveInGallery Whether or not include "Save to gallery" as the first item in the list
     */
    public ImageShareAsAdapter(final Context context,
                               Intent intent,
                               boolean includeSaveInGallery) {
        this.context = context;
        mPackageManager = context.getPackageManager();
        this.intent = intent;
        this.includeSaveInGallery = includeSaveInGallery;
    }

    // Query permissions to be provided by the client,
    // NotifyDataSetChanged is OK since this is first time load
    @SuppressLint({"QueryPermissionsNeeded", "NotifyDataSetChanged"})
    private Runnable getRunnable() {
        return new Runnable() {
            @Override
            public void run() {
                compositeDisposable.add(
                        Single.fromCallable(() -> {
                            List<ResolveInfo> list;
                            list = mPackageManager.queryIntentActivities(intent, 0);
                            sortResolveInfoList(list);
                            return list;
                        }).subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(list -> {
                                    dataList = list;
                                    notifyDataSetChanged();
                                })
                );
                mDelayedHandler.removeCallbacks(this);
                if (sharingOptionInterface != null)
                    sharingOptionInterface.onLoadCompleted();
            }
        };
    }

    private void sortResolveInfoList(List<ResolveInfo> list) {
        List<String> orderOfApps = getAppList();
        Collections.sort(list, (o1, o2) -> {
            int index1 = orderOfApps.indexOf(o1.activityInfo.packageName);
            int index2 = orderOfApps.indexOf(o2.activityInfo.packageName);
            if (index1 == -1 && index2 == -1)
                return 0;
            else if (index1 == -1)
                return 1;
            else if (index2 == -1)
                return -1;
            else return index1 - index2;
        });
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        // Create Handler for Prevent DeadSystemException
        // It cause due to PackageInfo calls and system is busy in processing the multiple
        // request of monkey operations which cause DeadSystemException
        mDelayedHandler.removeCallbacksAndMessages(null);
        mDelayedHandler.postDelayed(getRunnable(), 1000);
        recyclerView.addOnAttachStateChangeListener(detachListener);
    }

    @Override
    public void onDetachedFromRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
        recyclerView.removeOnAttachStateChangeListener(detachListener);
        onDestroy();
    }

    /**
     * Set interface for listening to click actions
     *
     * @param sharingInterface Interface for listening to actions
     */
    public void setSharingInterface(SharingOptionInterface sharingInterface) {
        this.sharingOptionInterface = sharingInterface;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new ViewHolder(ItemBobbleCoreShareBinding.inflate(inflater, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (includeSaveInGallery && position == 0) {
            onBindViewHolderSaveInGallery(holder);
        }
        int adjustedPosition = position;
        if (includeSaveInGallery)
            adjustedPosition--;
        if (adjustedPosition >= 0)
            onBindViewHolderApps(holder, adjustedPosition);
    }

    private void onBindViewHolderSaveInGallery(ViewHolder holder) {
        holder.binding.icon.setBackgroundResource(R.drawable.ic_bobble_core_round_background);
        holder.binding.icon.setImageResource(R.drawable.ic_bobble_core_save_in_gallery);
        holder.binding.name.setText(R.string.bobble_core_save_in_gallery);
        holder.binding.getRoot().setOnClickListener(new StressProofClickListener() {
            @Override
            public void onHandleClick(@NonNull View view) {
                if (sharingOptionInterface != null)
                    sharingOptionInterface.onSaveToGallery();
            }
        });
    }

    private void onBindViewHolderApps(ViewHolder holder, int position) {
        ResolveInfo info = dataList.get(position);
        Disposable oldDisposable = disposables.get(position);
        if (oldDisposable != null)
            oldDisposable.dispose();
        disposables.put(position,
                Single.fromCallable(() -> info.loadIcon(mPackageManager))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(drawable -> {
                            holder.binding.icon.setImageDrawable(drawable);
                        })
        );
        holder.binding.icon.setBackgroundResource(0);
        holder.binding.name.setText(dataList.get(position).loadLabel(mPackageManager));
        holder.binding.containerLayout.setOnClickListener(new StressProofClickListener() {
            @Override
            public void onHandleClick(@NonNull View view) {
                if (sharingOptionInterface != null)
                    sharingOptionInterface.onOptionSelected(info);
            }
        });
    }

    @Override
    public int getItemCount() {
        return (dataList != null ? dataList.size() : 0) + (includeSaveInGallery ? 1 : 0);
    }

    private void onDestroy() {
        for (int i = 0; i < disposables.size(); i++) {
            int key = disposables.indexOfKey(i);
            disposables.get(key).dispose();
        }
    }

    //Used to order the list of apps when sharing
    private List<String> getAppList() {
        return new ArrayList<>(Arrays.asList("com.whatsapp",
                "com.gbwhatsapp",
                "jp.naver.line.android",
                "com.instagram.android",
                "com.facebook.orca",
                "com.facebook.katana",
                "org.telegram.messenger",
                "com.facebook.mlite",
                "com.facebook.lite",
                "com.snapchat.android",
                "com.tencent.mm",
                "com.twitter.android",
                "in.mohalla.sharechat",
                "com.google.android.apps.messaging",
                "com.google.android.talk",
                "com.whatsapp.w4b",
                "com.promessage.message",
                "com.google.android.gm",
                "com.google.android.gm.lite",
                "com.samsung.android.email.provider",
                "com.yahoo.mobile.client.android.mail",
                "com.tencent.mobileqq",
                "org.thunderdog.challegram",
                "com.azarlive.android",
                "com.viber.voip"));
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        ItemBobbleCoreShareBinding binding;

        ViewHolder(ItemBobbleCoreShareBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public abstract static class SharingOptionInterface {
        /**
         * Callback when an app is selected
         *
         * @param resolveInfo {@link ResolveInfo} of the application selected
         */
        public abstract void onOptionSelected(ResolveInfo resolveInfo);

        /**
         * Callback when loading of app list is completed
         */
        public void onLoadCompleted() {
        }

        /**
         * Callback when save to gallery is clicked.
         */
        public void onSaveToGallery() {
        }
    }
}
