package com.touchtalent.bobblesdk.core.utils

import java.util.*

fun String.toLocale(): Locale? {
    val localeParams =
        split("_".toRegex(), limit = 3).toTypedArray()
    return when (localeParams.size) {
        1 -> Locale(localeParams[0])
        2 -> Locale(localeParams[0], localeParams[1])
        3 -> Locale(localeParams[0], localeParams[1], localeParams[2])
        else -> null
    }
}