package com.touchtalent.bobblesdk.core.interfaces

interface WebViewCacheConfigSettingsInterface {

    fun getWebViewPreCacheMaxSizeInKb() : Long

    fun shouldEnableWebViewCaching() : Boolean
}