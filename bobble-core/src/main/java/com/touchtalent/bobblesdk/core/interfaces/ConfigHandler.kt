package com.touchtalent.bobblesdk.core.interfaces

import org.json.JSONObject

interface ConfigHandler {
    suspend fun handleConfig(response: JSONObject)
}