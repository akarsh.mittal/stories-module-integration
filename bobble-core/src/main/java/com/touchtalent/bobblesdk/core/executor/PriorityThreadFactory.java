package com.touchtalent.bobblesdk.core.executor;

import android.os.Process;

import java.util.concurrent.ThreadFactory;

/**
 * Created by Prashant Gupta on 24-09-2016.
 */

public class PriorityThreadFactory implements ThreadFactory {

    private final int threadPriority;
    private String name;
    private int counter = 0;

    public PriorityThreadFactory(int threadPriority) {
        this.threadPriority = threadPriority;
    }

    public PriorityThreadFactory(String name, int threadPriority) {
        this.threadPriority = threadPriority;
        this.name = name;
    }

    @Override
    public Thread newThread(final Runnable runnable) {
        Runnable wrapperRunnable = () -> {
            try {
                Process.setThreadPriority(threadPriority);
            } catch (Throwable ignored) {
            }
            runnable.run();
        };
        if (name != null)
            return new Thread(wrapperRunnable, name + "-" + counter++);
        return new Thread(wrapperRunnable);
    }

}
