package com.touchtalent.bobblesdk.core.utils

import android.net.Uri

/**
 * created by anil_
 * on 23/02/23
 * at 9:32 AM
 */

fun Uri.getQueryParamsMap(): Map<String, String> {
    return kotlin.runCatching {
        val map = hashMapOf<String, String>()
        for (param in queryParameterNames) {
            map[param] = getQueryParameter(param)?.let {
                if (it.equals("null", ignoreCase = true))
                    return@let null
                return@let it
            } ?: ""
        }
        return@runCatching map
    }.getOrElse { hashMapOf() }
}

fun Uri.getUriPath(): String = kotlin.runCatching {
    path?.split("/")?.let {
        if (it.size >= 2) {
            return@let it[1]
        }
        return@let null
    } ?: ""
}.getOrElse { "" }

fun Uri.getSubPath(): String = kotlin.runCatching {
    path?.split("/")?.let {
        if (it.isEmpty())
            return@let null

        if (it.size >= 3)
            return@let it[2]

        return@let null
    } ?: ""
}.getOrElse { "" }

fun Uri.getSubPathList(): List<String> =
    kotlin.runCatching { getSubPath().split("/") }.getOrElse { emptyList() }
