package com.touchtalent.bobblesdk.core.views

import android.view.View
import com.touchtalent.bobblesdk.core.utils.GeneralUtils

/**
 * Click listener that adds a delay of certain time between any 2 consecutive clicks. This prevents
 * monkey like operation to trigger rare race conditions
 */
abstract class StressProofClickListener : View.OnClickListener {
    override fun onClick(view: View) {
        if (GeneralUtils.canProceedWithClick())
            onHandleClick(view)
    }

    abstract fun onHandleClick(view: View)
}