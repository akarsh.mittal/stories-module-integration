package com.touchtalent.bobblesdk.core.utils

import android.graphics.Paint
import android.os.Build
import java.text.BreakIterator
import java.util.regex.Pattern
import kotlin.math.roundToInt

object TextUtil {


    fun getTextWidth(text: CharSequence, paint: Paint): Int {
        val length = text.length
        val widths = FloatArray(length)
        val count: Int = paint.getTextWidths(text, 0, length, widths)
        var width = 0
        for (i in 0 until count) {
            width += (widths[i] + 0.5f).roundToInt()
        }
        return width
    }

    var breakIterator: BreakIterator = BreakIterator.getCharacterInstance()

    fun getLengthWithUnicode(value: String?): Int {
        breakIterator.setText(value)
        var count = 0
        while (breakIterator.next() != BreakIterator.DONE) {
            count++
        }
        return count
    }

    /**
     * Return actual length of string with unicode use it above N
     *
     * @param s need string contain emoji or other unicode
     * @return length of string
     */
    fun codePointsLength(s: String): Int {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            s.codePoints().count().toInt()
        else
            getLengthWithUnicode(s)
    }


    fun extractEmojisFromText(text: String): List<String> {
        val pattern = Pattern.compile(
            "(?:[\\u2700-\\u27bf]|" +
                    "(?:[\\ud83c\\udde6-\\ud83c\\uddff]){2}|" +
                    "[\\ud800\\udc00-\\uDBFF\\uDFFF]|[\\u2600-\\u26FF])[\\ufe0e\\ufe0f]?(?:[\\u0300-\\u036f\\ufe20-\\ufe23\\u20d0-\\u20f0]|[\\ud83c\\udffb-\\ud83c\\udfff])?" +
                    "(?:\\u200d(?:[^\\ud800-\\udfff]|" +
                    "(?:[\\ud83c\\udde6-\\ud83c\\uddff]){2}|" +
                    "[\\ud800\\udc00-\\uDBFF\\uDFFF]|[\\u2600-\\u26FF])[\\ufe0e\\ufe0f]?(?:[\\u0300-\\u036f\\ufe20-\\ufe23\\u20d0-\\u20f0]|[\\ud83c\\udffb-\\ud83c\\udfff])?)*|" +
                    "[\\u0023-\\u0039]\\ufe0f?\\u20e3|\\u3299|\\u3297|\\u303d|\\u3030|\\u24c2|[\\ud83c\\udd70-\\ud83c\\udd71]|[\\ud83c\\udd7e-\\ud83c\\udd7f]|\\ud83c\\udd8e|[\\ud83c\\udd91-\\ud83c\\udd9a]|[\\ud83c\\udde6-\\ud83c\\uddff]|[\\ud83c\\ude01-\\ud83c\\ude02]|\\ud83c\\ude1a|\\ud83c\\ude2f|[\\ud83c\\ude32-\\ud83c\\ude3a]|[\\ud83c\\ude50-\\ud83c\\ude51]|\\u203c|\\u2049|[\\u25aa-\\u25ab]|\\u25b6|\\u25c0|[\\u25fb-\\u25fe]|\\u00a9|\\u00ae|\\u2122|\\u2139|\\ud83c\\udc04|[\\u2600-\\u26FF]|\\u2b05|\\u2b06|\\u2b07|\\u2b1b|\\u2b1c|\\u2b50|\\u2b55|\\u231a|\\u231b|\\u2328|\\u23cf|[\\u23e9-\\u23f3]|[\\u23f8-\\u23fa]|\\ud83c\\udccf|\\u2934|\\u2935|[\\u2190-\\u21ff]"
        )
        val matcher = pattern.matcher(text)
        val matchList: MutableList<String> = ArrayList()
        while (matcher.find()) {
            val emoji = matcher.group()
            if (emoji.isNotEmpty())
                matchList.add(emoji)
        }
        return matchList
    }

    @JvmStatic
    fun String?.isStartWithNumber(): Boolean {
        if (this.isNullOrEmpty())
            return false
        return this[0].isDigit()
    }

    @JvmStatic
    fun String?.isValidEmail(): Boolean {
        if (this.isNullOrEmpty())
            return false
        return android.util.Patterns.EMAIL_ADDRESS.matcher(this).matches()
    }

    fun String.isOnlyEmojis():Boolean{
        val emojis = extractEmojisFromText(this)
        return emojis.sumOf { it.length } == length
    }
}