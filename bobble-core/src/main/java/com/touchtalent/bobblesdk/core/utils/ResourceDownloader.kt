package com.touchtalent.bobblesdk.core.utils

import com.androidnetworking.AndroidNetworking
import com.touchtalent.bobblesdk.core.BobbleCoreSDK
import com.touchtalent.bobblesdk.core.api.CoreRetrofitClient
import com.touchtalent.bobblesdk.core.prefs.ResourcesCacheDatastore
import com.touchtalent.bobblesdk.core.utils.ResourceDownloader.downloadResourcesSuspend
import com.touchtalent.bobblesdk.core.utils.ResourceDownloader.restrictCacheDir
import io.reactivex.Completable
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.*
import java.io.File
import java.io.FileOutputStream
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

/**
 * Utility class to download and cache resources from server.
 * 1. Ensures zero redundancy - If a request is already in flight repeat request for same resource
 * will wait for the previous network call to finish
 * 2. Synchronous and asynchronous support.
 * 3. Timeout support
 * 4. Support resources from assets
 * 5. Support ZIP assets - automatic unzipping and cache detection
 * 6. Cache Management - LRU based cache system
 *
 * ### Cache management -
 * While requesting for resources via [downloadResourcesSuspend], its possible to register the
 * destination path for a Cache Group.
 * A cache group is group of folders - a combination of which represents a LRU cache folder.
 *
 * Every file accessed through [downloadResourcesSuspend] (and cacheKey not null) is added as a
 * cache entry, registered for the specified cache group, along with its last used timestamp.
 * Calling [restrictCacheDir] will cause the cache group to trim down to a specified size.
 */
object ResourceDownloader {

    private val queue = ConcurrentHashMap<String, CountDownLatch>()
    private val jobQueue = ConcurrentHashMap<String, Deferred<String?>>()
    private val lifetimeCoroutineScope = CoroutineScope(Dispatchers.IO)

    internal data class CacheEntry(val filePath: String, val lastAccessedTime: Long)

    /**
     * Trims the specified cache group to specified size.
     */
    suspend fun restrictCacheDir(
        key: String,
        sizeInBytes: Long
    ) = lifetimeCoroutineScope.launch {
        require(sizeInBytes > 0) { "Size must be greater than 0" }
        val allPath = ResourcesCacheDatastore.getCachePathMap(key)
        val fileMap = mutableListOf<CacheEntry>()
        var totalFileSize = 0L
        BLog.d("restrictCacheDir", "allPaths: $allPath")
        allPath.forEach { dirPath ->
            totalFileSize += FileUtil.size(dirPath)
        }
        BLog.d("restrictCacheDir", "totalFileSize: $totalFileSize")
        if (totalFileSize < sizeInBytes)
            return@launch
        allPath.forEach { dirPath ->
            val files = File(dirPath).listFiles() ?: return@forEach
            files.forEach internalForEach@{
                if (it.name.endsWith(".0")) {
                    val lastAccessedTime =
                        FileUtil.readFile(it.absolutePath)?.toLongOrNull()
                            ?: return@internalForEach
                    val originalFile = it.absolutePath.removeSuffix(".0")
                    fileMap.add(CacheEntry(originalFile, lastAccessedTime))
                }
            }
        }

        fileMap.sortByDescending { it.lastAccessedTime }

        while (totalFileSize > sizeInBytes) {
            if (fileMap.isEmpty()) break
            val lastAccessedItem = fileMap.removeLast()
            val filePathToDelete = lastAccessedItem.filePath
            val fileMetaData = "${lastAccessedItem.filePath}.0"
            val sizeReduced = FileUtil.size(filePathToDelete)
            totalFileSize -= sizeReduced
            if (!FileUtil.delete(filePathToDelete))
                createError<Unit>(Exception("Unable to delete $filePathToDelete"))
            if (!FileUtil.delete(fileMetaData))
                createError<Unit>(Exception("Unable to delete $fileMetaData"))
        }
        BLog.d("restrictCacheDir", "totalFileSize after cleaning: $totalFileSize")
    }

    /**
     * Downloads given [url] at given [destDir]. If a request for same URL is already in flight,
     * this function would wait for the response of the previous call preventing multiple downloads
     * of the same URL.
     *
     * URL to local files in assets are also supported. The url should be of the format -
     * "file:///android_asset/{path inside assets folder}". The file from asset is copied to the
     * dest path and new path returned
     *
     * @param url URL to download
     * @param destDir Absolute path of directory where url should be downloaded
     * @param timeoutInMillis timeout to wait if the function is run in blocking mode
     * @param checkCache true if the folder should be checked for previous download in destDir
     * @param cacheKey Cache key for enabling LRU cache on the destination folder, the [destDir]
     * will be registered with this key and cache will be trimmed inside it when [restrictCacheDir]
     * is invoked
     * @param forceCache true if this function should return success only in case of cache hit (the
     * download will still continue on background, it just won't suspend the function and return)
     *
     * @return a pair of path of the downloaded/copied resource if download/copy succeeds, null otherwise
     * to cacheStatus, false if from cache, true if from internet, false otherwise (from cache),
     */
    suspend fun downloadResourcesSuspend(
        url: String,
        destDir: String,
        timeoutInMillis: Long = -1,
        checkCache: Boolean = true,
        cacheKey: String? = null,
        forceCache: Boolean = false
    ): Pair<String?, Boolean> = withContext(Dispatchers.IO) {
        if (checkCache || forceCache) {
            val path = FileUtil.join(destDir, FileUtil.getFileNameFromUrl(url))
                .removeSuffix(".zip")
            if (FileUtil.exists(path)) {
                cacheKey?.let { createLastAccessedTimeFile(it, path) }
                return@withContext path to false
            }
        }

        //Create destination directory if doesn't exists
        if (!FileUtil.exists(destDir)) {
            FileUtil.createDirAndGetPath(destDir)
        }

        val job = synchronized(jobQueue) {
            jobQueue[url] ?: run {
                val newJob = lifetimeCoroutineScope.async {
                    val returnVal = downloadResourceInternalSuspend(url, destDir)
                    @Suppress("DeferredResultUnused")
                    jobQueue.remove(url)
                    return@async returnVal
                }
                jobQueue[url] = newJob
                newJob
            }
        }
        return@withContext if (forceCache) {
            null to false
        } else {
            if (timeoutInMillis == -1L)
                job.await()
            else withTimeout(timeoutInMillis) {
                job.await()
            }
            val fileName = FileUtil.getFileNameFromUrl(url) ?: return@withContext null to true
            val file = FileUtil.join(destDir, fileName).removeSuffix(".zip")
            return@withContext if (FileUtil.exists(file)) {
                cacheKey?.let { createLastAccessedTimeFile(it, file) }
                file to true
            } else null to true
        }
    }

    suspend fun scanNewFile(filePath: String, cacheKey: String) = withContext(Dispatchers.IO) {
        val lastAccessedFileName = "$filePath.0"

        if (FileUtil.exists(filePath) && !FileUtil.exists(lastAccessedFileName)) {
            createLastAccessedTimeFile(cacheKey, filePath)
        }
    }

    private suspend fun createLastAccessedTimeFile(cacheKey: String, path: String) {
        val lastAccessedFileSize = "$path.0"
        FileUtil.getParent(path)?.let { registerForClearance(cacheKey, it) }
        FileUtil.create(lastAccessedFileSize)
        FileUtil.write(lastAccessedFileSize, System.currentTimeMillis().toString())
    }

    private suspend fun registerForClearance(cacheKey: String, parent: String) {
        ResourcesCacheDatastore.addPathToCacheGroup(cacheKey, parent)
    }

    private suspend fun downloadResourceInternalSuspend(
        url: String,
        destDir: String,
    ): String? = withContext(Dispatchers.IO) {
        //Check if the URL is asset path
        if (url.startsWith("file:///android_asset/")) {
            val assetPath = url.removePrefix("file:///android_asset/")
            val fileName = FileUtil.getFileNameFromUrl(url)
            val filePath = FileUtil.join(destDir, fileName)
            return@withContext runCatching {
                BobbleCoreSDK.applicationContext.assets.open(assetPath).use { inputStream ->
                    FileOutputStream(filePath).use { outputStream ->
                        if (FileUtil.copy(inputStream, outputStream))
                            filePath
                        else null
                    }
                }
            }.onFailure {
                BLog.printStackTrace(it)
            }.getOrNull()
        }
        if (!NetworkUtil.isInternetConnected(BobbleCoreSDK.applicationContext))
            return@withContext null
        val fileName = FileUtil.getFileNameFromUrl(url) ?: return@withContext null
        val tmpFileName = "${fileName}.tmp"
        runCatching {
            CoreRetrofitClient.downloadFile(url, destDir, tmpFileName).getOrThrow()
        }.onFailure {
            BLog.w("ResourceDownloader", "Failed to download: $url", it)
            return@withContext null
        }
        if (url.endsWith(".zip")) {
            val zipFile = FileUtil.join(destDir, tmpFileName)
            val unzipDirectory = FileUtil.join(destDir, "${fileName.removeSuffix(".zip")}.tmp")
            val unzipSuccess = ZipUtil.unzip(zipFile, unzipDirectory)
            if (!unzipSuccess)
                return@withContext null
            FileUtil.rename(unzipDirectory, unzipDirectory.removeSuffix(".tmp"))
            return@withContext unzipDirectory.removeSuffix(".tmp")
        } else {
            val origFileName = FileUtil.join(destDir, tmpFileName)
            val newFileName = FileUtil.join(destDir, fileName)
            FileUtil.rename(origFileName, newFileName)
            return@withContext newFileName
        }
    }

    /**
     * Same as [downloadResourcesSuspend] but lacks
     * 1. Cache status as return param
     * 2. LRU cache support
     * 3. Blocks current thread until response.
     */
    @Deprecated(message = "Use downloadResourcesSuspend instead")
    fun downloadResource(
        url: String,
        destDir: String,
        async: Boolean = false,
        timeoutInMillis: Long = -1,
        checkCache: Boolean = false,
        forceCache: Boolean = false
    ): String? {
        if (checkCache || forceCache) {
            val path = FileUtil.join(destDir, FileUtil.getFileNameFromUrl(url))
                .removeSuffix(".zip")
            if (FileUtil.exists(path))
                return path
        }

        val queuedRequestCountDownLatch = queue[url] ?: run {
            val countDownLatch = CountDownLatch(1)
            queue[url] = countDownLatch
            Completable.fromRunnable {
                downloadResourceInternal(url, destDir)
            }.doOnComplete {
                queue.remove(url)?.countDown()
            }.doOnError {
                queue.remove(url)?.countDown()
            }.subscribeOn(Schedulers.io())
                .subscribe()
            return@run countDownLatch
        }
        return if (async || forceCache) {
            null
        } else {
            return if (!async) {
                try {
                    if (timeoutInMillis == -1L)
                        queuedRequestCountDownLatch.await()
                    else queuedRequestCountDownLatch.await(timeoutInMillis, TimeUnit.MILLISECONDS)
                } catch (e: InterruptedException) {
                    BLog.printStackTrace(e)
                }
                val fileName = FileUtil.getFileNameFromUrl(url) ?: return null
                val file = FileUtil.join(destDir, fileName).removeSuffix(".zip")
                return if (FileUtil.exists(file)) file
                else null
            } else null
        }
    }

    private fun downloadResourceInternal(
        url: String,
        destDir: String,
    ): String? {
        //Check if the URL is asset path
        if (url.startsWith("file:///android_asset/")) {
            val assetPath = url.removePrefix("file:///android_asset/")
            val fileName = FileUtil.getFileNameFromUrl(url)
            val filePath = FileUtil.join(destDir, fileName)
            try {
                BobbleCoreSDK.applicationContext.assets.open(assetPath).use { inputStream ->
                    FileOutputStream(filePath).use { outputStream ->
                        return if (FileUtil.copy(inputStream, outputStream))
                            filePath
                        else null
                    }
                }
            } catch (e: Exception) {
                BLog.printStackTrace(e)
                return null
            }
        }
        if (!NetworkUtil.isInternetConnected(BobbleCoreSDK.applicationContext))
            return null
        val fileName = FileUtil.getFileNameFromUrl(url) ?: return null
        val tmpFileName = "${fileName}.tmp"
        val request = AndroidNetworking
            .download(url, destDir, tmpFileName)
            .doNotCacheResponse()
            .build()
        val response = try {
            request.executeForDownload()
        } catch (e: Exception) {
            null
        }
        if (response?.isSuccess != true) {
            BLog.printStackTrace(Exception("Failed to download resource: $url"))
            return null
        }
        if (url.endsWith(".zip")) {
            val zipFile = FileUtil.join(destDir, tmpFileName)
            val unzipDirectory = FileUtil.join(destDir, "${fileName.removeSuffix(".zip")}.tmp")
            val unzipSuccess = ZipUtil.unzip(zipFile, unzipDirectory)
            if (!unzipSuccess)
                return null
            FileUtil.rename(unzipDirectory, unzipDirectory.removeSuffix(".tmp"))
            return unzipDirectory.removeSuffix(".tmp")
        } else {
            val origFileName = FileUtil.join(destDir, tmpFileName)
            val newFileName = FileUtil.join(destDir, fileName)
            FileUtil.rename(origFileName, newFileName)
            return newFileName
        }
    }

}