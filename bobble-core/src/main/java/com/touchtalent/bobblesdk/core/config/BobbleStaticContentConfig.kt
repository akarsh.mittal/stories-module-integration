package com.touchtalent.bobblesdk.core.config

import androidx.annotation.DrawableRes
import androidx.annotation.RawRes

/**
 * Config class for *bobble-content* module
 * @property seededContentJson Raw resource path for JSON for seeded content.
 * @property seededWatermark Drawable resource path for default watermark.
 */
class BobbleStaticContentConfig {
    @RawRes
    var seededContentJson: Int = 0

    @DrawableRes
    var seededWatermark: Int = 0

    var enableAutoDownload: Boolean = true
}