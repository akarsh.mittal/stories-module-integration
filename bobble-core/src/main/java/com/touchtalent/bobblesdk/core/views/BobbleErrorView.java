package com.touchtalent.bobblesdk.core.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;

import androidx.annotation.ColorInt;
import androidx.annotation.DrawableRes;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.graphics.drawable.DrawableCompat;

import com.bumptech.glide.Glide;
import com.touchtalent.bobblesdk.core.R;
import com.touchtalent.bobblesdk.core.databinding.LayoutBobbleErrorViewBinding;

/**
 * Common error view supporting an error image, title, subtitle and a retry button.
 * To be used for all error cases for a uniform UI across modules. All params can be set via
 * attributes, or via code.
 * <p>
 * All UI elements are hidden until and unless values are provided for them
 */
public class BobbleErrorView extends ConstraintLayout {

    LayoutBobbleErrorViewBinding binding;

    public BobbleErrorView(Context context) {
        super(context);
        init(null);
    }

    public BobbleErrorView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public BobbleErrorView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(@Nullable AttributeSet attrs) {
        binding = LayoutBobbleErrorViewBinding.inflate(LayoutInflater.from(getContext()), this);
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.BobbleErrorView);
        int errorResource = typedArray.getResourceId(R.styleable.BobbleErrorView_bevErrorResource, 0);
        setErrorResource(errorResource);
        int color = typedArray.getColor(R.styleable.BobbleErrorView_bevPrimaryColor, Color.RED);
        setRetryButtonColor(color);
        color = typedArray.getColor(R.styleable.BobbleErrorView_bevErrorTitleColor, Integer.MIN_VALUE);
        if (color != Integer.MIN_VALUE)
            setTitleTextColor(color);
        color = typedArray.getColor(R.styleable.BobbleErrorView_bevErrorSubtitleColor, Integer.MIN_VALUE);
        if (color != Integer.MIN_VALUE)
            setSubtitleTextColor(color);
        typedArray.recycle();
    }

    /**
     * Sets title text color for the error view
     *
     * @param color Integer value of the color to be set
     */
    public void setTitleTextColor(@ColorInt int color) {
        binding.errorTitle.setTextColor(color);
    }

    /**
     * Sets sub-title text color for the error view
     *
     * @param color Integer value of the color to be set
     */
    public void setSubtitleTextColor(@ColorInt int color) {
        binding.errorSubTitle.setTextColor(color);
    }

    /**
     * Sets the image for error. e.g - Bobble-girl in case of bobble, UFO logo in case of Mint
     *
     * @param errorResource Drawable resource of the image
     */
    public void setErrorResource(@DrawableRes int errorResource) {
        if (binding.errorImage == null)
            return;
        if (errorResource == 0) {
            binding.errorImage.setVisibility(GONE);
            return;
        }
        binding.errorImage.setVisibility(VISIBLE);
        Glide.with(this)
                .load(errorResource)
                .into(binding.errorImage);
    }

    /**
     * Sets the retry button color.
     *
     * @param color Integer value of the color to be set
     */
    private void setRetryButtonColor(@ColorInt int color) {
        DrawableCompat.setTint(binding.retryBtn.getBackground(), color);
    }

    /**
     * Sets the retry button text
     *
     * @param retryText Text for the retry button
     */
    public void setRetryButtonText(@StringRes int retryText) {
        binding.retryBtn.setText(retryText);
    }

    /**
     * Set click listener on retry button
     *
     * @param listener Click listener for callbacks
     */
    public void setRetryClickListener(OnClickListener listener) {
        binding.retryBtn.setVisibility(VISIBLE);
        binding.retryBtn.setOnClickListener(listener);
    }

    /**
     * Changes the text of retry button to "Loading..." and disables the button click
     */
    public void setLoading() {
        binding.retryBtn.setText(R.string.bobble_core_loading);
        binding.retryBtn.setClickable(false);
    }

    /**
     * Removes "loading..." from retry button and enables the button back
     */
    public void clearLoading() {
        binding.retryBtn.setText(R.string.bobble_core_retry);
        binding.retryBtn.setClickable(true);
    }

    /**
     * Set the title text
     */
    public void setTitleText(@StringRes int title) {
        binding.errorTitle.setVisibility(VISIBLE);
        binding.errorTitle.setText(title);
    }

    /**
     * Set the sub-title text
     */
    public void setSubtitleText(@StringRes int title) {
        binding.errorSubTitle.setVisibility(VISIBLE);
        binding.errorSubTitle.setText(title);
    }

    /**
     * Toggle image visibility
     *
     * @param show true if image needs to be visible, false otherwise
     */
    public void toggleImage(boolean show) {
        if (binding.errorImage != null)
            binding.errorImage.setVisibility(show ? VISIBLE : GONE);
    }

    /**
     * Sets the primary color, the color of retry button is overriden by this color
     *
     * @param primaryColor Integer value of the color to be set
     */
    public void setPrimaryColor(int primaryColor) {
        setRetryButtonColor(primaryColor);
    }
}
