package com.touchtalent.bobblesdk.core.interfaces

import android.content.Context
import com.touchtalent.bobblesdk.core.enums.FlowEvent
import com.touchtalent.bobblesdk.core.model.LoginMetadata
import kotlinx.coroutines.flow.Flow

interface OtherModuleLoginInterface {

    suspend fun onUserLoggedIn(login: LoginMetadata)

    fun openLoginActivity(context: Context)

    fun observeEvents() : Flow<FlowEvent>
}