package com.touchtalent.bobblesdk.core.cache;

import android.content.Context;
import android.os.Build;
import android.os.Environment;

import java.io.File;

import kotlin.Deprecated;

/**
 * Created by amitshekhar on 06/08/15.
 */
@Deprecated(message = "External files no more used after introduction of scoped storage")
public final class DiskUtils {


    public static final int IO_BUFFER_SIZE = 8 * 1024;

    private DiskUtils() {
        // This class in not publicly instantiable.
    }

    public static boolean isExternalStorageRemovable() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
            return Environment.isExternalStorageRemovable();
        }
        return true;
    }

    public static File getExternalCacheDir(Context context) {
        if (hasExternalCacheDir()) {
            return context.getExternalCacheDir();
        }

        // Before Froyo we need to construct the external cache dir ourselves
        final String cacheDir = "/Android/data/" + context.getPackageName() + "/cache/";
        return new File(Environment.getExternalStorageDirectory().getPath() + cacheDir);
    }

    public static boolean hasExternalCacheDir() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO;
    }
}
