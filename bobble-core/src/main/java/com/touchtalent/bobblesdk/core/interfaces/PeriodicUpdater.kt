package com.touchtalent.bobblesdk.core.interfaces

import com.touchtalent.bobblesdk.core.BobbleCoreSDK
import com.touchtalent.bobblesdk.core.prefs.BobbleCorePrefs
import com.touchtalent.bobblesdk.core.utils.NetworkUtil
import kotlinx.coroutines.launch

/**
 * In context of keyboard, keyboard close event is generally polled and checked for any pending
 * server sync event. This interface provides a mechanism for modules to hook keyboard close event
 * to perform periodic sync events.
 *
 * The implementation takes care of synchronising the updates, such that only one update call is
 * running at a particular point of time
 *
 * The implementation needs to make sure to call [registerSuccess] and [registerFailure] as the
 * result of update
 *
 * *Note:* This is not a replacement for WorkManager. Carrying out periodic events upon keyboard
 * close is convenient since it doesn't not involve any scheduling and is ensured to be always called.
 */
abstract class PeriodicUpdater {

    private var isUpdateOnGoing = false

    /**
     * Called by the parent app on keyboard close event. This might be called on a background
     * thread as well
     * @param force if the updates need to bypass the time constraint
     */
    fun update(force: Boolean) {
        BobbleCoreSDK.applicationScope.launch {
            val lastCalledTime = getLastCalledTime()
            val timeInterval = getTimeIntervalInMillis()
            if (requiresInternet() && !NetworkUtil.isInternetConnected(BobbleCoreSDK.applicationContext))
                return@launch

            if (!canProceed())
                return@launch

            if ((!bypassForce() && force) || System.currentTimeMillis() - lastCalledTime > timeInterval) {
                if (isUpdateOnGoing)
                    return@launch
                isUpdateOnGoing = true
                onUpdate()
            }
        }
    }

    private fun getLastCalledTime(): Long {
        return BobbleCorePrefs.getLastCalledTime(getId())
    }

    /**
     * @return true if this updater require and active Internet connection.
     */
    open fun requiresInternet() = true

    /**
     * @return true if this updater decides to bypass force in [update]
     */
    open fun bypassForce() = false


    /**
     * @return additional check if needed in [update]
     * Like KillSwitch for certain api
     */
    open suspend fun canProceed() = true

    /**
     * Id for this updater, used for storing prefs
     */
    abstract fun getId(): String

    /**
     * Get the interval in milliseconds at which this updater must be invoked
     */
    abstract suspend fun getTimeIntervalInMillis(): Long

    /**
     * Callback responsible for carrying out actual update work. Implementation should ensure
     * calling [registerSuccess]/[registerFailure]
     */
    abstract suspend fun onUpdate()

    /**
     * Mark update not in progress and update last called time
     */
    fun registerSuccess() {
        isUpdateOnGoing = false
        BobbleCorePrefs.putLastCalledTime(getId(), System.currentTimeMillis())
    }

    /**
     * Mark update not in progress and update last called time
     */
    fun registerFailure() {
        isUpdateOnGoing = false
        BobbleCorePrefs.putLastCalledTime(getId(), System.currentTimeMillis())
    }
}