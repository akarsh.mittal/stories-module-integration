package com.touchtalent.bobblesdk.core.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * This is a utility class for handling network data.
 *
 * Created by amitshekhar on 05/10/16.
 */

public final class NetworkUtil {

    private NetworkUtil() {
        // This utility class is not publicly instantiable.
    }

    /**
     * Check if user is connected to internet
     *
     * @param context Context
     * @return true if internet is connected, false otherwise
     */
    public static boolean isInternetConnected(Context context) {
        try {
            final ConnectivityManager cm = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            if (cm != null) {
                final NetworkInfo info = cm.getActiveNetworkInfo();
                return (info != null && info.isConnected());
            }
        } catch (Exception e) {
            BLog.printStackTrace(e);
        }
        return false;
    }

    /**
     * Check if user is connected to a WiFi
     *
     * @param context Context
     * @return true if WiFi is connected, false otherwise
     */
    public static boolean isWifiConnected(Context context) {
        final ConnectivityManager conMgr = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (conMgr != null) {
            final NetworkInfo wifi = conMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            return wifi != null && wifi.isConnected();
        }
        return false;
    }

}
