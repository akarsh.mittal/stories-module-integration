package com.touchtalent.bobblesdk.core.executor

import android.app.ActivityManager
import android.content.Context
import com.touchtalent.bobblesdk.core.BobbleCoreSDK
import com.touchtalent.bobblesdk.core.utils.BLog
import kotlin.math.ceil

/**
 * Decides the optimal number of threads required by an executor for computational work.
 * In certain low end devices like Oppo A15, a process is killed if CPU usage exceeds a certain ratio.
 * Hence, it is required to limit the thread count for devices like these.
 *
 * Optimal number is calculated based on RAM available and CPU cores available on the device.
 */
fun optimalThreadCountForComputationalParallelism(): Int {
    val noOfCores = Runtime.getRuntime().availableProcessors()
    BLog.d("Multithreading", "No of cores: $noOfCores")
    // Half the number of cores is an optimal number for threads
    var threadCount = noOfCores / 2
    // But in low end devices, CPU cores can be as high as 8, but RAM limited to 3GB.
    // For such configuration devices, no of threads should not exceed the RAM (in GB).
    // For e.g - for a device of 3GB RAM, total number of threads must not exceed 2 (1.5 rounded up
    // to nearest integer)
    val activityManager =
        BobbleCoreSDK.applicationContext.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
    val memoryInfo = ActivityManager.MemoryInfo()
    activityManager.getMemoryInfo(memoryInfo)
    val ram = ceil(memoryInfo.totalMem / (1024 * 1024 * 1024f))
    BLog.d("Multithreading", "RAM size: ${ram}GB")
    if (threadCount > ram) {
        threadCount = ceil(threadCount / 2f).toInt()
    }
    // The thread count must be a minimum of 2 to achieve parallelism, else it will end up being a
    // single threaded code
    return threadCount.coerceAtLeast(2)
}