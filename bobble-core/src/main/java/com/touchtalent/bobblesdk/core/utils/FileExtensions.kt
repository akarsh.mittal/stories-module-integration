package com.touchtalent.bobblesdk.core.utils


/**
 * created by anil_
 * on 21/03/23
 * at 7:02 PM
 */

const val GIF = "gif"
const val JPG = ".jpg"
const val JPEG = "jpeg"
const val PNG = "png"
const val WEBP = "webp"