package com.touchtalent.bobblesdk.core.moshi

import android.graphics.Color
import androidx.annotation.ColorInt
import com.squareup.moshi.FromJson
import com.squareup.moshi.ToJson

/**
 * Moshi adapter to convert hex color code (RGB or ARGB) to [ColorInt] and vice versa
 *
 * Also supports converting map of strings to map of colors
 * @see HexColor
 * @see HexColorMap
 */
class ColorAdapter {
    /**
     * @suppress
     */
    @ToJson
    fun toJson(@HexColor rgb: Int): String {
        return String.format("#%08x", rgb)
    }

    /**
     * @suppress
     */
    @FromJson
    @HexColor
    fun fromJson(rgb: String): Int {
        try {
            return Color.parseColor(rgb)
        } catch (e: java.lang.Exception) {
            return Color.parseColor("#ffffff")
        }
    }

    /**
     * @suppress
     */
    @ToJson
    fun toJson(@HexColor rgb: Int?): String? {
        return rgb?.let { toJson(it) }
    }

    /**
     * @suppress
     */
    @FromJson
    @HexColor
    fun fromJson(rgb: String?): Int? {
        try {
            return rgb?.let { fromJson(it) }
        } catch (e: java.lang.Exception) {
            return Color.parseColor("#ffffff")
        }
    }

    /**
     * @suppress
     */
    @ToJson
    fun toJson(@HexColorMap rgb: Map<String, Int>?): Map<String, String>? {
        val map = mutableMapOf<String, String>()
        rgb?.entries?.forEach {
            map[it.key] = String.format("#%08x", it.value)
        } ?: return null
        return map
    }

    /**
     * @suppress
     */
    @FromJson
    @HexColorMap
    fun fromJson(rgb: Map<String, String>?): Map<String, Int>? {
        val map = mutableMapOf<String, Int>()
        rgb?.entries?.forEach {
            map[it.key] = Color.parseColor(it.value)
        } ?: return null
        return map
    }
}