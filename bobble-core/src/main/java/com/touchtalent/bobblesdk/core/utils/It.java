package com.touchtalent.bobblesdk.core.utils;

import androidx.annotation.Nullable;

import java.util.List;

import kotlin.Deprecated;

/**
 * Created by geetgobindsingh on 09/09/17.
 */

@Deprecated(message = "Deprecated in favour of kotlin extension functions")
public class It {
    private It() {
        // Utility class with static methods
    }

    public static boolean isFalse(boolean condition) {
        return !condition;
    }

    public static boolean isTrue(boolean condition) {
        return condition;
    }

    public static boolean isNull(@Nullable Object object) {
        return object == null;
    }

    public static boolean isNotNull(@Nullable Object object) {
        return object != null;
    }

    public static boolean isSame(@Nullable String stringOne, @Nullable String stringTwo) {
        if (isNull(stringOne) || isNull(stringTwo)) {
            return false;
        }
        return stringOne.equalsIgnoreCase(stringTwo);
    }

    public static boolean isNotSame(@Nullable String stringOne, @Nullable String stringTwo) {
        if (isNull(stringOne) || isNull(stringTwo)) {
            return true;
        }
        return !stringOne.equalsIgnoreCase(stringTwo);
    }

    public static boolean equals(@Nullable Object a, @Nullable Object b) {
        return (a == b) || (a != null && a.equals(b));
    }

    public static boolean isSame(int intOne, int intTwo) {
        return intOne == intTwo;
    }

    public static boolean isSame(long intOne, long intTwo) {
        return intOne == intTwo;
    }

    public static boolean isNotSame(int intOne, int intTwo) {
        return intOne != intTwo;
    }

    public static boolean isNotSame(long intOne, long intTwo) {
        return intOne != intTwo;
    }

    public static boolean isEmpty(String str) {
        return str == null || str.isEmpty() || str.equalsIgnoreCase("null");
    }

    public static boolean isNotEmpty(String str) {
        return str != null && !str.isEmpty() && !str.equalsIgnoreCase("null");
    }

    public static boolean isNotEmptyList(List<?> list) {
        return list != null && !list.isEmpty();
    }

    public static boolean isEmptyList(List<?> list) {
        return list == null || list.isEmpty();
    }

    public static boolean isZero(int input) {
        return input == 0;
    }

    public static boolean isNotZero(int input) {
        return input != 0;
    }

    public static boolean isZero(Long input) {
        return input == 0;
    }

    public static boolean isNotZero(Long input) {
        return input != null && input != 0;
    }

    public static boolean isSame(List<String> list1, List<String> list2) {
        if (list1 == list2 || (isEmptyList(list1) && isEmptyList(list2)))
            return true;
        if ((list1 == null || list2 == null) || (list1.size() != list2.size()))
            return false;
        for (int i = 0; i < list1.size(); i++) {
            if (!equals(list1.get(i), list2.get(i)))
                return false;
        }
        return true;
    }
}