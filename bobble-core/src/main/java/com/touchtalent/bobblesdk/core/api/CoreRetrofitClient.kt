package com.touchtalent.bobblesdk.core.api

import com.touchtalent.bobblesdk.core.BobbleCoreSDK
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.Retrofit
import java.io.File
import java.io.FileOutputStream
import java.io.InputStream
import java.util.concurrent.TimeUnit

object CoreRetrofitClient {

    private val apiService by lazy {
        Retrofit.Builder()
            .baseUrl(BobbleCoreSDK.crossAppInterface.baseUrl() + "/")
            .client(
                BobbleCoreSDK.okHttpClient.newBuilder()
                    .cache(null)
                    .connectTimeout(20, TimeUnit.SECONDS)
                    .build()
            )
            .build()
            .create(CoreApiService::class.java)
    }

    /**
     * Downloads a [url] at a given [destPath] with [filename] as the name of the downloaded file
     * @return Result.Success for successful download, Result.failure([DownloadException]) for failure
     * @see DownloadException
     */
    @Suppress("BlockingMethodInNonBlockingContext")
    suspend fun downloadFile(url: String, destPath: String, filename: String): Result<Unit> {
        val response = kotlin.runCatching { apiService.download(url) }.getOrElse {
            return Result.failure(DownloadException(it))
        }
        val responseBody =
            response.body() ?: return Result.failure(DownloadException(response))
        val totalBytes = responseBody.contentLength()
        var bytesCopied = 0L
        var input: InputStream? = null
        return withContext(Dispatchers.IO) {
            try {
                val inputTmp =
                    responseBody.byteStream()
                        ?: return@withContext Result.failure(DownloadException("Null response byte stream"))
                input = inputTmp
                val fos = FileOutputStream(File(destPath, filename))
                fos.use { output ->
                    val buffer = ByteArray(128 * 1024) // or other buffer size
                    var read: Int
                    while (inputTmp.read(buffer).also { read = it } != -1) {
                        output.write(buffer, 0, read)
                        bytesCopied += read
                    }
                    output.flush()
                }
            } catch (e: Exception) {
                val downloadStats = DownloadStats(bytesCopied, totalBytes)
                return@withContext Result.failure(DownloadException(e, downloadStats))
            } finally {
                input?.close()
            }
            return@withContext Result.success(Unit)
        }
    }
}
