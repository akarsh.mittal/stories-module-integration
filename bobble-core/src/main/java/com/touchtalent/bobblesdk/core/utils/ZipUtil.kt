package com.touchtalent.bobblesdk.core.utils

import java.io.*
import java.util.zip.ZipEntry
import java.util.zip.ZipInputStream
import java.util.zip.ZipOutputStream

/**
 * Utility class to zip and unzip compressed files.
 */
object ZipUtil {
    private const val BUFFER_SIZE = 8192

    /**
     * Zip file/folder at [filePath] to [zipPath]
     * @return true if success, false otherwise
     */
    fun zip(filePath: String, zipPath: String): Boolean {
        try {
            val file = File(filePath)
            val bis: BufferedInputStream?
            val fos = FileOutputStream(zipPath)
            val zos = ZipOutputStream(
                BufferedOutputStream(
                    fos
                )
            )
            if (file.isDirectory) {
                val baseLength = (file.parent?.length ?: return false) + 1
                zipFolder(zos, file, baseLength)
            } else {
                val data = ByteArray(BUFFER_SIZE)
                val fis = FileInputStream(filePath)
                bis = BufferedInputStream(fis, BUFFER_SIZE)
                val entryName = file.name
                val entry = ZipEntry(entryName)
                zos.putNextEntry(entry)
                var count: Int
                while (bis.read(data, 0, BUFFER_SIZE).also { count = it } != -1) {
                    zos.write(data, 0, count)
                }
            }
            zos.close()
        } catch (e: Exception) {
            e.printStackTrace()
            return false
        }
        return true
    }

    @Throws(IOException::class)
    private fun zipFolder(zos: ZipOutputStream?, folder: File?, baseLength: Int) {
        if (zos == null || folder == null) {
            return
        }
        val fileList = folder.listFiles()
        if (fileList == null || fileList.isEmpty()) {
            return
        }
        for (file in fileList) {
            if (file.isDirectory) {
                zipFolder(zos, file, baseLength)
            } else {
                val data = ByteArray(BUFFER_SIZE)
                val unmodifiedFilePath = file.path
                val realPath = unmodifiedFilePath.substring(baseLength)
                val fi = FileInputStream(unmodifiedFilePath)
                val bis = BufferedInputStream(
                    fi,
                    BUFFER_SIZE
                )
                val entry = ZipEntry(realPath)
                zos.putNextEntry(entry)
                var count: Int
                while (bis.read(data, 0, BUFFER_SIZE).also { count = it } != -1) {
                    zos.write(data, 0, count)
                }
                bis.close()
            }
        }
    }

    /**
     * Unzips a zip file at [zipPath] at unzip the contents to [unzipFolder]
     * Deletes the source zip file, if unzip success
     * @return true, if success, false otherwise
     */
    fun unzip(zipPath: String, unzipFolder: String): Boolean {
        if (!FileUtil.exists(zipPath)) {
            return false
        }
        if (!FileUtil.mkdirs(unzipFolder, true)) {
            return false
        }
        try {
            val fin = FileInputStream(zipPath)
            val zin = ZipInputStream(fin)
            var ze: ZipEntry?
            while (zin.nextEntry.also { ze = it } != null) {
                val entryName = ze?.name ?: return false
                val entryPath = "$unzipFolder/$entryName"
                if (ze!!.isDirectory) {
                    FileUtil.mkdirs(entryPath)
                } else {
                    if (!FileUtil.create(entryPath, true)) {
                        continue
                    }
                    val fOut = FileOutputStream(entryPath)
                    val buffer = ByteArray(BUFFER_SIZE)
                    var len: Int
                    while (zin.read(buffer).also { len = it } != -1) {
                        fOut.write(buffer, 0, len)
                    }
                    fOut.close()
                    zin.closeEntry()
                }
            }
            zin.close()
        } catch (e: Exception) {
            BLog.printStackTrace(e)
            return false
        }
        FileUtil.delete(zipPath)
        return true
    }
}