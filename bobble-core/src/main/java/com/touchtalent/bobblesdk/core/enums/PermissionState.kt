package com.touchtalent.bobblesdk.core.enums

enum class PermissionState {
    ACCEPTED,
    DENIED
}