package com.touchtalent.bobblesdk.core.cache

import android.graphics.Bitmap
import com.touchtalent.bobblesdk.core.executor.BobbleSchedulers
import com.touchtalent.bobblesdk.core.utils.BLog
import com.touchtalent.bobblesdk.core.utils.FileUtil
import io.reactivex.Completable
import java.io.*
import java.util.*
import java.util.concurrent.locks.ReentrantReadWriteLock
import kotlin.concurrent.read
import kotlin.concurrent.write

/**
 * LRU cache similar to [DiskLruCache].
 * [DiskLruCache] provides methods to use the cache by fetching [Bitmap] directly from file.
 * However, in some cases, the client might not want to convert file path immediately to bitmap
 * (e.g - use the path directly in Glide), hence the loading of Bitmap becomes redundant.
 *
 * This class takes care of this issue by just registering the cache as used and promoting it in the
 * LRU sequence by using [useCache] function.
 *
 * @property path Path of the cache where it needs to be initialised
 * @property maxSize Max size, in bytes for the cache
 */
class DiskLruFileCache(private val path: String, private val maxSize: Long) {

    private val entryMapQueue = EntryMapQueue()
    private val reentrantLock = ReentrantReadWriteLock()

    private val journalFile = FileUtil.join(path, "journal")

    private var cacheSize = 0L

    init {
        runAsyncWithWriteLock {
            ensureCacheDirectoryExists()
            readJournal()
        }
    }

    private fun readJournal() {
        try {
            if (!FileUtil.exists(journalFile))
                return
            val bufferedReader = BufferedReader(InputStreamReader(FileInputStream(journalFile)))
            bufferedReader.use {
                var line = it.readLine()
                while (line != null) {
                    readEntry(line)
                    line = it.readLine()
                }
            }
            removeCacheNotInJournal()
        } catch (e: Exception) {
            // Clear cache if any issue found with loading journal
            FileUtil.delete(path)
            BLog.printStackTrace(e)
        }
    }

    private fun removeCacheNotInJournal() {
        ensureCacheDirectoryExists()
        val files = File(path).listFiles() ?: return
        for (file in files)
            if (!entryMapQueue.contains(file.name))
                FileUtil.delete(file)
    }

    /**
     * Checks whether the cache exists
     * @param cacheKey Cache key of the file to be checked
     */
    fun exists(cacheKey: String): Boolean {
        val contains = entryMapQueue.contains(cacheKey)
        if (contains) {
            ensureCacheDirectoryExists()
            val fileExists = FileUtil.exists(FileUtil.join(path, cacheKey))
            if (!fileExists)
                entryMapQueue.remove(cacheKey)
            return fileExists
        }
        return false
    }

    /**
     * Marks the cache as used and promote it in the LRU sequence
     * @param cacheKey Cache key of the file to be used
     * @return Fully qualified path of the cache file created
     */
    fun useCache(cacheKey: String): String {
        return runWithReadLock {
            val entry = entryMapQueue.get(cacheKey)
                ?: throw Exception("Cache key doesn't exists: $cacheKey")
            updateLastUsed(entry)
            return@runWithReadLock FileUtil.join(path, cacheKey)
        }
    }

    private fun updateLastUsed(entry: Entry) {
        entry.lastUsed = System.currentTimeMillis()
        entryMapQueue.update(entry)
    }

    /**
     * Add a bitmap to the cache with the given cache key
     * @param cacheKey Cache key for new entry
     * @param bitmap Bitmap to cache
     * @return Fully qualified path of the cache file created
     */
    fun addBitmap(cacheKey: String, bitmap: Bitmap): String {
        return runWithReadLock {
            ensureCacheDirectoryExists()
            val cachePath = FileUtil.join(path, cacheKey)
            FileOutputStream(cachePath).use {
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, it)
            }
            val size = File(cachePath).length()
            addOrUpdateEntry(cacheKey, size)
            return@runWithReadLock FileUtil.join(path, cacheKey)
        }
    }

    private fun ensureCacheDirectoryExists() {
        FileUtil.mkdirs(path)
    }

    private fun addOrUpdateEntry(
        cacheKey: String,
        size: Long,
        lastUsed: Long = System.currentTimeMillis()
    ) {
        val entry = entryMapQueue.get(cacheKey)
        if (entry == null) {
            val newEntry = Entry(cacheKey, lastUsed, size)
            cacheSize += size
            entryMapQueue.add(cacheKey, newEntry)
        } else {
            cacheSize -= entry.size
            entry.size = size
            entry.lastUsed = lastUsed
            cacheSize += size
            entryMapQueue.update(entry)
        }
        trimToMaxSize()
    }

    private fun trimToMaxSize() {
        while (cacheSize > maxSize) {
            removeStaleEntry()
        }
    }

    private fun removeStaleEntry() {
        val entry = entryMapQueue.poll() ?: return
        val cachePath = FileUtil.join(path, entry.key)
        cacheSize -= entry.size
        FileUtil.delete(cachePath)
    }

    /**
     * Flushes the in-memory content of the cache to a file
     */
    fun flush() {
        FileOutputStream(journalFile).use {
            val stringBuilder = StringBuilder()
            entryMapQueue.forEach { entry ->
                stringBuilder.setLength(0)
                stringBuilder.append(entry.key).append(" ")
                    .append(entry.lastUsed).append(" ")
                    .append(entry.size).append("\n")
                it.write(stringBuilder.toString().toByteArray())
            }
        }
    }

    private fun readEntry(line: String) {
        val entryArray = line.split(" ")
        if (entryArray.size == 3) {
            val cacheKey = entryArray[0]
            val cacheLastUsed = entryArray[1].toLong()
            val size = entryArray[2].toLong()
            addOrUpdateEntry(cacheKey, size, cacheLastUsed)
        }
    }

    private fun <T> runWithReadLock(callBack: () -> T): T {
        return reentrantLock.read(callBack)
    }

    private fun runAsyncWithWriteLock(callBack: () -> Unit) {
        Completable.fromRunnable {
            reentrantLock.write(callBack)
        }.subscribeOn(BobbleSchedulers.io())
            .subscribe()
    }

    /**
     * POJO class representing an entry for the [DiskLruFileCache]
     * @property key Key of the cache
     * @property lastUsed Timestamp of the cache last used
     * @property size Size, in bytes, of the cache file
     */
    class Entry(val key: String, var lastUsed: Long, var size: Long) : Comparable<Entry> {
        override fun compareTo(other: Entry): Int {
            return (lastUsed - other.lastUsed).toInt()
        }

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false
            other as Entry
            if (key != other.key) return false
            return true
        }

        override fun hashCode(): Int {
            return key.hashCode()
        }
    }
}

/**
 * Queue based on LRU for holding and querying elements in cache
 */
class EntryMapQueue {
    private val treeSet = TreeSet<DiskLruFileCache.Entry>()
    private val map = TreeMap<String, DiskLruFileCache.Entry>()

    /**
     * Add a key to the queue, move it to the top if already exists.
     * @param key Cache key to add
     * @param value Cache entry to be added
     */
    fun add(key: String, value: DiskLruFileCache.Entry) {
        synchronized(this) {
            map[key] = value
            treeSet.add(value)
        }
    }

    /**
     * Update the contents of the cache, if exists, otherwise add it.
     * @param value Cache entry to be updated
     */
    fun update(value: DiskLruFileCache.Entry) {
        synchronized(this) {
            treeSet.remove(value)
            treeSet.add(value)
        }
    }

    /**
     * Fetch and remove the last entry in the queue (least used).
     * @return Cache entry if not empty, null otherwise
     */
    fun poll(): DiskLruFileCache.Entry? {
        synchronized(this) {
            val entry = treeSet.pollFirst() ?: return null
            return map.remove(entry.key)
        }
    }

    /**
     * Check if the given key exists in the queue.
     * @param key Key to check
     * @return true if exists, false otherwise
     */
    fun contains(key: String): Boolean {
        return synchronized(this) { map.containsKey(key) }
    }

    /**
     * Remove the specified key from queue, if exists.
     * @param key Key to remove
     */
    fun remove(key: String) {
        return synchronized(this) {
            map.remove(key)?.let { treeSet.remove(it) }
        }
    }

    /**
     * Fetch entry corresponding the given key.
     * @param key Key to fetch entry for
     * @return Cache entry, if given key exists, null otherwise
     */
    fun get(key: String): DiskLruFileCache.Entry? {
        return synchronized(this) {
            map[key]
        }
    }

    /**
     * Iterate the queue in sequential order (least used at the end).
     * @param callback Function to run on every element
     */
    fun forEach(callback: (item: DiskLruFileCache.Entry) -> Unit) {
        synchronized(this) {
            map.forEach { callback.invoke(it.value) }
        }
    }
}