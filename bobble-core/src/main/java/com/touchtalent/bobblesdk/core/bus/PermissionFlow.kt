package com.touchtalent.bobblesdk.core.bus

import com.touchtalent.bobblesdk.core.BobbleCoreSDK
import com.touchtalent.bobblesdk.core.enums.PermissionName
import com.touchtalent.bobblesdk.core.enums.PermissionState
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.launch

object PermissionFlow {
    private val _events = MutableSharedFlow<Pair<PermissionName,PermissionState>>()
    val events = _events.asSharedFlow()
    private var isFromSettings = false
    fun onReactedToPermission(permissionName: PermissionName, permissionState: PermissionState) {
        try {
            BobbleCoreSDK.applicationScope.launch {
                _events.emit(Pair(permissionName, permissionState))
            }
        } catch (e: Exception){
            e.printStackTrace()
        }
    }
    fun setIsFromSettingsTrue() {
        isFromSettings = true
    }
    fun resetAndEmmitMock() {
        if(isFromSettings) {
            isFromSettings = false
            onReactedToPermission(PermissionName.SETTINGS_PAGE,PermissionState.ACCEPTED)
        }
    }
}