package com.touchtalent.bobblesdk.core.room

import androidx.room.TypeConverter
import com.google.gson.reflect.TypeToken
import com.touchtalent.bobblesdk.core.BobbleCoreSDK
import com.touchtalent.bobblesdk.core.model.Tracker
import com.touchtalent.bobblesdk.core.utils.It
import java.lang.reflect.Type

/**
 * Room converter for serialisation of list of [Tracker]
 */
class ImpressionTrackerConverter {
    /**
     * @suppress
     */
    @TypeConverter
    fun impressionTrackerToString(tracker: List<Tracker?>?): String? {
        if (tracker == null)
            return ""
        return BobbleCoreSDK.getGson().toJson(tracker)
    }

    /**
     * @suppress
     */
    @TypeConverter
    fun stringToImpressionTracker(string: String?): List<Tracker?>? {
        if (It.isEmpty(string))
            return ArrayList()
        val type: Type = object : TypeToken<List<Tracker?>?>() {}.type
        return BobbleCoreSDK.getGson().fromJson(string, type)
    }

}