package com.touchtalent.bobblesdk.core.model

import android.graphics.Point
import com.google.gson.reflect.TypeToken
import com.touchtalent.bobblesdk.core.BobbleCoreSDK
import com.touchtalent.bobblesdk.core.utils.BLog

/**
 *
 * This object represents a single head with a single head type.
 *
 * @property characterId Id for this head character. Different BobbleHead object of different head types
 * could share same character id. characterId of a RT head and server head is guaranteed to be different
 * @property headId Unique ID for a head corresponding to a specific head type
 * @property facePointMap Face point map (luxand) of the head in serialised format (Map<Long, Point>)
 * @property headPath Local path of the head
 * @property rawImagePath Original image which was used to create this head. Could be a local path or
 * a HTTP url pointing to a synced head artifact. This can be used to create new head types of
 * existing heads
 * @property height Height of the image pointed by [headPath]
 * @property width Width of the image pointed by [headPath]
 * @property name Name of the head as assigned by the user
 * @property headCreationTime Time at which the first head type of this head was created
 * @property variantCreationTime Time at which the current head type of this head was created
 * @property relation Relation of the head to the user.
 * @property gender Gender of the head. Can be "male" or "female"
 * @property ageGroup Age group of the head. Format - "%d-%d" or "%d"
 * @property headSource Source of the head, could be Mascot, user created, synced, etc
 * @property bobbleType Integer denoting type of bobble head. <1000 represent deprecated RT heads,
 * server heads otherwise
 * @property faceTone Skin color of the head, e.g - #E8BEAC
 * @property isSynced Whether the head is synced to server or not
 * @property serverSyncId Sync id received from server after head is synced
 * @property shareUrl URL to share head
 */
class BobbleHead(
    var characterId: Long,
    var headId: String,
    var facePointMap: String,
    var headPath: String,
    var rawImagePath: String?,
    var height: Int,
    var width: Int
) {
    var name: String? = null
    var headCreationTime: Long = System.currentTimeMillis()
    var variantCreationTime: Long = System.currentTimeMillis()

    var relation: String? = null
    var gender: String = ""
    var ageGroup: String? = null
    var headSource: HeadSource = HeadSource.UNKNOWN
    var bobbleType: Int = 0
    var faceTone: String? = null

    var serverSyncId: String? = null
    var shareUrl: String? = null
    var isSynced = false

    /**
     * Check if this head is a mascot head
     * @see HeadSource.MASCOT
     */
    fun isMascotHead() = headSource == HeadSource.MASCOT

    /**
     * Get face feature point in parsed Map format.
     * @return Map of feature point, null if the JSON is null or corrupt
     */
    fun getFeatureFacePointMap(): Map<Long, Point>? {
        return try {
            BobbleCoreSDK.getGson()
                .fromJson(facePointMap, object : TypeToken<Map<Long, Point>>() {}.type)
        } catch (e: Exception) {
            BLog.printStackTrace(e)
            null
        }
    }

    /**
     * Get face feature point in parsed array format.
     * @return Array of feature points, null if the JSON is null or corrupt
     */
    fun getFeaturePointArray(): ArrayList<Point>? {
        val facePointMap: Map<Long, Point>? = getFeatureFacePointMap()
        facePointMap?.let {
            return getHeadPointsArray(facePointMap)
        }
        return null
    }

    private fun getHeadPointsArray(facePointMap: Map<Long, Point>): ArrayList<Point>? {
        val facePointsList = ArrayList<Point>()
        for (index in 0..facePointMap.size) {
            facePointMap[index.toLong()]?.let { facePointsList.add(it) }
        }
        //72 is number of face points
        return if (facePointMap.size != 72) null else facePointsList
    }

    /**
     * Get head category of this head.
     * @see HeadCategory
     */
    fun getHeadCategory(): HeadCategory {
        return when {
            relation.equals("me") -> HeadCategory.SELF
            headSource == HeadSource.MASCOT -> HeadCategory.MASCOT
            else -> HeadCategory.FNF
        }
    }
}

/**
 * Represents the source of head.
 */
enum class HeadSource {
    /**
     * Unknown source
     */
    UNKNOWN,

    /**
     * The head was created by the user
     */
    USER_CREATED,

    /**
     * The head was downloaded from a shared head/connection panel
     */
    CONNECTION_HEAD,

    /**
     * The head has been synced from server, original source is unknown. P.S - [MASCOT] heads are
     * never synced
     */
    SYNCED,

    /**
     * Mascot heads are dummy heads of celebrities that are added for use when user hasn't created
     * any head
     */
    MASCOT
}

/**
 * Category of the head based on relation and source
 */
enum class HeadCategory {
    /**
     * The head is of user's self face
     */
    SELF,

    /**
     * The head belongs to friends and family of the user
     */
    FNF,

    /**
     * The head is a mascot head
     */
    MASCOT
}
