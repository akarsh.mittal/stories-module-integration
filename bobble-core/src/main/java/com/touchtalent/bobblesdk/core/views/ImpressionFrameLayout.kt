package com.touchtalent.bobblesdk.core.views

import android.content.Context
import android.graphics.Canvas
import android.util.AttributeSet
import android.view.ViewTreeObserver
import android.widget.FrameLayout
import com.touchtalent.bobblesdk.core.utils.ViewUtil

private const val MINIMUM_PERCENTAGE_FOR_IMPRESSION = 50

/**
 * Frame layout to provide callbacks for impression. The impression is logged only and only if the
 * view is user-visible at a percentage higher than [MINIMUM_PERCENTAGE_FOR_IMPRESSION].
 *
 * If this layout is used in RecyclerView, make sure to call [reset] while loading new data to
 * generate a new impression for the new data
 *
 * @property onImpression Lambda function to dispatch callback for impression
 */
open class ImpressionFrameLayout @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null
) : FrameLayout(context, attrs) {

    private var canLogEvent = true

    private var onGlobalLayoutListener = ViewTreeObserver.OnGlobalLayoutListener {
        logImpression()
    }

    private val onPreDrawListener = ViewTreeObserver.OnPreDrawListener {
        logImpression()
        return@OnPreDrawListener true
    }

    var onImpression: (() -> Unit)? = null

    init {
        this.setWillNotDraw(false)
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        logImpression()
    }

    /**
     * Resets the status to NOT LOGGED for new impression events to be generated
     */
    fun reset() {
        canLogEvent = true
    }

    private fun logImpression() {
        // This function is being from onDraw() and onScrollChangeListener(). The sequence of
        // conditions being executed is paramount for performance and visibility percentage should
        // be the last item to be evaluated
        if (canLogEvent &&
            onImpression != null &&
            ViewUtil.getVisiblePercentage(this) >= MINIMUM_PERCENTAGE_FOR_IMPRESSION
        ) {
            onImpression?.invoke()
            canLogEvent = false
        }
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        viewTreeObserver.addOnGlobalLayoutListener(onGlobalLayoutListener)
        viewTreeObserver.addOnPreDrawListener(onPreDrawListener)
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        viewTreeObserver.removeOnGlobalLayoutListener(onGlobalLayoutListener)
        viewTreeObserver.removeOnPreDrawListener(onPreDrawListener)
    }

}