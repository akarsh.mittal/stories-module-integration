package com.touchtalent.bobblesdk.core.executor;

import java.util.concurrent.Executor;

/**
 * Created by Prashant Gupta on 20-08-2016.
 */

public interface ExecutorSupplier {

    Executor forMainThreadTasks();

    CommonThreadPoolExecutor forCommonThreadTasks();

    CommonThreadPoolExecutor forContentTasks();
}