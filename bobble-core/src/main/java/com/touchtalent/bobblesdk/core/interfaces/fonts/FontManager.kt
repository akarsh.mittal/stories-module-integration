package com.touchtalent.bobblesdk.core.interfaces.fonts

import android.graphics.Typeface

/** Interface for font management - A single font may be shared across multiple use cases and a
single repository should be maintained for all. The implementation of this font manager should make
sure same fonts are not being downloaded again and again
 */
interface FontManager {
    /**
     * @return whether a given font url exists or not
     */
    fun exists(url: String): Boolean

    /**
     * Get typeface for given font url. Implementation should take care of caching the url
     * @param url URL of the font to fetch
     * @param timeoutInMillis maximum timeout for the font to load
     * @return [Typeface] for given url
     */
    suspend fun getFontTypeface(url: String, timeoutInMillis: Long, forceCache: Boolean = false): Typeface?

    /**
     * Get a default typeface - can be used in no-internet available cases
     * @return Fallback [Typeface] to be used in case of a failure
     */
    fun getDefaultTypeface(): Typeface
}