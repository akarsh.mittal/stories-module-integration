package com.touchtalent.bobblesdk.core.api

import com.touchtalent.bobblesdk.core.BobbleCoreSDK
import com.touchtalent.bobblesdk.core.cache.cache
import com.touchtalent.bobblesdk.core.utils.FileLruCache
import okhttp3.MediaType
import okhttp3.Protocol
import okhttp3.Request
import okhttp3.ResponseBody
import okio.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Converter
import retrofit2.HttpException
import retrofit2.Response
import java.io.IOException
import java.lang.reflect.Type
import java.util.concurrent.TimeUnit
import kotlin.io.use

class TimeoutCacheCall<T>(
    private val responseConverter: Converter<ResponseBody, Any?>,
    private val responseType: Type,
    private val delegateCall: Call<T>,
    private val timeoutMs: Long,
    private val parametersToCache: List<String>,
    private val fileLruCache: FileLruCache
) : Call<T> {

    init {
        delegateCall.timeout().timeout(timeoutMs, TimeUnit.MILLISECONDS)
    }

    override fun clone(): Call<T> {
        return TimeoutCacheCall(
            responseConverter,
            responseType,
            delegateCall.clone(),
            timeoutMs,
            parametersToCache,
            fileLruCache
        )
    }

    override fun execute(): Response<T> {
        throw UnsupportedOperationException("doesn't support execute")
    }

    override fun isExecuted(): Boolean {
        return delegateCall.isExecuted
    }

    override fun cancel() {
        delegateCall.cancel()
    }

    override fun isCanceled(): Boolean {
        return delegateCall.isCanceled
    }

    override fun request(): Request {
        return delegateCall.request()
    }

    override fun timeout(): Timeout {
        return delegateCall.timeout()
    }

    override fun enqueue(callback: Callback<T>) {
        delegateCall.enqueue(object : Callback<T> {
            override fun onResponse(call: Call<T>, response: Response<T>) {
                kotlin.runCatching {
                    val cacheKey = getCacheKey(request())
                    if (response.isSuccessful) {
                        cacheResponse(cacheKey, response)
                        callback.onResponse(this@TimeoutCacheCall, response)
                    } else onFailure(this@TimeoutCacheCall, HttpException(response))
                }.onFailure {
                    onFailure(this@TimeoutCacheCall, it)
                }
            }

            override fun onFailure(call: Call<T>, t: Throwable) {
                kotlin.runCatching {
                    val cacheKey = getCacheKey(request())
                    val cachedResponse = getCachedResponse(cacheKey, request())
                    if (cachedResponse != null) {
                        callback.onResponse(this@TimeoutCacheCall, parseResponse(cachedResponse))
                        return
                    } else callback.onFailure(this@TimeoutCacheCall, t)
                }.onFailure {
                    callback.onFailure(this@TimeoutCacheCall, it)
                }
            }
        })
    }

    private fun getCachedResponse(cacheKey: String, request: Request): okhttp3.Response? {
        val byteArray = fileLruCache.get(cacheKey) ?: return null
        val responseBody = ResponseBody.create(MediaType.get("application/json"), byteArray)
        return okhttp3.Response.Builder().protocol(Protocol.HTTP_2).request(request).code(200)
            .body(responseBody).message("200 OK Client cached").build()
    }

    private fun cacheResponse(cacheKey: String, result: Response<T>) {
        val body = result.body() ?: return
        val data = BobbleCoreSDK.moshi.adapter<Any?>(responseType).toJson(body)
        fileLruCache.put(cacheKey, data)
    }

    private fun getCacheKey(request: Request): String {
        return cache {
            process(request.url().encodedPath())
            for (param in parametersToCache) {
                process(request.url().queryParameter(param))
            }
        }
    }

    internal class NoContentResponseBody(
        private val contentType: MediaType?, private val contentLength: Long
    ) : ResponseBody() {
        override fun contentType(): MediaType? {
            return contentType
        }

        override fun contentLength(): Long {
            return contentLength
        }

        override fun source(): BufferedSource {
            throw IllegalStateException("Cannot read raw response body of a converted body.")
        }
    }

    @Throws(IOException::class)
    fun buffer(body: ResponseBody): ResponseBody {
        val buffer = Buffer()
        body.source().readAll(buffer)
        return ResponseBody.create(body.contentType(), body.contentLength(), buffer)
    }

    @Throws(IOException::class)
    fun parseResponse(rawResponseFinal: okhttp3.Response): Response<T> {
        var rawResponse = rawResponseFinal
        val rawBody = rawResponse.body() ?: throw java.lang.IllegalStateException()

        // Remove the body's source (the only stateful object) so we can pass the response along.
        rawResponse = rawResponse.newBuilder()
            .body(NoContentResponseBody(rawBody.contentType(), rawBody.contentLength())).build()
        val code = rawResponse.code()
        if (code < 200 || code >= 300) {
            return rawBody.use {
                // Buffer the entire body to avoid future I/O.
                val bufferedBody = buffer(it)
                Response.error(bufferedBody, rawResponse)
            }
        }
        if (code == 204 || code == 205) {
            rawBody.close()
            return Response.success(null, rawResponse)
        }
        val catchingBody = ExceptionCatchingResponseBody(rawBody)
        return try {
            val body: T = responseConverter.convert(catchingBody) as T
            Response.success(body, rawResponse)
        } catch (e: RuntimeException) {
            // If the underlying source threw an exception, propagate that rather than indicating it was
            // a runtime exception.
            catchingBody.throwIfCaught()
            throw e
        }
    }

    internal class ExceptionCatchingResponseBody(private val delegate: ResponseBody) :
        ResponseBody() {
        private val delegateSource: BufferedSource
        var thrownException: IOException? = null

        init {
            delegateSource = object : ForwardingSource(delegate.source()) {
                @Throws(IOException::class)
                override fun read(sink: Buffer, byteCount: Long): Long {
                    return try {
                        super.read(sink, byteCount)
                    } catch (e: IOException) {
                        thrownException = e
                        throw e
                    }
                }
            }.buffer()
        }

        override fun contentType(): MediaType? {
            return delegate.contentType()
        }

        override fun contentLength(): Long {
            return delegate.contentLength()
        }

        override fun source(): BufferedSource {
            return delegateSource
        }

        override fun close() {
            delegate.close()
        }

        fun throwIfCaught() {
            thrownException?.let { throw it }
        }
    }

}