package com.touchtalent.bobblesdk.core.moshi

import com.squareup.moshi.JsonQualifier

/**
 * Denotes that the annotated element represents a color of either RGB or ARGB format -
 * e.g - #RRGGGBB or #AARRGGBB
 */
@Retention(AnnotationRetention.RUNTIME)
@JsonQualifier
annotation class HexColor